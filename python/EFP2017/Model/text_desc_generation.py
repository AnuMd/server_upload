import cv2,os, math, numpy as np,PIL.ImageOps
from operator import itemgetter
from PIL import Image

from iterative_functions import iterative_functions_class

class text_desc_generator_class():

    def __init__(self,current_directory,file_name,path_gravvitas):
        self.current_directory = current_directory
        self.file_name = file_name
        self.path_gravvitas = path_gravvitas
        self.iterative_obj = iterative_functions_class()

    def generate_text_description(self,modified_outer_contour,modified_outer_contour_lines,
                                  new_img_height,new_img_width, all_floorplan_components,
                                  modified_room_cordinates,avg_door_width):
        only_image_name = self.file_name[0:-4]
        # --extract floorplan_data
        rooms_point_list = all_floorplan_components[0]
        doors_rect_list = all_floorplan_components[4]

        #--- 1. generate building shape
        building_text = self.get_building_shape(self.current_directory, self.path_gravvitas,
                                                    self.file_name, modified_outer_contour,
                                                    new_img_height, new_img_width)

        #--- 2. generate building size based on avg_door_width
        building_size_text = self.get_building_size(avg_door_width,modified_outer_contour,
                                                    new_img_height, new_img_width)

        #--- 3. generate no of rooms
        room_no_text  = self.get_no_of_rooms(rooms_point_list)

        rooms_point_list = self.remove_OP_from_rooms_list(rooms_point_list)

        #--- 4. generate position of entrance doors
        entrance_door,shortest_distance_to_room,internal_rooms,door_text = self.get_entrance_door_text(
            self.current_directory,modified_outer_contour_lines,modified_outer_contour,
            rooms_point_list,doors_rect_list,new_img_height, new_img_width)

        #--- 5. generate external and internal room order
        room_order_text,external_room_names_in_order,internal_rooms = self.get_external_internal_room_text(entrance_door,modified_outer_contour_lines,shortest_distance_to_room,internal_rooms)

        #--- 6. generate individual room details
        object_existence_room_text = self.get_individual_room_obj_existence_text(modified_room_cordinates)

        #---- 7. get door room data
        room_door_text = self.get_room_door_relations(rooms_point_list,doors_rect_list,new_img_height,new_img_width,avg_door_width,external_room_names_in_order,internal_rooms)


        # --- 8. intro text
        fp_name = 'This is floorplan ' + only_image_name + '.'


        floorplan_text = fp_name+' '+building_text+' '+building_size_text+' '+room_no_text+' '+door_text+' '+room_order_text+' '+ room_door_text+' '+object_existence_room_text

        # print floorplan_text
        return floorplan_text


    # ----- all methods related to text description generation
    def get_building_shape(self, current_directory, path_gravvitas, file_name, outer_contour_line,
                           orginal_img_height, orginal_img_width):
        only_file_name = file_name[:-4]

        contour_shape = []

        # ---- approxpoyDP image to reduce jagged lines
        # ----implementation of Douglas-Peucker algorithm
        epsilon = 0.0025 * cv2.arcLength(outer_contour_line, True)
        approx = cv2.approxPolyDP(outer_contour_line, epsilon, True)
        approx_poly_image = np.zeros((orginal_img_height, orginal_img_width, 3), dtype='uint8')
        cv2.drawContours(approx_poly_image, [approx], -1, (255, 255, 255), -1)
        cv2.imwrite(path_gravvitas + 'temp_1_approx_poly_image.png', approx_poly_image)

        # ----dilate approxpolyDP image
        kernel = np.ones((10, 10), 'uint8')
        dilated = cv2.dilate(approx_poly_image, kernel, iterations=2)
        cv2.imwrite(path_gravvitas + only_file_name + 'temp_2_dilated_image.png', dilated)

        # -- read both 1.dilated & 2.approx images.
        # -- Store their contours in two arrays: both have to refer
        # -- same contour by row1, row2

        # ----draw image- black, contour-black, bbbox- white and find spaces in
        # ----bbbox not belonging to contour
        # --get new contours of dilated image
        dilated_contour_img = cv2.imread(path_gravvitas + only_file_name + 'temp_2_dilated_image.png',
                                         cv2.IMREAD_GRAYSCALE)
        ret, thresh = cv2.threshold(dilated_contour_img, 0, 255, 1)
        dilated_contours, hierachy = cv2.findContours(thresh, 1, 2)
        image_area = orginal_img_height * orginal_img_width
        total_num_contours = len(dilated_contours)
        detected_shapes = 0
        for cont, contour in enumerate(dilated_contours):
            contour_area = cv2.contourArea(contour)
            # ---remove image contour
            if contour_area / image_area < 0.95:
                # ---- draw the old approx contour since we want bb_box to be smaller than contour
                # ---- to remove unwanted white spaces between black contour and white bb_box
                rect = cv2.minAreaRect(approx)
                v1, v2, v3 = rect
                bb_width, bb_height = v2

                # ----find rectangle shape using bb_area, contour_area: ratio
                bb_area = bb_width * bb_height
                # ---1.27 coz dilated contour is bigger than approx contour bb_box
                if contour_area / bb_area > 0.95 and contour_area / bb_area < 1.27:
                    # ----find square shape using bb_w, bb_h: ratio
                    bb_ratio = bb_width / bb_height
                    # ---fp_30.png bb_ratio: 1.26215-----------------------------> check with eyes
                    if bb_ratio >= 0.95 and bb_ratio <= 1.05:
                        contour_shape.append("a square shape")
                        detected_shapes += 1
                    else:
                        contour_shape.append("a rectangular shape")
                        detected_shapes += 1
                else:
                    # --special case for all other shapes
                    box = cv2.cv.BoxPoints(rect)
                    box = np.int0(box)
                    bb_image = np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8)
                    cv2.drawContours(bb_image, [box], -1, (255, 255, 255), -1)
                    cv2.drawContours(bb_image, [contour], -1, (0, 0, 0), -1)
                    cv2.imwrite(path_gravvitas + only_file_name + 'temp_3_bb_image.png', bb_image)

        # ----check if there are un-shape_detected contours
        if detected_shapes < total_num_contours - 1:
            # ---find if contour is L shape by looking of bb has only one left over component with rect/square shape
            left_over_comp_img = cv2.imread(path_gravvitas + only_file_name + 'temp_3_bb_image.png',
                                            cv2.IMREAD_GRAYSCALE)
            # os.remove(path_gravvitas + file_name+ 'temp_3_bb_image.png')
            ret, thresh = cv2.threshold(left_over_comp_img, 0, 255, 1)
            left_over_contours, hierachy = cv2.findContours(thresh, 1, 2)
            rect_contour_count = 0
            for cont, lftov_contour in enumerate(left_over_contours):
                contour_area = cv2.contourArea(lftov_contour)
                if contour_area / image_area < 0.95:
                    rect = cv2.minAreaRect(lftov_contour)
                    v1, v2, v3 = rect
                    bb_width, bb_height = v2
                    bb_area = bb_width * bb_height
                    if contour_area / bb_area > 0.85 and contour_area / bb_area < 1.05:
                        rect_contour_count += 1
            # ----if len(left over contours) < 2 && shape(left over contours) == rectangle: L shape
            if rect_contour_count == 1 and len(left_over_contours) <= 2:
                contour_shape.append("an L shape")
            else:
                contour_shape.append("an irregular shape")

            os.remove(path_gravvitas + only_file_name + 'temp_3_bb_image.png')
        os.remove(path_gravvitas + only_file_name + 'temp_2_dilated_image.png')

        building_text = 'The building has '+contour_shape[0]

        return building_text

    def get_building_size(self,avg_door_width,outer_contour,new_img_height,new_img_width):
        # empty_new_image = ~(np.zeros((new_img_height,new_img_width, 3), np.uint8))
        # cv2.drawContours(empty_new_image, [outer_contour], -1, (0,0,0), -1)
        # cv2.imwrite(self.path_gravvitas + self.file_name+'_test.png', empty_new_image)

        contour_area = cv2.contourArea(outer_contour)
        building_size = ((0.82 ** 2) * contour_area) / (avg_door_width ** 2)
        building_size_text = 'and is approximately '+str(int(building_size))+' square meters in size.'
        return building_size_text

    def get_no_of_rooms(self,rooms_point_list):
        #---- 1. count no of rooms by use type (single/opne plan)
        individual_rooms  = 0
        open_plan_rooms = []
        for room_row in rooms_point_list:
            if room_row[1]=='single':
                individual_rooms = individual_rooms + 1
            #--since we want to find how many open plans, we dont need to just count all
            #-- rooms that is part of open plan
            else:
                if room_row[1] not in open_plan_rooms:
                    open_plan_rooms.append(room_row[1])


        #---- 2. grammar section based on detected element numbers
        #----2.1 grammar for single use rooms
        if individual_rooms==1:
            indi_room_text = 'There is '+ str(individual_rooms)+' single-use room and '
        elif individual_rooms >1:
            indi_room_text = 'There are ' + str(individual_rooms) + ' single-use rooms and '
        else:
            indi_room_text = ''
        #----2.2 grammar for open plan areas
        if len(open_plan_rooms)==1:
            op_text = str(len(open_plan_rooms)) + ' open plan area in this floorplan..'
        elif len(open_plan_rooms) > 1:
            op_text = str(len(open_plan_rooms)) + ' open plan areas in this floorplan..'
        else:
            op_text = ''

        #----3. combine two sentence parts and return
        return indi_room_text + op_text

    def remove_OP_from_rooms_list(self,rooms_point_list):
        rooms_point_list_2 = []
        for row in rooms_point_list:
            if row[1] != 'OP':
                rooms_point_list_2.append(row)
        return rooms_point_list_2

    def get_entrance_door_text(self,current_directory,outer_contour_lines,outer_contour,
                               rooms_point_list,doors_rect_list,img_height, img_width):
        # ---find_shortest_distance_from_contour_to_rooms
        shortest_distance_to_room, internal_rooms = self.find_shortest_distance_from_contour_to_rooms(
            outer_contour_lines, rooms_point_list)

        # --finding all entrance doors (doors closest to contour)
        entr_door_details = self.find_all_entrance_doors(outer_contour, doors_rect_list)

        # ---find rooms closest to each entrance
        door_contour_data = self.find_contour_lines_for_doors(entr_door_details,
                                                              outer_contour_lines)

        room_contour_data = self.find_contour_line_closest_to_each_room(shortest_distance_to_room)

        possible_rooms_for_each_door = self.find_possible_rooms_for_each_door(door_contour_data, room_contour_data)

        door_room_distance = self.find_room_door_distances(possible_rooms_for_each_door,
                                                           self.iterative_obj)

        entrance_door = self.find_main_entrance_door(door_room_distance, current_directory)

        door_positions = self.get_door_positions(entr_door_details, outer_contour,
                                                 img_height, img_width)

        door_text = self.generate_position_text(entrance_door, door_positions)

        return entrance_door,shortest_distance_to_room,internal_rooms,door_text

    def get_external_internal_room_text(self,entrance_door, outer_contour_lines,
                                        shortest_distance_to_room,internal_rooms):
        external_room_names_in_order = self.get_external_room_order(entrance_door,
                                       outer_contour_lines,shortest_distance_to_room)
        room_order_text = self.create_text(external_room_names_in_order,
                                           internal_rooms)

        return room_order_text,external_room_names_in_order,internal_rooms

    def get_individual_room_obj_existence_text(self,modified_room_cordinates):
        #---get list of rooms with > 0 objects
        object_existence_room_list = []
        for r,room_row in enumerate(modified_room_cordinates):
            room_data = room_row[0]
            object_data = room_row[1]
            if len(object_data) > 0:
                object_existence_room_list.append(room_data[1])

        #--- categorize selected rooms
        room_text = self.category_based_rooms_string(object_existence_room_list,article=True)

        #--- assign categorized rooms text to string
        obj_room_text = ''
        if len(room_text) > 0:
            if len(object_existence_room_list) == 1:
                obj_room_text = room_text.capitalize() + ' is also shown in a separate diagram.'
            else:
                obj_room_text = room_text.capitalize() + ' are also shown in separate diagrams.'

        return obj_room_text

    def find_shortest_distance_from_contour_to_rooms(self, outer_contour_lines, rooms_point_list):
        # --find lines for all rooms and store in "room_lines_list[room_name,room_contour_lines_list]"
        room_lines_list = []
        for room_row in rooms_point_list:
            room_name = room_row[0]
            room_points = room_row[-1]
            room_in_lines = []
            first_element = 0
            for l, cordinate in enumerate(room_points):
                x, y = cordinate
                if l == 0:
                    room_in_lines.append([])
                    room_in_lines[l].append([x, y])
                    first_element = [x, y]
                elif l == len(room_points) - 1:
                    room_in_lines.append([])
                    room_in_lines[l].append([x, y])
                    room_in_lines[l - 1].append([x, y])
                    room_in_lines[l].append(first_element)
                else:
                    room_in_lines.append([])
                    room_in_lines[l].append([x, y])
                    room_in_lines[l - 1].append([x, y])
            room_lines_list.append([room_name, room_in_lines])

        # --per room find the room line that has shortest distance to a contour line and
        # -- store it in "shortest_distance_to_room[room_name,room_line_closest_to_contour]"
        shortest_distance_to_room = []
        internal_rooms = []
        for room_row, each_room in enumerate(room_lines_list):
            room_name = each_room[0]
            room_lines = each_room[1]
            room_lines_distances = []
            for room_line in room_lines:
                room_x, room_y = self.iterative_obj.find_centre_of_line(room_line)
                min_distance_to_contour = 3000
                min_contour_line, min_room_line = 0, 0
                cont_intersect_x, cont_intersect_y, min_room_x, min_room_y = 0, 0, 0, 0
                for cont_index, cont_line in enumerate(outer_contour_lines):
                    distance_from_text, intersect_x, intersect_y = self.iterative_obj.calculate_distance_from_point_to_line(
                        [room_x, room_y], cont_line)
                    if distance_from_text != -1 and distance_from_text < min_distance_to_contour:
                        min_distance_to_contour = distance_from_text
                        min_contour_line = [cont_line, cont_index]
                        cont_intersect_x, cont_intersect_y = int(intersect_x), int(intersect_y)
                        min_room_line = room_line
                        min_room_x, min_room_y = room_x, room_y
                if min_room_line != 0:
                    room_lines_distances.append(
                        [min_room_line, min_contour_line[0], min_contour_line[1],
                         min_distance_to_contour, min_room_x,
                         min_room_y, cont_intersect_x, cont_intersect_y])
            room_lines_distances.sort(key=itemgetter(3))
            if len(room_lines_distances) > 0 and room_lines_distances[0][3] < 100:
                shortest_distance_to_room.append([room_name, room_lines_distances[0], room_lines])
            else:
                internal_rooms.append(room_name)
        return shortest_distance_to_room, internal_rooms

    def find_all_entrance_doors(self, outer_contour_wth_point_data, doors_rect_list):
        door_details = []
        for door_row in doors_rect_list:
            door_contour = self.iterative_obj.convert_points_to_contour(door_row)
            center_x, center_y = self.iterative_obj.get_centre_of_contour(door_contour)
            # find distance from door centre_point to contour
            distance_from_contour = cv2.pointPolygonTest(outer_contour_wth_point_data, (center_x, center_y), True)
            #-- add as possible entrances
            if distance_from_contour < 25:
                door_details.append([door_row, [center_x, center_y],
                                     distance_from_contour])
        #--- sort by distance from entrance to outer contour
        door_details.sort(key=itemgetter(2))
        return door_details

    def find_contour_lines_for_doors(self, door_details, outer_contour_lines):
        doors_inside_contour = []
        for row_num, door_row in enumerate(door_details):
            door_centre_x, door_centre_y = door_row[1]
            min_distance = 1000

            # --find cont_index for each door_row
            cont_index = -1
            for c, cont_line in enumerate(outer_contour_lines):
                distance_from_door, intersect_x, intersect_y = self.iterative_obj.calculate_distance_from_point_to_line(
                    [door_centre_x, door_centre_y], cont_line)
                if distance_from_door != -1 and distance_from_door < min_distance:
                    min_distance = distance_from_door
                    cont_index = c
            if cont_index != -1:
                doors_inside_contour.append([door_row[0], door_row[1], door_row[2], cont_index])

        return doors_inside_contour

    def find_contour_line_closest_to_each_room(self, shortest_distance_to_room):
        room_contour_data = []
        for room_row in shortest_distance_to_room:
            room_name = room_row[0]
            room_contour_index = room_row[1][2]
            room_lines = room_row[2]
            room_contour_data.append([room_name, room_lines, room_contour_index])
        return room_contour_data

    def find_possible_rooms_for_each_door(self, door_contour_data, room_contour_data):
        possible_rooms_for_each_door = []
        for door_index, door_row in enumerate(door_contour_data):
            contour_line_index = door_row[3]
            possible_rooms_for_each_door.append([door_row])
            for room_row in room_contour_data:
                if room_row[2] == contour_line_index:
                    room_data = [room_row[0], contour_line_index, room_row[1]]
                    possible_rooms_for_each_door[door_index].append(room_data)
        return possible_rooms_for_each_door

    def find_room_door_distances(self, possible_rooms_for_each_door, iterative_object):
        door_room_distance = []
        for door_num, possible_row in enumerate(possible_rooms_for_each_door):
            door_room_distance.append(possible_row[0])
            door_centre = possible_row[0][1]
            if len(possible_row) > 1:
                room_data = possible_row[1:]
                for each_room in room_data:
                    room_name = each_room[0]
                    contour_index = each_room[1]
                    each_room_lines = each_room[2]
                    min_distance = 1000
                    for single_line in each_room_lines:
                        distance_from_door, intersect_x, intersect_y = iterative_object.calculate_distance_from_point_to_line(
                            door_centre, single_line)
                        if distance_from_door != -1 and distance_from_door < min_distance:
                            min_distance = distance_from_door
                    door_room_distance[door_num].append([room_name, min_distance, contour_index])
        return door_room_distance

    def find_main_entrance_door(self, door_room_distance, current_directory):
        entrance_door = []
        entry_room_file = open(current_directory + '/Model/floor_plan_text/entrance_door_rooms.txt', 'r+')
        entry_room_string = entry_room_file.read()
        entry_room__words_list = entry_room_string.split()

        for door_room_row in door_room_distance:
            room_data = door_room_row[4:]
            room_data.sort(key=itemgetter(1))
            if len(room_data) > 0:
                closest_contour_index = -1
                # --remove leading and ending white spaces in string
                closest_room = room_data[0][0].strip()
                for entry_room in entry_room__words_list:
                    if closest_room == entry_room:
                        closest_contour_index = room_data[0][2]
                        entrance_door.append([closest_contour_index, door_room_row[:3]])
        return entrance_door

    def get_door_positions(self, door_details, outer_contour_line, height, width):
        x1, x2, x3, x4, y1, y2, y3, y4, angle, centerx, centery = self.get_bounding_box_cordinates(
            outer_contour_line)

        door_positions = []
        for door_row in door_details:
            door_centre_x, door_centre_y = door_row[1]
            # cv2.circle(contour_image, (door_centre_x, door_centre_y), 5, (0, 0, 255), -1)
            # --top row
            if (door_centre_x >= x1 and door_centre_x <= x2) and (door_centre_y >= y1 and door_centre_y <= y2):
                door_position = 'top left'
            elif (door_centre_x > x2 and door_centre_x <= x3) and (door_centre_y >= y1 and door_centre_y <= y2):
                door_position = 'top centre'
            elif (door_centre_x > x3 and door_centre_x <= x4) and (door_centre_y >= y1 and door_centre_y <= y2):
                door_position = 'top right'
            # --centre row
            elif (door_centre_x >= x1 and door_centre_x <= x2) and (door_centre_y > y2 and door_centre_y <= y3):
                door_position = 'centre left'
            elif (door_centre_x > x2 and door_centre_x <= x3) and (door_centre_y > y2 and door_centre_y <= y3):
                door_position = 'centre middle'
            elif (door_centre_x > x3 and door_centre_x <= x4) and (door_centre_y > y2 and door_centre_y <= y3):
                door_position = 'centre right'
            # --bottom row
            elif (door_centre_x >= x1 and door_centre_x <= x2) and (door_centre_y > y3 and door_centre_y <= y4):
                door_position = 'bottom left'
            elif (door_centre_x > x2 and door_centre_x <= x3) and (door_centre_y > y3 and door_centre_y <= y4):
                door_position = 'bottom centre'
            elif (door_centre_x > x3 and door_centre_x <= x4) and (door_centre_y > y3 and door_centre_y <= y4):
                door_position = 'bottom right'
            else:
                door_position = 'Outside Contour Bounding Box'
            door_positions.append([[door_centre_x, door_centre_y], door_position])

        # ---find main door position
        return door_positions

    def generate_position_text(self, entrance_door, door_positions):
        if len(entrance_door) > 0:
            main_door_x, main_door_y = entrance_door[0][1][1]
            main_door_position = other_doors_text = ''
            other_positions = []
            for door_row in door_positions:
                door_x, door_y = door_row[0]
                positon = door_row[1]
                if door_x == main_door_x and door_y == main_door_y:
                    main_door_position = positon
                else:
                    # other_positions = other_positions + ' ' + positon + ','
                    other_positions.append(positon)

            main_door_text = 'The main entrance is located at the ' + main_door_position + ' of the floorplan.'
            if len(other_positions) > 1:
                other_doors_text = ' There are '+str(len(other_positions))+ 'more entrances at the '
                for pos,position in enumerate(other_positions):
                    if pos==len(other_positions)-1:
                        #---other_doors_text[:-1] coz we want to remove the comma inserted earlier
                        other_doors_text = other_doors_text[:-2] +' and '+position+'..'
                    else:
                        other_doors_text = other_doors_text+position+', '
            #-- adding 2 dots for tactile processing
            else:
                main_door_text = main_door_text+'.'
            final_door_text = main_door_text + other_doors_text

        else:
            final_door_text = 'No main entrances are detected..'

        return final_door_text

    def get_external_room_order(self, entrance_door, outer_contour_lines, shortest_distance_to_room):
        room_names_in_order = []
        if len(entrance_door) > 0:
            # ----entrance_door can have more than one if more than one door is connected to several hall, entry kind of rooms: hence 'entrance_door[0]'
            min_cont_index = entrance_door[0][0]
            door_data = entrance_door[0][1]
            door_centre_x, door_centre_y = door_data[1]

            # --re-order contour to start from cont_line closest to entrance door
            re_ordered_contour_lines = outer_contour_lines[min_cont_index:] + outer_contour_lines[:min_cont_index]

            room_cont_relation = []
            for cont_row, each_cont_line in enumerate(re_ordered_contour_lines):
                room_cont_relation.append([each_cont_line])
                x1, y1 = each_cont_line[0]
                x2, y2 = each_cont_line[1]
                for room_cont_data in shortest_distance_to_room:
                    # --get closest contour line cordinates
                    x3, y3 = room_cont_data[1][1][0]
                    x4, y4 = room_cont_data[1][1][1]
                    if (x1 == x3 and y1 == y3) and (x2 == x4 and y2 == y4):
                        # --room name
                        text = room_cont_data[0]
                        # --closest room line
                        room_line = room_cont_data[1][0]
                        # --room & contour possible intersection point
                        cont_intersect_x, cont_intersect_y = room_cont_data[1][6], room_cont_data[1][7]
                        room_cont_relation[cont_row].append([text, room_line, [cont_intersect_x, cont_intersect_y]])

            elements_to_delete = []
            # --check and delete contour lines that are not related to a room
            for row_num, each_row in enumerate(room_cont_relation):
                if len(each_row) < 2:
                    elements_to_delete.append(row_num)

            # --delete contour lines that do not have a connected room
            room_cont_relation = [i for j, i in enumerate(room_cont_relation) if j not in elements_to_delete]

            for row_num, each_contour_line_data in enumerate(room_cont_relation):
                x1, y1 = each_contour_line_data[0][0]
                for element_num, each_room_line in enumerate(each_contour_line_data[1:]):
                    cont_intersect_x, cont_intersect_y = each_room_line[2]
                    if row_num == 0:
                        distance = math.hypot(door_centre_x - cont_intersect_x, door_centre_y - cont_intersect_y)
                    else:
                        distance = math.hypot(x1 - cont_intersect_x, y1 - cont_intersect_y)
                    each_contour_line_data[1:][element_num].append(distance)
                connected_rooms = each_contour_line_data[1:]
                del room_cont_relation[row_num][1:]
                connected_rooms.sort(key=itemgetter(3))
                room_cont_relation[row_num].append(connected_rooms)

            # ---find external room names in the correct order
            for each_contour_line in room_cont_relation:
                for each_room in each_contour_line[1]:
                    room_names_in_order.append(each_room[0])
        return room_names_in_order

    def create_text(self, external_room_names_in_order, internal_rooms):
        external_room_text = internal_room_text = ''

        #---1. external room names string
        external_room_names_string = self.put_list_to_string(external_room_names_in_order)
        if len(external_room_names_string) > 0:
            external_room_text = 'Starting from the main entrance, the rooms in clockwise order are ' + external_room_names_string + '.'

        #---2. internal room names string
        internal_room_names_string = self.category_based_rooms_string(internal_rooms,article=False)
        if len(internal_room_names_string) > 0:
            if len(internal_rooms) == 1:
                internal_room_text = internal_room_names_string + ' is situated internally..'
            else:
                internal_room_text = internal_room_names_string + ' are situated internally..'

        #---3. combine texts and return
        return external_room_text + internal_room_text

    def put_list_to_string(self, room_names_in_order):
        room_names_string = ''
        for r, ex_room_name in enumerate(room_names_in_order):
            # --if room has not text
            if ex_room_name == '0':
                ex_room_name = 'a room with missing text label'

            #--generate string for case when list has only 1 element
            if len(room_names_in_order)==1:
                room_names_string = str(ex_room_name)
                break
            #--normal cases with >1 room names
            else:
                if r == len(room_names_in_order) - 1 :
                    room_names_string = room_names_string[:-2] + ' and ' + str(ex_room_name)
                else:
                    room_names_string = room_names_string + str(ex_room_name) + ', '
        return room_names_string

    def category_based_rooms_string(self,internal_rooms,article):
        categories = [] #---[room_type,count]
        for r,org_room in enumerate(internal_rooms):
            if org_room.strip()=='0':
                room = 'missing text label room'
            #--- we dont want to count how many 'bedroom 1', 'bedroom 2' are there
            #--- so we take only 'bedroom' <- 'bedroom 1'
            else:
                room = ''.join(x for x in org_room if x.isalpha())

            if r == 0:
                categories.append([room,1])
            else:
                #--- extract column from categories
                room_types = [row[0] for row in categories]
                if room in room_types:
                    #--since len(categories)==len(roomtype), indexes are also same
                    categories[room_types.index(room)][1] = categories[room_types.index(room)][1] +1
                else:
                    categories.append([room, 1])

        #--- setting grammar correct
        category_text = ''
        for cr,cat_row in enumerate(categories):
            #--add a 's' if plural
            if cat_row[1] > 1:
                name = str(cat_row[1])+' '+cat_row[0]+ 's'
            else:
                if article:
                    name = 'the ' + cat_row[0]
                else:
                    name = str(cat_row[1]) + ' ' + cat_row[0]

            # --generate string for case when list has only 1 element
            if len(categories) == 1:
                category_text = category_text + name
                break
            # --normal cases with >1 room names
            else:
                if cr == len(categories) - 1:
                    category_text = category_text[:-2] + ' and ' + name
                else:
                    category_text = category_text + name + ', '

        return category_text

    def get_bounding_box_cordinates(self, outer_contour_line):
        bbox = cv2.minAreaRect(outer_contour_line)

        centerx, centery = bbox[0]
        width, height = bbox[1]
        angle = bbox[2]

        x1 = int(centerx - width / 2)
        x4 = int(centerx + width / 2)
        y1 = int(centery - height / 2)
        y4 = int(centery + height / 2)

        x2 = int((2 * x1 + x4) / 3)
        x3 = int((x1 + 2 * x4) / 3)
        y2 = int((2 * y1 + y4) / 3)
        y3 = int((y1 + 2 * y4) / 3)

        return x1, x2, x3, x4, y1, y2, y3, y4, angle, centerx, centery

    def rotate_image(self, path_gravvitas, contour_image, rotate_angle):
        rotated_image = contour_image.rotate(rotate_angle, expand=True, resample=Image.BICUBIC)
        rotated_image.save(path_gravvitas + 'Rotated_image.jpg')

    def get_room_door_relations(self,rooms_point_list,doors_rect_list,new_img_height,new_img_width,avg_door_width,external_room_names_in_order,internal_rooms):
        room_lines_list = []
        for room_row_num,room_row in enumerate(rooms_point_list):
            room_type = room_row[1]
            room_cont_points = room_row[2]

            if room_type != 'OP':
                corner_point_list = self.get_room_corner_points(room_cont_points,new_img_height, new_img_width,avg_door_width)
                lines_list = self.iterative_obj.convert_points_to_lines(corner_point_list)
                room_lines_list.append([room_row_num,room_row[0],lines_list])

        door_room_relations = []
        for door_row_num,door_row in enumerate(doors_rect_list):
            # print door_row_num, door_row
            p1 = door_row[0]
            p3 = door_row[2]
            cx,cy = self.iterative_obj.find_centre_of_line([p1,p3])

            rooms_close_to_door = []
            for room_row in room_lines_list:
                # print room_row[1]
                room_lines = room_row[2]

                #--per room find room line closest to door
                min_distance = 1000
                for each_room_line in room_lines:
                    distance_from_door, intersect_x, intersect_y = self.iterative_obj. calculate_distance_from_point_to_line([cx,cy],each_room_line)
                    if distance_from_door != -1 and distance_from_door < min_distance:
                        point_is_inside = self.iterative_obj.check_point_inside_line([intersect_x,intersect_y],each_room_line)
                        if point_is_inside:
                            # closest_room_line = each_room_line
                            min_distance = distance_from_door

                if min_distance < (avg_door_width/3):
                    rooms_close_to_door.append([room_row[0],min_distance])


            if len(rooms_close_to_door) > 0:
                #--find closest 2 rooms for door
                rooms_close_to_door.sort(key=itemgetter(1))
                closest_room_pair = rooms_close_to_door[:2]

                # #--debug draw
                # cont_image = ~(np.zeros((new_img_height, new_img_width, 3), np.uint8))
                # for room_row_num, room_row in enumerate(rooms_point_list):
                #     room_cont_points = room_row[2]
                #     room_cont = self.iterative_obj.convert_points_to_contour(room_cont_points)
                #     cv2.drawContours(cont_image, [room_cont], -1, (0, 0, 0), 3)
                # for selected_room in closest_room_pair:
                #     room_num = selected_room[0]
                #     print rooms_point_list[room_num][0]
                #     room_cont_points = rooms_point_list[room_num][2]
                #     room_cont = self.iterative_obj.convert_points_to_contour(room_cont_points)
                #     cv2.drawContours(cont_image, [room_cont], -1, (0, 255, 0), 3)
                # cv2.circle(cont_image, (cx,cy), 10, (0, 0, 255), -1)
                # # print self.path_gravvitas + str(door_row_num)+'_room_nd_door.png'
                # cv2.imwrite(self.path_gravvitas + str(door_row_num)+'_room_nd_door.png', cont_image)

                closest_room_pair_details = [row[0] for row in closest_room_pair]
                door_room_relations.append([door_row_num,closest_room_pair_details])



        # for door_room_row in door_room_relations:
        #     print door_room_row
        #     for r in door_room_row[1]:
        #         print rooms_point_list[r][0]
        # print '-------------------'

        #--find room relations
        room_relations = []
        for room_row_num,room_row in enumerate(rooms_point_list):

            other_rooms_data = []
            for door_room_row in door_room_relations:
                rooms_list = door_room_row[1]
                if room_row_num in rooms_list:
                    if len(rooms_list)>1:
                        current_room_index = rooms_list.index(room_row_num)
                        other_room_index = 1 - current_room_index
                        other_room_row_num = rooms_list[other_room_index]
                        other_rooms_data.append(rooms_point_list[other_room_row_num][0])
                    else:
                        other_rooms_data.append(rooms_point_list[room_row_num][0])


            if len(other_rooms_data)>0:
                room_relations.append([room_row[0],other_rooms_data])

        room_door_text = ''
        room_door_text = self.generate_room_door_text(external_room_names_in_order,room_relations,room_door_text)
        room_door_text = self.generate_room_door_text(internal_rooms,room_relations,room_door_text)

        return room_door_text



    def get_room_corner_points(self,room_cont_points,new_img_height, new_img_width,avg_door_width):
        current_cont = ~(np.zeros((new_img_height, new_img_width, 3), np.uint8))
        room_cont = self.iterative_obj.convert_points_to_contour(room_cont_points)
        # print room_cont
        cv2.drawContours(current_cont, [room_cont], -1, (0, 255, 0), 3)
        # cv2.imwrite(self.path_gravvitas + '-corner_pointCOnt.png', current_cont)

        gray = cv2.cvtColor(current_cont, cv2.COLOR_RGB2GRAY)
        corners = cv2.goodFeaturesToTrack(gray, 200, 0.27, avg_door_width / 10)
        corners = np.int0(corners)
        new_corners = []
        # if debug_mode:
        #     simple_cont2 = ~(np.zeros((img_height, img_width, 3), np.uint8))
        #     cv2.drawContours(simple_cont2, [cnt], -1, (0, 0, 0), -1)
        for i in corners:
            x, y = i.ravel()
            new_corners.append([x, y])
        #     if debug_mode:
        #         cv2.circle(simple_cont2, (x, y), 10, (0, 0, 255), -1)
        # if debug_mode:
        #     cv2.imwrite(path_output + '-4step-' + str(c) + '*Corner Points.png', simple_cont2)

        cont_point_list = self.iterative_obj.get_points_from_contour(room_cont)
        for num, r in enumerate(cont_point_list):
            cont_point_list[num].append([])

        # ----4. order corner points based on contour
        for p_index, point in enumerate(new_corners):
            x1, y1 = point
            closest_index, min_distance = -1, 1000
            for index, cont_point in enumerate(cont_point_list):
                x2, y2 = cont_point[0], cont_point[1]
                distance = math.hypot(x2 - x1, y2 - y1)
                if distance < min_distance:
                    closest_index = index
                    min_distance = distance
            new_corners[p_index].append(closest_index)
        new_corners.sort(key=itemgetter(2))
        contable_corner_points = []
        for row in new_corners:
            contable_corner_points.append([row[0], row[1]])

        return contable_corner_points

    def generate_room_door_text(self,external_room_names_in_order,room_relations,text):

        for external_room in external_room_names_in_order:
            if external_room != 'corridor':
                main_room_names_list = [row[0] for row in room_relations]
                if external_room in main_room_names_list:
                    interested_row_index = main_room_names_list.index(external_room)
                    connected_rooms = room_relations[interested_row_index][1]

                    text = text + 'The ' + external_room + ' has'

                    if external_room in connected_rooms:
                        text = text + ' an external door'
                        room_index = connected_rooms.index(external_room)
                        del connected_rooms[room_index]

                        if len(connected_rooms) > 1:
                            text = text + ' and'

                    if len(connected_rooms) > 1:
                        text = text + ' entrances to '
                    if len(connected_rooms) == 1:
                        text = text + ' an entrance to '

                    loop_executed = False
                    for connected_room_name in connected_rooms:
                        text = text + connected_room_name + ', '
                        loop_executed = True

                    if loop_executed:
                        text = text[:-2] + '. '
                    else:
                        text = text+'. '

        return text
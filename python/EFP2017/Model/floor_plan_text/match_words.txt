balcony : balconbois,balcon
bathroom : bath,bains,douche,dou,bathldry,bathroom,bth,shower,showerroom,shwr,tolbath,vanity,masterbath
bedroom : ch,chambre,bedroom,bdrm,bed,maidsroom,chambreamis,chambreenfants,chambreparents
bir : builtinrobe,bir
car port : car port,carport,abrivoiture,abrimoto,carshelter
cellar : cellar,cellier,cave
closet : closet,cl,clo,clos,clst,cupboard,lin,linen,robe,shelves,pl,placard
corridor : corridor,corr,hallway,sidehallway,degagement,dgt,degt
dining : dining,dining room,sejour
dressing room : dressing room,dress,dressing,lingerie,vestiaire,vest
ensuite : ensuite,ens,ent
entry : entry,hall,hal
garage : garage,garages,abrigarage,cargarage,carpark,carsgarage,carspace
kitchen : kitchen,kit,kitch,cuisine
laundry : laundry,laun,laun,laundryarea,laundryutility,ldry,ldy,buanderielingerie
living : living,living room,repas
master bedroom : masterbedroom,masterbdrm,masterbed,mastersuite
office : office,bureau
porch : porch,porche,salonveranda,veranda,patio,arcades
shed : shed,verriere,abribois,pergola
storage : storage,storeroom,rangement,buanderie,rgt
terrace : terrace,terrassecouverte,terrasse,terrasebois
toilet : toilet,wc,pwd,pwdr
other room : salletv,localtechnique,atelier,terreplein,vide
wir : wir,walkincloset,masterwic


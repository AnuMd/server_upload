from pre_processing_detection import pre_process_class
from text_desc_generation import text_desc_generator_class
from svg_fp_generation import svg_fp_generation_class
from tactile_fp_generation import tactile_floorplan_generation_class

class accessible_fp_class():
    def __init__(self,current_directory,path_output,file_name,file_number,avg_door_width,
                 path_gravvitas,path_gravvitas_svg,tactile_path,
                 orginal_img_height, orginal_img_width):
        self.current_directory = current_directory
        self.path_output = path_output
        self.file_name = file_name
        self.file_number = file_number
        self.avg_door_width = avg_door_width
        self.path_gravvitas = path_gravvitas
        self.path_gravvitas_svg = path_gravvitas_svg
        self.tactile_path = tactile_path
        self.orginal_img_height, self.orginal_img_width = orginal_img_height, orginal_img_width

    def generate_accessible_floorplans(self):
        #--pre-process detection data to get needed data
        pre_process_obj = pre_process_class(self.current_directory,self.path_output,
                          self.file_name,self.avg_door_width,self.path_gravvitas,
                          self.orginal_img_height,self.orginal_img_width)
        pre_processed_results = pre_process_obj.pre_process_detection()
        modified_outer_contour, modified_outer_contour_lines, new_img_height, new_img_width, all_fp_comps,modified_room_cordinates = pre_processed_results

        #-- generate text description
        text_desc_generator_obj = text_desc_generator_class(self.current_directory,
                                                self.file_name,self.path_gravvitas)
        fp_text = text_desc_generator_obj.generate_text_description(modified_outer_contour,
                   modified_outer_contour_lines,new_img_height,new_img_width, all_fp_comps,
                   modified_room_cordinates,self.avg_door_width)

        # -- generate gravvitas fps
        svg_generator_obj = svg_fp_generation_class(self.current_directory,
                          self.file_name,self.path_gravvitas,self.path_gravvitas_svg,
                          self.orginal_img_height,self.orginal_img_width,                                           self.avg_door_width, self.file_number)
        svg_generator_obj.generate_svg_floorplan(new_img_height,new_img_width,
                           all_fp_comps, fp_text,modified_room_cordinates)

        # fp_text = ''
        # -- generate tactile fps
        tactile_fp_generator_obj = tactile_floorplan_generation_class(self.path_gravvitas,
                                   self.current_directory,self.orginal_img_height,
                                   self.orginal_img_width,self.avg_door_width)
        tactile_fp_generator_obj.generate_tactile_floorplan(self.file_name,
                                    self.tactile_path, new_img_height,
                                    new_img_width, all_fp_comps,fp_text,
                                    modified_room_cordinates)

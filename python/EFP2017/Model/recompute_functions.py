import cv2,time
import os
import math
import itertools
import numpy as np
import PIL.ImageOps

from PIL import Image
from operator import itemgetter

from color_check import color_check_class
from open_plan_line import line
from open_plan import planClass
from penalties_and_scores import penalties_scores_class
from iterative_functions import iterative_functions_class
from line_optimization_functions import line_optimization_function_class
from generate_other_lines import generate_other_lines_class

class recompute_functions_class:
    def __init__(self,gray_height,gray_width,open_plan_path,region_grow_path,voronoi_lines_path,edge_extensions_path,shortest_path_path,line_optimizaiton_path,line_scoring_path,avg_door_width):
        self.avg_door_width = avg_door_width
        self.penalties_scores_obj = penalties_scores_class()
        self.iterative_obj = iterative_functions_class()
        self.line_optimization_function_obj = line_optimization_function_class()
        self.gray_height,self.gray_width=gray_height,gray_width
        self.open_plan_path, self.region_grow_path, self.voronoi_lines_path, self.edge_extensions_path, self.shortest_path_path, self.line_optimizaiton_path, self.line_scoring_path = open_plan_path,region_grow_path,voronoi_lines_path,edge_extensions_path,shortest_path_path,line_optimizaiton_path,line_scoring_path

    def region_grow_function(self,contour_check,open_plan_text_cordinate,outer_cont):
        image = Image.fromarray(outer_cont)
        width, height = image.size
        pixels = image.load()

        # ---- recheck if can be combined
        threshold_array_row_length = len(open_plan_text_cordinate)
        top_threshold_pixel_array = [[0 for b in range(threshold_array_row_length)] for c in range(width)]
        bottom_threshold_pixel_array = [[height for b in range(threshold_array_row_length)] for c in range(width)]
        left_threshold_pixel_array = [[0 for b in range(threshold_array_row_length)] for c in range(height)]
        right_threshold_pixel_array = [[width for b in range(threshold_array_row_length)] for c in range(height)]

        # -----create array to store all direction and pixel data and initialize it
        pixel_data = [[-1 for column in range(width)] for row in range(height)]

        # max_size = (max(width,height))/2
        color_val_obj = color_check_class(pixels, top_threshold_pixel_array, bottom_threshold_pixel_array,
                                          left_threshold_pixel_array, right_threshold_pixel_array)
        temp_array_copy = []
        temp_ring_array = []
        flag_list = [0 for x in range(len(open_plan_text_cordinate))]
        for ring_count in range(1500):
            for current_txt_label in range(len(open_plan_text_cordinate)):
                if flag_list[current_txt_label] == 0:
                    each_cordinate = open_plan_text_cordinate[current_txt_label]
                    if ring_count == 0:
                        # ---- update pixel_data array with txt_lable cordinate
                        column, row = each_cordinate
                        pixel_data[row][column] = current_txt_label
                        # -----add txt_label cordinate to temp_array
                        ta_length = len(temp_array_copy)
                        temp_array_copy.append([])
                        temp_array_copy[ta_length].append(each_cordinate)
                        temp_array_copy[ta_length].append(current_txt_label)
                    flag_count = 0
                    text_exists_flag = -1
                    for row in temp_array_copy:
                        if row[1] == current_txt_label:
                            text_exists_flag = 1
                            col_coord, row_coord = row[0]
                            top_pixel = col_coord, row_coord - 1
                            left_pixel = col_coord - 1, row_coord
                            bottom_pixel = col_coord, row_coord + 1
                            right_pixel = col_coord + 1, row_coord

                            top_col, top_row = top_pixel
                            if cv2.pointPolygonTest(contour_check, (top_col, top_row), False) == 1:
                                if pixel_data[top_row][top_col] == -1:
                                    temp_ring_array, pixel_data = color_val_obj.color_match(top_pixel, pixel_data,
                                                                                            temp_ring_array, 'T',
                                                                                            current_txt_label)
                                    flag_count = flag_count + 1

                            left_col, left_row = left_pixel
                            if cv2.pointPolygonTest(contour_check, (left_col, left_row), False) == 1:
                                if pixel_data[left_row][left_col] == -1:
                                    temp_ring_array, pixel_data = color_val_obj.color_match(left_pixel, pixel_data,
                                                                                            temp_ring_array, 'L',
                                                                                            current_txt_label)
                                    flag_count = flag_count + 1

                            bottom_col, bottom_row = bottom_pixel
                            if cv2.pointPolygonTest(contour_check, (bottom_col, bottom_row), False) == 1:
                                if pixel_data[bottom_row][bottom_col] == -1:
                                    temp_ring_array, pixel_data = color_val_obj.color_match(bottom_pixel, pixel_data,
                                                                                            temp_ring_array, 'B',
                                                                                            current_txt_label)
                                    flag_count = flag_count + 1

                            right_col, right_row = right_pixel
                            if cv2.pointPolygonTest(contour_check, (right_col, right_row), False) == 1:
                                if pixel_data[right_row][right_col] == -1:
                                    temp_ring_array, pixel_data = color_val_obj.color_match(right_pixel, pixel_data,
                                                                                            temp_ring_array, 'R',
                                                                                            current_txt_label)
                                    flag_count = flag_count + 1
                        else:
                            text_exists_flag = 0

                    if flag_count == 0 and text_exists_flag == 1:
                        flag_list[current_txt_label] = 1
                        break
            temp_array_copy = temp_ring_array
            temp_ring_array = []

            if sum(flag_list) == len(open_plan_text_cordinate):
                # if debug_mode:
                #     print ring_count
                break
        for img_row in range(height - 1):
            for img_column in range(width - 1):
                # print img_row,img_column
                if pixel_data[img_row][img_column] == 0:
                    pixels[img_column, img_row] = (255, 0, 0)
                elif pixel_data[img_row][img_column] == 1:
                    pixels[img_column, img_row] = (0, 255, 0)
                elif pixel_data[img_row][img_column] == 2:
                    pixels[img_column, img_row] = (0, 0, 255)
                elif pixel_data[img_row][img_column] == 3:
                    pixels[img_column, img_row] = (255, 255, 0)
                elif pixel_data[img_row][img_column] == 4:
                    pixels[img_column, img_row] = (102, 51, 0)
                elif pixel_data[img_row][img_column] == 5:
                    pixels[img_column, img_row] = (255, 0, 255)
                elif pixel_data[img_row][img_column] == 6:
                    pixels[img_column, img_row] = (0, 255, 255)
                elif pixel_data[img_row][img_column] == 7:
                    pixels[img_column, img_row] = (255, 0, 125)
                elif pixel_data[img_row][img_column] == 8:
                    pixels[img_column, img_row] = (255, 128, 0)
                elif pixel_data[img_row][img_column] == 9:
                    pixels[img_column, img_row] = (125, 0, 255)
                elif pixel_data[img_row][img_column] == 10:
                    pixels[img_column, img_row] = (102, 0, 0)
                elif pixel_data[img_row][img_column] == 11:
                    pixels[img_column, img_row] = (0, 150, 75)
                elif pixel_data[img_row][img_column] == 12:
                    pixels[img_column, img_row] = (102, 0, 102)
                elif pixel_data[img_row][img_column] == 13:
                    pixels[img_column, img_row] = (150, 150, 0)
                elif pixel_data[img_row][img_column] == 14:
                    pixels[img_column, img_row] = (200, 255, 200)

        # output_image.save(self.output_directory+name+'_STEP_02-1.png')
        # image.save(region_grow_path+name+'_STEP_02.png')
        # region_grow_end_time = time.time()
        # print 'RG time : ',region_grow_end_time-region_grow_start_time

        return image

    def find_voronoi_lines(self,region_growed_image,open_plan_text_cordinate,image_name,name,major_voronoi_data,major_all_contour_lines,debug_mode):
        # new_lines_start = time.time()

        # new_partitions_path= region_grow_path+name+'/'
        # os.mkdir(new_partitions_path)

        # pil_image = Image.open(region_grow_path+name+'_STEP_02.png')
        clrs = region_growed_image.getcolors()
        existing_colors = []
        for row in clrs:
            color = row[1]
            exists = False
            if (color[0] == 0 and color[1] == 0 and color[2] == 0) or (
                    color[0] == 255 and color[1] == 255 and color[2] == 255):
                exists = True
            if row[0] > 5 and exists == False:
                existing_colors.append(row[1])

        all_contours = []
        all_contour_lines = []
        count = 0
        if debug_mode:
            redraw_contours = []
        # pixels_2 = image.load()

        np_image = np.array(region_growed_image)

        for r, g, b in existing_colors:
            indices = np.where(np.all(np_image == (r, g, b), axis=-1))
            coordinates = zip(indices[0], indices[1])

            image_3 = Image.new('RGB', (self.gray_width, self.gray_height), color="white")
            pixels_3 = image_3.load()

            for row in coordinates:
                x, y = row
                pixels_3[y, x] = (0, 0, 0)

            gray_room = cv2.cvtColor(np.array(image_3), cv2.COLOR_RGB2GRAY)
            ret, thresh = cv2.threshold(gray_room, 0, 255, 1)
            contours, hierachy = cv2.findContours(thresh, 1, 2)
            if debug_mode:
                redraw_contours.append(contours)

            if len(contours) > 0:
                current_cont = contours[-1]
                image_contour = self.iterative_obj.get_points_from_contour(current_cont)
                image_contour_lines = self.iterative_obj.convert_points_to_lines(image_contour)

                # --find text inside current cont
                text_cordinate = 0
                for text_row, text_cord in enumerate(open_plan_text_cordinate):
                    if cv2.pointPolygonTest(current_cont, (tuple(text_cord)), False) == 1:
                        text_cordinate = text_row
                        break

                # ------ add all room contours to one array
                all_contours.append(image_contour)
                if len(image_contour_lines) > 0:
                    all_contour_lines.append([])
                    all_contour_lines[count].append(image_contour_lines)
                    all_contour_lines[count].append(text_cordinate)

            count = count + 1

        # #-------write all room contours to a new image
        if debug_mode:
            empty_new_image2 = ~(np.zeros((self.gray_height, self.gray_width, 3), np.uint8))
            for each in redraw_contours:
                cv2.drawContours(empty_new_image2, each, -1, (0, 0, 0), 1, cv2.cv.CV_AA)
            cv2.imwrite(self.region_grow_path + name + '_STEP_03.png', empty_new_image2)

        voronoi_data = []
        common_contour_line = []
        count = 0
        # outer_contour=[]
        common_outer_contour_line = []
        outer_count = 0
        # iterative_obj = iterative_functions_class()
        for cont, contour in enumerate(all_contour_lines):
            for line1 in contour[0]:
                x1, y1 = line1[0]
                x2, y2 = line1[1]
                # --get gradeint and centre point of line1
                m1, c1 = self.iterative_obj.find_mc_of_line(x1, y1, x2, y2)
                # center_x, center_y = iterative_obj.find_centre_of_line(line1)
                for cont2, contour2 in enumerate(all_contour_lines):
                    if cont2 != cont:
                        for line2 in contour2[0]:
                            x3, y3 = line2[0]
                            x4, y4 = line2[1]
                            # --measure distance from centre_point in line to line2
                            # distance_from_point, intersect_x, intersect_y = iterative_obj.calculate_distance_from_point_to_line([center_x, center_y],line2)
                            # ---get gradient of line2
                            m2, c2 = self.iterative_obj.find_mc_of_line(x3, y3, x4, y4)
                            voronoi_line = False
                            if (m1 == 'a' and m2 == 'a') and (abs(x1 - x3) <= 2):
                                voronoi_line = self.iterative_obj.find_line_subsets(line1, line2)
                            elif (m1 != 'a' and m2 != 'a') and abs(m1 - m2) < 0.02 and (abs(c1 - c2) <= 2):
                                voronoi_line = self.iterative_obj.find_line_subsets(line1, line2)

                            # ----in line & line2 if gradient is appprox similar, close to each other: add as voronoi
                            if voronoi_line:
                                voronoi_exists = False
                                for row in common_contour_line:
                                    t1, t2, line_c = row
                                    if t1 == contour[1] and t2 == contour2[
                                        1] and self.iterative_obj.find_similarity_two_lines_ordered(line_c, line1):
                                        voronoi_exists = True
                                if voronoi_exists == False:
                                    common_contour_line.append([])
                                    common_contour_line[count].append(contour[1])
                                    common_contour_line[count].append(contour2[1])
                                    common_contour_line[count].append(line1)
                                    count = count + 1
                            else:
                                common_outer_contour_line.append([])
                                common_outer_contour_line[outer_count].append(contour[1])
                                common_outer_contour_line[outer_count].append(contour2[1])
                                common_outer_contour_line[outer_count].append(line1)
                                outer_count = outer_count + 1
        temp_voronoi_data = []
        for r, row in enumerate(common_contour_line):
            if r == 0:
                temp_voronoi_data.append(row)
            else:
                round1_cord1 = row[0]
                round1_cord2 = row[1]
                exists = False
                for v, vor_row in enumerate(temp_voronoi_data):
                    round2_cord1 = vor_row[0]
                    round2_cord2 = vor_row[1]
                    if round1_cord1 == round2_cord1 and round1_cord2 == round2_cord2:
                        exists = True
                        count = v
                        break
                if exists is True:
                    temp_voronoi_data[count].append(row[2])
                else:
                    temp_voronoi_data.append(row)

        # --check if [0,1 [line...]] and [1,0[line2.....] exists
        for r1, row1 in enumerate(temp_voronoi_data):
            if r1 == 0:
                voronoi_data.append(row1)
            else:
                exists = False
                for r2, row2 in enumerate(voronoi_data):
                    if row2[0] == row1[1] and row2[1] == row1[0]:
                        exists = True
                        break
                if exists == False:
                    voronoi_data.append(row1)

        if debug_mode:
            contour_image = cv2.imread(self.region_grow_path + name + '_STEP_01.png', cv2.IMREAD_COLOR)
            for r, row in enumerate(voronoi_data):
                contour_image_copy = contour_image.copy()
                for v_line in row[2:]:
                    cv2.line(contour_image_copy, (tuple(v_line[0])), (tuple(v_line[1])), (0, 0, 255), 2, cv2.cv.CV_AA)
                one_text, second_text = False, False
                for row_num, text in enumerate(open_plan_text_cordinate):
                    if row_num == row[0]:
                        voronoi_text_cordinate = text
                        cv2.circle(contour_image_copy, (tuple(voronoi_text_cordinate)), 5, (255, 0, 0), -1)
                        one_text = True
                    if row_num == row[1]:
                        voronoi_text_cordinate = text
                        cv2.circle(contour_image_copy, (tuple(voronoi_text_cordinate)), 5, (255, 0, 0), -1)
                        second_text = True
                    if one_text and second_text:
                        break
                # cv2.circle(contour_image_copy, (tuple(row[0])), 5, (255, 0, 0), -1)
                # cv2.circle(contour_image_copy, (tuple(row[1])), 5, (255, 0, 0), -1)
                cv2.imwrite(self.region_grow_path + name + ' : ' + str(r) + '_STEP_04.png', contour_image_copy)
            cv2.imwrite(self.region_grow_path + name + '_STEP_04.png', contour_image)

            outer_cont_image = ~(np.zeros((self.gray_height, self.gray_width, 3), np.uint8))
            for row in common_outer_contour_line:
                for line_c in row[2:]:
                    cv2.line(outer_cont_image, (tuple(line_c[0])), (tuple(line_c[1])), (0, 0, 0), 1, cv2.cv.CV_AA)
            cv2.imwrite(self.region_grow_path + name + '_STEP_05.png', outer_cont_image)

            cv2.imwrite(self.open_plan_path + image_name, contour_image)

            voronoi_image = cv2.imread(self.region_grow_path + name + '_STEP_01.png', cv2.IMREAD_COLOR)
            # for each in redraw_contours:
            #     cv2.drawContours(voronoi_image, each, -1, (0,0,0), 1,cv2.cv.CV_AA)
            for row in voronoi_data:
                for v_line in row[2:]:
                    cv2.line(voronoi_image, (tuple(v_line[0])), (tuple(v_line[1])), (255, 0, 0), 2, cv2.cv.CV_AA)
            for cordinate in open_plan_text_cordinate:
                cv2.circle(voronoi_image, (tuple(cordinate)), 5, (0, 0, 255), -1)
            cv2.imwrite(self.region_grow_path + name + '_STEP_06.png', voronoi_image)

        major_voronoi_data.append(voronoi_data)
        major_all_contour_lines.append(all_contour_lines)

        # print '-----voronoi_data----'
        # for row in voronoi_data:
        #     print row
        # print '-----all_contour_lines----'
        # for row in all_contour_lines:
        #     print row

        # print 'NL line time : ', time.time()-new_lines_start
        return major_voronoi_data,major_all_contour_lines

    def extend_edges(self,current_open_plan_contour_data,open_plan_level,contour_to_check,image_edge_extensions_path,image_path,image_contour_lines,debug_mode):
        original_edges_and_extensions = []
        if debug_mode:
            empty_new_image = ~(np.zeros((self.gray_height, self.gray_width, 3), np.uint8))
            # ----changed line thickness to 2 since when its 1 findcontours is not finding it
            cv2.drawContours(empty_new_image, [contour_to_check], -1, (0, 0, 0), 2, cv2.cv.CV_AA)
            cv2.imwrite(image_edge_extensions_path + '_STEP_01.png', empty_new_image)
            os.remove(image_path)
            cv2.imwrite(image_path, empty_new_image)

        count = 0
        # radius = avg_door_width/4
        # ----set minimum line length based on level since with each level small line segments length increase
        plan_level = int(open_plan_level)
        # print '-------------------------------',plan_level,'-----------------------'
        if plan_level < 3:
            current_min_line_length = self.avg_door_width / 40
        elif plan_level == 3:
            current_min_line_length = self.avg_door_width / 30
        else:
            current_min_line_length = self.avg_door_width / 20

        # line extraction and finding intersections
        for l1, level1 in enumerate(image_contour_lines):
            x1, y1 = level1[0]
            x2, y2 = level1[1]
            # --check if length of currnet line is not too small
            line_length = math.hypot(x2 - x1, y2 - y1)
            if line_length > current_min_line_length:
                # ----find if two points beside line are inside contour
                line_obj1 = line()
                point_inside_line = line_obj1.find_points_next_to_end_points_in_line(x1, y1, x2, y2, line_length,contour_to_check)
                if point_inside_line:
                    # print 'loop 3', l1,level1
                    # --find line numbers to find before and after lines
                    line_number_before, line_number_after = 0, 0
                    if l1 == 0:
                        line_number_before = len(image_contour_lines) - 1
                    elif l1 == len(image_contour_lines) - 1:
                        line_number_after = 0
                    else:
                        line_number_before = l1 - 1
                        line_number_after = l1 + 1
                    # --find length of line before current line
                    line_before = image_contour_lines[line_number_before]
                    before_x1, before_y1 = line_before[0]
                    before_x2, before_y2 = line_before[1]
                    length_line_before = math.hypot(before_x2 - before_x1, before_y2 - before_y1)
                    # --find length of line after current line
                    line_after = image_contour_lines[line_number_after]
                    after_x1, after_y1 = line_after[0]
                    after_x2, after_y2 = line_after[1]
                    length_line_after = math.hypot(after_x2 - after_x1, after_y2 - after_y1)
                    # --if line before OR after are more than door width: extend that line
                    if length_line_before > int(self.avg_door_width / 10) or length_line_after > int(self.avg_door_width / 10) or line_length > current_min_line_length:
                        # print 'loop 4', l1,level1
                        for l2, level2 in enumerate(image_contour_lines):
                            if l1 == 0:
                                lower = len(image_contour_lines) - 1
                            else:
                                lower = l1 - 1
                            if l1 == len(image_contour_lines) - 1:
                                upper = 0
                            else:
                                upper = l1 + 1
                            check_value = False
                            if l2 == l1 or l2 == upper or l2 == lower:
                                check_value = True
                            if check_value == False:
                                # if l2>l1:
                                x3, y3 = level2[0]
                                x4, y4 = level2[1]
                                max_x1 = max(x3, x4)
                                min_x1 = min(x3, x4)
                                max_y1 = max(y3, y4)
                                min_y1 = min(y3, y4)
                                line_obj = line()
                                intersect_x, intersect_y = line_obj.find_contour_intersections(x1, y1, x2, y2, x3, y3,x4, y4)
                                if (intersect_x >= min_x1 and intersect_x <= max_x1 and intersect_y >= min_y1 and intersect_y <= max_y1):
                                    # print 'loop 5', l1,level1
                                    distance_to_first_cordinate_line1 = math.hypot(intersect_x - x1, intersect_y - y1)
                                    distance_to_second_cordinate_line1 = math.hypot(intersect_x - x2, intersect_y - y2)
                                    closest_point_to_intersection = min(distance_to_first_cordinate_line1,
                                                                        distance_to_second_cordinate_line1)
                                    extended_edge_p1_x, extended_edge_p1_y, extended_edge_p2_x, extended_edge_p2_y = 0, 0, 0, 0
                                    if closest_point_to_intersection == distance_to_first_cordinate_line1:
                                        extended_edge_p1_x, extended_edge_p1_y, extended_edge_p2_x, extended_edge_p2_y = x1, y1, intersect_x, intersect_y
                                    if closest_point_to_intersection == distance_to_second_cordinate_line1:
                                        extended_edge_p1_x, extended_edge_p1_y, extended_edge_p2_x, extended_edge_p2_y = x2, y2, intersect_x, intersect_y

                                    max_e_x = max(extended_edge_p1_x, extended_edge_p2_x)
                                    min_e_x = min(extended_edge_p1_x, extended_edge_p2_x)
                                    max_e_y = max(extended_edge_p1_y, extended_edge_p2_y)
                                    min_e_y = min(extended_edge_p1_y, extended_edge_p2_y)
                                    text_intersects_flag = False
                                    line_obj = line()
                                    # -----check if improved_line intersects with the text boundary box
                                    for t, text_data in enumerate(current_open_plan_contour_data[3:]):
                                        for bb_line in text_data[3]:
                                            # print bb_line
                                            bb_line_x1, bb_line_y1 = bb_line[0]
                                            bb_line_x2, bb_line_y2 = bb_line[1]
                                            bb_line_max_x = max(bb_line_x1, bb_line_x2)
                                            bb_line_min_x = min(bb_line_x1, bb_line_x2)
                                            bb_line_max_y = max(bb_line_y1, bb_line_y2)
                                            bb_line_min_y = min(bb_line_y1, bb_line_y2)
                                            intersect_x1, intersect_y1 = line_obj.find_contour_intersections(x1, y1, x2,y2,bb_line_x1,bb_line_y1,bb_line_x2,bb_line_y2)
                                            if (intersect_x1 >= min_e_x and intersect_x1 <= max_e_x and intersect_y1 >= min_e_y and intersect_y1 <= max_e_y) and (intersect_x1 >= bb_line_min_x and intersect_x1 <= bb_line_max_x and intersect_y1 >= bb_line_min_y and intersect_y1 <= bb_line_max_y):
                                                text_intersects_flag = True
                                                break
                                        if text_intersects_flag:
                                            break

                                    if not text_intersects_flag:
                                        num_of_points_outside_contour = line_obj.check_line_inside_contour_special(extended_edge_p1_x, extended_edge_p1_y, extended_edge_p2_x,extended_edge_p2_y, contour_to_check)
                                        if num_of_points_outside_contour < 10:
                                            ee_x1, ee_y1 = int(extended_edge_p1_x), int(extended_edge_p1_y)
                                            ee_x2, ee_y2 = int(extended_edge_p2_x), int(extended_edge_p2_y)

                                            original_edges_and_extensions.append([[[x1, y1], [x2, y2]],
                                                                                  [[ee_x1, ee_y1], [ee_x2, ee_y2]]])
                                            if debug_mode:
                                                cv2.circle(empty_new_image, (intersect_x, intersect_y), 5, (0, 255, 0),
                                                           -1)
                                                cv2.circle(empty_new_image, (extended_edge_p1_x, extended_edge_p1_y), 5,
                                                           (0, 0, 0), -1)
                                            count = count + 1
        # cv2.imwrite(image_edge_extensions_path+'_TESTTTTTTTTTTTT.png',extended_line_sections_image)
        if debug_mode:
            cv2.imwrite(image_edge_extensions_path + '_STEP_03.png', empty_new_image)
            extended_line_sections_image = ~(np.zeros((self.gray_height, self.gray_width, 3), np.uint8))
            cv2.drawContours(extended_line_sections_image, [contour_to_check], -1, (0, 0, 0), 2, cv2.CV_AA)
            for line_row in original_edges_and_extensions:
                point = line_row[1]
                x1, y1 = point[0]
                x2, y2 = point[1]
                cv2.line(extended_line_sections_image, (x1, y1), (x2, y2), (0, 255, 0), 2, cv2.CV_AA)
            cv2.imwrite(image_edge_extensions_path + '_STEP_04.png', extended_line_sections_image)

        return original_edges_and_extensions


    def find_shortest_path(self,common_name,only_outer_cont_image,major_voronoi_data,common_room_number,open_plan_text_cordinate,major_shortest_path_data,contour_check,debug_mode):
        # -----------------------find shortest path
        if debug_mode:
            image_shortest_path_path = self.shortest_path_path + common_name + '/'
            os.mkdir(image_shortest_path_path)

        outer_cont = only_outer_cont_image.copy()
        image = Image.fromarray(outer_cont)
        # width, height = image.size
        pixels = image.load()
        # outer_cont_copy1 = outer_cont.copy()

        shortest_path_data = []
        vornoi_data = major_voronoi_data[common_room_number]
        for r_count, v_row in enumerate(vornoi_data):
            top_threshold_pixel_array = [0 for c in range(self.gray_width)]
            bottom_threshold_pixel_array = [self.gray_height for c in range(self.gray_width)]
            left_threshold_pixel_array = [0 for c in range(self.gray_height)]
            right_threshold_pixel_array = [self.gray_width for c in range(self.gray_height)]
            one_text, second_text = False, False
            starting_cordinate, end_cordinate, starting_row_ID, end_row_ID = 0, 0, 0, 0
            for row_num, text_cord in enumerate(open_plan_text_cordinate):
                if row_num == v_row[0]:
                    starting_cordinate = text_cord
                    starting_row_ID = row_num
                    one_text = True
                if row_num == v_row[1]:
                    end_cordinate = text_cord
                    end_row_ID = row_num
                    second_text = True
                if one_text and second_text:
                    break
            # if r_count==0:
            pixel_data = [[0 for column in range(self.gray_width)] for row in range(self.gray_height)]
            shortest_path_points = []
            color_val_obj = color_check_class(pixels, top_threshold_pixel_array, bottom_threshold_pixel_array,left_threshold_pixel_array, right_threshold_pixel_array)
            temp_array_copy = []
            temp_ring_array = []
            # ------start of single txt co-ordinate
            end_ring = False
            for ring_count in range(5000):
                if ring_count == 0:
                    column, row = starting_cordinate
                    pixel_data[row][column] = 'S'
                    column1, row1 = end_cordinate
                    pixel_data[row1][column1] = 'F'
                    temp_array_copy.append(starting_cordinate)

                for row in temp_array_copy:
                    col_coord, row_coord = row
                    top_pixel = col_coord, row_coord - 1
                    left_pixel = col_coord - 1, row_coord
                    bottom_pixel = col_coord, row_coord + 1
                    right_pixel = col_coord + 1, row_coord

                    top_col, top_row = top_pixel
                    if pixel_data[top_row][top_col] is 'F':
                        shortest_path_points.append([top_col, top_row])
                        shortest_path_points = color_val_obj.get_shortest_path(row, pixel_data, shortest_path_points)
                        end_ring = True
                        break
                    else:
                        if (pixel_data[top_row][top_col] == 0) and (
                                cv2.pointPolygonTest(contour_check, (top_col, top_row), False) == 1):
                            temp_ring_array, pixel_data = color_val_obj.shortest_path_color_match(top_pixel, pixel_data,temp_ring_array, 'T')

                    left_col, left_row = left_pixel
                    if pixel_data[left_row][left_col] is 'F':
                        shortest_path_points.append([left_col, left_row])
                        shortest_path_points = color_val_obj.get_shortest_path(row, pixel_data, shortest_path_points)
                        end_ring = True
                        break
                    else:
                        if (pixel_data[left_row][left_col] == 0) and (
                                cv2.pointPolygonTest(contour_check, (left_col, left_row), False) == 1):
                            temp_ring_array, pixel_data = color_val_obj.shortest_path_color_match(left_pixel,pixel_data,temp_ring_array, 'L')

                    bottom_col, bottom_row = bottom_pixel
                    if pixel_data[bottom_row][bottom_col] is 'F':
                        shortest_path_points.append([bottom_col, bottom_row])
                        shortest_path_points = color_val_obj.get_shortest_path(row, pixel_data, shortest_path_points)
                        end_ring = True
                        break
                    else:
                        if (pixel_data[bottom_row][bottom_col] == 0) and (
                                cv2.pointPolygonTest(contour_check, (bottom_col, bottom_row), False) == 1):
                            temp_ring_array, pixel_data = color_val_obj.shortest_path_color_match(bottom_pixel,pixel_data,temp_ring_array, 'B')

                    right_col, right_row = right_pixel
                    if pixel_data[right_row][right_col] is 'F':
                        shortest_path_points.append([right_col, right_row])
                        shortest_path_points = color_val_obj.get_shortest_path(row, pixel_data, shortest_path_points)
                        end_ring = True
                        break
                    else:
                        if (pixel_data[right_row][right_col] == 0) and (
                                cv2.pointPolygonTest(contour_check, (right_col, right_row), False) == 1):
                            temp_ring_array, pixel_data = color_val_obj.shortest_path_color_match(right_pixel,pixel_data,temp_ring_array, 'R')

                temp_array_copy = temp_ring_array
                temp_ring_array = []
                if end_ring:
                    break

            shortest_path = []
            shortest_path_data.append([])
            shortest_path_data[r_count].append(end_row_ID)
            shortest_path_data[r_count].append(starting_row_ID)

            for p, point in enumerate(shortest_path_points):
                x, y = point
                if p == 0:
                    shortest_path.append([])
                    shortest_path[p].append([x, y])
                elif p == len(shortest_path_points) - 1:
                    shortest_path[p - 1].append([x, y])
                else:
                    shortest_path.append([])
                    shortest_path[p].append([x, y])
                    shortest_path[p - 1].append([x, y])

            # if debug_mode:
            #     outer_cont_copy2 = outer_cont.copy()
            for each_line in shortest_path:
                shortest_path_data[r_count].append(each_line)
        #         if debug_mode:
        #             cv2.line(outer_cont_copy2, (tuple(each_line[0])), (tuple(each_line[1])), (0, 0, 255), 2,
        #                      cv2.cv.CV_AA)
        #             cv2.line(outer_cont_copy1, (tuple(each_line[0])), (tuple(each_line[1])), (0, 0, 255), 2,
        #                      cv2.cv.CV_AA)
        #     if debug_mode:
        #         cv2.imwrite(image_shortest_path_path + '_STEP_02_' + str(r_count) + '.png', outer_cont_copy2)
        # if debug_mode:
        #     cv2.imwrite(image_shortest_path_path + '_STEP_03.png', outer_cont_copy1)
        major_shortest_path_data.append(shortest_path_data)


        return major_shortest_path_data,shortest_path_data

    def calculate_best_line(self,open_plan_path,room,common_name,major_all_contour_lines,major_voronoi_data,common_room_number,current_open_plan_contour_data,text_words,special_cases_list,original_edges_and_extensions,shortest_path_data,open_plan_text_cordinate,text_bounding_boxes,interesting_lines,name,final_output_path,orginal_image_name,room_number,major_max_lines,weight_list,only_outer_cont_image, debug_mode):

        furniture_words_list, non_open_plan_rooms_words_list, wc_words_list, bath_words_list, corridor_words_list = special_cases_list
        # # ----------------------scoring
        # # open_area_list = os.listdir(open_plan_path)
        # # open_area_list.sort()
        #
        # # ------go through each open plan (this was found earlier by checking the number of text labels in each room)
        # # for room_num,room in enumerate(open_area_list):
        # room_image_org = cv2.imread(open_plan_path + room, cv2.IMREAD_GRAYSCALE)
        floor_plan_copy = cv2.cvtColor(only_outer_cont_image,cv2.COLOR_RGB2GRAY)
        img_height, img_width = floor_plan_copy.shape
        # image_name = str(room)
        # name = image_name[0:-4]
        # --- set path to store all outputs
        c_line_scoring_path = self.line_scoring_path + common_name + '/'
        os.mkdir(c_line_scoring_path)

        # ----------create a copy of floor plan to be used later
        # floor_plan_copy = room_image_org.copy()

        # -----------------put all voronoi lines and edge extensions in to the same image
        all_contour_lines = major_all_contour_lines[common_room_number]
        voronoi_data = major_voronoi_data[common_room_number]
        # edge_extensions_between_coordinates=major_edge_extensions_between_coordinates[common_room_number]
        # if debug_mode:
        #     for row in voronoi_data:
        #         for v_line in row[2:]:
        #             cv2.line(room_image_org,(tuple(v_line[0])),(tuple(v_line[1])),(0,0,0),2,cv2.cv.CV_AA)
        #     for row2 in edge_extensions_between_coordinates:
        #         for e_line1 in row2[2:]:
        #             e_line = e_line1[0]
        #             cv2.line(room_image_org,(tuple(e_line[0])),(tuple(e_line[1])),(0,0,255),2,cv2.cv.CV_AA)
        #     cv2.imwrite(c_line_scoring_path+"_STEP_07__ALL LINES.png", room_image_org)

        # print 'edge_extensions_between_coordinates'
        # for row in edge_extensions_between_coordinates:
        #     print row

        # -------sort contours to order them by text_cordinate_ID
        partition_area_lines = sorted(all_contour_lines, key=itemgetter(1))

        only_contour_lines = []
        for cont, partiton_row in enumerate(partition_area_lines):
            # print partiton_row[1]
            contour_text_ID = partiton_row[1]
            only_contour_lines.append([])
            only_contour_lines[cont].append(contour_text_ID)
            for contour_line in partiton_row[0]:
                x1, y1 = contour_line[0]
                x2, y2 = contour_line[1]
                contour_line_is_voronoi_line = False
                for voronoi_row in voronoi_data:
                    exists = False
                    # if voronoi_row[0]==contour_text_ID:
                    for v_line in voronoi_row[2:]:
                        x3, y3 = v_line[0]
                        x4, y4 = v_line[1]
                        if (abs(x1 - x3) < 5 and abs(y1 - y3) < 5 and abs(x2 - x4) < 5 and abs(y2 - y4) < 5) or (
                                abs(x1 - x4) < 5 and abs(y1 - y4) < 5 and abs(x2 - x3) < 5 and abs(y2 - y3) < 5):
                            contour_line_is_voronoi_line = True
                            exists = True
                            break
                    if exists:
                        break
                if contour_line_is_voronoi_line == False:
                    only_contour_lines[cont].append(contour_line)

        if debug_mode:
            for cont, contour in enumerate(only_contour_lines):
                test_image = cv2.imread(open_plan_path + room, cv2.IMREAD_COLOR)
                for l1, each_line in enumerate(contour[1:]):
                    cv2.line(test_image, (tuple(each_line[0])), (tuple(each_line[1])), (0, 0, 255), 2, cv2.cv.CV_AA)
                cv2.imwrite(c_line_scoring_path + str(cont) + "_TEST_IMAGE.png", test_image)

        # -sort contours to order them by text_cordinate_ID
        partition_area_lines = sorted(all_contour_lines, key=itemgetter(1))

        # print '-----voronoi_data111----'
        # for row in voronoi_data:
        #     print row
        # print '----partition_area_lines111-----'
        # for row in partition_area_lines:
        #     print row

        # -get ordered voronoi lines

        voronoi_lines_in_contour_line_order = []
        count = 0

        for cont, contour in enumerate(partition_area_lines):
            # --take contourID as contour_text_ID
            contour_text_ID = contour[-1]
            voronoi_lines_in_contour_line_order.append([])
            # --add contourID to array
            voronoi_lines_in_contour_line_order[count].append(contour_text_ID)
            # -----iterate through contour[0] because all line details are appended as a single element (elelment 0) in to line
            num_of_lines = len(contour[0])
            # --take contour lines 1 by 1
            for l1, line1 in enumerate(contour[0]):
                x1, y1 = line1[0]
                x2, y2 = line1[1]
                m1, c1 = self.iterative_obj.find_mc_of_line(x1, y1, x2, y2)
                # --per contour line, compare with voronoi data
                for cont2, contour2 in enumerate(voronoi_data):
                    # --take 2 textIDs in voronoi row
                    text1 = contour2[0]
                    text2 = contour2[1]
                    # --if current contour(contour_text_ID)==text1: we are comparing same contour and its related voronoi lines
                    if contour_text_ID == text1 or contour_text_ID == text2:
                        # --take voronoi line data
                        for line2 in contour2[2:]:
                            x3, y3 = line2[0]
                            x4, y4 = line2[1]
                            m2, c2 = self.iterative_obj.find_mc_of_line(x3, y3, x4, y4)
                            # --check if current_contour line and current_voronoi is paralell and very close
                            voronoi_line = False
                            if (m1 == 'a' and m2 == 'a') and (abs(x1 - x3) <= 2):
                                voronoi_line = self.iterative_obj.find_line_subsets(line1, line2)
                            elif (m1 != 'a' and m2 != 'a') and abs(m1 - m2) < 0.02 and (abs(c1 - c2) <= 2):
                                voronoi_line = self.iterative_obj.find_line_subsets(line1, line2)
                            # ----in line & line2 if gradient is appprox similar, close to each other: add as voronoi
                            if voronoi_line:
                                # --add voronoi line, textID related to it, line number for later use
                                if contour_text_ID == text1:
                                    text_num = text2
                                elif contour_text_ID == text2:
                                    text_num = text1
                                else:
                                    continue

                                # --check if this line already exists
                                vor_data_exists_flag = False
                                if len(voronoi_lines_in_contour_line_order[count]) > 1:
                                    cur_line_data = voronoi_lines_in_contour_line_order[count][1:]
                                    for existing_line in cur_line_data:
                                        line_cord, t2, l_num = existing_line
                                        if t2 == text_num and l_num == l1:
                                            vor_data_exists_flag = True
                                            break
                                if vor_data_exists_flag == False:
                                    voronoi_lines_in_contour_line_order[count].append([line1, text_num, l1])
                                # elif contour_text_ID == text2:
                                #     voronoi_lines_in_contour_line_order[count].append([line1, text1, l1])
            voronoi_lines_in_contour_line_order[count].append(num_of_lines)
            count = count + 1

        # ------- STEP 02 : Find Boundaries
        # Explanation:
        # Using 'voronoi_lines_in_contour_line_order' find boundaries
        # At the same time order voronoi lines according to boundaries
        # -- how to order voronoi lines:
        # -- in the 'voronoi_lines_in_contour_line_order[]' we had [txt1,[line1,txt2,line_num],[line2,txt3,line_num],[line3,txt4,line_num]]
        # -- so for each row: add line to 'seperated_voronoi_lines' & add textID to 'boundaries'
        # -- order all lines by line_num in voronoi_lines_in_contour_line_order[row1] each boundary
        # -- sort boundaries in each row by textID2s
        # --refer book

        # print '----voronoi_lines_in_contour_line_order---'
        # for row in voronoi_lines_in_contour_line_order:
        #     print row

        seperated_voronoi_lines = []
        boundaries = []
        for r, row in enumerate(voronoi_lines_in_contour_line_order):
            # print 'row',row
            text1 = row[0]
            line_data = row[1:-1]
            temp_array_vor = []
            temp_boundaries = []
            last_line_number, last_textID = -1, -1
            x1, y1, x2, y2, x3, y3, x4, y4 = 0, 0, 0, 0, 0, 0, 0, 0
            # ---boundaries list creation
            boundaries.append([])
            boundaries[r].append(text1)
            # ---seperated_voronoi_lines list creation
            seperated_voronoi_lines.append([])
            seperated_voronoi_lines[r].append(text1)
            for e, element in enumerate(line_data):
                # print 'element',element
                # --take cords of voronoi line
                current_line = element[0]
                x1, y1 = current_line[0]
                x2, y2 = current_line[1]
                # --take textID current voronoi line is connected to
                text_ID = element[1]
                # --line number of voronoi line (according to contour line order)
                line_number = element[2]
                # --if first element: just add everything to 2 arrays
                if e == 0:
                    temp_array_vor.append([current_line])
                    temp_boundaries.append([text_ID])
                    last_line_number = line_number
                    x3, y3, x4, y4 = x1, y1, x2, y2

                else:
                    # ---check if last line in array and is sharing a cordinate with very first line (e==0)
                    if e == len(row[1:-1]) - 1 and (
                            (x1 == x3 and y1 == y3 or x1 == x4 and y1 == y4) or (
                            x2 == x4 and y2 == y4 or x2 == x3 and y2 == y3)):

                        if len(row[1:-1]) == 2:
                            if (line_number - last_line_number) == 1:
                                temp_array_vor[-1].append(current_line)
                                exists_flag = False
                                for t_row1 in temp_boundaries[-1]:
                                    if t_row1 == text_ID:
                                        exists_flag = True
                                        break
                                if exists_flag == False:
                                    temp_boundaries[-1].append(text_ID)
                            else:
                                temp_array_vor[0].insert(0, (current_line))
                                exists_flag = False
                                for t_row1 in temp_boundaries[0]:
                                    if t_row1 == text_ID:
                                        exists_flag = True
                                        break
                                if exists_flag == False:
                                    temp_boundaries[0].insert(0, text_ID)
                        # -----check is this last line is also connected to line before it
                        elif (line_number - last_line_number) <= 1:
                            # -----insert current line to last element in temp_array_vor
                            if (line_number - last_line_number) == 1:
                                temp_array_vor[-1].append(current_line)
                            # ---reverse alst element in temp_array_vor, & add each line to temp_array_vor[0]
                            for each_el in list(reversed(temp_array_vor[-1])):
                                temp_array_vor[0].insert(0, each_el)
                            # -----delete last element
                            del temp_array_vor[-1]

                            # -----------------textID array change----------
                            # -------take all textIDs from last element and put them in first one too
                            # -------do the same for text_cordinates array
                            all_last_boundary_texts = temp_boundaries[-1]
                            texts = []
                            for text_num, last_boundary_text in enumerate(
                                    list(reversed(all_last_boundary_texts))):
                                exists_flag = False
                                for t_row1 in temp_boundaries[0]:
                                    if t_row1 == last_boundary_text:
                                        exists_flag = True
                                if exists_flag == False:
                                    temp_boundaries[0].insert(0, last_boundary_text)
                                    texts.append(last_boundary_text)
                            if len(list(set(all_last_boundary_texts) - set(texts))) == 0:
                                del temp_boundaries[-1]
                            else:
                                temp_boundaries[-1] == list(set(all_last_boundary_texts) - set(texts))
                        # -------if last line is connected to first line but is not connected to line taken in array immediately before
                        else:
                            # ---add current line to temp_array_vor[0]
                            temp_array_vor[0].insert(0, current_line)
                            exists_flag = False
                            for t_row1 in temp_boundaries[0]:
                                if t_row1 == text_ID:
                                    exists_flag = True
                                    break
                            if exists_flag == False:
                                temp_boundaries[0].insert(0, text_ID)
                        # -----common to both if/else
                        last_line_number = line_number
                    # --------if NOT last line but last entered line and current line is connected
                    elif (line_number - last_line_number) <= 1:
                        if (line_number - last_line_number) == 1:
                            # if ~((x1==x3 and y1==y3 or x1==x4 and y1==y4) or (x2==x4 and y2==y4  or x2==x3 and y2==y3)):
                            temp_array_vor[-1].append(current_line)
                        exists_flag = False
                        for t_row1 in temp_boundaries[-1]:
                            if t_row1 == text_ID:
                                exists_flag = True
                                break
                        if exists_flag == False:
                            temp_boundaries[-1].append(text_ID)
                        last_line_number = line_number
                    # ---- if NOT last line but is creating a new/seperate boundary
                    else:
                        temp_array_vor.append([current_line])
                        temp_boundaries.append([text_ID])
                        last_line_number = line_number
            seperated_voronoi_lines[r].append(temp_array_vor)
            boundaries[r].append(temp_boundaries)

        # print '----seperated_voronoi_lines---'
        # for row in seperated_voronoi_lines:
        #     print row
        # print '----boundaries---'
        # for row in boundaries:
        #     print row

        # -------------write contour of each partition in to new image
        output_path = c_line_scoring_path + 'sequential_output/'
        os.mkdir(output_path)
        open_plan_partition_path = c_line_scoring_path + 'open_plan_partitions/'
        os.mkdir(open_plan_partition_path)

        # for r,row in enumerate(partition_area_lines):
        #     contour_image = ~(np.zeros((img_height,img_width,3), np.uint8))
        #     for each_line in row[0]:
        #         cv2.line(contour_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,0),2,cv2.cv.CV_AA)
        #     cv2.imwrite(open_plan_partition_path+str(row[1])+"_contour.png", contour_image)

        # contour_outer_image = cv2.imread(shortest_path_path+common_name+'/_STEP_01.png',cv2.IMREAD_GRAYSCALE)
        # open_plan_c = planClass('test', 'test', contour_outer_image,'test',False)
        # improved_cont_lines = open_plan_c.improve_outer_contour(contour_outer_image,avg_door_width)
        image_contour_lines = current_open_plan_contour_data[1]
        # image_contour_lines = line_obj.get_contour_lines(contour_outer_image)

        if debug_mode:
            contour_image_test = ~(np.zeros((img_height, img_width, 3), np.uint8))
            for each_line in image_contour_lines:
                cv2.line(contour_image_test, (tuple(each_line[0])), (tuple(each_line[1])), (0, 0, 0), 2, cv2.cv.CV_AA)
            cv2.imwrite(output_path + 'TEMPORARY.png', contour_image_test)

        # ----2017/03/26
        # ------special case for bedroom,corridor and wc,etc
        corridor_list, room_list, wc_list, bath_list, furniture_list = [], [], [], [], []
        for row_num, row in enumerate(text_words):
            if row[0] in furniture_words_list:
                furniture_list.append(row_num)
            elif row[0] in non_open_plan_rooms_words_list:
                room_list.append(row_num)
            elif row[0] in wc_words_list:
                wc_list.append(row_num)
            elif row[0] in bath_words_list:
                bath_list.append(row_num)
            elif row[0] in corridor_words_list:
                corridor_list.append(row_num)

        seperated_voronoi_lines_copy = seperated_voronoi_lines
        seperated_voronoi_lines = []
        boundaries_copy = boundaries
        boundaries = []
        line_obj = line()
        room_type = 0
        if len(furniture_list) > 0:
            for row in furniture_list:
                seperated_voronoi_lines, boundaries = line_obj.new_list_creation2(seperated_voronoi_lines_copy,boundaries_copy,seperated_voronoi_lines, boundaries,row)
                room_type = 'furniture'
                break
        elif len(room_list) > 0:
            for row in room_list:
                seperated_voronoi_lines, boundaries = line_obj.new_list_creation2(seperated_voronoi_lines_copy,boundaries_copy,seperated_voronoi_lines, boundaries,row)
                room_type = 'NOPRoom'
                break
        elif len(wc_list) > 0:
            for row in wc_list:
                seperated_voronoi_lines, boundaries = line_obj.new_list_creation2(seperated_voronoi_lines_copy,boundaries_copy,seperated_voronoi_lines, boundaries,row)
                room_type = 'wc'
                break
        elif len(bath_list) > 0:
            for row in bath_list:
                seperated_voronoi_lines, boundaries = line_obj.new_list_creation2(seperated_voronoi_lines_copy,boundaries_copy,seperated_voronoi_lines, boundaries,row)
                room_type = 'bath'
                break
        elif len(corridor_list) > 0:
            for row in corridor_list:
                seperated_voronoi_lines, boundaries = line_obj.new_list_creation2(seperated_voronoi_lines_copy,boundaries_copy,seperated_voronoi_lines, boundaries,row)
                room_type = 'corridor'
                break
        else:
            seperated_voronoi_lines = seperated_voronoi_lines_copy
            boundaries = boundaries_copy
            room_type = 'OP'
        # print 'Room Type is---- ',room_type
        # for row in boundaries:
        #     print row
        # for row in open_plan_text_cordinate:
        #     print row
        # for row in text_words:
        #     print row
        # ------END OF special case for bedroom,corridor and wc,etc
        # room_type = 'OP'
        # ----2017/03/26

        # print '----seperated_voronoi_lines---'
        # for row in seperated_voronoi_lines:
        #     print row

        # ----------------------------------------------------------------------------------------------------------------
        # ------- STEP 03 : Separate Edge Extensions based on Boundaries
        # --seperate edge extensions and its data (original edge, similar to ipl or not
        edge_extensions = []
        edge_extension_data = []
        for eel in original_edges_and_extensions:
            edge_extension_data.append([eel[0], eel[2]])
            edge_extensions.append(eel[1])

        # ---assign eel to boundaries
        seperated_edge_lines = []
        for r_num, boundary_row in enumerate(boundaries):
            # --[1,[[2,3],[4]]]
            text1 = boundary_row[0]
            seperated_edge_lines.append([])
            seperated_edge_lines[r_num].append(text1)
            other_text_lines = []
            for text2_set in boundary_row[1]:
                eel_for_text2_set = []
                single_lines = []
                sp_intersect_eel_combined = []
                for text2 in text2_set:
                    shortest_path = self.line_optimization_function_obj.find_relevant_shortest_path(text1, text2,
                                                                                                    shortest_path_data)
                    sp_intersect_eel = self.line_optimization_function_obj.find_lines_intersect_with_sp(
                        shortest_path, edge_extensions)
                    sp_intersect_eel_combined = self.line_optimization_function_obj.check_if_line_repeats(
                        sp_intersect_eel_combined, sp_intersect_eel)
                # --find if EEL is partitioning text as it should be
                eel_partitioning_text = self.line_optimization_function_obj.find_lines_partitioning_relevant_text(
                    text1, text2_set, sp_intersect_eel_combined, image_contour_lines,
                    open_plan_text_cordinate, img_height, img_width, self.avg_door_width)
                eel_for_text2_set.append(eel_partitioning_text)
                for line_set in eel_for_text2_set:
                    for each_line in line_set:
                        single_lines.append(each_line)
                other_text_lines.append(single_lines)
            seperated_edge_lines[r_num].append(other_text_lines)

        # ---assign ipl to boundaries
        seperated_ipl_lines = []
        for r_num, boundary_row in enumerate(boundaries):
            # --[1,[[2,3],[4]]]
            text1 = boundary_row[0]
            seperated_ipl_lines.append([])
            seperated_ipl_lines[r_num].append(text1)
            other_text_lines = []
            for text2_set in boundary_row[1]:
                ipl_for_text2_set = []
                single_lines = []
                sp_intersect_ipl_combined = []
                for text2 in text2_set:
                    shortest_path = self.line_optimization_function_obj.find_relevant_shortest_path(text1, text2,
                                                                                                    shortest_path_data)
                    sp_intersect_ipl = self.line_optimization_function_obj.find_lines_intersect_with_sp(
                        shortest_path, interesting_lines)
                    sp_intersect_ipl_combined = self.line_optimization_function_obj.check_if_line_repeats(
                        sp_intersect_ipl_combined, sp_intersect_ipl)

                ipl_partitioning_text = self.line_optimization_function_obj.find_lines_partitioning_relevant_text(
                    text1, text2_set, sp_intersect_ipl_combined, image_contour_lines,
                    open_plan_text_cordinate, img_height, img_width, self.avg_door_width)
                ipl_for_text2_set.append(ipl_partitioning_text)
                for line_set in ipl_for_text2_set:
                    for each_line in line_set:
                        single_lines.append(each_line)
                other_text_lines.append(single_lines)
            seperated_ipl_lines[r_num].append(other_text_lines)

        # ----- special case for placard
        bw_image = floor_plan_copy.copy()
        max_line_floor_plan = cv2.cvtColor(bw_image, cv2.COLOR_GRAY2RGB)
        max_line = []

        # ---2017/03/26
        # ---if only 2 txts+seperated_edge_lines is 0+one word is placard : execute special case
        row_num, furniture_exists = -1, False
        if len(furniture_list) > 0:
            furniture_exists = True
            row_num = furniture_list[0]

        # --check if at least one row in seperated_edge_lines has content
        edge_list_empty = False
        for row, edge_row in enumerate(seperated_edge_lines):
            if edge_row[0] == row_num:
                if len(edge_row[1]) == 1:
                    if len(edge_row[1][0]) == 0:
                        edge_list_empty = True
                        break
                else:
                    # --for each boundary check if @least one boundary(connected to PL) has EE. If yes (edge_list_empty = False) break
                    for each_boundary in edge_row[1]:
                        if len(each_boundary) == 0:
                            edge_list_empty = True
                        else:
                            edge_list_empty = False
                            break
                    break
        # # ---find if texts are too close to each other
        # if edge_list_empty and furniture_exists:
        #     average_distance = line_obj.calculate_distance_between_text(boundaries, open_plan_text_cordinate,
        #                                                                 row_num)
        # else:
        #     average_distance = 0
        # # print 'average_distance',average_distance, avg_door_width
        # # if edge_list_empty and furniture_exists and average_distance>avg_door_width:
        # #     max_line_floor_plan = floor_plan_copy.copy()
        # #     max_line,max_line_floor_plan = line_obj.special_case_cupboard(image_contour_lines,text_bounding_boxes,text_words,row_num,output_path, max_line_floor_plan,boundaries)
        # # #     print 'PL case'
        # # else:
        # # #     print 'Else'
        # # # ---2017/03/26

        line_drawable = False
        # --new change on 2017/05/04 to have special case if furniture exista at any place
        if furniture_exists:
            line_drawable, max_line, max_line_floor_plan = self.line_optimization_function_obj.partition_cupboard(
                image_contour_lines, text_bounding_boxes,
                text_words, row_num, output_path,
                max_line_floor_plan, self.avg_door_width)

        if line_drawable == False or furniture_exists == False:
            # print 'Else'
            # --2017/03/26

            # ------- STEP 04 : Separate Improved Voronoi Lines based on Boundaries
            # Explanation:
            # 1. Pre-processing of voronoi lines: Remove line segments<door width and connect other lines
            # 2. Find cordinates of optimized_voronoi_lines
            # 3. Get possible lines from cordinates
            # 4. If these lines don't intersect with contour lines/text cordinates/is in between 2 text cordinates take them as 'improved_voronoi_lines'

            # 1. Pre-processing of voronoi lines: Remove line segments<door width and connect other lines
            optimized_voronoi_lines = []
            min_line_length = self.avg_door_width / 10
            first_line_check, first_line = 0, 0
            short_length_flag = False
            for r, voronoi_row1 in enumerate(seperated_voronoi_lines):
                boundary_line = []
                optimized_voronoi_lines.append([])
                optimized_voronoi_lines[r].append(voronoi_row1[0])
                for boundary in voronoi_row1[1]:
                    line_connection = []
                    for l, each_line in enumerate(boundary):
                        x1, y1 = each_line[0]
                        x2, y2 = each_line[1]
                        line_length = math.hypot(x2 - x1, y2 - y1)
                        if l == 0 and line_length < min_line_length:
                            first_line_check = True
                            first_line = each_line
                            short_length_flag = False
                        elif l == len(boundary) - 1 and line_length < min_line_length:
                            if first_line_check:
                                line_connection.append([first_line[0], each_line[1]])
                                first_line_check = False
                            else:
                                last_element = line_connection[-1]
                                del line_connection[-1]
                                line_connection.append([last_element[1], each_line[1]])
                            short_length_flag = False
                        else:
                            if line_length > min_line_length:
                                if first_line_check:
                                    line_connection.append([first_line[0], each_line[1]])
                                    first_line_check = False
                                elif short_length_flag and len(line_connection) > 0:
                                    last_element = line_connection[-1]
                                    line_connection.append([last_element[1], each_line[1]])
                                else:
                                    line_connection.append(each_line)
                                short_length_flag = False
                            else:
                                short_length_flag = True
                    boundary_line.append(line_connection)
                optimized_voronoi_lines[r].append(boundary_line)

            # ----------get cordinates out of boundary lines
            boundary_cordinates = []
            for r, voronoi_row1 in enumerate(optimized_voronoi_lines):
                boundary_line = []
                boundary_cordinates.append([])
                boundary_cordinates[r].append(voronoi_row1[0])  # -----------******************
                voronoi_row = voronoi_row1[1]  # -----------******************
                for boundary in voronoi_row:
                    line_connection = []
                    for l, each_line in enumerate(boundary):
                        if l == 0:
                            line_connection.append(each_line[0])
                            line_connection.append(each_line[1])
                        else:
                            line_connection.append(each_line[1])
                    boundary_line.append(line_connection)
                boundary_cordinates[r].append(boundary_line)  # -----------******************

            improved_voronoi_cordinates = []
            for b_cord_r, row1 in enumerate(boundary_cordinates):  # -----------******************
                improved_voronoi_cordinates.append([])  # -----------******************
                improved_voronoi_cordinates[b_cord_r].append(row1[0])  # -----------******************
                row = row1[1]  # -----------******************
                boundary_line = []
                for boundary in row:
                    line_connection = []
                    boundary_length = len(boundary)
                    if boundary_length > 2:
                        # --------create cordinate_lookup list with details about number of cordinates to look at in each boundary
                        test_list = []
                        for row in range(2, boundary_length):
                            test_list.append(row)
                        cordinate_lookup = []
                        for L in range(1, len(test_list) + 1):
                            row = []
                            for subset in itertools.combinations(test_list, L):
                                row.append(list(subset))
                            cordinate_lookup.append(row)
                        for r_count, row in enumerate(cordinate_lookup):
                            for element in row:
                                temp_line = []
                                for r, each_line in enumerate(boundary):
                                    if r + 1 not in element:
                                        temp_line.append(each_line)
                                line_connection.append(temp_line)
                    boundary_line.append(line_connection)
                improved_voronoi_cordinates[b_cord_r].append(boundary_line)  # -----------******************

            improved_voronoi_lines_temp = []
            for r, row1 in enumerate(improved_voronoi_cordinates):  # -----------******************
                improved_voronoi_lines_temp.append([])  # -----------******************
                improved_voronoi_lines_temp[r].append(row1[0])  # -----------******************
                row = row1[1]  # -----------******************
                boundary_line = []
                for boundary in row:
                    line_connection = []
                    for cordinate_set in boundary:
                        cordinate_list = []
                        for c, cords in enumerate(cordinate_set):
                            # if len(cordinate_set)>2:
                            if c == 0:
                                cordinate_list.append([])
                                cordinate_list[c].append(cords)
                            elif c == len(cordinate_set) - 1:
                                cordinate_list[c - 1].append(cords)
                            else:
                                cordinate_list.append([])
                                cordinate_list[c].append(cords)
                                cordinate_list[c - 1].append(cords)
                        line_connection.append(cordinate_list)
                    boundary_line.append(line_connection)
                improved_voronoi_lines_temp[r].append(boundary_line)  # -----------******************

            # 4. If these lines don't intersect with contour lines/text cordinates/is in between 2 text cordinates take them as 'improved_voronoi_lines'
            # 4.2 Find lines that don't intersect with contour lines and text cordinates
            contour_non_intersect_improved_voronoi_lines = self.line_optimization_function_obj.find_lines_not_intersecting_with_contour(
                improved_voronoi_lines_temp, boundaries, open_plan_text_cordinate, image_contour_lines,
                text_bounding_boxes)

            # 4.3 Find lines that are in between 2 text cords (not putting all text labels in one side)
            improved_voronoi_lines_temp2 = self.line_optimization_function_obj.find_lines_seperating_text(
                contour_non_intersect_improved_voronoi_lines, boundaries, open_plan_text_cordinate,
                floor_plan_copy, self.avg_door_width, output_path)

            # 4.4 if SMPL has > 1 line per boundary: select line with min(segments) as SMPL
            # 4.4 if gradient(SMPL) is ~= 90/0 create another candidate with 90/0 gradient
            # test_image = floor_plan_copy.copy()
            # colored_test_image = cv2.cvtColor(test_image, cv2.COLOR_GRAY2RGB)
            # iterative_obj = iterative_functions_class()
            for r_num, row in enumerate(improved_voronoi_lines_temp2):
                # --take each boundary one by one
                for b_num, boundary in enumerate(row[1]):
                    # --sort sublists in boundary (SMPL_LineSets) based on number of lines in each SMPL_LineSet
                    boundary.sort(key=len)
                    if len(boundary) > 0:
                        # --select SMPL_LineSet with min(segments) from sorted array
                        new_boundary_lines = [boundary[0]]
                        first_element_length = len(boundary[0])
                        # --if SMPLineSet with  min(segments) is 1, then there's going to be only one such line
                        # --so we check if min(segments) > 1
                        if first_element_length > 1:
                            for line_set_num, improved_line_set in enumerate(boundary):
                                if line_set_num > 0:
                                    # --in sorted list check if 2,3... elements also have the same number of elements as 1
                                    if len(improved_line_set) == first_element_length:
                                        new_boundary_lines.append(improved_line_set)
                                    # --since list is sorted, we can break
                                    else:
                                        break

                        # --finding approx 0/90 line per SMPL
                        for num, smpl_set in enumerate(new_boundary_lines):
                            if len(smpl_set) == 1:
                                current_line = smpl_set[0]
                                x1, y1 = current_line[0]
                                x2, y2 = current_line[1]
                                line_gradient, line_c = self.iterative_obj.find_mc_of_line(x1, y1, x2, y2)
                                if line_gradient == 0 or line_gradient == 'a' or (
                                        line_gradient < 0.4 and line_gradient > -0.4) or (
                                        line_gradient > 3 and line_gradient < -3):
                                    new_line = self.line_optimization_function_obj.find_approx_straight_line(
                                        current_line, line_gradient, image_contour_lines)
                                    new_boundary_lines.append([new_line])
                                    break

                        improved_voronoi_lines_temp2[r_num][1][b_num] = new_boundary_lines

            contour_non_intersect_2 = self.line_optimization_function_obj.find_lines_not_intersecting_with_contour(
                improved_voronoi_lines_temp2, boundaries, open_plan_text_cordinate, image_contour_lines,
                text_bounding_boxes)
            # 4.3 Find lines that are in between 2 text cords (not putting all text labels in one side)
            improved_voronoi_lines = self.line_optimization_function_obj.find_lines_seperating_text(
                contour_non_intersect_2, boundaries, open_plan_text_cordinate, floor_plan_copy,self.avg_door_width, output_path)

            if debug_mode:
                image_test = floor_plan_copy.copy()
                for r1, row in enumerate(improved_voronoi_lines):
                    for b1, boundary in enumerate(row[1]):
                        for i, improved_line_set in enumerate(boundary):
                            # print r1,b1,i,improved_line_set
                            # image_test = floor_plan_copy.copy()
                            for improved_line in improved_line_set:
                                cv2.line(image_test, (tuple(improved_line[0])), (tuple(improved_line[1])), (255, 0, 0),
                                         2, cv2.cv.CV_AA)
                cv2.imwrite(c_line_scoring_path + name + "IMPORVED LINES.png", image_test)

                # ----------for display purposes draw all lines in to an image
                black_all_lines_image = floor_plan_copy.copy()
                all_lines_image = cv2.cvtColor(black_all_lines_image, cv2.COLOR_GRAY2RGB)
                for r, row in enumerate(optimized_voronoi_lines):
                    for b, boundary in enumerate(row[1]):
                        for v_line in boundary:
                            cv2.line(all_lines_image, (tuple(v_line[0])), (tuple(v_line[1])), (240, 30, 230), 3,
                                     cv2.cv.CV_AA)
                for row in voronoi_data:
                    for v_line in row[2:]:
                        cv2.line(all_lines_image, (tuple(v_line[0])), (tuple(v_line[1])), (0, 150, 255), 3,
                                 cv2.cv.CV_AA)
                for row in seperated_edge_lines:
                    for boundary in row[1]:
                        for e_line1 in boundary:
                            e_line = e_line1[0]
                            cv2.line(all_lines_image, (tuple(e_line[0])), (tuple(e_line[1])), (0, 255, 0), 2,
                                     cv2.cv.CV_AA)
                for row in improved_voronoi_lines:
                    for boundary in row[1]:
                        for improved_line_set in boundary:
                            for improved_line in improved_line_set:
                                cv2.line(all_lines_image, (tuple(improved_line[0])), (tuple(improved_line[1])),
                                         (255, 0, 0), 2, cv2.cv.CV_AA)
                for row in seperated_ipl_lines:
                    for boundary in row[1]:
                        for ipl_line1 in boundary:
                            ipl_line = ipl_line1[0]
                            cv2.line(all_lines_image, (tuple(ipl_line[0])), (tuple(ipl_line[1])), (255, 255, 0), 2,
                                     cv2.cv.CV_AA)

                for row_number, cordinate in enumerate(open_plan_text_cordinate):
                    for boundary_row in boundaries:
                        if boundary_row[0] == row_number:
                            cv2.circle(all_lines_image, (tuple(cordinate)), 5, (0, 0, 255), -1)
                cv2.imwrite(c_line_scoring_path + "_STEP_06_COMPLETE LINE SET.png", all_lines_image)

            # print '-------improved_voronoi_lines------'
            # for row in improved_voronoi_lines:
            #     print row

            non_voronoi_use = []
            for row_count, row in enumerate(boundaries):
                # contour_image = ~(np.zeros((img_height,img_width,3), np.uint8))
                # print row
                text1 = row[0]
                # print 'text1',text1
                non_voronoi_use.append([])
                non_voronoi_use[row_count].append(text1)
                boundary_lines = []
                for boundary_num, boundary in enumerate(row[1]):
                    boundary_row = []
                    for only_contour in only_contour_lines:
                        if only_contour[0] == text1:
                            boundary_row.append(only_contour[1:])
                            # print only_contour[1:]
                            # for non_vornoi_line in only_contour[1:]:
                            #     cv2.line(contour_image,(tuple(non_vornoi_line[0])),(tuple(non_vornoi_line[1])),(255,0,0),2,cv2.cv.CV_AA)
                            break
                    all_texts = [text1] + boundary
                    # print 'all_texts',all_texts
                    for vor_row in voronoi_data:
                        if vor_row[0] == text1 or vor_row[1] == text1:
                            boundary_row.append(vor_row[2:])
                    boundary_lines.append(boundary_row)
                    # cv2.imwrite(c_line_scoring_path+str(row_count)+str(boundary_num)+"_CHECK_LINES.png", contour_image)

                non_voronoi_use[row_count].append(boundary_lines)

            # debug_mode = True
            if debug_mode:
                # for row in boundaries:
                #     print row
                # for row in open_plan_text_cordinate:
                #     print row
                # for row in text_words:
                #     print row
                for r, row in enumerate(non_voronoi_use):
                    for b, boundary in enumerate(row[1]):
                        contour_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
                        for non_vornoi_line_set in boundary:
                            for non_vornoi_line in non_vornoi_line_set:
                                cv2.line(contour_image, (tuple(non_vornoi_line[0])), (tuple(non_vornoi_line[1])),
                                         (255, 0, 0), 2, cv2.cv.CV_AA)
                        cv2.imwrite(c_line_scoring_path + str(r) + str(b) + "_Z_NEWWWWWW_LINES.png", contour_image)

            os.mkdir(c_line_scoring_path + 'Scores and Penalties/')
            score_path = c_line_scoring_path + 'Scores and Penalties/'
            os.mkdir(c_line_scoring_path + 'Test Outputs/')
            Test_path = c_line_scoring_path + 'Test Outputs/'

            voronoi_penalties, edge_extension_penalties, improved_voronoi_penalties, ipl_penalties = self.penalties_scores_obj.calculate_penalties(
                room_type, self.avg_door_width,
                img_height, img_width, weight_list,
                shortest_path_data, edge_extension_data,
                open_plan_text_cordinate, boundaries,
                seperated_voronoi_lines, seperated_edge_lines,
                improved_voronoi_lines, seperated_ipl_lines,
                non_voronoi_use, floor_plan_copy,
                score_path, Test_path, debug_mode)

            # print '-----boundaries-----'
            # for row in boundaries:
            #     print row
            # print '-----seperated_voronoi_lines-----'
            # for row in seperated_voronoi_lines:
            #     print row
            # print '-----seperated_edge_lines-----'
            # for row in seperated_edge_lines:
            #     print row
            # print '-----improved_voronoi_lines-----'
            # for row in improved_voronoi_lines:
            #     print row
            # print '-----seperated_ipl_lines-----'
            # for row in seperated_ipl_lines:
            #     print row
            #
            #
            #
            #
            # print '-----voronoi_penalties-----'
            # for row in voronoi_penalties:
            #     print row
            # print '-----edge_extension_penalties-----'
            # for row in edge_extension_penalties:
            #     print row
            # print '-----improved_voronoi_penalties-----'
            # for row in improved_voronoi_penalties:
            #     print row
            # print '-----ipl_penalties-----'
            # for row in ipl_penalties:
            #     print row

            # ------- STEP 07 : Calculate Scores
            voronoi_scores, edge_extension_scores, improved_voronoi_scores, ipl_scores = self.penalties_scores_obj.calculate_scores(
                voronoi_penalties, edge_extension_penalties,
                improved_voronoi_penalties, ipl_penalties,
                seperated_voronoi_lines, seperated_edge_lines,
                improved_voronoi_lines, seperated_ipl_lines,
                open_plan_text_cordinate, boundaries,
                score_path, debug_mode)

            # #---
            # voronoi_penalties = copy.copy(voronoi_scores)
            # # ------- STEP 07 : Calculate scores
            # # Explanation:
            # # 1. Calculate voronoi score = 0 for all
            # # 2. Calculate edge_extension score = voronoi penalty - edge penalty
            # # 3. Calculate improved_voronoi score = voronoi penalty - improved voronoi penalty
            # # 1. Calculate voronoi score = 0 for all
            # voronoi_scores = self.line_optimization_function_obj.calculate_voronoi_score(
            #     voronoi_penalties)
            # # 2. Calculate edge_extension score = voronoi penalty - edge penalty
            # edge_extension_scores = self.line_optimization_function_obj.calculate_eel_score(
            #     voronoi_penalties, edge_extension_scores)
            # # 3.Calculate improved_voronoi score = voronoi penalty - improved voronoi penalty
            # improved_voronoi_scores = self.line_optimization_function_obj.calculate_ivl_score(
            #     voronoi_penalties, improved_voronoi_scores)
            # # 4. Calculate ipl score = voronoi penalty - ipl penalty
            # ipl_scores = self.line_optimization_function_obj.calculate_ipl_score(
            #     voronoi_penalties, ipl_penalties)
            #
            if debug_mode:
                self.line_optimization_function_obj.print_all_scores(
                    boundaries, voronoi_scores, edge_extension_scores,
                    improved_voronoi_scores, seperated_voronoi_lines,
                    seperated_edge_lines, improved_voronoi_lines,
                    seperated_ipl_lines, ipl_scores,
                    open_plan_text_cordinate, score_path)

            # print '----edge_extension_scores--'
            # for row in edge_extension_scores:
            #     print row
            # print '----improved_voronoi_scores--'
            # for row in improved_voronoi_scores:
            #     print row
            # print '----voronoi_penalties--'
            # for row in voronoi_penalties:
            #     print row

            # #------- STEP 08 : Get Best line from all 3 line types
            bw_image = floor_plan_copy.copy()
            max_line_floor_plan = cv2.cvtColor(bw_image, cv2.COLOR_GRAY2RGB)
            line_obj = line()
            max_line, max_line_floor_plan = line_obj.find_maximum_score(
                edge_extension_scores, improved_voronoi_scores,
                ipl_scores, voronoi_penalties,
                seperated_voronoi_lines, improved_voronoi_lines,
                seperated_edge_lines, seperated_ipl_lines,
                max_line, max_line_floor_plan
            )
        if debug_mode:
            cv2.imwrite(c_line_scoring_path + '_STEP_08_MAX_LINE.png', max_line_floor_plan)
        cv2.imwrite(final_output_path + str(orginal_image_name) + '-' + str(room_number) + '.png', max_line_floor_plan)
        major_max_lines.append(max_line)

        return major_max_lines
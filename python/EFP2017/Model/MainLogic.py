__author__ = 'stefano'


from connectedcomponent import eps_circle, eps_ellipse, eps_rect
import shutil,os,cv2,time, sys, subprocess,getopt,traceback,copy,signal
import numpy as np
from PIL import Image
from distutils.dir_util import copy_tree
from franges import frange
from franges import drange
from scipy import ndimage
from operator import itemgetter
from natsort import natsorted
from shutil import copyfile
from xlwt import Workbook


from image import Image_class
from graphCC import GraphCC
from room_object_match import room_object_match_class
import imageOperations as imop
from copy import deepcopy
from connectedcomponent import CC
from pre_process import pre_process_class
from iterative_functions import iterative_functions_class
from room_functions import room_functions_class
from evaluate import evaluation_class
from cupboard_find import cupboard_class
from accessible_fp_generation import accessible_fp_class
from close_contour import contour_closing_functions
from close_cont_detect_elements import close_cont_detect_elements_class
from text_functions import text_funcitons_class
from pre_processing_detection import pre_process_class


class MainFormLogic():
    #-------initialization method
    def __init__(self):
        self.thresholds = [0,0,0,0,0]
        self.count = 0
        self.name = ''
        self.path_floorplan = ''
        self.converted_floorplan_path = ''
        self.path_output = ''
        self.path_output_d = ''
        self.path_results = ''
        self.path_final_lines = ''
        self.path_gravvitas = ''
        self.path_gravvitas_text = ''
        self.path_gravvitas_svg = ''
        self.object_path = ''
        self.object_output_path = ''
        self.current_directory =''
        self.name = ''
        #--make 'debug_mode' True to see intermediate results-------
        debug_mode = False
        #--fra,eng
        self.language = "fra"
        self.iterative_obj = iterative_functions_class()
        signal.signal(signal.SIGALRM, self.handler)


        #----get path
        self.current_directory, self.input_path, self.output_path = self.get_path()

        # #--Add new objects method
        # self.add_new_objects()


        #--create folders
        self.create_output_folders(self.current_directory,debug_mode)


        #--get list of input floorplans
        list_images_floorplan = self.pre_process_images(self.current_directory)

        #--define weight_lists
        criteria_weight_list = [['line_length', 0.6], ['Gradient', 0.75], ['TxtDistance', 1.0],['EELengthRatio', 0.4], ['ConvexityDefect', 0.5],['IplWeight',0.5]]
        weight_list = [row[1] for row in criteria_weight_list]

        #---read special case names
        self.special_cases_list = self.iterative_obj.read_special_cases_files(self.current_directory)

        #--write to excel
        wb = Workbook()
        sheet_name = 'Timing results'
        ws = wb.add_sheet(sheet_name)
        # # --print header values
        # ws.write(0, 0, 'Image Name')
        # ws.write(0, 1, 'PreProcess Time')
        # ws.write(0, 2, 'CC Time')
        # ws.write(0, 3, 'Text Time')
        # ws.write(0, 4, 'Wall_D_win Time')
        # ws.write(0, 5, 'Cnt_Cl Time')
        # ws.write(0, 6, 'CpD Time')
        # ws.write(0, 7, 'Room Time')
        # ws.write(0, 8, 'OP Door Time')
        # ws.write(0, 9, 'Accs Time')
        # ws.write(0, 10, 'Total Time')

        # print 'self.current_directory',self.current_directory
        # print 'self.input_path', self.input_path
        # print 'self.output_path', self.output_path
        ##---take each floorplan name from list of images
        for file_number, file_name in enumerate(list_images_floorplan):
            signal.alarm(1200)
            try:
                # ws.write(file_number+1, 0, file_name)
                # start_image_process_time = time.time()
                print 'Image is : ',file_name
                img = cv2.imread(self.path_floorplan + file_name, cv2.IMREAD_GRAYSCALE)
                orginal_img_height, orginal_img_width = img.shape
                avg_door_width = self.floorplan_component_ID(file_name,img,weight_list,self.path_floorplan,orginal_img_height, orginal_img_width,[wb,ws,file_number+1],debug_mode)
                # print '--',avg_door_width


                # accessible_start_time = time.time()
                # print 'Accessible FP Generation'
                # avg_door_width = 93
                # self.path_gravvitas_text = self.current_directory + '/GraVVITAS/158/TEXT/'
                # self.path_output = self.current_directory + '/OUTPUT/158/'
                accessible_fp_obj = accessible_fp_class(self.current_directory,self.path_output,file_name,str(file_number),avg_door_width,self.path_gravvitas_text,self.path_gravvitas_svg,self.tactile_path,orginal_img_height,orginal_img_width)
                accessible_fp_obj.generate_accessible_floorplans()
                # accessible_end_time = time.time()
                # ws.write(file_number + 1, 9, (accessible_end_time - accessible_start_time))
                #
                # # end_image_process_time = time.time()
                # ws.write(file_number+1, 10, (accessible_end_time - start_image_process_time))
                # wb.save(self.path_final_lines + 'Timing.xls')



            except(KeyboardInterrupt,SystemExit):
                print "User interrupted!"
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

            except Exception,exc:
                print exc
                print "---------------------Timeout Error occured with-------------------------", str(file_name)

            except:
                print "---------------------Error occured with-------------------------", str(file_name)
                traceback.print_exc()

        # wb.save(self.path_final_lines + 'Timing.xls')

        #----for server running
        # self.write_output_to_server_path(self.path_gravvitas_svg, self.output_path+'/GraVVITAS/')
        # self.write_output_to_server_path(self.tactile_path, self.output_path + '/Tactile/')
        # self.write_output_to_server_path(self.path_gravvitas_svg, self.output_path + '/TextDesc/')

        shutil.make_archive(self.output_path+'/GraVVITAS', 'zip', self.path_gravvitas_svg)
        shutil.make_archive(self.output_path + '/Tactile', 'zip', self.tactile_path)
        shutil.make_archive(self.output_path + '/TextDesc', 'zip', self.path_gravvitas_svg)


    def handler(self,signum, frame):
        print "Forever is over!"
        raise Exception("end of time")




    #shows the total execution time for the function
    def contatempo(fn):
        def interna(*args,**kw):
            start=time.time()
            result=fn(*args,**kw)
            tempo=time.time()-start
            print "Function: %s - Execution time: %4.2f" % (fn.__name__,tempo)
            return result
        return interna

    def create_output_folders(self,current_directory,debug_mode):
        #create folders to store output - OUTPUT/OUTPUT_DEBUG/RESULTS_TEST/EDGES folders
        self.path_output=current_directory+'/OUTPUT/'
        if not(os.path.exists(self.path_output)):
                os.mkdir(self.path_output)
        self.path_output_d=current_directory+'/OUTPUT_DEBUG/'
        if not(os.path.exists(self.path_output_d)):
                os.mkdir(self.path_output_d)
        self.path_results = current_directory+'/RESULTS_TEST/'
        if not(os.path.exists(self.path_results)):
                os.mkdir(self.path_results)
        self.path_final_lines = current_directory+'/Final_Lines/'
        if (os.path.exists(self.path_final_lines)):
            shutil.rmtree(self.path_final_lines)
        os.mkdir(self.path_final_lines)
        #--create path for OBJECTS results
        self.object_path = current_directory+'/OBJECTS/'
        if not os.path.exists(self.object_path):
            os.mkdir(self.object_path)
        #--create place to write text for GraVVITTAS
        self.path_gravvitas=current_directory+'/GraVVITAS/'
        if not(os.path.exists(self.path_gravvitas)):
            os.mkdir(self.path_gravvitas)
        #--create place to store final SVG to pass to JS
        self.svg_path=current_directory+'/SVG_results/'
        if not(os.path.exists(self.svg_path)):
            os.mkdir(self.svg_path)
        #--create place to store tactile results
        self.tactile_path=current_directory+'/Tactile_FP/'
        if not(os.path.exists(self.tactile_path)):
            os.mkdir(self.tactile_path)
        # --create place to store evaluation results
        self.evaluation_path = current_directory + '/EVALUATION/'
        if not (os.path.exists(self.evaluation_path)):
            os.mkdir(self.evaluation_path)
        # --create place to store Weight Test results
        self.path_weight_results = current_directory + '/Weight Test/'
        if (os.path.exists(self.path_weight_results)):
            shutil.rmtree(self.path_weight_results)
        os.mkdir(self.path_weight_results)

        #--incremental folder creation--------------------------------------
        #OUTPUT folder incremental values for folder name
        list_dir_output = os.listdir(self.path_output)
        if len(list_dir_output)>0:
            t = [int(x) for x in list_dir_output]
            t.sort()
            name = str(t[len(t)-1]+1)
            self.path_output = self.path_output+name+'/'
            os.mkdir(self.path_output)
        else:
            self.path_output = self.path_output+str(1)+'/'
            name="1"
            os.mkdir(self.path_output)

        #OUTPUT_DEBUG folder incremental values for folder name
        if not(os.path.exists(self.path_output_d+ name +"/")):
            os.mkdir(self.path_output_d+ name +"/")
            self.path_output_d = self.path_output_d+ name +"/"
        else:
            shutil.rmtree(self.path_output_d+ name +"/")
            os.mkdir(self.path_output_d+ name +"/")
            self.path_output_d = self.path_output_d+ name +"/"

        #GRAVVITAS folder incremental values for folder name
        if not(os.path.exists(self.path_gravvitas+ name +"/")):
            os.mkdir(self.path_gravvitas+ name +"/")
            self.path_gravvitas = self.path_gravvitas+ name +"/"
        else:
            shutil.rmtree(self.path_gravvitas+ name +"/")
            os.mkdir(self.path_gravvitas+ name +"/")
            self.path_gravvitas = self.path_gravvitas+ name +"/"
        self.path_gravvitas_svg=self.path_gravvitas+'/SVG/'
        if not(os.path.exists(self.path_gravvitas_svg)):
            os.mkdir(self.path_gravvitas_svg)
        self.path_gravvitas_text=self.path_gravvitas+'/TEXT/'
        if not(os.path.exists(self.path_gravvitas_text)):
            os.mkdir(self.path_gravvitas_text)

        #tactile folder incremental values for folder name
        if not(os.path.exists(self.tactile_path+ name +"/")):
            os.mkdir(self.tactile_path+ name +"/")
            self.tactile_path = self.tactile_path+ name +"/"
        else:
            shutil.rmtree(self.tactile_path+ name +"/")
            os.mkdir(self.tactile_path+ name +"/")
            self.tactile_path = self.tactile_path+ name +"/"

        #OBJECTS folder incremental values for folder name
        list_dir_output = os.listdir(self.object_path)
        if len(list_dir_output)>0:
            t = [int(x) for x in list_dir_output]
            t.sort()
            self.name = str(t[len(t)-1]+1)+'/'
        else:
            self.name = '1/'
        self.object_output_path = self.object_path
        self.object_path = self.object_path+self.name
        if not os.path.exists(self.object_path):
            os.mkdir(self.object_path)

        # EVALUATION folder incremental values for folder name
        list_dir_output = os.listdir(self.evaluation_path)
        if len(list_dir_output) > 0:
            t = [int(x) for x in list_dir_output]
            t.sort()
            self.name = str(t[len(t) - 1] + 1) + '/'
        else:
            self.name = '1/'
        # self.eval_output_path = self.evaluation_path
        self.eval_output_path = self.evaluation_path + self.name
        if not os.path.exists(self.eval_output_path):
            os.mkdir(self.eval_output_path)



        #create subfolders inside main OUTPUT and OUTPUT_DEBUG folders
        self.create_path_extract_text()
        self.create_path_wall()
        if debug_mode:
            self.create_path_find_door()
        self.create_path_find_windows()
        self.create_path_preprocessing()
        self.create_path_merge_wwd()
        self.create_path_stairs()
        self.create_path_merge_all()
        self.create_path_output()

    def pre_process_images(self,current_directory):
        input_path_floorplan = self.input_path

        #--check if input folder exists, carry out rest of the program if it does
        if not(os.path.exists(input_path_floorplan)):
            print 'Directory Not Found.'
            sys.exit()
        else:
            # self.path_floorplan = input_path_floorplan
            # print 'input_path_floorplan',input_path_floorplan
            original_fp_path_unsorted = os.listdir(input_path_floorplan)
            original_fp_path = natsorted(original_fp_path_unsorted)
            # for each_image in original_fp_path:


            self.converted_floorplan_path = self.current_directory+'/input_fps/converted_fps/'
            if (os.path.exists(self.converted_floorplan_path)):
                shutil.rmtree(self.converted_floorplan_path)
            os.makedirs(self.converted_floorplan_path)


            for floor in original_fp_path:
                # print 'str(floor)',str(floor)
                if str(floor)=='.DS_Store':
                    os.remove(input_path_floorplan+str(floor))
                else:
                    if floor[-4:]=='.png':
                        shutil.copy(input_path_floorplan+str(floor),self.converted_floorplan_path+str(floor))
                    elif floor[-4:]=='.svg' or floor[-4:]=='.SVG':
                        im_name = str(floor)
                        only_im_name = im_name[:-4]
                        # os.system('convert -density 20 '+input_path_floorplan+str(floor)+' -set density 20 '+self.converted_floorplan_path+only_im_name+'_20.png')
                        # os.system('convert -density 50 '+input_path_floorplan+str(floor)+' -set density 50 '+self.converted_floorplan_path+only_im_name+'_50.png')
                        # os.system('convert -density 60 '+input_path_floorplan+str(floor)+' -set density 60 '+self.converted_floorplan_path+only_im_name+'_60.png')
                        # os.system('convert -density 70 '+input_path_floorplan+str(floor)+' -set density 70 '+self.converted_floorplan_path+only_im_name+'_70.png')
                        # os.system('convert -density 100 '+input_path_floorplan+str(floor)+' -set density 100 '+self.converted_floorplan_path+only_im_name+'_100.png')
                        # os.system('convert -density 150 '+input_path_floorplan+str(floor)+' -set density 150 '+self.converted_floorplan_path+only_im_name+'_150.png')
                        # os.system('convert -density 200 '+input_path_floorplan+str(floor)+' -set density 200 '+self.converted_floorplan_path+only_im_name+'_200.png')
                        os.system('convert -density 250 '+input_path_floorplan+str(floor)+' -set density 250 '+self.converted_floorplan_path+only_im_name+'.png')
                        # os.system('convert -density 500 ' + input_path_floorplan + str(
                        #     floor) + ' -set density 500 ' + self.converted_floorplan_path + only_im_name + '_500.png')

                    else:
                        im_conv = Image.open(input_path_floorplan+str(floor))
                        im_name = str(floor)
                        only_im_name = im_name[:-4]
                        im_conv.save(self.converted_floorplan_path+only_im_name+'.png')

            self.path_floorplan = self.converted_floorplan_path
            list_images_floorplan_unsorted = os.listdir(self.converted_floorplan_path)
            list_images_floorplan = natsorted(list_images_floorplan_unsorted)

            self.create_path_open_plan(list_images_floorplan)
            return list_images_floorplan

    def floorplan_component_ID(self,file_name,img,weight_list, orginal_img_path,orginal_img_height, orginal_img_width,excel,debug_mode):
        preprocess_start_time = time.time()
        # ---extract only image name without extensions
        only_image_name = file_name[0:-4]
        path = self.path_output_d + only_image_name + '/'
        if not (os.path.exists(path)):
            os.mkdir(path)
        path_output = self.path_output + only_image_name + '/'
        if not (os.path.exists(path_output)):
            os.mkdir(path_output)


        image_color = cv2.imread(orginal_img_path + file_name, cv2.IMREAD_COLOR)

        # ---------Otsu's thresholding to clean image and remove smoothing effects
        ret2, otsu = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        cv2.imwrite(self.path_output_d + 'preprocessing/' + only_image_name + '_otsu.png', otsu)
        preprocess_end_time = time.time()

        image = Image_class(self.current_directory, file_name, self.path_output,
                            self.path_output_d, otsu, image_color)

        testing_flag = False
        if testing_flag:
            cc_image_path = image.test_image_process()
        else:
            # print '1. Finding connected components and classify'
            list_cc_text, list_cc_no_text = image.find_CC_and_classify()

            # print '2. Thin lines removal and clean image'
            cleaned_image = image.remove_thin_lines_and_clean_image(list_cc_no_text,
                                            self.path_final_lines+only_image_name)

            # print '3. Refine text CC'
            list_cc_text = image.improve_textCC_accuracy(cleaned_image, list_cc_text)
            cc_image_path = image.generate_and_improve_text_image(list_cc_text)

        cc_end_time = time.time()

        # print '4. Text Recognition'
        self.text_functions_obj = text_funcitons_class(self.path_output, self.path_output_d,self.current_directory, file_name)
        text_cordinates = self.text_functions_obj.identify_text(otsu,self.language,only_image_name, cc_image_path,self.path_gravvitas_text, orginal_img_width, orginal_img_height, debug_mode)

        text_end_time = time.time()


        # print '5. Finding stairs'
        image.find_stairs(self.path_gravvitas_text, only_image_name)
        str_end_time = time.time()



        # print '6. Walls  Door Window classification'
        #-- find CCs for door window walls
        image.find_connected_componet_no_text(only_image_name, orginal_img_path)
        #-- find walls part 1
        external_wall_list,avg_wall_width = image.classification_walls(debug_mode)
        #-- find doors
        door_list = image.classification_door(debug_mode)
        self.write_list_rect('contour', door_list, orginal_img_path + file_name,
                             only_image_name,'_5_door_details.txt','Door')
        #---find walls part  2 (Improve walls by re-checking interior walls based on door info
        wall_list = image.classification_interior_walls(debug_mode,
                                                                 external_wall_list)
        self.write_list_rect('contour', wall_list, orginal_img_path + file_name,
                             only_image_name,'_7_wall_details.txt','Wall')
        #--find windows
        window_list = image.classification_windows(debug_mode)
        self.write_list_rect('contour', window_list,orginal_img_path + file_name,
                             only_image_name,'_4_window_details.txt','Window')
        #--write detections to image
        image.save_wall_door_window_results(debug_mode)
        #--find average door width
        room_obj = room_functions_class()
        door_path = self.path_output + only_image_name + '/Stefano_output.png'
        avg_door_width = room_obj.find_door_width(self.current_directory, door_path, only_image_name)
        # print avg_door_width,avg_wall_width
        wall_door_wind_end_time = time.time()


        #--temp
        # wall_d_win_img = cv2.imread(self.path_output + only_image_name + '/Stefano_output.png')
        # for text_row in text_cordinates:
        #     text, centre, bb_points = text_row
        #     text_draw_x, text_draw_y = centre
        #     cv2.rectangle(wall_d_win_img, (bb_points[0], bb_points[1]), (bb_points[2], bb_points[3]),(0, 0, 255), 2)
        #     cv2.putText(wall_d_win_img, text.upper(), (text_draw_x - 10, text_draw_y - 30),
        #                 cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 3, cv2.CV_AA)
        # cv2.imwrite(self.path_final_lines+only_image_name+'_detections.png',wall_d_win_img)


        # print "7. Close outer contour"
        testing_mode = False
        debug_mode = False
        # ---- try to close contour
        if testing_mode:
            self.path_gravvitas_text = self.current_directory + '/GraVVITAS/188/TEXT/'
            avg_wall_width,avg_door_width = 43,112

        #--get wall, doow, window data from lists too, without reading text files
        contour_closing_obj = contour_closing_functions(avg_wall_width,avg_door_width)
        connection_data,outer_cont,connection_rects = contour_closing_obj.close_contour(self.path_gravvitas_text,only_image_name,self.path_output,orginal_img_height,orginal_img_width,debug_mode)
        #--detect elements
        cont_object_detect_obj = close_cont_detect_elements_class(avg_wall_width,
                                      self.current_directory,orginal_img_width,
                                      orginal_img_height)


        connection_components = cont_object_detect_obj.identify_elements(connection_data,orginal_img_path + file_name,outer_cont,connection_rects,only_image_name, self.path_output,avg_door_width)
        cont_object_detect_obj.add_detections(connection_components,
                                               orginal_img_height, orginal_img_width,
                                               only_image_name, self.path_output,
                                              self.path_gravvitas_text,debug_mode,
                                              testing_mode)
        cnt_close_end_time = time.time()





        # print '8. Cupboard Recognition'
        if testing_mode:
            self.path_gravvitas_text = self.current_directory + '/GraVVITAS/26/TEXT/'
        cupboard_obj = cupboard_class(self.path_output_d, self.path_final_lines,
                                      only_image_name, avg_door_width,
                                      orginal_img_width, orginal_img_height)
        otsu_path = self.path_output_d + 'preprocessing/' + only_image_name + '_otsu.png'
        cupboard_contours = cupboard_obj.find_cupboard(otsu_path, self.path_output, only_image_name,self.current_directory, text_cordinates,debug_mode)
        if testing_mode:
            self.path_output = self.current_directory + '/OUTPUT/347/'
        cupboard_obj.add_cupboard_to_main_image(cupboard_contours, self.path_output,
                                                only_image_name, self.current_directory,
                                                self.path_gravvitas_text,evaluation=False)

        cpd_end_time = time.time()




        # print '9. Finding rooms/partitioning OPs'
        room_obj = room_functions_class()
        door_path = self.path_output + only_image_name + '/new_walls_windows_door.png'
        avg_door_width = room_obj.find_door_width(self.current_directory, door_path, only_image_name)
        room_obj.all_room_functions(self.current_directory,self.path_output_d,
                                                          self.path_output,
                                                          self.path_gravvitas_text,
                                                          self.path_final_lines,
                                                          avg_door_width,weight_list,
                                                          only_image_name, otsu,
                                                          text_cordinates,
                                                          self.special_cases_list,                                                              debug_mode)
        room_find_end_time = time.time()
        # print "Room Time : ", room_find_end_time-cpd_end_time

        # print '10. Find more doors from OP partitions'
        testing_mode = False

        if testing_mode:
            self.path_gravvitas_text = self.current_directory + '/GraVVITAS/134/TEXT/'
            avg_wall_width,avg_door_width = 43,112
            self.path_output = self.current_directory + '/OUTPUT/134/'

        no_text_image_path = self.path_output+'/'+only_image_name+'/no_text.png'
        room_obj.find_open_plan_doors(self.current_directory,self.path_gravvitas_text,only_image_name,orginal_img_height,orginal_img_width,no_text_image_path,avg_wall_width,avg_door_width,debug_mode)

        op_door_end_time = time.time()

        # if testing_mode:
        #     self.path_gravvitas_text = self.current_directory + '/GraVVITAS/51/TEXT/'
        #     avg_door_width = 112

        # print '11. Draw all detections to image'
        # pre_process_obj = pre_process_class(self.current_directory, self.path_output, file_name, avg_door_width,self.path_gravvitas_text, orginal_img_height, orginal_img_width)
        # self.write_all_detections_to_Image(pre_process_obj,self.path_gravvitas_text,self.path_final_lines,orginal_img_height, orginal_img_width,only_image_name)

        # text_end_time, wall_door_wind_end_time, cnt_close_end_time, cpd_end_time, room_find_end_time, op_door_end_time = 0,0,0,0,0,0

        self.write_timing_to_excel(preprocess_start_time,preprocess_end_time,cc_end_time,text_end_time,wall_door_wind_end_time,cnt_close_end_time,cpd_end_time,room_find_end_time,op_door_end_time,excel)

        # avg_door_width = 0
        return avg_door_width


    def get_path(self):
        # current_directory, input_path, output_path = self.get_path_from_local_machine()
        current_directory, input_path, output_path = self.get_path_from_js()
        # print 'current_directory',current_directory
        # print 'input_path',input_path
        # print 'output_path',output_path
        return current_directory, input_path, output_path

    def get_path_from_local_machine(self):
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code
        current_directory = os.path.abspath(os.path.join(os.getcwd(), '..'))
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/input_fps/anu/
        input_path = current_directory+ '/input_fps/anu/'
        # input_path = current_directory + '/input_fps/current_working_fps/'
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/SVG_results/final_floorplan.svg
        output_path = current_directory+'/SVG_results/final_floorplan.png'
        return current_directory, input_path, output_path

    def get_path_from_js(self):
        js_message = sys.stdin.readlines()
        input_path, current_directory,output_path = '','',''
        for row_num,each in enumerate(js_message):
            if row_num==0:
                current_directory = each.rstrip()
            elif row_num==1:
                input_path = each.rstrip()
            else:
                output_path = each.rstrip()
        return current_directory, input_path, output_path

    def write_list_rect(self,type, component_list,orginal_img_path,only_image_name,
                        text_file_name,text):
        # --write to text file for gravvitas
        file = open(self.path_gravvitas_text + only_image_name + text_file_name, 'a')
        #---write to image
        image_to_write = cv2.imread(orginal_img_path)
        for row in component_list:
            if type == 'rect':
                x1, y1 = row[0]
                x2, y2 = row[1]
                cv2.rectangle(image_to_write, (int(x1) - 1, int(y1) - 1), (int(x2) + 1, int(y2) + 1),
                              (255, 255, 255), -1)
                file.write(text+' : ' + str(x1) + ',' + str(y1) + ',' + str(x2) + ',' + str(y2) + ' : [1] \n')
            elif type == 'contour':
                contour = ''
                for c, cord in enumerate(row):
                    if c == 0:
                        contour = contour + '[' + str(cord[0]) + ',' + str(cord[1]) + ']'
                    else:
                        contour = contour + ',[' + str(cord[0]) + ',' + str(cord[1]) + ']'
                file.write(text + ' : ' + contour + ' : [1] \n')
        file.close()
        cv2.imwrite(self.path_output_d+'results/'+only_image_name+'_'+text+'.png', image_to_write)

    def write_all_detections_to_Image(self,pre_process_obj,path_gravvitas,path_final_lines,img_height, img_width,only_image_name):

        stairs_point_list = pre_process_obj.read_stairs_detail_file(path_gravvitas, only_image_name)
        windows_point_list = pre_process_obj.read_windows_detail_file(path_gravvitas, only_image_name)
        doors_rect_list = pre_process_obj.read_doors_detail_file(path_gravvitas, only_image_name)
        wall_point_list = pre_process_obj.read_walls_detail_file(path_gravvitas, only_image_name)
        text_data_list = pre_process_obj.read_text_detail_file(path_gravvitas, only_image_name)
        unknown_point_list = pre_process_obj.read_unknown_detail_file(path_gravvitas, only_image_name)

        elements_image = np.zeros((img_height, img_width, 3), np.uint8)
        for window_row in windows_point_list:
            window_contour = self.iterative_obj.convert_points_to_contour(window_row)
            cv2.drawContours(elements_image, [window_contour], -1, (0,255,255), -1)

        for door_row in doors_rect_list:
            door_contour = self.iterative_obj.convert_points_to_contour(door_row)
            cv2.drawContours(elements_image, [door_contour], -1, (0,255,0), -1)

        for wall_row in wall_point_list:
            wall_contour = self.iterative_obj.convert_points_to_contour(wall_row)
            cv2.drawContours(elements_image, [wall_contour], -1, (0,0,255), -1)

        for stairs_row in stairs_point_list:
            stairs_contour = self.iterative_obj.convert_points_to_contour(stairs_row)
            cv2.drawContours(elements_image, [stairs_contour], -1, (255,0,255), -1)

        for unknown_ele_row in unknown_point_list:
            unknown_ele_contour = self.iterative_obj.convert_points_to_contour(unknown_ele_row)
            cv2.drawContours(elements_image, [unknown_ele_contour], -1, (255,255,0), -1)

        for text_row in text_data_list:
            text, centre, bb_points_org = text_row
            text_draw_x, text_draw_y = centre[0]
            bb_points = bb_points_org[0]
            cv2.rectangle(elements_image, (bb_points[0], bb_points[1]), (bb_points[2], bb_points[3]), (0, 0, 255), 2)
            cv2.putText(elements_image, text.upper(), (text_draw_x - 10, text_draw_y - 30),cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 3, cv2.CV_AA)

        cv2.imwrite(path_final_lines+only_image_name+'_All_detections.png',elements_image)


    def write_timing_to_excel(self,preprocess_start_time,preprocess_end_time,cc_end_time,text_end_time,wall_door_wind_end_time,cnt_close_end_time,cpd_end_time,room_find_end_time,op_door_end_time,excel):
        wb,ws,row_number = excel
        ws.write(row_number, 1, (preprocess_end_time - preprocess_start_time))
        old_time = preprocess_end_time
        col_num = 2
        for function_time in [cc_end_time,text_end_time,wall_door_wind_end_time,cnt_close_end_time,cpd_end_time,room_find_end_time,op_door_end_time]:
            ws.write(row_number, col_num, (function_time - old_time))
            wb.save(self.path_final_lines + 'Timing.xls')
            old_time = function_time
            col_num = col_num + 1

    def write_output_to_server_path(self,system_output_path,server_output_path):
        if (os.path.exists(system_output_path)):
            shutil.rmtree(system_output_path)
        os.makedirs(system_output_path)

        # files_list = os.listdir(system_output_path)
        # for file in files_list:
        #     copyfile(system_output_path + str(file), server_output_path)


    def create_path_find_oblique_walls(self):
        path = self.path_output_d+'oblique_walls/'
        if not(os.path.exists(path)):
            os.mkdir(path)

    def create_path_stairs(self):
        path = self.path_output_d+'stairs/'
        if not(os.path.exists(path)):
            os.mkdir(path)

    def create_path_merge_all(self):
        path = self.path_output_d+'merge_all/'
        if not(os.path.exists(path)):
            os.mkdir(path)

    def create_path_merge_wwd(self):
        path_merge_wall=self.path_output_d+'merge_walls_doors_windows/'
        if not(os.path.exists(path_merge_wall)):
            os.mkdir(path_merge_wall)
       # os.mkdir(path_merge_wall+'step1/')

    def create_path_find_windows(self):
        path_window=self.path_output_d+'windows/'
        if not(os.path.exists(path_window)):
            os.mkdir(path_window)
       # os.mkdir(path_window+'step1/')

    def create_path_output(self):
        path_out=self.path_output_d+'output/'
        if not(os.path.exists(path_out)):
            os.mkdir(path_out)
        path_results = self.path_output_d+'results/'
        if not(os.path.exists(path_results)):
            os.mkdir(path_results)

    def create_path_wall(self):
        path_external_wall=self.path_output_d+'wall/'
        if not(os.path.exists(path_external_wall)):
            os.mkdir(path_external_wall)
            os.mkdir(self.path_output_d+'oblique_walls/')

    def create_path_find_door(self):
        path_external_wall=self.path_output_d+'door/'
        if not(os.path.exists(path_external_wall)):
            os.mkdir(path_external_wall)
            os.mkdir(path_external_wall+'ante/')


    def create_path_extract_text(self):
        extract_text_path = self.path_output_d+'Extract_text/'
        if not(os.path.exists(extract_text_path)):
            os.mkdir(extract_text_path)
            os.mkdir(extract_text_path+'area_dim/')
            os.mkdir(extract_text_path+'area_dim_2/')
            os.mkdir(extract_text_path+'asp_ratio/')
            os.mkdir(extract_text_path+'density/')
            os.mkdir(extract_text_path+'final_images/')
            os.mkdir(extract_text_path+'text_identification/')

         #in the path "path_clean_image", I save the output of the function clean_image (image.py)
        path_clean_image=extract_text_path+'clean_image/'
        if os.path.exists(path_clean_image):
            shutil.rmtree(path_clean_image)
        else:
            os.mkdir(path_clean_image)
            os.mkdir(path_clean_image+'Canny/')
            os.mkdir(path_clean_image+'Hough/')
            os.mkdir(path_clean_image+'Clean/')
            os.mkdir(path_clean_image+'Open/')# in this path there are the final image

    def create_path_preprocessing(self):
        path_preprocessing=self.path_output_d+'preprocessing/'
        if not(os.path.exists(path_preprocessing)):
            os.mkdir(path_preprocessing)


    def create_path_open_plan(self,list_images_floorplan):
        path_open_plan=self.path_output_d+'Rooms/'
        if not(os.path.exists(path_open_plan)):
            os.mkdir(path_open_plan)
        for each_image in list_images_floorplan:
            image_name = each_image[0:-4]
            image_folder_path = path_open_plan+image_name+'/'
            if not(os.path.exists(image_folder_path)):
                os.mkdir(image_folder_path)
                os.mkdir(image_folder_path+'All_Rooms/')
                os.mkdir(image_folder_path+'OpenPlans/')







if __name__ == '__main__':
    start = time.time()
    hwl1 = MainFormLogic()
    end = time.time()
    print end - start

__author__ = 'Pierpaolo'

import matplotlib.path as mplPath
import numpy as np

class EdgeCC(object):
    def __init__(self, cc1, cc2, attributes):
        if cc1.id < cc2.id:
            self.node1 = cc1
            self.node2 = cc2
        else:
            self.node1 = cc2
            self.node2 = cc1
        self._minCorner = min(attributes[0], attributes[1])
        self._maxCorner = max(attributes[0], attributes[1])
        self._areaRatio = attributes[2]
        #inside = 1 if cc1 is inside cc2, = 2 if cc2 inside cc1, 0 otherwise
        self.inside = self.check_inside()
        self.type = 'touch'

    def check_inside(self):
        cc1 = self.node1
        cc2 = self.node2
        cnt = cc2.contourSimple[0]
        go = True
        for xA in cnt:
            if go:
                cx = xA[0][0]+cc2.x_min
                cy = xA[0][1]+cc2.y_min
                inside = cc1.check_point_inside(cx, cy)
                if inside == False:
                    go = False
        if go:
            # print 'cc '+str(cc2.id)+' dentro la cc '+str(cc1.id)
            return 2
        go = True
        for xA in cnt:
            if go:
                cx = xA[0][0]+cc2.x_min
                cy = xA[0][1]+cc2.y_min
                inside = cc1.check_point_inside_rectangle(cx, cy)
                if inside == False:
                    go = False
        if go:
            # print 'cc '+str(cc2.id)+' dentro al rettangolo della cc '+str(cc1.id)
            return 4
        cnt = cc1.contourSimple[0]
        go = True
        for xA in cnt:
            if go:
                cx = xA[0][0]+cc1.x_min
                cy = xA[0][1]+cc1.y_min
                inside = cc2.check_point_inside(cx, cy)
                if inside == False:
                    go = False
        if go:
            # print 'cc '+str(cc1.id)+' dentro la cc '+str(cc2.id)
            return 1
        go = True
        for xA in cnt:
            if go:
                cx = xA[0][0]+cc1.x_min
                cy = xA[0][1]+cc1.y_min
                inside = cc2.check_point_inside_rectangle(cx, cy)
                if inside == False:
                    go = False
        if go:
            # print 'cc '+str(cc1.id)+' dentro al rettangolo della cc '+str(cc2.id)
            return 3

        else:
            return 0

    #returns the id of the cc inside the other, -1 otherwise
    def get_inside(self):
        if self.inside == 1:
            return self.node1.id
        if self.inside == 2:
            return self.node2.id
        return -1


        # if len(cc1.polygon) == 0:
        #     poly = []
        #     for xA in cc1.contourSimple[0]:
        #         poly.append((xA[0][0]+cc1.x_min, xA[0][1]+cc1.y_min))
        #     cc1.polygon = mplPath.Path(np.array(poly))

        # if len(cc2.polygon) == 0:
            # poly = []
            # for xA in cc1.contourSimple[0]:
            #     poly.append((xA[0][0]+cc2.x_min, xA[0][1]+cc2.y_min))
            # cc2.polygon = mplPath.Path(np.array(poly))

        # print 'contour = '+str(self.contourSimple[0])
        # print 'poly = '+str(poly)
        # print 'polygon = '+str(bbPath)
        # print 'centroide = '+str(self.cx[0])+', '+str(self.cy[0])
        inside = self.polygon.contains_point((cx, cy))
        # print 'centroide inside? '+str(inside)
        return inside

    def get_attributes(self):
        return self._minCorner, self._maxCorner, self._areaRatio, self.inside

    def get_nodes_id(self):
        return self.node1.id, self.node2.id

    def get_nodes(self):
        return self.node1, self.node2

    def set_area_ratio(self, area):
        self._areaRatio = area

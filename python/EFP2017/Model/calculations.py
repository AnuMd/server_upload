__author__ = 'anu'

import math
import cv2
import numpy as np

class calculations_class:

    def find_angle_of_line(self,x1,y1,x2,y2):
        delta_x = x2-x1
        delta_y = y1-y2

        tan = math.atan2(delta_y,delta_x)

        angle = -1
        if tan>0:
            angle = tan * 360 / (2*math.pi)
        else:
            angle = ((2*math.pi)+tan) * 360 / (2*math.pi)

        final_angle = angle % 360

        return final_angle


    def find_perpendicular_point_to_line(self,x1,y1,x2,y2,N):
        #--find centre point of the line
        # center_x, center_y = line_x1+((line_x2-line_x1)/2),line_y2+((line_y1-line_y2)/2)
        # print 'center_x, center_y',center_x, center_y
        # x1,y1 = line_x1,line_y1
        # x2,y2 = line_x2,line_y2
        # x3,y3 = center_x, center_y

        #--find perpendicular points on two sides of the line
        dx = x1-x2
        dy = y1-y2
        dist = math.sqrt(dx*dx + dy*dy)
        dx /= dist
        dy /= dist
        x3 = int(x1 + (N/2)*dy)
        y3 = int(y1 - (N/2)*dx)
        x4 = int(x1 - (N/2)*dy)
        y4 = int(y1 + (N/2)*dx)

        return x3,y3,x4,y4
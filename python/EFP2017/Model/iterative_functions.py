__author__ = 'anu'

import cv2,os,math, tesseract, re, unicodedata,subprocess,shutil
from operator import itemgetter
from bs4 import BeautifulSoup
from scipy import misc,ndimage as ndi
from skimage import measure,filter
import numpy as np
import matplotlib.pyplot as plt
# import skimage; print(skimage.__version__)



from PIL import Image

from open_plan import planClass
from open_plan_line import line

class iterative_functions_class:
    def __init__(self):
        self.line_obj = line()

    def make_directory(self, path, delete_and_write):
        # --if exists delete and remake (folder resets every time)
        if delete_and_write:
            if os.path.exists(path):
                shutil.rmtree(path)
            os.mkdir(path)
        # --else create it only if doesn't exist==don't remake
        else:
            if not (os.path.exists(path)):
                os.mkdir(path)

    def find_open_plans(self,current_directory,input_image,new_output_path,open_plan_path,orginal_open_plan_text_cordinate,original_text_words,text_bounding_boxes_original,avg_door_width):
        # height,width = input_image.shape
        ret,thresh = cv2.threshold(input_image,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        cv2.imwrite(new_output_path+'-Orginal.png',input_image)

        #--calculate number of texts in each contour
        text_data_unsorted =[]
        # print 'len(contours)', len(contours)
        for cnt_number,contour in enumerate(contours):
            text_count = 0
            temp_text_list = []
            for tr, text_cordinate in enumerate(orginal_open_plan_text_cordinate):
                if cv2.pointPolygonTest(contour,(text_cordinate[0],text_cordinate[1]),False)==1:
                    text_count += 1
                    temp_text_list.append(tr)
            text_data_unsorted.append([cnt_number,temp_text_list,len(temp_text_list)])
        text_data = sorted(text_data_unsorted,key=itemgetter(2),reverse=True)
        openPlan = planClass(current_directory,open_plan_path, 'no_execution', input_image,'no_execution',False)
        # print '---------text_data------------'
        # for row in text_data:
        #     print row

        # print sum([row[2] for row in text_data[1:]])

        if text_data[0][2] == sum([row[2] for row in text_data[1:]]):
            del text_data[0]

        # print '---------text_data DELETED ------------'
        # for row in text_data:
        #     print row

        all_detected_contour_lines = []
        for text_data_row in text_data:
            contID = text_data_row[0]
            contour = contours[contID]
            if len(text_data_row[1])>1:
                # if contID==0:
                    # outer_cont2 = ~(np.zeros((1500, 2000, 3), np.uint8))
                    # cv2.drawContours(outer_cont2,[contour],-1,(0,0,0),2)
                    # cv2.imwrite(open_plan_path+str(contID)+'TEST.png',outer_cont2)
                #--1. insert contour details to list
                contour_image,all_detected_contour_lines = openPlan.improve_contours(input_image,avg_door_width,contID,all_detected_contour_lines,contour)
                cv2.imwrite(open_plan_path+str(contID)+'.png',contour_image)

                #--2. insert contour text details
                for textID in text_data_row[1]:
                    cont_text_word = original_text_words[textID]
                    cont_text_cordinate = orginal_open_plan_text_cordinate[textID]
                    cont_bb_box = text_bounding_boxes_original[textID]
                    tx0,ty0,tx1,ty1 = cont_bb_box
                    box_lines = [
                        [[tx0,ty0],[tx1,ty0]],
                        [[tx1,ty0],[tx1,ty1]],
                        [[tx1,ty1],[tx0,ty1]],
                        [[tx0,ty1],[tx0,ty0]]
                    ]
                    for c_row,contour_row in enumerate(all_detected_contour_lines):
                        if contour_row[0]==contID:
                            all_detected_contour_lines[c_row].append([cont_text_word,cont_text_cordinate,cont_bb_box,box_lines])
                            break
        return all_detected_contour_lines


    def crop_image(self,original_image_path,required_height, required_width):
        orginal_image = Image.open(original_image_path)
        half_the_width = orginal_image.size[0] / 2
        half_the_height = orginal_image.size[1] / 2
        cropped_image= orginal_image.crop(
            (
                half_the_width - (required_width/2),
                half_the_height - (required_height/2),
                half_the_width + (required_width/2),
                half_the_height + (required_height/2)
            )
        )
        return cropped_image

    def anu_object_detection(self,path_gravvitas,input_image_path,output_path,image_name,dictionary_path):
        object_details_file = open(path_gravvitas+image_name+'_2_object_details.txt', 'w')
        # print input_path+'no_text.png'
        # print 'output_path',output_path
        # img_no_text = cv2.imread(input_path+'no_text.png', cv2.IMREAD_GRAYSCALE)
        # img_tmp = cv2.imread(input_path+'Stefano_output.png', cv2.IMREAD_COLOR)
        # img_tmp = cv2.inRange(img_tmp, 1, 255)
        # img_no_text2 = img_no_text-img_tmp
        # blw7 = cv2.cvtColor(img_no_text2, cv2.COLOR_GRAY2RGB)
        # cv2.imwrite(output_path+image_name+'before_objects.png', blw7)
        if not os.path.exists(output_path):
            os.mkdir(output_path)

        img_rgb = cv2.imread(input_image_path)
        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)

        list_object_folders = os.listdir(dictionary_path)
        list_object_folders.sort()
        for each_object_template_folder in list_object_folders:
            list_templates = os.listdir(dictionary_path+each_object_template_folder)
            list_templates.sort()
            for each_object_template in list_templates:
                file_name = str(each_object_template)
                # print dictionary_path+each_object_template_folder+'/'+file_name
                if file_name[-4:]=='.png':
                    template = cv2.imread(dictionary_path+each_object_template_folder+'/'+file_name,0)
                    w, h = template.shape[::-1]
                    res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
                    threshold = 0.7
                    loc = np.where( res >= threshold)
                    matched = False
                    x1,y1,x2,y2 = 0,0,0,0
                    for pt in zip(*loc[::-1]):
                        cv2.putText(img_rgb, str(file_name[:-4]), (pt[0],pt[1]-20), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 2,cv2.cv.CV_AA)
                        x1,y1,x2,y2 = pt[0],pt[1],pt[0] + w, pt[1] + h
                        matched = True
                    if matched:
                        obj_name =  file_name[0:-4]
                        print x1,y1,x2,y2
                        object_details_file.write(obj_name+' : '+str(x1)+','+ str(y1)+','+ str(x2)+','+str(y2)+'\n')


        cv2.imwrite(output_path+image_name+'_Template_Matched.png',img_rgb)
        object_details_file.close()

    #-- rectangle methods
    def order_polygon_points(self,points,centre_point):
        cx, cy = centre_point

        angle_list = []
        for r_num,rand_point in enumerate(points):
            xi,yi = rand_point
            angle = math.degrees((math.atan2((cy-yi),(xi-cx))))
            angle_list.append(angle)

        sorted_indices =  np.argsort(angle_list)

        sorted_points = []
        for index in sorted_indices:
            sorted_points.append(points[index])

        return sorted_points

    def order_rectangle_points(self,x1,y1,x2,y2,x3,y3,x4,y4):
        xs = [x1, x2, x3, x4]
        ys = [y1, y2, y3, y4]

        min_x = min(xs)
        min_indices = [index for index, element in enumerate(xs)
                       if min_x == element]

        if len(min_indices)==1:
            index = min_indices[0]
            rect_p1 = [xs[index],ys[index]]
        else:
            y_list, x_list = [], []
            for i in min_indices:
                y_list.append(ys[i])
                x_list.append(xs[i])
            min_y = min(y_list)
            min_y_indices = [index for index, element in enumerate(y_list)
                             if min_y == element]
            index = min_y_indices[0]
            rect_p1 = [x_list[index],y_list[index]]

        max_x = max(xs)
        max_indices = [index for index, element in enumerate(xs)
                       if max_x == element]
        if len(max_indices)==1:
            index = max_indices[0]
            rect_p3 = [xs[index], ys[index]]
        else:
            y_list, x_list = [], []
            for i in max_indices:
                y_list.append(ys[i])
                x_list.append(xs[i])
            max_y = max(y_list)
            max_y_indices = [index for index, element in enumerate(y_list)
                             if max_y == element]
            index = max_y_indices[0]
            rect_p3 = [x_list[index], y_list[index]]


        unsorted_points = [[x1,y1],[x2,y2],[x3,y3],[x4,y4]]
        left_over_elements = []
        for point in unsorted_points:
            p1,p2 = point
            if rect_p1[0]==p1 and rect_p1[1]==p2:
                continue
            elif rect_p3[0]==p1 and rect_p3[1]==p2:
                continue
            else:
                left_over_elements.append(point)

        rect_p2 = left_over_elements[0]
        rect_p4 = left_over_elements[1]

        rect_points = [rect_p1,rect_p2,rect_p3,rect_p4]

        return rect_points

    def find_lines_rectangle(self,x1,y1,x2,y2):
        L1 = [[x1,y1],[x2,y1]]
        L2 = [[x2,y1],[x2,y2]]
        L3 = [[x2,y2],[x1,y2]]
        L4 = [[x1,y2],[x1,y1]]
        return L1,L2,L3,L4

    def find_points_rectangle(self,x1,y1,x2,y2):
        p1 = [x1,y1]
        p2 = [x2,y1]
        p3 = [x2,y2]
        p4 = [x1,y2]
        return p1, p2, p3, p4

    #--line methods
    def find_similarity_two_lines_ordered(self, line1, line2):
        x1,y1 = line1[0]
        x2,y2 = line1[1]
        x3,y3 = line2[0]
        x4,y4 = line2[1]

        if (x1==x3 and y1==y3) or (x2==x4 and y2==y4):
            return True
        else:
            return False

    def find_centre_of_line(self,line):
        p1,p2 = line
        x1,y1 = p1
        x2,y2 = p2
        center_x, center_y= (x2+x1)/2,(y1+y2)/2
        # center_x, center_y= x1+((x2-x1)/2),y1+((y1-y2)/2)
        return center_x, center_y

    def get_points_on_line(self, line, no_of_segments):
        point_list = []
        x1,y1 = line[0]
        x2,y2 = line[1]
        for i in range(1,no_of_segments):
            current_iter = no_of_segments-i
            x = ((current_iter * x1) + (i * x2)) / no_of_segments
            y = ((current_iter * y1) + (i * y2)) / no_of_segments
            point_list.append([x,y])
        return point_list

    #--SAME AS open_plan_line.py function
    def find_mc_of_line(self,x1,y1,x2,y2):
        #get m,c for horizontal line
        if y1==y2:
            m = 0
            c = y1
        #get m,c for vertical line
        elif x1==x2:
            m = 'a'
            c = 'a'
        #get m,c for other lines
        else:
            m =(y1-y2)/float(x1-x2)
            c = y1-(m*x1)
        #--returns gradient as tan value of angle
        return m,c

    def find_angle_of_line(self,current_line):
        x1, y1 = current_line[0]
        x2, y2 = current_line[1]
        delta_x = x2-x1
        delta_y = y1-y2

        tan = math.atan2(delta_y,delta_x)

        angle = -1
        if tan>0:
            angle = tan * 360 / (2*math.pi)
        else:
            angle = ((2*math.pi)+tan) * 360 / (2*math.pi)

        final_angle = angle % 360

        return final_angle

    def find_line_gradient(self,x1,y1,x2,y2):
        a = x1-x2
        b = y1-y2
        np.seterr(divide='ignore', invalid='ignore')
        gradient_x = a/math.sqrt(math.pow(a,2)+math.pow(b,2))
        gradient_y = b/math.sqrt(math.pow(a,2)+math.pow(b,2))
        #--returns gradient as x_differnce and y_difference
        return gradient_x,gradient_y

    def find_line_subsets(self,line1,line2):
        line_is_subset = False
        x1, y1 = line1[0]
        x2, y2 = line1[1]
        x3, y3 = line2[0]
        x4, y4 = line2[1]

        distance_from_text, intersect_x, intersect_y = self.calculate_distance_from_point_to_line([x1, y1],line2)
        point1_is_inside = self.check_point_approx_inside_line([intersect_x, intersect_y],line2,2)

        if point1_is_inside:
            distance_from_text, intersect_x, intersect_y = self.calculate_distance_from_point_to_line([x2, y2], line2)
            point2_is_inside = self.check_point_approx_inside_line([intersect_x, intersect_y], line2,2)

            if point2_is_inside:
                line_is_subset = True

        if line_is_subset==False:
            distance_from_text, intersect_x, intersect_y = self.calculate_distance_from_point_to_line([x3, y3], line1)
            point3_is_inside = self.check_point_approx_inside_line([intersect_x, intersect_y], line1,2)

            if point3_is_inside:
                distance_from_text, intersect_x, intersect_y = self.calculate_distance_from_point_to_line([x4, y4],
                                                                                                          line1)
                point4_is_inside = self.check_point_approx_inside_line([intersect_x, intersect_y], line1,2)

                if point4_is_inside:
                    line_is_subset = True

        return line_is_subset

    def check_point_approx_inside_line(self,point,line1,thresh):
        point_is_inside = self.check_point_inside_line(point,line1)
        if point_is_inside == False:
            point_is_inside = self.check_points_approx_equal(point,line1[0],thresh)
            if point_is_inside == False:
                point_is_inside = self.check_points_approx_equal(point, line1[1], thresh)
        return point_is_inside

    def check_point_inside_line(self,point,line1):
        point_is_inside = False
        x1, y1 = line1[0]
        x2, y2 = line1[1]
        min_x = min(x1,x2)
        max_x = max(x1,x2)
        min_y = min(y1, y2)
        max_y = max(y1, y2)

        point_x, point_y = point

        if (point_x>= min_x and point_x<=max_x) and (point_y>=min_y and point_y<=max_y):
            point_is_inside= True

        return point_is_inside

    def check_points_approx_equal(self,point1,point2,delta):
        approx_equal = False
        x1,y1 = point1
        x2,y2 = point2
        if abs(x1-x2)< delta and abs(y1-y2)<delta:
            approx_equal = True
        return approx_equal

    def calculate_distance_from_point_to_line(self,point, line):
        distance_from_text = -1
        x0,y0 = point
        x1,y1 = line[0]
        x2,y2 = line[1]
        max_x = max(x1,x2)
        min_x = min(x1,x2)
        max_y = max(y1,y2)
        min_y = min(y1,y2)
        #----find a,b (intersection point from x0,y0 to line)
        if x1==x2:
            intersect_x = x1
            intersect_y = y0
        else:
            m = (y2-y1)/float(x2-x1)
            if y1==y2:
                c = y1
            else:
                c = y2-(m*x2)
            intersect_x = (x0+(m*y0)-(m*c))/float(math.pow(m,2)+1)
            intersect_y = (m*intersect_x)+c

        #----find if intersection point is on the line segment
        if (intersect_x>=min_x and intersect_x<=max_x and intersect_y>=min_y and intersect_y<=max_y):
            distance_from_text = math.hypot(intersect_x - x0, intersect_y - y0)

        return distance_from_text, intersect_x,intersect_y

    def get_shortest_path_from_point_to_line(self,point, line):
        distance_from_text, intersect_x, intersect_y = self.calculate_distance_from_point_to_line(point, line)
        if distance_from_text == -1:
            return []
        else:
            return [point,[int(intersect_x),int(intersect_y)]]

    def find_other_cord_of_point_on_line(self,cord_name,cord_value,m,c):
        if cord_name=='x':
            other_cord = m*cord_value+c
        if cord_name=='y':
            # print 'm',m,'c',c,'cord_value',cord_value
            other_cord = (cord_value/m)-(c/m)
        return other_cord
    #---same as in open plan line.py ,
    # just changed input parameters from 'x1,y1,x2,y2,x3,y3,x4,y4' to line1, line2
    def find_lines_intersection(self,line1, line2):
        x1, y1=line1[0]
        x2, y2=line1[1]
        x3,y3 = line2[0]
        x4,y4 = line2[1]
        #-------for line1
        #get m,c for horizontal line
        if y1==y2:
            m1 = 0
            c1 = y1
        #get m,c for vertical line
        elif x1==x2:
            m1 = 'a'
            c1 = 'a'
        #get m,c for other lines
        else:
            m1 =(y1-y2)/float(x1-x2)
            c1 = y1-(m1*x1)

        #------for line2
        #get m,c for horizontal line
        if y3==y4:
            m2 = 0
            c2 = y3
        #get m,c for vertical line
        elif x3==x4:
            m2 = 'a'
            c2 = 'a'
        #get m,c for other lines
        else:
             m2 = (y3-y4)/float(x3-x4)
             c2 = y3-(m2*x3)

        intersect_x,intersect_y = 0,0
        # print (m1,m2,c1,c2,x1,y1,x2,y2,'--',x3,y3,x4,y4)
        #------find intersections

        #if not paralell lines find intersections
        if m1!= m2:
            #for vertical lines
            if (m1 == 'a') or (m2 == 'a'):
                if m1 == 'a':
                    intersect_x = x1
                    intersect_y = (m2*intersect_x)+c2
                else:
                    intersect_x= x3
                    intersect_y = (m1*intersect_x)+c1

            #for horizontal lines
            elif (m1 == 0) or (m2 == 0):
                if m1 == 0:
                    intersect_y = y1
                    if m2 == 'a':
                        intersect_x = x3
                    else:
                        intersect_x = (intersect_y/m2)-(c2/m2)
                else:
                    intersect_y = y3
                    if m1 == 'a':
                        intersect_x = x1
                    else:
                        intersect_x = (intersect_y/m1)-(c1/m1)

            #for all other lines
            else:
                intersect_x = (c2-c1)/(m1-m2)
                intersect_y = (m1*intersect_x)+c1
        # print intersect_x,intersect_y

        return int(intersect_x), int(intersect_y)

    def find_paralell_lines(self,line,distance):
        parallel_lines = []
        x1,y1 = line[0]
        x2,y2 = line[1]

        m,c = self.find_mc_of_line(x1,y1,x2,y2)

        #--if vertical
        if m==0:
            xp1, yp1 = x1, y1-distance
            xp2, yp2 = x2, y2-distance
            xp3, yp3 = x1, y1+distance
            xp4, yp4 = x2, y2+distance
        elif m=='a':
            xp1, yp1 = x1 - distance, y1
            xp2, yp2 = x2 - distance, y2
            xp3, yp3 = x1 + distance, y1
            xp4, yp4 = x2 + distance, y2
        else:
            #--- gradient of line perpendicular to given line
            # m1 = -1/m
            yp1 = y1 + (distance/(math.sqrt(m**2 + 1)))
            xp1 = m*(y1-yp1) + x1
            yp3 = y1 - (distance/(math.sqrt(m**2 + 1)))
            xp3 = (m *(y1-yp3)) + x1
            delta_x = x2-x1
            delta_y = y2-y1
            xp2 = xp1 + delta_x
            yp2 = yp1 + delta_y
            xp4 = xp3 + delta_x
            yp4 = yp3 + delta_y

        parallel_lines = [
            [[int(xp1),int(yp1)],[int(xp2),int(yp2)]],
            [[int(xp3),int(yp3)],[int(xp4),int(yp4)]]
        ]

        return parallel_lines



    def close_contour(self, path_output,only_image_name,avg_door_width):
        main_path = path_output+only_image_name+'/'
        #--test path
        avg_door_width = 90
        main_path ='/home/ub/Documents/floorplan_recognition/tesimagistrale/input_fps/contour_close/'

        # Read image
        im_in = cv2.imread(main_path+ 'Stefano_output.png', cv2.IMREAD_GRAYSCALE)
        # kernel = np.ones((50,100), np.uint8)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (100, 40))
        closing = cv2.morphologyEx(im_in, cv2.MORPH_CLOSE, kernel)
        cv2.imwrite(main_path + "ClosedImage.jpg", closing)

        ret, thresh = cv2.threshold(closing, 0, 255, 1)
        contours, hierachy = cv2.findContours(thresh, 1, 2)
        img_height, img_width = im_in.shape
        image_area = img_height * img_width
        for cont, contour in enumerate(contours):
            contour_area = cv2.contourArea(contour)
            # ---remove image contour
            if contour_area / image_area < 0.95 and contour_area / image_area > 0.2:
                print 'Contour selected'
                contour_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
                cv2.drawContours(contour_image, [contour], -1, (0, 0, 0), 5)
                cv2.imwrite(main_path + str(cont) + 'New_cont.png', contour_image)
                # # Threshold.
        # # Set values equal to or above 220 to 0.
        # # Set values below 220 to 255.
        # th, im_th = cv2.threshold(im_in, 220, 255, cv2.THRESH_BINARY_INV)
        #
        # # Copy the thresholded image.
        # im_floodfill = im_th.copy()
        #
        # # Mask used to flood filling.
        # # Notice the size needs to be 2 pixels than the image.
        # h, w = im_th.shape[:2]
        # mask = np.zeros((h + 2, w + 2), np.uint8)
        #
        # # Floodfill from point (0, 0)
        # cv2.floodFill(im_floodfill, mask, (0, 0), 255)
        #
        # # Invert floodfilled image
        # im_floodfill_inv = cv2.bitwise_not(im_floodfill)
        #
        # # Combine the two images to get the foreground.
        # im_out = im_th | im_floodfill_inv
        #
        # # Display images.
        # cv2.imwrite(main_path+"Thresholded Image", im_th)
        # cv2.imwrite(main_path+"Floodfilled Image", im_floodfill)
        # cv2.imwrite(main_path+"Inverted Floodfilled Image", im_floodfill_inv)
        # cv2.imwrite(main_path+"Foreground", im_out)


        # stefano_image = misc.imread(main_path + 'Stefano_output.png', True)
        # histo = np.histogram(stefano_image, bins=np.arange(0, 256))
        # edges = filter.canny(histo / 255.)
        # fill_coins = ndi.binary_fill_holes(edges)





        # contours = measure.find_contours(stefano_image,0.001)
        # # Display the image and plot all contours found
        # fig, ax = plt.subplots()
        # ax.imshow(stefano_image, interpolation='nearest', cmap=plt.cm.gray)
        #
        # for n, contour in enumerate(contours):
        #     ax.plot(contour[:, 1], contour[:, 0], linewidth=2)
        #
        # ax.axis('image')
        # ax.set_xticks([])
        # ax.set_yticks([])
        # plt.show()

    #-----cupboard find methods
    def resize_line(self,x1,y1,x2,y2,extension_length):
        line_length = math.hypot(x2 - x1, y2 - y1)
        x_difference = ((x2 - x1) * extension_length) / line_length
        y_difference = ((y2 - y1) * extension_length) / line_length

        x3, y3 = int(x1 - x_difference), int(y1 - y_difference)
        x4, y4 = int(x2 + x_difference), int(y2 + y_difference)

        return x3, y3, x4, y4

    #--angle finding methods
    def find_internal_angles(self, p1, ref, p2):
        x1, y1 = p1[0] - ref[0], p1[1] - ref[1]
        x2, y2 = p2[0] - ref[0], p2[1] - ref[1]

        #---find if angle_type is 'interior' or 'exterior'
        # True if cross is positive
        # False if negative or zero
        if x1 * y2 < x2 * y1:
            angle_type = 'I'
        else:
            angle_type = 'E'

        #--calculate angle
        if angle_type == 'I':
            angle = self.calculate_angle_vectors(x1,y1,x2,y2)
        else:
            inverse_angle = self.calculate_angle_vectors(x1, y1, x2, y2)
            angle = 360- inverse_angle

        return angle

    def calculate_angle_vectors(self,vx1,vy1,vx2,vy2):
        # Use dotproduct to find angle between vectors
        # This always returns an angle between 0, pi
        numer = (vx1 * vx2 + vy1 * vy2)
        denom = math.sqrt((vx1 ** 2 + vy1 ** 2) * (vx2 ** 2 + vy2 ** 2))
        angle = math.degrees(math.acos(numer / denom))
        return angle

    def change_image_density(self,input_image,results_path):
        input_path = results_path+'input.png'
        output_path = results_path+'output.png'
        #--write input image
        cv2.imwrite(input_path,input_image)
        #--get current_image_format
        proc = subprocess.Popen(['identify -format "%x %y" ' + input_path],
                                stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
        #--extract x,y resolution info
        resolution_data = out.split()
        x_resolution, y_resolution = resolution_data[0], resolution_data[2]

        # --double the resolution
        new_resolution = round(int(float(x_resolution)), -1) * 2
        # print 'new_resolution', new_resolution, 'x_resolution, y_resolution', x_resolution, y_resolution

        os.system('convert -density ' + str(new_resolution) + ' ' + input_path + ' -set density ' + str(
            new_resolution) + ' ' + output_path)

        output_image = cv2.imread(output_path,cv2.IMREAD_GRAYSCALE)
        os.remove(input_path)
        os.remove(output_path)

        return output_image

    #----- contour methods--------------------------------------------------
    def simplify_contour(self,contour,avg_door_width,thresh1,thresh2):
        contour_points = []
        for l, line_level1 in enumerate(contour):
            for line_level2 in line_level1:
                contour_points.append(line_level2)
                break

        cont_lines_to_pre_process, first_element = [], 0
        for l, cordinate in enumerate(contour_points):
            x, y = cordinate
            if l == 0:
                cont_lines_to_pre_process.append([])
                cont_lines_to_pre_process[l].append([x, y])
                first_element = [x, y]
            elif l == len(contour_points) - 1:
                cont_lines_to_pre_process.append([])
                cont_lines_to_pre_process[l].append([x, y])
                cont_lines_to_pre_process[l - 1].append([x, y])
                cont_lines_to_pre_process[l].append(first_element)
            else:
                cont_lines_to_pre_process.append([])
                cont_lines_to_pre_process[l].append([x, y])
                cont_lines_to_pre_process[l - 1].append([x, y])

        cont_points_after_pre_process = []
        min_line_length = int(avg_door_width / thresh1)-2
        min_length_line, last_element = False, 0
        for l, each_line in enumerate(cont_lines_to_pre_process):
            x1, y1 = each_line[0]
            x2, y2 = each_line[1]
            line_length = math.hypot(x2 - x1, y2 - y1)
            if l == 0:
                cont_points_after_pre_process.append(each_line[0])
                if line_length < min_line_length:
                    continue
                else:
                    cont_points_after_pre_process.append(each_line[1])

            elif l == len(cont_lines_to_pre_process) - 1:
                if line_length < min_line_length:
                    del cont_points_after_pre_process[-1]
                if min_length_line == True:
                    cont_points_after_pre_process.append(each_line[0])
                cont_points_after_pre_process.append(each_line[1])
            else:
                if line_length > min_line_length:
                    cont_points_after_pre_process.append(each_line[0])
                    cont_points_after_pre_process.append(each_line[1])

        final_contour_points, i_point, last_point = [], 0, 0
        line_gradient_x, line_gradient_y = 0, 0
        line_gradient_empty = False
        for i, current_point in enumerate(cont_points_after_pre_process):
            x1, y1 = cont_points_after_pre_process[i - 1]
            x2, y2 = current_point

            if i == 0:
                final_contour_points.append(current_point)
            elif i == len(cont_points_after_pre_process) - 1:
                final_contour_points.append(current_point)
            else:
                if i == 1 or line_gradient_empty:
                    current_line_length = math.hypot(x2 - x1, y2 - y1)
                    if current_line_length > avg_door_width / thresh1:
                        current_gradient_x, current_gradient_y = self.line_obj.find_line_gradient(x1, y1, x2, y2)
                        line_gradient_x, line_gradient_y = current_gradient_x, current_gradient_y
                        line_gradient_empty = False
                    else:
                        line_gradient_empty = True
                        continue

                x3, y3 = cont_points_after_pre_process[i + 1]
                next_gradient_x, next_gradient_y =  self.line_obj.find_line_gradient(x2, y2, x3, y3)
                x_difference = abs(line_gradient_x - next_gradient_x)
                y_difference = abs(line_gradient_y - next_gradient_y)

                if ~(x_difference < 0.05 and y_difference < 0.05):
                    next_line_length = math.hypot(x3 - x2, y3 - y2)
                    if next_line_length > avg_door_width / thresh1:
                        final_contour_points.append(current_point)
                        line_gradient_x, line_gradient_y = next_gradient_x, next_gradient_y


        new_contour_points = []
        for p, current_point in enumerate(final_contour_points):
            if p == 0:
                new_contour_points.append(current_point)
            else:
                current_x, current_y = current_point
                last_x, last_y = new_contour_points[-1]
                x_difference = abs(last_x - current_x)
                y_difference = abs(last_y - current_y)
                if x_difference < int(avg_door_width / thresh2)-2:
                    new_contour_points.append([last_x, current_y])
                elif y_difference < int(avg_door_width / thresh2)-2:
                    new_contour_points.append([current_x, last_y])
                else:
                    new_contour_points.append(current_point)


        image_contour_lines, first_element = [], 0
        for l, cordinate in enumerate(new_contour_points):
            x, y = cordinate
            if l == 0:
                image_contour_lines.append([])
                image_contour_lines[l].append([x, y])
                first_element = [x, y]
            elif l == len(final_contour_points) - 1:
                image_contour_lines[l - 1].append([x, y])
                if ~(x == first_element[0] and y == first_element[1]):
                    image_contour_lines.append([])
                    image_contour_lines[l].append([x, y])
                    image_contour_lines[l].append(first_element)
            else:
                image_contour_lines.append([])
                image_contour_lines[l].append([x, y])
                image_contour_lines[l - 1].append([x, y])
                # last_element = [x,y]
        last_element = -1
        min_length = int(avg_door_width / thresh1)-2
        image_contour_lines2 = []
        for n, each_line in enumerate(image_contour_lines):
            # print each_line
            x1, y1 = each_line[0]
            x2, y2 = each_line[1]
            line_length = math.hypot(x2 - x1, y2 - y1)
            if n == last_element:
                continue
            elif n == len(image_contour_lines) - 1:
                if line_length < min_length:
                    image_contour_lines2[-1][1] = image_contour_lines2[0][0]
                    break
            else:
                if line_length < min_length:
                    image_contour_lines2.append([each_line[0], image_contour_lines[n + 1][1]])
                    last_element = n + 1
                else:
                    image_contour_lines2.append(each_line)
                    last_element = -1

        new_contour_lines = image_contour_lines2

        simplified_contour = self.find_contour_from_contour_lines(new_contour_lines)

        return simplified_contour,new_contour_lines

    #--for one set of contour lines return the contour
    def find_contour_from_contour_lines(self,contour_lines):
        point_list = self.convert_lines_to_points(contour_lines)
        contour = self.convert_points_to_contour(point_list)
        return contour

    # --for one set of contour lines return the contour points
    def convert_lines_to_points(self,contour_lines):
        point_list = []
        for each_contour_line in contour_lines:
            x3, y3 = each_contour_line[0]
            point_list.append([x3, y3])
        return point_list

    # --for one set of contour points return the contour
    def convert_points_to_contour(self,point_list):
        add_bracket_cont_points = []
        for cont_point in point_list:
            add_bracket_cont_points.append([cont_point])
        contour = np.asarray(add_bracket_cont_points)
        return contour

    def find_lines_from_contour(self,contour):
        contour_point_list = self.get_points_from_contour(contour)
        contour_lines_list = self.convert_points_to_lines(contour_point_list)
        return contour_lines_list

    def get_points_from_contour(self,contour):
        contour_point_list = []
        for level1 in contour:
            for level2 in level1:
                x,y = level2
                contour_point_list.append([x,y])
        return contour_point_list

    def get_rectangle_from_line(self,line,distance):
        parallel_lines = self.find_paralell_lines(line,distance)
        x3, y3 = parallel_lines[0][0]
        x4, y4 = parallel_lines[0][1]
        x5, y5 = parallel_lines[1][0]
        x6, y6 = parallel_lines[1][1]
        rect_points = self.order_rectangle_points(x3, y3, x4, y4, x5, y5, x6, y6)
        return rect_points

    def convert_points_to_lines(self,points_list):
        lines_list = []
        first_element = 0
        for l, cordinate in enumerate(points_list):
            x, y = cordinate
            if l == 0:
                lines_list.append([])
                lines_list[l].append([x, y])
                first_element = [x, y]
            elif l == len(points_list) - 1:
                lines_list.append([])
                lines_list[l].append([x, y])
                lines_list[l - 1].append([x, y])
                lines_list[l].append(first_element)
            else:
                lines_list.append([])
                lines_list[l].append([x, y])
                lines_list[l - 1].append([x, y])
        return lines_list

    def check_line_inside_contour(self,x1,y1,x2,y2,cnt):
        #--changed on 19/03/2017
        line_length = math.hypot(x2-x1,y2-y1)
        num_of_points_to_check = int(line_length/3)
        num_of_points_inside_contour =0
        for a in range(1,num_of_points_to_check):
            m = a
            n = num_of_points_to_check-a

            x0 = ((n*x1)+(m*x2))/(m+n)
            y0 = ((n*y1)+(m*y2))/(m+n)

            dist = cv2.pointPolygonTest(cnt,(x0,y0),False)
            if dist == 1:
                num_of_points_inside_contour= num_of_points_inside_contour+1
                # if num_of_points_outside_contour>2:
                #     break

        return num_of_points_inside_contour

    def get_contour_length_between_lines(self,contour, line1_num, line2_num):
        if line1_num<line2_num:
            l1 = line1_num
            l2 = line2_num
        else:
            l1 = line2_num
            l2 = line1_num
        line_set = contour[l1+1:l2]
        total_length = 0
        for each_line in line_set:
            x1,y1 = each_line[0]
            x2,y2 = each_line[1]

            line_length = math.hypot(x2-x1,y2-y1)
            total_length = total_length + line_length

        return total_length

    def get_centre_of_contour(self,contour):
        M = cv2.moments(contour)
        cx = int(M['m10'] / M['m00'])
        cy = int(M['m01'] / M['m00'])
        return cx, cy

    def get_list_from_text_files(self, path):
        file = open(path, 'r+')
        string = file.read()
        words_list = string.split()
        return words_list

    def generate_string_from_point_list(self,points):
        point_string = ''
        for c, cord in enumerate(points):
            if c == 0:
                point_string = point_string + '[' + str(cord[0]) + ',' + str(cord[1]) + ']'
            else:
                point_string = point_string + ',[' + str(cord[0]) + ',' + str(cord[1]) + ']'
        return point_string

    def read_special_cases_files(self,current_directory):
        furniture_words_list = self.get_list_from_text_files(
            current_directory + '/Model/floor_plan_text/furniture_list.txt')
        non_open_plan_rooms_words_list = self.get_list_from_text_files(
            current_directory + '/Model/floor_plan_text/non_open_plan_rooms_list.txt')
        wc_words_list = self.get_list_from_text_files(
            current_directory + '/Model/floor_plan_text/wc_list.txt')
        bath_words_list = self.get_list_from_text_files(
            current_directory + '/Model/floor_plan_text/bath_list.txt')
        corridor_words_list = self.get_list_from_text_files(
            current_directory + '/Model/floor_plan_text/corridor_list.txt')

        special_cases_list = furniture_words_list, non_open_plan_rooms_words_list,wc_words_list,bath_words_list,corridor_words_list

        return special_cases_list
authour = 'anu'

import cv2, math, numpy as np,os
from PIL import Image
from operator import itemgetter

from iterative_functions import iterative_functions_class

class cupboard_class:

    def __init__(self,path_output_d,path_final_lines,only_image_name,avg_door_width,
                 img_width,img_height):
        self.path_output_d = path_output_d
        self.iterative_functions_obj = iterative_functions_class()
        self.cupboard_path = path_final_lines+only_image_name
        self.img_width,self.img_height = img_width,img_height
        self.avg_door_width = avg_door_width

    def find_cupboard(self,otsu_path, input_path,only_image_name,current_directory,
                      text_cordinates, debug_mode):
        input_image_path = input_path+only_image_name+'/no_text.png'
        #--testing
        # input_image_path = current_directory + '/input_fps/temp/'+only_image_name+'_no_text.png'

        floorplan_img = cv2.imread(input_image_path, cv2.IMREAD_GRAYSCALE)
        # img_height, img_width = floorplan_img.shape

        cupboard_data = []

        # --furniture list
        furniture_file = open(current_directory + '/Model/floor_plan_text/furniture_list.txt', 'r+')
        furniture_string = furniture_file.read()
        furniture_words_list = furniture_string.split()

        # --find text data of 'cupboard'
        for text_row in text_cordinates:
            for furniture_names in furniture_words_list:
               if text_row[0] == furniture_names:
                cupboard_data.append(text_row)
                # print text_row

        possible_cupboards,cupboard_contours = [], []
        #--crop image to include only one cupboard
        for cupboard_num, cupboard_row in enumerate(cupboard_data):
            cupboard_textcordinate = cupboard_row[1]
            #--get bounding box cords
            x1,y1,x2,y2 = cupboard_row[2]
            #resize bb
            extension_length = abs(x2-x1)*5
            x3, y3, x4, y4 = self.iterative_functions_obj.resize_line(x1, y1,x2,y2, extension_length)
            #--extract resozed_BB area and write to new image
            stencil = np.zeros(floorplan_img.shape).astype(floorplan_img.dtype)
            p1, p2, p3, p4 = self.iterative_functions_obj.find_points_rectangle(x3, y3, x4, y4)
            contours = [np.array([[p1], [p2], [p3],[p4]])]

            color = [255, 255, 255]
            cv2.fillPoly(stencil, contours, color)
            extracted_image = cv2.bitwise_and(floorplan_img, stencil)
            # cv2.imwrite(self.cupboard_path+'--'+str(cupboard_num)+'_cropped.jpg', extracted_image)

            # output_path = self.cupboard_path+'-'+str(cupboard_num)
            # new_resolution_image = self.iterative_functions_obj.change_image_density(extracted_image,output_path)

            #--detect new contours
            ret, thresh = cv2.threshold(extracted_image, 0, 255, 1)
            contours, hierachy = cv2.findContours(thresh, 1, 2)
            selected_contours = []
            for count, current_cont in enumerate(contours):
                # test_img = ~(np.zeros((self.img_height, self.img_width, 3), np.uint8))
                # cv2.drawContours(test_img,[current_cont],-1,(0,0,0),3)
                # cv2.imwrite(self.cupboard_path + str(count) + '_contour.png', test_img)

                if cv2.pointPolygonTest(current_cont, (tuple(cupboard_textcordinate)), False) == 1:
                    current_area = cv2.contourArea(current_cont)
                    selected_contours.append([current_cont,current_area])
                    # test_img = ~(np.zeros((self.img_height, self.img_width, 3), np.uint8))
                    # cv2.drawContours(test_img, [current_cont], -1, (0, 0, 0), 3)
                    # cv2.imwrite(self.cupboard_path + str(count) + '_contour.png', test_img)

            #--sort selected contours based on area DESC
            contours_sorted = sorted(selected_contours, key=itemgetter(1))
            #--sort the contours to find smallest conotur-PL
            if len(contours_sorted)>0:
                possible_cupboards.append(contours_sorted[0][0])
                if len(contours_sorted[0][0])>0:
                    cupboard_contours.append([cupboard_row,contours_sorted[0][0]])


        # for possible_cup in possible_cupboards:
        #     if len(possible_cup) > 0:
        #         cupboard_contours.append(possible_cup)

        return cupboard_contours

    def add_cupboard_to_main_image2(self,cupboard_contours,image_path,only_image_name,current_directory):
        # --non testing path
        input_image_path = image_path+only_image_name+'/Stefano_output.png'
        main_image = cv2.imread(input_image_path, cv2.IMREAD_GRAYSCALE)

        #---testing
        # temp_image_path = current_directory + '/input_fps/temp/'+only_image_name+'_output.png'
        # main_image = cv2.imread(temp_image_path, cv2.IMREAD_GRAYSCALE)


        ret, thresh = cv2.threshold(main_image, 0, 255, 1)
        redraw_contours, hierachy = cv2.findContours(thresh, 1, 2)

        pl_image = ~(np.zeros((self.img_height, self.img_width, 3), np.uint8))
        # --draw cupboards
        for each_cupboard in cupboard_contours:
            cv2.drawContours(pl_image, [each_cupboard], -1, (0, 0, 0), 2)
        for cn_num,each in enumerate(redraw_contours):
            if cn_num != len(redraw_contours)-1:
                pl_image = ~(np.zeros((self.img_height, self.img_width, 3), np.uint8))
                cv2.drawContours(pl_image, [each], -1, (255, 255, 0), 3, cv2.cv.CV_AA)
                # cv2.imwrite(image_path + only_image_name +'/'+str(cn_num)+ '_allconts.png', pl_image)
        # cv2.imwrite(image_path+only_image_name+'/allconts.png', pl_image)

        #--find PL line using conotur detection
        gray_pl_image = cv2.cvtColor(pl_image, cv2.COLOR_RGB2GRAY)
        ret, thresh = cv2.threshold(gray_pl_image, 0, 255, 0)
        contours, hierachy = cv2.findContours(thresh, 1, 2)
        for cn,each_cupboard in enumerate(contours):
            if cn==0:
                # pl_image = ~(np.zeros((self.img_height, self.img_width, 3), np.uint8))
                # cv2.drawContours(pl_image, [each_cupboard], -1, (0, 0, 0), 1)
                # cv2.imwrite(image_path + only_image_name + '/' + str(cn) + 'cont1.png', pl_image)

                simple_contour,contour_lines = self.iterative_functions_obj.simplify_contour(each_cupboard,
                                              self.avg_door_width,20,15)
                cupboard_line = contour_lines[0]
                parallel_lines = self.iterative_functions_obj.find_paralell_lines(cupboard_line,2)
                points = [parallel_lines[0][0],parallel_lines[0][1],
                          parallel_lines[1][0],parallel_lines[1][1]]
                cx,cy = self.iterative_functions_obj.find_centre_of_line(cupboard_line)
                sorted_points = self.iterative_functions_obj.order_polygon_points(points,[cx,cy])
                # cv2.rectangle(pl_image,(tuple(sorted_points[0])),(tuple(sorted_points[2])),(0,0,255),2)
                #
                # cv2.imwrite(image_path + only_image_name + '/'+str(cn)+'cont2.png', pl_image)


        #--draw cupboards
        for each_cupboard in cupboard_contours:
            cv2.drawContours(main_image, [each_cupboard], -1, (0, 0, 255), 5)
        cv2.imwrite(input_image_path,main_image)

    def add_cupboard_to_main_image(self, cupboard_contours, image_path, only_image_name,
                                   current_directory,path_gravvitas_text,evaluation):
        # --non testing path
        input_image_path = image_path + only_image_name + '/new_walls_windows_door_1.png'
        main_image = cv2.imread(input_image_path,cv2.IMREAD_GRAYSCALE)
        color_main_image = cv2.imread(input_image_path,cv2.IMREAD_COLOR)

        # ---testing
        # temp_image_path = current_directory + '/input_fps/temp/' + only_image_name + '_output.png'
        # main_image = cv2.imread(temp_image_path, cv2.IMREAD_GRAYSCALE)
        # color_main_image = cv2.cvtColor(main_image, cv2.COLOR_GRAY2RGB)



        ret, thresh = cv2.threshold(main_image, 0, 255, 1)
        redraw_contours, hierachy = cv2.findContours(thresh, 1, 2)

        # --testing
        # pl_image_all = ~(np.zeros((self.img_height, self.img_width, 3), np.uint8))
        # for cn_num, each_contour in enumerate(redraw_contours):
        #     cv2.drawContours(pl_image_all, [each_contour], -1, (255, 0, 0), 3, cv2.cv.CV_AA)
        # cv2.imwrite(image_path + only_image_name + '_cont00.png', pl_image_all)

        wall_text_file = open(path_gravvitas_text+only_image_name+'_7_wall_details.txt','a')
        for cupboard_data in cupboard_contours:
            cupboard_txt_row = cupboard_data[0]
            cupboard_cont = cupboard_data[1]
            cupboard_textcordinate = cupboard_txt_row[1]
            simple_contour, lines_not_used = self.iterative_functions_obj.simplify_contour(cupboard_cont,
                                                                                           self.avg_door_width, 20, 15)
            # ---1. for OP partitoning draw the whole cupboard
            cv2.drawContours(color_main_image, [simple_contour], -1, (0, 0, 255), 2)

            cupboard_lines = self.iterative_functions_obj.find_lines_from_contour(simple_contour)
            # # ---testing
            # pl_image = ~(np.zeros((self.img_height, self.img_width, 3), np.uint8))
            # cv2.drawContours(pl_image, [cupboard_cont], -1, (255, 0, 0), 3, cv2.cv.CV_AA)
            # cv2.circle(pl_image, (tuple(cupboard_textcordinate)), 5, (0, 0, 255), -1)
            # cv2.imwrite(image_path + only_image_name + '/_cupboard---cont.png', pl_image)
            # pl_image = ~(np.zeros((self.img_height, self.img_width, 3), np.uint8))
            # cv2.drawContours(pl_image, [simple_contour], -1, (0, 255, 0), 3, cv2.cv.CV_AA)
            # cv2.circle(pl_image, (tuple(cupboard_textcordinate)), 5, (0, 0, 255), -1)
            # cv2.imwrite(image_path + only_image_name + '/_cupboard---simple_cont.png', pl_image)

            fp_contour = []
            for cn_num, each_contour in enumerate(redraw_contours):
                # # ---testing
                # pl_image = ~(np.zeros((self.img_height, self.img_width, 3), np.uint8))
                # cv2.drawContours(pl_image, [each_contour], -1, (255, 0, 0), 3, cv2.cv.CV_AA)
                # cv2.circle(pl_image, (tuple(cupboard_textcordinate)), 5, (0, 0, 255), -1)
                # cv2.imwrite(image_path + only_image_name + '/' + str(cn_num) + '_cont1.png', pl_image)

                #--find fp contours that contains 'PL' text
                if cn_num != len(redraw_contours)-1 and cn_num != len(redraw_contours)-2:
                    if cv2.pointPolygonTest(each_contour, (tuple(cupboard_textcordinate)),
                                            False) == 1:
                        fp_contour.append([each_contour,cv2.contourArea(each_contour)])
                        # # ---testing
                        # pl_image = ~(np.zeros((self.img_height, self.img_width, 3), np.uint8))
                        # cv2.drawContours(pl_image, [each_contour], -1, (255, 255, 0), 3, cv2.cv.CV_AA)
                        # cv2.circle(pl_image,(tuple(cupboard_textcordinate)),5,(0,0,255),-1)
                        # cv2.imwrite(image_path + only_image_name + '/' + str(cn_num) + '_cont2.png', pl_image)

            if len(fp_contour)>0:
                #--sort fp contours with 'PL' inside based on area-to get min_area fp contour
                fp_contour.sort(key=itemgetter(1))
                fp_contour_for_cupboard = fp_contour[0][0]

                #---2.start line finding process for wall.txt
                missing_cupboard_lines,max_distance =[], 0
                for cupboard_line in cupboard_lines:
                    cx,cy = self.iterative_functions_obj.find_centre_of_line(cupboard_line)
                    distance = cv2.pointPolygonTest(fp_contour_for_cupboard,
                                                    (cx,cy),True)
                    if distance > 1:
                        missing_cupboard_lines.append(cupboard_line)
                        # max_distance = distance
                # print 'missing_cupboard_lines',missing_cupboard_lines
                # if len(missing_cupboard_lines)>0:
                #     for missing_cupboard_line in missing_cupboard_lines:
                #         cv2.line(color_main_image,(tuple(missing_cupboard_line[0])),(tuple(missing_cupboard_line[1])),(255,0,0),5)
                for cupboard_line in missing_cupboard_lines:
                    parallel_lines = self.iterative_functions_obj.find_paralell_lines(cupboard_line, 2)
                    points = [parallel_lines[0][0], parallel_lines[0][1],
                              parallel_lines[1][0], parallel_lines[1][1]]
                    cx, cy = self.iterative_functions_obj.find_centre_of_line(cupboard_line)
                    sorted_points = self.iterative_functions_obj.order_polygon_points(points, [cx, cy])
                    points_string = ''
                    for c, cord in enumerate(sorted_points):
                        if c == 0:
                            points_string = points_string + '[' + str(cord[0]) + ',' + str(cord[1]) + ']'
                        else:
                            points_string = points_string + ',[' + str(cord[0]) + ',' + str(cord[1]) + ']'
                    if evaluation == False:
                        wall_text_file.write('Wall : ' + points_string + ' : [1] \n')

                    # cupboard_contour = self.iterative_functions_obj.convert_points_to_contour(sorted_points)
                    # cv2.drawContours(color_main_image, [cupboard_contour],-1, (0, 0, 255), -1)

        #     #--testing
        #     cv2.putText(pl_image_all, cupboard_txt_row[0], (tuple(cupboard_textcordinate)),
        #                 cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0),2)
        # #--testing
        # cv2.imwrite(image_path + only_image_name + '_cont0.png', pl_image_all)
        # cv2.imwrite(image_path + only_image_name + '_completed.png', color_main_image)
        #---non testing
        cv2.imwrite(image_path + only_image_name + '/new_walls_windows_door.png',color_main_image)
        wall_text_file.close()



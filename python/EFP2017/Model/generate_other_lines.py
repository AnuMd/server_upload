author = 'anu'

import cv2,math, numpy as np

from line_optimization_functions import line_optimization_function_class
from open_plan_line import line
from iterative_functions import iterative_functions_class

class generate_other_lines_class:

    def __init__(self):
        self.line_optimization_function_obj = line_optimization_function_class()
        self.iterative_functions_obj = iterative_functions_class()
        self.line_obj = line()

    #---generate_lines_from_interesting_points() and it's sub functions
    def generate_lines_from_interesting_points(
            self, only_image_name,final_lines_path, original_edges_and_extensions, contour_lines,
            text_bounding_boxes, text_cordinates,avg_door_width,height,width):
        contour_points = self.iterative_functions_obj.convert_lines_to_points(contour_lines)
        #--calculate interior angles for all contour points
        contour_angle_data = self.find_angles_of_contour_pts(contour_points,final_lines_path)

        #--for all interesting contour points: find closeby contour points
        valid_lines = self.find_close_by_contour_points(
            contour_lines,contour_angle_data,contour_points,avg_door_width,final_lines_path+only_image_name)

        #--check if two lines are the same
        unique_valid_lines = self.delete_similar_lines(valid_lines)

        #--find NPL that do not go across a text
        text_non_intersect_valid_lines = self.check_if_lines_intersect_with_text_cordinates(
            text_bounding_boxes,unique_valid_lines)

        #--find NPL that partitions @least one text
        text_partition_valid_lines = self.check_if_lines_partition_atleast_one_text(
            text_non_intersect_valid_lines,contour_lines,text_cordinates,height,width,
            avg_door_width)

        #--find NPL that are not too close to each other
        distance_passed_valid_lines = self.find_if_lines_are_too_close(text_partition_valid_lines,avg_door_width)

        #--find NPLs that are not == to EELs
        original_edges_and_extensions,interesting_lines = self.match_ipl_with_eel(distance_passed_valid_lines,original_edges_and_extensions)

        # #----testing
        # test_image = ~(np.zeros((height,width, 3), np.uint8))
        # for cont_line in contour_lines:
        #     cv2.line(test_image, (tuple(cont_line[0])), (tuple(cont_line[1])), (0, 0, 0), 2, cv2.cv.CV_AA)
        # for bb_row in text_bounding_boxes:
        #     tx0, ty0, tx1, ty1 = bb_row
        #     L1, L2, L3, L4 = self.iterative_functions_obj.find_lines_rectangle(tx0, ty0, tx1, ty1)
        #     bb_lines = [L1, L2, L3, L4 ]
        #     for bb_line in bb_lines:
        #         cv2.line(test_image, (tuple(bb_line[0])), (tuple(bb_line[1])), (255, 0, 0), 2, cv2.cv.CV_AA)
        #
        # test_image1 = test_image.copy()
        # for each_line in unique_valid_lines:
        #     cv2.line(test_image1, (tuple(each_line[0])), (tuple(each_line[1])), (0, 0, 255), 2, cv2.cv.CV_AA)
        # cv2.imwrite(final_lines_path + '-' + only_image_name + '-step_01.png', test_image1)
        #
        # test_image2 = test_image.copy()
        # for each_line in text_non_intersect_valid_lines:
        #     cv2.line(test_image2, (tuple(each_line[0])), (tuple(each_line[1])), (0, 0, 255), 2, cv2.cv.CV_AA)
        # cv2.imwrite(final_lines_path + '-' + only_image_name + '-step_02.png', test_image2)
        #
        # test_image3 = test_image.copy()
        # for each_line in text_partition_valid_lines:
        #     cv2.line(test_image3, (tuple(each_line[0])), (tuple(each_line[1])), (0, 0, 255), 2, cv2.cv.CV_AA)
        # cv2.imwrite(final_lines_path + '-' + only_image_name + '-step_03.png', test_image3)
        #
        # test_image4 = test_image.copy()
        # for each_line in distance_passed_valid_lines:
        #     cv2.line(test_image4, (tuple(each_line[0])), (tuple(each_line[1])), (0, 0, 255), 2, cv2.cv.CV_AA)
        # cv2.imwrite(final_lines_path + '-' + only_image_name + '-step_04.png', test_image4)
        #
        # test_image5 = test_image.copy()
        # for each_line in interesting_lines:
        #     cv2.line(test_image5, (tuple(each_line[0])), (tuple(each_line[1])), (0, 0, 255), 2, cv2.cv.CV_AA)
        # cv2.imwrite(final_lines_path + '-' + only_image_name + '-step_05.png', test_image5)

        return original_edges_and_extensions,interesting_lines,contour_angle_data

    def find_angles_of_contour_pts(self, contour_points1,final_lines_path):
        contour_angle_data = []
        contour_points = list(reversed(contour_points1))

        for pt_num, contour_pt in enumerate(contour_points):
            if pt_num == 0:
                ref_pt = contour_pt
                p1 = contour_points[-1]
                p2 = contour_points[pt_num + 1]
            elif pt_num == len(contour_points) - 1:
                ref_pt = contour_pt
                p1 = contour_points[pt_num - 1]
                p2 = contour_points[0]
            else:
                ref_pt = contour_pt
                p1 = contour_points[pt_num - 1]
                p2 = contour_points[pt_num + 1]



            angle = self.iterative_functions_obj.find_internal_angles(p1, ref_pt, p2)
            contour_angle_data.append([contour_pt, angle])


        return list(reversed(contour_angle_data))

    def find_close_by_contour_points(self,contour_lines, contour_angle_data,contour_points,avg_door_width,final_lines_path):
        # test_image = ~(np.zeros((3508, 2479, 3), np.uint8))
        # for cont_line in contour_lines:
        #     cv2.line(test_image, (tuple(cont_line[0])), (tuple(cont_line[1])), (0, 0, 0), 2, cv2.cv.CV_AA)

        valid_lines = []
        for pt_num1,ct_anlge_row in enumerate(contour_angle_data):
            selected_contour_point = ct_anlge_row[0]
            selected_angle = ct_anlge_row[1]
            #---if angle>180 ---> it is a interesting point
            if selected_angle >= 180:
                x1,y1 = selected_contour_point
                ## --testing line
                # test_image1 = test_image.copy()
                # cv2.circle(test_image1, (x1, y1), 10, (0, 0, 255), -1)

                p1, p2 = self.find_points_beside_current_pt(contour_points, pt_num1)
                not_valid_pt_nums = [p1, pt_num1, p2]
                for pt_num2,each_contour_point in enumerate(contour_points):
                    #--find contour points that are not == selected_contour_pt_num
                    #--& not == pt_numbers beside selected_contour_pt_num
                    intersection = list(set([pt_num2]) & set(not_valid_pt_nums))
                    if len(intersection) == 0:
                        x2,y2 = each_contour_point
                        distance_from_text = math.hypot(x1 - x2, y1 - y2)

                        if distance_from_text <= avg_door_width*2:
                            #----check if cont_point2 is visible to cont_point1
                            max_sx = max(x1, x2)
                            min_sx = min(x1, x2)
                            max_sy = max(y1, y2)
                            min_sy = min(y1, y2)
                            # --count how many contour lines intersects with sp
                            contour_intersection_count = 0
                            contour = self.iterative_functions_obj.find_contour_from_contour_lines(contour_lines)
                            for each_contour_line in contour_lines:
                                x3, y3 = each_contour_line[0]
                                x4, y4 = each_contour_line[1]
                                if ~(x1==x3 and y1==y3) and ~(x1==x4 and y1==y4) and ~(x2 == x3 and y2 == y3) and ~(x2 == x4 and y2 == y4):
                                    max_cx = max(x3, x4)
                                    min_cx = min(x3, x4)
                                    max_cy = max(y3, y4)
                                    min_cy = min(y3, y4)
                                    intersect_x, intersect_y = self.line_obj.find_contour_intersections(x1, y1, x2, y2, x3,
                                                                                                        y3, x4, y4)
                                    if (intersect_x >= min_sx and intersect_x <= max_sx and intersect_y >= min_sy and intersect_y <= max_sy) and (
                                        intersect_x >= min_cx and intersect_x <= max_cx and intersect_y >= min_cy and intersect_y <= max_cy):
                                        contour_intersection_count = contour_intersection_count + 1

                            if contour_intersection_count == 0:
                                num_of_intersection_points = self.line_obj.check_line_inside_contour_return_num_of_points(x1,y1,x2,y2,contour)
                                if num_of_intersection_points<10:
                                    valid_lines.append([selected_contour_point, each_contour_point])
                                # --testing line
                                # cv2.circle(test_image1, (x2, y2), 5, (255, 0, 0), -1)
                # cv2.imwrite(final_lines_path + '-'+str(pt_num1)+'-cont_lines.png', test_image1)
        # for each_line in valid_lines:
        #     cv2.line(test_image, (tuple(each_line[0])), (tuple(each_line[1])), (0, 0, 255), 2, cv2.cv.CV_AA)
        # cv2.imwrite(final_lines_path + '-cont_lines.png', test_image)

        return valid_lines

    def delete_similar_lines(self,valid_lines):
        unique_valid_lines = []
        indexes_to_ignore = []
        for l1,lines_iteration1 in enumerate(valid_lines):
            # print 'indexes_to_ignore', indexes_to_ignore
            # print 'l1,lines_iteration1', l1,lines_iteration1
            intersection = []
            if len(indexes_to_ignore) > 0:
                intersection = list(set([l1]) & set(indexes_to_ignore))
            # print 'intersection',intersection
            if len(intersection) == 0:
                for l2,lines_iteration2 in enumerate(valid_lines):
                    if l1 < l2:
                        if self.check_if_two_lines_similar(lines_iteration1, lines_iteration2, 0):
                            indexes_to_ignore.append(l2)
                            # print 'l2,lines_iteration2',l2,lines_iteration2
                            break
                unique_valid_lines.append(lines_iteration1)
                # print 'appended',lines_iteration1
        return unique_valid_lines


    def find_points_beside_current_pt(self,contour_points,pt_num):
        if pt_num == 0:
            p1 = len(contour_points)+1
            p2 = pt_num + 1
        elif pt_num == len(contour_points) - 1:
            p1 = pt_num - 1
            p2 = 0
        else:
            p1 = pt_num - 1
            p2 = pt_num + 1
        return p1,p2

    def check_if_lines_intersect_with_text_cordinates(self,text_bounding_boxes,valid_lines):
        text_non_intersect_valid_lines = []
        #--generate bb_box lines from bb_box cords
        b_box_lines = []
        for bb_row in text_bounding_boxes:
            tx0, ty0, tx1, ty1 = bb_row
            L1, L2, L3, L4 = self.iterative_functions_obj.find_lines_rectangle(tx0, ty0, tx1, ty1)
            b_box_lines.append([L1, L2, L3, L4])

        #--check if bb_box lines intersect with valid_lines
        for i_line in valid_lines:
            x1, y1 = i_line[0]
            x2, y2 = i_line[1]
            i_line_max_x = max(x1, x2)
            i_line_min_x = min(x1, x2)
            i_line_max_y = max(y1, y2)
            i_line_min_y = min(y1, y2)
            intersect_flag = False
            for bb_box in b_box_lines:
                for bb_line in bb_box:
                    bb_line_x1, bb_line_y1 = bb_line[0]
                    bb_line_x2, bb_line_y2 = bb_line[1]
                    bb_line_max_x = max(bb_line_x1, bb_line_x2)
                    bb_line_min_x = min(bb_line_x1, bb_line_x2)
                    bb_line_max_y = max(bb_line_y1, bb_line_y2)
                    bb_line_min_y = min(bb_line_y1, bb_line_y2)
                    intersect_x1, intersect_y1 = self.line_obj.find_contour_intersections(x1, y1, x2, y2,
                                                                                     bb_line_x1,
                                                                                     bb_line_y1,
                                                                                     bb_line_x2,
                                                                                     bb_line_y2)
                    if (intersect_x1 >= i_line_min_x and intersect_x1 <= i_line_max_x and intersect_y1 >= i_line_min_y and intersect_y1 <= i_line_max_y) and (
                        intersect_x1 >= bb_line_min_x and intersect_x1 <= bb_line_max_x and intersect_y1 >= bb_line_min_y and intersect_y1 <= bb_line_max_y):
                        intersect_flag = True
                        break
                if intersect_flag:
                    break
            #--if no intersection, add it as a valid lines
            if intersect_flag == False:
                text_non_intersect_valid_lines.append(i_line)

        return text_non_intersect_valid_lines

    def check_if_lines_partition_atleast_one_text(self,text_non_intersect_valid_lines,
                                                  contour_lines,text_cordinates,height,width,
                                                  avg_door_width):
        text_partition_valid_lines = []
        #--draw all contour lines to a image
        temp_image = ~(np.zeros((height, width, 3), np.uint8))
        for cont_line in contour_lines:
            cv2.line(temp_image, (tuple(cont_line[0])), (tuple(cont_line[1])), (0, 0, 0), 2, cv2.cv.CV_AA)
        #--check if each NPL partitions text
        for row_num,valid_line in enumerate(text_non_intersect_valid_lines):
            temp_image_copy = temp_image.copy()
            cv2.line(temp_image_copy, (tuple(valid_line[0])), (tuple(valid_line[1])), (0, 0, 0), 2, cv2.cv.CV_AA)

            #--read image with new line drawn
            temp_image_copy_gray = cv2.cvtColor(temp_image_copy, cv2.COLOR_RGB2GRAY)
            ret, thresh = cv2.threshold(temp_image_copy_gray, 0, 255, 1)
            contours, hierachy = cv2.findContours(thresh, 1, 2)
            text_partition_flag = False
            #--check how many contours it create
            if len(contours) > 2:
                for num, cnt in enumerate(contours):
                    if num != len(contours) - 1 and cv2.contourArea(cnt) > avg_door_width * 10:
                        text_cordinate_count = 0
                        for each_text in text_cordinates:
                            if cv2.pointPolygonTest(cnt, tuple(each_text), False) == 1:
                                text_cordinate_count += 1
                        #--if a contour has 0 text or has tot_texts all in one contour->bad NPL
                        if text_cordinate_count != 0 and text_cordinate_count != len(text_cordinates):
                            text_partition_flag = True
                            break
            #--insert lines that met condition
            if text_partition_flag:
                text_partition_valid_lines.append(valid_line)

        return text_partition_valid_lines

    def find_if_lines_are_too_close(self, partition_lines,avg_door_width):
        distance_passed_valid_lines = []
        #--distance_condition
        min_distance = avg_door_width/2
        indexes_to_ignore = []
        for l1,lines_iteration1 in enumerate(partition_lines):
            intersection = []
            if len(indexes_to_ignore)>0:
                intersection = list(set([l1]) & set(indexes_to_ignore))
            if len(intersection) == 0:
                for l2,lines_iteration2 in enumerate(partition_lines):
                    if l1 < l2:
                        if self.check_if_two_lines_similar(lines_iteration1,lines_iteration2,min_distance):
                            indexes_to_ignore.append(l2)
                            break
                distance_passed_valid_lines.append(lines_iteration1)

        return distance_passed_valid_lines

    def check_if_two_lines_similar(self,line1,line2,threshold):
        x1, y1 = line1[0]
        x2, y2 = line1[1]
        x3, y3 = line2[0]
        x4, y4 = line2[1]
        # print x1, y1, x2, y2
        # print x3, y3, x4, y4
        # print 'abs(x1 - x3)',abs(x1 - x3), 'abs(y1 - y3)',abs(y1 - y3)
        # print 'abs(x2 - x4)',abs(x2 - x4), 'abs(y2 - y4)',abs(y2 - y4)
        # print 'abs(x1 - x4)',abs(x1 - x4), 'abs(y1 - y4)', abs(y1 - y4)
        # print 'abs(x2 - x3)',abs(x2 - x3), 'abs(y2 - y3)',abs(y2 - y3)

        return ((abs(x1 - x3) <= threshold and abs(y1 - y3) <= threshold) and (abs(x2 - x4) <= threshold and abs(y2 - y4) <= threshold)) or (
            (abs(x1 - x4) <= threshold and abs(y1 - y4) <= threshold) and (abs(x2 - x3) <= threshold and abs(y2 - y3) <= threshold))

    def match_ipl_with_eel(self,interesting_lines,original_edges_and_extensions):
        for ee_row_num,ee_row in enumerate(original_edges_and_extensions):
            ee_line = ee_row[1]
            similar_flag = False
            for i_row, interesting_line in enumerate(interesting_lines):
                #--if it already doesnt have a 'S' set for that line (not found a similar line)
                if len(interesting_line) < 3:
                    similar_flag = self.check_if_two_lines_similar(interesting_line,ee_line,2)
                    if similar_flag:
                        interesting_lines[i_row].append('S')
                        break

            if similar_flag:
                original_edges_and_extensions[ee_row_num].append('S')
            else:
                original_edges_and_extensions[ee_row_num].append('NS')

        #--keep only ipls that don't have a similar EEL
        interesting_points_line2 = []
        for int_line in interesting_lines:
            if len(int_line) < 3:
                interesting_points_line2.append(int_line)


        return original_edges_and_extensions,interesting_points_line2
    #----end generate_lines_from_interesting_points()



    #---find orthogonal lines() and it's sub functions
    def find_orthogonal_lines(self,open_plan_contour_lines,contour_angle_data):
        #---find lines connected to points with angle!=90 and !=270
        self.find_interesting_lines_on_angle(contour_angle_data,open_plan_contour_lines)


    def find_interesting_lines_on_angle(self,contour_angle_data,open_plan_contour_lines):
        interesting_lines = []
        for angle_data_row in contour_angle_data:
            contour_point = angle_data_row[0]
            angle = angle_data_row[1]
            if angle != 90 and angle != 270:
                # --find open_plan_contour_lines that has contour_point as a point on line
                for contour_line in open_plan_contour_lines:
                    similar = self.compare_point_with_lines(contour_point, contour_line)
                    if similar:
                        interesting_lines.append(contour_line)
        return interesting_lines



    def compare_point_with_lines(self,contour_point,contour_line):
        px1,py1 = contour_point
        x1, y1 = contour_line[0]
        x2, y2 = contour_line[1]

        if (px1==x1 and py1==y1) or (px1==x2 and py1==y2):
            return True
        else:
            return False
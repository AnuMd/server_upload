__author__ = 'anu'

class color_check_class:
    def __init__(self,pixels, top_threshold_pixel_array,bottom_threshold_pixel_array,left_threshold_pixel_array, right_threshold_pixel_array):
        self.pixels = pixels
        self.top_threshold_pixel_array = top_threshold_pixel_array
        self.bottom_threshold_pixel_array = bottom_threshold_pixel_array
        self.left_threshold_pixel_array = left_threshold_pixel_array
        self.right_threshold_pixel_array =right_threshold_pixel_array

    def color_match(self,cordinate, pixel_data, temp_ring_array,position,current_txt_label):
        image_column,image_row = cordinate
        pixel_data[image_row][image_column]= current_txt_label
        tra_length = len(temp_ring_array)
        temp_ring_array.append([])
        temp_ring_array[tra_length].append(cordinate)
        temp_ring_array[tra_length].append(current_txt_label)
        return temp_ring_array, pixel_data


    # def shortest_path_color_match(self,cordinate, pixel_data, temp_ring_array,position):
    #     column,row = cordinate
    #     #--- check if it exists in pixel_data already(taken by another txt cordinate iteration)
    #     color = self.pixels[column,row]
    #
    #     index,value = 0, 0
    #     threshold_pixel_array = []
    #     if position is 'T':
    #         threshold_pixel_array = self.top_threshold_pixel_array
    #         index, value = column, row
    #
    #         if row> threshold_pixel_array[index]:
    #             pixel_data[row][column]= position
    #             temp_ring_array.append(cordinate)
    #
    #     if position is 'L':
    #         threshold_pixel_array = self.left_threshold_pixel_array
    #         index, value = row, column
    #         if column> threshold_pixel_array[index]:
    #             pixel_data[row][column]= position
    #             temp_ring_array.append(cordinate)
    #     if position is 'B':
    #         threshold_pixel_array = self.bottom_threshold_pixel_array
    #         index, value = column, row
    #         if row< threshold_pixel_array[index]:
    #             pixel_data[row][column]= position
    #             temp_ring_array.append(cordinate)
    #     if position is 'R':
    #         threshold_pixel_array = self.right_threshold_pixel_array
    #         index, value = row, column
    #         if column< threshold_pixel_array[index]:
    #             pixel_data[row][column]= position
    #             temp_ring_array.append(cordinate)
    #     # if position is 'TL':
    #     #     threshold_pixel_array = self.top_threshold_pixel_array
    #     #     threshold_pixel_array_1 = self.left_threshold_pixel_array
    #     #     thresh_row, thresh_value = column, row
    #     #     thresh_row_1, thresh_value_1 = row, column
    #     #     if row> threshold_pixel_array[thresh_row] and column> threshold_pixel_array_1[thresh_row_1] :
    #     #         pixel_data[row][column]= position
    #     #         temp_ring_array.append(cordinate)
    #     # if position is 'BL':
    #     #     threshold_pixel_array = self.bottom_threshold_pixel_array
    #     #     threshold_pixel_array_1 = self.left_threshold_pixel_array
    #     #     thresh_row, thresh_value = column, row
    #     #     thresh_row_1, thresh_value_1 = row, column
    #     #     if row< threshold_pixel_array[thresh_row] and column> threshold_pixel_array_1[thresh_row_1]:
    #     #         pixel_data[row][column]= position
    #     #         temp_ring_array.append(cordinate)
    #     # if position is 'BR':
    #     #     threshold_pixel_array = self.bottom_threshold_pixel_array
    #     #     threshold_pixel_array_1 = self.right_threshold_pixel_array
    #     #     thresh_row, thresh_value = column, row
    #     #     thresh_row_1, thresh_value_1 = row, column
    #     #     if row< threshold_pixel_array[thresh_row] and column< threshold_pixel_array_1[thresh_row_1]:
    #     #         pixel_data[row][column]= position
    #     #         temp_ring_array.append(cordinate)
    #     # if position is 'TR':
    #     #     threshold_pixel_array = self.top_threshold_pixel_array
    #     #     threshold_pixel_array_1 = self.right_threshold_pixel_array
    #     #     thresh_row, thresh_value = column, row
    #     #     thresh_row_1, thresh_value_1 = row, column
    #     #     if row> threshold_pixel_array[thresh_row] and column< threshold_pixel_array_1[thresh_row_1]:
    #     #         pixel_data[row][column]= position
    #     #         temp_ring_array.append(cordinate)
    #
    #     if position is 'T' or position is 'L' or position is 'B' or position is 'R':
    #         if ((color[0] == 0) and (color[1] == 0) and (color[2] == 0)):
    #             threshold_pixel_array[index] = value
    #
    #     return temp_ring_array, pixel_data
    def shortest_path_color_match(self,cordinate, pixel_data, temp_ring_array,position):
        column,row = cordinate
        #--- check if it exists in pixel_data already(taken by another txt cordinate iteration)
        color = self.pixels[column,row]

        index,value = 0, 0
        threshold_pixel_array = []
        if position is 'T':
            threshold_pixel_array = self.top_threshold_pixel_array
            index, value = column, row

            # if row> threshold_pixel_array[index]:
            pixel_data[row][column]= position
            temp_ring_array.append(cordinate)

        if position is 'L':
            threshold_pixel_array = self.left_threshold_pixel_array
            index, value = row, column
            # if column> threshold_pixel_array[index]:
            pixel_data[row][column]= position
            temp_ring_array.append(cordinate)
        if position is 'B':
            threshold_pixel_array = self.bottom_threshold_pixel_array
            index, value = column, row
            # if row< threshold_pixel_array[index]:
            pixel_data[row][column]= position
            temp_ring_array.append(cordinate)
        if position is 'R':
            threshold_pixel_array = self.right_threshold_pixel_array
            index, value = row, column
            # if column< threshold_pixel_array[index]:
            pixel_data[row][column]= position
            temp_ring_array.append(cordinate)

        if position is 'T' or position is 'L' or position is 'B' or position is 'R':
            if ((color[0] == 0) and (color[1] == 0) and (color[2] == 0)):
                threshold_pixel_array[index] = value

        return temp_ring_array, pixel_data

    def get_shortest_path(self,cordinate, pixel_data,shortest_path_points):
        column,row = cordinate

        earlier__position = ''
        while pixel_data[row][column] is not 'S':
            position = pixel_data[row][column]
            pixel_data[row][column] = 'K'
            if earlier__position is not position:
                # print (x,y,position)
                shortest_path_points.append([column,row])

            if position is 'T':
                column,row = column, row+1
            if position is 'L':
                column,row = column+1, row
            if position is 'B':
                column,row = column, row-1
            if position is 'R':
                column,row = column-1, row
            if position is 'TL':
                column,row = column+1, row+1
            if position is 'BL':
                column,row = column+1, row-1
            if position is 'BR':
                column,row = column-1, row-1
            if position is 'TR':
                column,row = column-1, row+1

            earlier__position = position
        # print (x,y,position)
        shortest_path_points.append([column,row])
        return shortest_path_points







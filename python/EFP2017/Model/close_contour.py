__author__ = 'anu'

import re, cv2, numpy as np, math, os,random
from PIL import Image,ImageOps
from operator import itemgetter

from iterative_functions import iterative_functions_class
from open_plan_line import line

class contour_closing_functions():

    def __init__(self,avg_wall_width,avg_door_width):
        self.iterative_obj = iterative_functions_class()
        self.line_obj = line()
        self.avg_wall_width = avg_wall_width
        self.avg_door_width = avg_door_width
        self.door_threshold = self.avg_door_width*4
        self.min_contour_difference = 8
        self.closeby_cont_pair_diff = 6

    #-- method 03
    def close_contour(self,path_gravvitas,only_image_name,path_output,img_height,
                      img_width,debug_mode):
        rect_list = self.get_all_component_list(path_gravvitas, only_image_name,
                                  path_output, img_height, img_width,debug_mode)
        rect_outer_cont_list = self.get_outer_cont_rects(rect_list)
        outer_rect_image = self.draw_outer_rects(img_height, img_width,
                                                 rect_outer_cont_list,
                                                 path_output+only_image_name,debug_mode)
        disconnected_cont_list= self.find_opened_contours(outer_rect_image,
                                                 path_output+only_image_name,
                                                 img_height,img_width,debug_mode)

        #-- Only if there are any opened contours, try to close the door
        all_doors,outer_cont,connection_rects = [],0,[]
        if len(disconnected_cont_list) > 0:
            line_lists,simplified_contours = self.get_bbxes_all_outer_conts(outer_rect_image,
                                    path_output+only_image_name,img_height,img_width,
                                    disconnected_cont_list,debug_mode)
            border_point_data = self.get_points_on_image_border(img_height,img_width,
                                outer_rect_image,path_output+only_image_name,debug_mode)
            outermost_cont_lines = self.get_outermost_cont_lines(border_point_data,
                                                        line_lists,outer_rect_image,
                                                        path_output+only_image_name,
                                                        img_height,img_width,debug_mode)
            shortest_line_sets,shortest_conts = self.find_short_line_segs(line_lists,
                                                img_height,img_width,
                                                path_output+only_image_name,debug_mode)
            main_shortest_conts = shortest_conts.copy()
            #--out of all short walls, select ones in outer contour
            outer_cont_shortest_seg_list,shortest_conts = self.find_outer_cont_shortest_segs(shortest_line_sets,outermost_cont_lines,
                                          shortest_conts,path_output+only_image_name,debug_mode)
            #--sort lines by length
            sorted_shortest_lines = self.order_shortest_lines(outer_cont_shortest_seg_list,
                                         line_lists,shortest_conts,
                                         path_output+only_image_name,debug_mode)

            #----1. close contour method 01--------------------------
            door_line_list_1,connections = self.close_contour_method_01(sorted_shortest_lines,line_lists,simplified_contours,
                                shortest_conts,path_output+only_image_name,debug_mode)
            door_rect_as_points = self.create_doors(door_line_list_1,[])
            all_doors = door_line_list_1
            #---find wall ends that didnt make a connection
            closed_contour_flag = self.check_conotour_close_bb_area_measure(door_rect_as_points,
                                            simplified_contours,img_height,img_width,
                                            path_output + only_image_name)

            # ----2. close contour method 02--------------------------
            if closed_contour_flag==False:
            #     # print 'M2'
                door_line_list_2 = self.close_contour_method_02(connections,line_lists,sorted_shortest_lines,
                                simplified_contours,door_rect_as_points,door_line_list_1,
                                shortest_conts,path_output+only_image_name,debug_mode)
                door_rect_as_points = self.create_doors(door_line_list_2,door_rect_as_points)
                all_doors = all_doors + door_line_list_2
                closed_contour_flag2 = self.check_conotour_close_bb_area_measure(door_rect_as_points,
                                              simplified_contours, img_height, img_width,
                                              path_output + only_image_name)

                # ----3. close contour method 03--------------------------
                if closed_contour_flag2 == False:
                    # print 'M3'
                    connected_walls_2 = self.find_connected_wall_ends(sorted_shortest_lines,
                                                                      all_doors, shortest_conts,
                                                                      path_output + only_image_name)
                    door_line_list_3 = self.closing_method_03(connected_walls_2,sorted_shortest_lines,
                                           line_lists,simplified_contours,shortest_conts,
                                           path_output+only_image_name,debug_mode)
                    door_rect_as_points = self.create_doors(door_line_list_3, door_rect_as_points)
                    all_doors = all_doors + door_line_list_3

                    closed_contour_flag3 = self.check_conotour_close_bb_area_measure(door_rect_as_points,
                                                simplified_contours, img_height,img_width,
                                                path_output + only_image_name)



                    # ----4. close contour method 04--------------------------
                    if closed_contour_flag3==False:
                        # print 'M4'
                        connected_walls_3 = self.find_connected_wall_ends(sorted_shortest_lines,
                                                                          all_doors, shortest_conts,
                                                                          path_output + only_image_name)
                        door_line_list_4 = self.closing_method_04(connected_walls_3, sorted_shortest_lines,
                                                                  line_lists, simplified_contours,
                                                                  shortest_conts,
                                                                  path_output + only_image_name,debug_mode)
                        door_rect_as_points = self.create_doors(door_line_list_4, door_rect_as_points)
                        all_doors = all_doors + door_line_list_4

            outer_doors = self.keep_only_outer_doors(all_doors,simplified_contours,
                                       door_rect_as_points,img_height,img_width,
                                       path_output+only_image_name)
            door_rect_as_points_final = self.create_doors(outer_doors, [])

            rect_conts = []
            for rect in door_rect_as_points_final:
                #--to remove rects that go outside image area
                invalid = False
                for p in rect:
                    x1,y1 = p
                    if x1<0 or x1>img_width or y1<0 or y1>img_height:
                        invalid= True
                        break
                if invalid == False:

                    connection_rects.append(rect)
                    rect_cont = self.iterative_obj.convert_points_to_contour(rect)

                    #--to remove too big areas and too small areas
                    rect_area = cv2.contourArea(rect_cont)
                    if rect_area > (img_height*img_width)/20 or rect_area < (self.avg_door_width/10):
                        continue
                    else:
                        rect_conts.append(rect_cont)
                        if debug_mode:
                            cv2.drawContours(main_shortest_conts,[rect_cont],-1,(0,0,255),-1)

            outer_cont = self.find_outermost_contour(simplified_contours,rect_conts,img_height,img_width)

            if debug_mode:
                cv2.imwrite(path_output+only_image_name+'-rects.png',main_shortest_conts)

        connection_data = [row[0] for row in all_doors][:len(connection_rects)]
        # old_components = rect_list

        return connection_data,outer_cont,connection_rects

    # --used
    def get_all_component_list(self, path_gravvitas, only_image_name, path_output,
                               img_height, img_width, debug_mode):
            rect_list = []
            if debug_mode:
                walls_image = ~(np.zeros((img_height, img_width, 3), np.uint8))

            # --- get data from text files
            wall_point_list = self.read_component_file(path_gravvitas, only_image_name,
                                                       '_7_wall_details.txt')
            windows_point_list = self.read_component_file(path_gravvitas, only_image_name,
                                                          '_4_window_details.txt')
            doors_rect_list = self.read_component_file(path_gravvitas, only_image_name,
                                                       '_5_door_details.txt')
            #--temp
            color = [[0, 0, 255], [0, 255, 0], [255, 0, 0]]

            for window_row in windows_point_list:
                rect_list.append([window_row, 'win'])
                if debug_mode:
                    window_contour = self.iterative_obj.convert_points_to_contour(window_row)
                    cv2.drawContours(walls_image, [window_contour], -1, (random.choice(color)), -1)

            for door_row in doors_rect_list:
                rect_list.append([door_row, 'd'])
                if debug_mode:
                    door_contour = self.iterative_obj.convert_points_to_contour(door_row)
                    cv2.drawContours(walls_image, [door_contour], -1, (random.choice(color)), -1)

            for wall_row in wall_point_list:
                rect_list.append([wall_row, 'wl'])
                if debug_mode:
                    wall_contour = self.iterative_obj.convert_points_to_contour(wall_row)
                    cv2.drawContours(walls_image, [wall_contour], -1, (random.choice(color)), -1)

            if debug_mode:
                cv2.imwrite(path_output + only_image_name + '-0step.png', walls_image)

            return rect_list
    #--used
    def read_component_file(self, path_gravvitas, only_image_name,detection_type_file):
        comp_point_list = []
        file_path = path_gravvitas + only_image_name + detection_type_file
        if os.path.isfile(file_path):
            cmp_details_file = open(file_path, 'r')
            for text_line in cmp_details_file:
                line_seperate = text_line.split(':')
                if len(line_seperate) > 2:
                    contour_details = re.findall(r"[\w']+", line_seperate[1])
                    int_contour_details = [int(x) for x in contour_details]
                    cmp_contour = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                    comp_point_list.append(cmp_contour)
        return comp_point_list

    #--used
    def get_outer_cont_rects(self,rect_list):
        out_cnt_rect_shrtst_lne_cntr_lst = []
        rect_shortest_line_list = []
        rect_outer_cont_list = []
        wall_threshold = self.avg_wall_width*0.2
        for rect_num,rect_data in enumerate(rect_list):
            rect_row = rect_data[0]

            if len(rect_row)==2:
                x1,y1 = rect_row[0]
                x2,y2 = rect_row[1]
                L1, L2, L3, L4 = self.iterative_obj.find_lines_rectangle(x1,y1,x2,y2)
            else:
                L1, L2, L3, L4 =  self.iterative_obj.convert_points_to_lines(rect_row)

            L1_length = math.hypot(L1[0][0]-L1[1][0],L1[0][1]-L1[1][1])
            L2_length = math.hypot(L2[0][0] - L2[1][0], L2[0][1] - L2[1][1])

            valid_L1, valid_L2 = 0,0
            #-- longest leg is L_2
            if L1_length < L2_length:
                # if rect_num==7:
                #     print 'in if', L1_length ,L1_length - self.avg_wall_width,wall_threshold
                #--check if length of shorter leg(width) is more than threshold->thick wall
                # if (L1_length - self.avg_wall_width) > wall_threshold:
                if L1_length > wall_threshold:
                # if L1_length > self.avg_wall_width:
                    valid_L1, valid_L2 = L1,L3
                    # L1_cx,L1_cy = self.iterative_obj.find_centre_of_line(L1)
                    # L3_cx, L3_cy = self.iterative_obj.find_centre_of_line(L3)
                    # L1_c = L1_cx,L1_cy
                    # L3_c = L3_cx, L3_cy
                    # out_cnt_rect_shrtst_lne_lst.append([[L1_c,L3_c],rect_num])
            else:
                # if rect_num==7:
                #     print 'in else',L2_length ,L2_length - self.avg_wall_width,wall_threshold
                # if (L2_length - self.avg_wall_width) > wall_threshold:
                # if L2_length > self.avg_wall_width:
                if L2_length > wall_threshold:
                    # out_cnt_rect_shrtst_lne_lst.append([[L2, L4],rect_num])
                    valid_L1, valid_L2 = L2,L4

            if valid_L1 != 0 and valid_L2 != 0:
                L1_cx, L1_cy = self.iterative_obj.find_centre_of_line(valid_L1)
                L2_cx, L2_cy = self.iterative_obj.find_centre_of_line(valid_L2)
                L1_c = L1_cx,L1_cy
                L2_c = L2_cx, L2_cy
                out_cnt_rect_shrtst_lne_cntr_lst.append([rect_num,[[L1_c,False],[L2_c,False]]])
                rect_shortest_line_list.append([rect_num,valid_L1,valid_L2])
                rect_outer_cont_list.append(rect_row)

        return rect_outer_cont_list

    # ---used
    def draw_outer_rects(self, img_height, img_width, rect_outer_cont_list, path_output,
                         debug_mode):
        outer_rect_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
        for row in rect_outer_cont_list:
            if len(row) == 2:
                x1, y1 = row[0]
                x2, y2 = row[1]
                cv2.rectangle(outer_rect_image, (x1, y1), (x2, y2), (0, 0, 255), -1)
            else:
                wall_contour = self.iterative_obj.convert_points_to_contour(row)
                cv2.drawContours(outer_rect_image, [wall_contour], -1, (0, 0, 255), -1)
        if debug_mode:
            cv2.imwrite(path_output +'-1step.png', outer_rect_image)
        return outer_rect_image

    #--used
    def find_opened_contours(self,outer_rect_image,path_output,img_height,img_width,
                             debug_mode):
        disconnected_cont_list = []
        gray_image = cv2.cvtColor(outer_rect_image, cv2.COLOR_RGB2GRAY)
        ret, thresh = cv2.threshold(gray_image, 127, 255, 1)
        contours, hierachy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_TC89_KCOS)

        img_area = img_height * img_width
        for r_num, row in enumerate(hierachy[0]):
            if row[3] == -1:
                c = r_num
                cnt = contours[c]
                area_ratio = round(cv2.contourArea(cnt)/img_area,5)
                if area_ratio < 0.15:
                    # print c,'---', round(cv2.contourArea(cnt)/img_area,5)
                    if debug_mode:
                        current_cont = ~(np.zeros((img_height, img_width, 3), np.uint8))
                        cv2.drawContours(current_cont, [cnt], -1, (0, 0, 0), -1)
                        cv2.imwrite(path_output + '-2step-'+str(c)+'*Contour.png', current_cont)
                    cont_length = cv2.arcLength(cnt,True)
                    disconnected_cont_list.append([cnt,cont_length])

        disconnected_cont_list.sort(key=itemgetter(1),reverse=True)

        #--if largest disconnected rect is very small, then dont try to close,
        #---coz it's already closed
        if len(disconnected_cont_list)>0 and disconnected_cont_list[0][1]<320:
            return []
        else:
            return [row[0] for row in disconnected_cont_list]

    #--used
    def get_bbxes_all_outer_conts(self,outer_rect_image,path_output,img_height,
                                  img_width,disconnected_cont_list,debug_mode):
        # gray_image = cv2.cvtColor(outer_rect_image, cv2.COLOR_RGB2GRAY)
        ##-- needed only for thesis
        ## kernel = np.ones((300, 300), np.uint8)
        ## closing = cv2.morphologyEx(gray_image, cv2.MORPH_OPEN, kernel)
        ## cv2.imwrite(path_output + 'test-dilate1.png', closing)
        ## cv2.imwrite(path_output + 'test-dilate2.png', gray_image)

        # ret, thresh = cv2.threshold(gray_image, 127, 255, 1)
        # contours, hierachy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_TC89_KCOS)
        #
        # all_conts = ~(np.zeros((img_height, img_width, 3), np.uint8))
        # cv2.drawContours(all_conts, contours, -1, (0, 0, 0), -1)
        # # cv2.imwrite(path_output + '_1_allContours.png', all_conts)
        #
        # outer_conts = []
        # for r_num, row in enumerate(hierachy[0]):
        #     if row[3] == -1:
        #         outer_conts.append(contours[r_num])

        simplified_contours, line_lists = [],[]
        for c, cnt in enumerate(disconnected_cont_list):
            current_cont = ~(np.zeros((img_height, img_width, 3), np.uint8))
            cv2.drawContours(current_cont, [cnt], -1, (0, 0, 0), -1)
            if debug_mode:
                cv2.imwrite(path_output + '-3step-'+str(c)+'*Contour.png', current_cont)


            #---3. get corner points
            #--- better way of getting corners
            simple_cont = ~(np.zeros((img_height, img_width, 3), np.uint8))
            cv2.drawContours(simple_cont, [cnt], -1, (0, 0, 0), -1)
            gray = cv2.cvtColor(current_cont, cv2.COLOR_RGB2GRAY)

            #--cv2.goodFeaturesToTrack(input gray image, max no of corners we need,
            #-- accuracy of corners we need, min_distance between corners)
            #--- 1. 'max no of corners we need' - can be increased to any number
            #--- 2. 'min_distance between corners' is set to 0, coz
            #--- since 'accuracy of corners we need' controls no of points
            #--- 'accuracy of corners we need' is 0.27 coz we need slanted corners too
            corners = cv2.goodFeaturesToTrack(gray, 200, 0.27, self.avg_wall_width/10)
            corners = np.int0(corners)
            new_corners = []
            if debug_mode:
                simple_cont2 = ~(np.zeros((img_height, img_width, 3), np.uint8))
                cv2.drawContours(simple_cont2, [cnt], -1, (0, 0, 0), -1)
            for i in corners:
                x, y = i.ravel()
                new_corners.append([x,y])
                if debug_mode:
                    cv2.circle(simple_cont2, (x, y), 10, (0,0,255), -1)
            if debug_mode:
                cv2.imwrite(path_output+'-4step-' + str(c) + '*Corner Points.png',simple_cont2)

            cont_point_list = self.iterative_obj.get_points_from_contour(cnt)
            for num,r in enumerate(cont_point_list):
                cont_point_list[num].append([])

            #----4. order corner points based on contour
            for p_index,point in enumerate(new_corners):
                x1,y1 = point
                closest_index, min_distance = -1, 1000
                for index, cont_point in enumerate(cont_point_list):
                    x2,y2 = cont_point[0], cont_point[1]
                    distance = math.hypot(x2-x1,y2-y1)
                    if distance < min_distance:
                        closest_index = index
                        min_distance = distance
                new_corners[p_index].append(closest_index)
            new_corners.sort(key=itemgetter(2))
            contable_corner_points = []
            for row in new_corners:
                contable_corner_points.append([row[0],row[1]])

            simple_contour = self.iterative_obj.convert_points_to_contour(contable_corner_points)

            # print contable_corner_points

            #----5. get lines based on new points
            line_list = self.iterative_obj.convert_points_to_lines(contable_corner_points)

            #----6. find valid conoturs
            area = cv2.contourArea(simple_contour)/((img_height* img_width)/100)

            # print c, area
            #--filter contours by area to remove small outliers
            #--- doesnt remove small wallscoz tested well
            if area > 0.01:
                simplified_contours.append(simple_contour)
                line_lists.append(line_list)
                # print c,line_count, area

            # -- visualization purpose
            if debug_mode:
                color = [[0, 0, 255], [0, 255, 0], [255, 0, 0]]
                simple_cont2 = ~(np.zeros((img_height, img_width, 3), np.uint8))
                enum_num = [0,1,2]*len(line_list)
                for l,line in enumerate(line_list):
                    x1,y1 = line[0]
                    x2,y2 = line[1]
                    rand_col = tuple(color[enum_num[l]])
                    cv2.line(simple_cont2,(x1,y1),(x2,y2),rand_col,5,cv2.CV_AA)
                cv2.imwrite(path_output + '-5step-' + str(c) + '*Cont Lines.png', simple_cont2)

            # print c, len(line_list)

            # difference = len(cnt)-len(line_list)
            # denominator = len(cnt)+0.0
            # print str(int(round((difference/denominator),1)* 100)) +'%'


            # line_list = self.iterative_obj.find_lines_from_contour(current_cont)
            # line_lists.append(line_list)

            # x, y, w, h = cv2.boundingRect(cnt)
            # rotated_bounding_boxes.append([x,y])
            # rotated_bounding_boxes.append([x+w,y+h])

        return line_lists,simplified_contours

    #--used
    def get_points_on_image_border(self,img_height,img_width,outer_rect_image,
                                   path_output,debug_mode):
        x1,y1,x2,y2 = 0,0,img_width,img_height
        L1, L2, L3, L4 = self.iterative_obj.find_lines_rectangle(x1,y1,x2,y2)

        border_point_data = []
        for b_line in [L1, L2, L3, L4]:
            point_list = self.iterative_obj.get_points_on_line(b_line,3)
            border_point_data.append([b_line,point_list])
            if debug_mode:
                for point in point_list:
                    cv2.circle(outer_rect_image,(tuple(point)),15,(255,0,0),-1)
        if debug_mode:
            cv2.rectangle(outer_rect_image, (x1, y1), (x2, y2), (0, 0, 0), 10)
            cv2.imwrite(path_output + '-6step.png', outer_rect_image)
        return border_point_data

    #--used
    def get_outermost_cont_lines(self,border_point_data,line_lists,outer_rect_image,
                                 path_output,img_height,img_width,debug_mode):
        # cv2.imwrite(path_output + '-1-original.png', outer_rect_image)
        outermost_cont_lines = []
        for list_num,current_list in enumerate(line_lists):
            for line_num, current_line in enumerate(current_list):
                cx, cy = self.iterative_obj.find_centre_of_line(current_line)
                outer_cont_flag = False
                for border_data in border_point_data:
                    border_line = border_data[0]
                    perpendicular_line = self.iterative_obj.get_shortest_path_from_point_to_line([cx,cy],border_line)
                    if len(perpendicular_line)>1:
                        # ---check if perpendicular_line intersects with other contour lines
                        intersection_flag,int_pt = self.check_LOS_for_line(perpendicular_line,
                                            line_lists,list_num, line_num,[],all_lines=False)
                        if intersection_flag == False:
                            outermost_cont_lines.append([current_line,list_num,line_num])
                            outer_cont_flag = True
                            if debug_mode:
                                cv2.line(outer_rect_image, (tuple(perpendicular_line[0])),
                                     (tuple(perpendicular_line[1])), (0, 0, 0), 2, cv2.CV_AA)
                            break
                if outer_cont_flag == False:
                    # ---check if selected border point and current line connectors intersect with contour lines
                    # if outer_cont_flag==False:
                    for border_data in border_point_data:
                        border_points = border_data[1]
                        for border_point in border_points:
                            if debug_mode:
                                cv2.circle(outer_rect_image, (tuple(border_point)), 15, (255, 0, 0), -1)
                            line_to_check = [border_point,[cx,cy]]
                            intersection_flag,int_pt = self.check_LOS_for_line(line_to_check,
                                                     line_lists, list_num, line_num,[],all_lines=False)
                            if intersection_flag == False:
                                outermost_cont_lines.append([current_line, list_num, line_num])
                                outer_cont_flag = True
                                if debug_mode:
                                    cv2.line(outer_rect_image, (tuple(line_to_check[0])),
                                             (tuple(line_to_check[1])), (255, 0, 0), 5, cv2.CV_AA)
                                break
                        if outer_cont_flag:
                            break

                # ---extend current line and see if it meets border without intersecting other contour lines
                if outer_cont_flag == False:
                    invalid_points = []
                    if line_num == 0:
                        invalid_points.append(current_list[line_num+1][0])
                    elif line_num == len(current_list) - 1:
                        invalid_points.append(current_list[line_num-1][1])
                    else:
                        invalid_points.append(current_list[line_num + 1][0])
                        invalid_points.append(current_list[line_num - 1][1])

                    extended_lines = self.extend_current_line_to_image_border(current_line,
                                         img_height,img_width,border_point_data)
                    for ext_line in extended_lines:
                        intersection_flag,int_pt = self.check_LOS_for_line(ext_line,
                                  line_lists, list_num, line_num,invalid_points,all_lines=False)
                        if intersection_flag == False:
                            outermost_cont_lines.append([current_line,list_num,line_num])
                            outer_cont_flag = True
                            if debug_mode:
                                cv2.line(outer_rect_image, (tuple(ext_line[0])),
                                         (tuple(ext_line[1])), (0, 255, 0), 2, cv2.CV_AA)
                            break

        if debug_mode:
            outer_conts = ~(np.zeros((img_height, img_width, 3), np.uint8))
            for out_cont in outermost_cont_lines:
                o_line = out_cont[0]
                cv2.line(outer_conts, (tuple(o_line[0])),
                         (tuple(o_line[1])), (0, 0, 0), 5, cv2.CV_AA)
            cv2.imwrite(path_output + '-7step-3Filter.png', outer_conts)

            cv2.imwrite(path_output+'-7step-3Filter.png',outer_rect_image)

        return outermost_cont_lines

    # --used
    def check_LOS_for_line(self,check_line,line_lists,cur_list_num,
                           cur_line_num,invalid_points,all_lines):
        intersection_flag = False
        intersect_lines = []
        # -- check if perpendicular_line lntersects with any line
        x1, y1 = check_line[0]
        x2, y2 = check_line[1]

        max_sx = max(x1, x2)
        min_sx = min(x1, x2)
        max_sy = max(y1, y2)
        min_sy = min(y1, y2)

        for list_num,current_list in enumerate(line_lists):
            for line_num, current_line in enumerate(current_list):
                if list_num == cur_list_num and line_num == cur_line_num:
                    continue
                else:
                    x3, y3 = current_line[0]
                    x4, y4 = current_line[1]
                    max_cx = max(x3, x4)
                    min_cx = min(x3, x4)
                    max_cy = max(y3, y4)
                    min_cy = min(y3, y4)
                    intersect_x, intersect_y = self.iterative_obj.find_lines_intersection(check_line, current_line)
                    invalid_point_flag = False
                    if len(invalid_points) > 0:
                        for inv_point in invalid_points:
                            inv_x, inv_y = inv_point
                            if intersect_x==inv_x and intersect_y==inv_y:
                                invalid_point_flag = True
                                break

                    if invalid_point_flag==False:
                        if (intersect_x >= min_sx and intersect_x <= max_sx and intersect_y >= min_sy and intersect_y <= max_sy) and (
                                intersect_x >= min_cx and intersect_x <= max_cx and intersect_y >= min_cy and intersect_y <= max_cy):
                            intersection_flag = True
                            intersect_lines.append(current_line)
                            if all_lines == False:
                                break

        return intersection_flag,intersect_lines

    # --used
    def extend_current_line_to_image_border(self,line,img_height,img_width,border_point_data):
        extended_lines = []
        x1, y1 = line[0]
        x2, y2 = line[1]
        #---if vertical line
        if x1==x2:
            p2 = [x1,0]
            p3 = [x1, img_height]
            extended_lines.append([line[0],p2])
            extended_lines.append([line[0], p3])
        #-- if horizontal line
        elif y1==y2:
            p2 = [0, y1]
            p3 = [img_width, y1]
            extended_lines.append([line[0], p2])
            extended_lines.append([line[0], p3])
        #-- if slanted line
        else:
            intersection_points = []
            for border_data in border_point_data:
                border_line = border_data[0]
                x3,y3 = border_line[0]
                x4,y4 = border_line[1]
                max_cx = max(x3, x4)
                min_cx = min(x3, x4)
                max_cy = max(y3, y4)
                min_cy = min(y3, y4)
                intersect_x, intersect_y = self.iterative_obj.find_lines_intersection(line, border_line)
                if (intersect_x >= min_cx and intersect_x <= max_cx and intersect_y >= min_cy and intersect_y <= max_cy):
                    intersection_points.append([intersect_x, intersect_y])
            #--- if border intersection is at a border corner might get 3 border lines
            #--- intersecting with current line, so re-check to reduce repeat
            minimized_intersection_points = []
            if len(intersection_points)>2:
                for r1, row1 in enumerate(intersection_points):
                    for r2, row2 in enumerate(intersection_points):
                        if r2 > r1:
                            x1,y1 = row1
                            x2,y2 = row2
                            if x1==x2 and y1==y2:
                                continue
                            else:
                                minimized_intersection_points.append(row1)

            for row in minimized_intersection_points:
                extended_lines.append([line[0], row])

        return extended_lines

    #--used
    def find_short_line_segs(self,line_lists,img_height,img_width,path_output,debug_mode):
        shortest_conts = ~(np.zeros((img_height, img_width, 3), np.uint8))
        # wall_threshold = self.avg_wall_width / 1.5
        wall_threshold = self.avg_wall_width / 1.5
        shortest_line_sets = []
        for ls_index,cont_line_set in enumerate(line_lists):
            shortest_line_set = []
            for l_index,cont_line in enumerate(cont_line_set):
                x1,y1 = cont_line[0]
                x2,y2 = cont_line[1]
                cv2.line(shortest_conts,(x1,y1),(x2,y2),(0,0,0),5,cv2.CV_AA)
                length = math.hypot(x2-x1, y2-y1)
                if abs(self.avg_wall_width - length) < wall_threshold:
                    shortest_line_set.append([cont_line,[ls_index,l_index],
                                              len(cont_line_set)])
            shortest_line_sets.append(shortest_line_set)

        if debug_mode:
            shortest_copy = shortest_conts.copy()
            for line_set in shortest_line_sets:
                for s_line in line_set:
                    x1,y1 = s_line[0][0]
                    x2,y2 = s_line[0][1]
                    cv2.line(shortest_copy, (x1, y1), (x2, y2), (0, 255, 0), 10, cv2.CV_AA)
            #
            cv2.imwrite(path_output + '-8step-shortest.png', shortest_copy)

        return shortest_line_sets,shortest_conts

    #--used
    def find_outer_cont_shortest_segs(self,shortest_line_sets,outermost_cont_lines,
                                 shortest_conts,path_output,debug_mode):
        if debug_mode:
            shortest_conts_copy2 = shortest_conts.copy()
        outer_cont_shortest_seg_list = []
        for short_set in shortest_line_sets:
            for short_line_data in short_set:
                short_line = short_line_data[0]
                # x1, y1 = short_line[0]
                # x2, y2 = short_line[1]

                for out_line_data in outermost_cont_lines:
                    out_line = out_line_data[0]
                    x3,y3 = out_line[0]
                    x4,y4 = out_line[1]
                    if debug_mode:
                        cv2.line(shortest_conts_copy2, (x3, y3), (x4, y4), (255, 0, 0), 10, cv2.CV_AA)

                    if self.lines_sharing_point(out_line,short_line):
                    #     pass
                    # if self.iterative_obj.find_similarity_two_lines_ordered(short_line,out_line):
                    # if (x1==x3 and y1==y3) or (x1==x4 and y1==y4) or (
                    #         x2 == x3 and y2 == y3) or (x2 == x4 and y2 == y4) :
                        cx, cy = self.iterative_obj.find_centre_of_line(short_line)
                        outer_cont_shortest_seg_list.append([short_line,[cx, cy],
                                                        short_line_data[1],short_line_data[2]])
                        # print short_line,short_line_data[1]
                        x1,y1 = short_line[0]
                        x2,y2 = short_line[1]
                        if debug_mode:
                            cv2.line(shortest_conts_copy2, (x1, y1), (x2, y2), (0, 255, 0), 5, cv2.CV_AA)
                        break
        if debug_mode:
            cv2.imwrite(path_output + '-9step-2shortest.png', shortest_conts_copy2)

        return outer_cont_shortest_seg_list,shortest_conts

    #--used
    def lines_sharing_point(self,line1,line2):
        #--main lne
        x1, y1 = line1[0]
        x2, y2 = line1[1]
        #--sub line
        x3, y3 = line2[0]
        x4, y4 = line2[1]

        if (x1 == x3 and y1 == y3) or (x2 == x4 and y2 == y4):
            return True
        elif (x1 == x3 and y1 == y3) or (x1 == x4 and y1 == y4) or (
                x2 == x3 and y2 == y3) or (x2 == x4 and y2 == y4):
            return True
        else:
            return False

    #---used
    def order_shortest_lines(self,outer_cont_shortest_seg_list,line_lists,
                             shortest_conts, path_output,debug_mode):
        for row in outer_cont_shortest_seg_list:
            current_line = row[0]
            wall_cont, wall_line = row[2]
            angle_weight, wall_len_weight = 0.5, 0.5

            beside_lines = self.find_immediate_lines(line_lists, wall_cont, wall_line)
            beside_angles = []
            for l1, each_side_line in enumerate(beside_lines):
                angle = self.iterative_obj.find_angle_of_line(each_side_line)
                beside_angles.append(angle)

            angle_diff = abs(beside_angles[0] - beside_angles[1])
            angle_diff_no_direction = abs(angle_diff - 180)
            wall_angle_difference = min(angle_diff, angle_diff_no_direction)
            # --calculate penalty
            penalty_angle = wall_angle_difference / 90

            x1,y1 = current_line[0]
            x2,y2 = current_line[1]
            wall_length = math.hypot(x2-x1,y2-y1)
            wall_length_diff = abs(wall_length - 2 / 3 * self.avg_wall_width)
            penalty_wall_length = wall_length_diff / self.avg_wall_width

            total_penalty = (penalty_angle*angle_weight)+(
                    penalty_wall_length*wall_len_weight)

            row.append(round(wall_length,2))
            row.append(total_penalty)

        outer_cont_shortest_seg_list.sort(key=itemgetter(5))
        sorted_shortest_lines = outer_cont_shortest_seg_list

        if debug_mode:
            for d_line in sorted_shortest_lines:
                centre_line = d_line[0]
                x1, y1 = centre_line[0]  # -- p1
                x2, y2 = centre_line[1]  # -- p2
                cv2.line(shortest_conts, (x1, y1), (x2, y2), (0, 255, 0), 10, cv2.CV_AA)


            cv2.imwrite(path_output + '-10step-shortest lines_ordered.png', shortest_conts)

        return sorted_shortest_lines

    # --used
    def close_contour_method_01(self,sorted_shortest_lines,line_lists,simplified_contours,
                                shortest_conts,path_output,debug_mode):
        paired_shortest_segs, pair_map, connection_data = self.connect_shortest_segs(sorted_shortest_lines,
                                                                                     simplified_contours, line_lists,
                                                                                     shortest_conts,
                                                                                     path_output)
        door_line_list_main = self.filter_doors(paired_shortest_segs, line_lists, pair_map,
                                                sorted_shortest_lines,shortest_conts,
                                                path_output,debug_mode)
        return door_line_list_main,connection_data

    #---used
    def connect_shortest_segs(self,sorted_shortest_lines,simplified_contours,
                              line_lists,shortest_conts,path_output):
        paired_shortest_segs = []
        pair_map = [[-1 for column in range(len(sorted_shortest_lines))
                     ] for row in range(len(sorted_shortest_lines))]
        connection_data = [0 for row in range(len(sorted_shortest_lines))]
        for l1, short_line_data1 in enumerate(sorted_shortest_lines):
            # print short_line_data1
            short_line1 = short_line_data1[0]
            cx1,cy1 = short_line_data1[1]
            ls_index1, l_index1 = short_line_data1[2]
            cont1_line_count = short_line_data1[3]
            short_line1_len = short_line_data1[4]
            short_line1_penalty = short_line_data1[5]
            for l2, short_line_data2 in enumerate(sorted_shortest_lines):
                if l1 < l2:
                    # print short_line_data2
                    short_line2 = short_line_data2[0]
                    cx2, cy2 = short_line_data2[1]
                    ls_index2, l_index2 = short_line_data2[2]
                    cont2_line_count = short_line_data2[3]
                    short_line2_len = short_line_data2[4]
                    short_line2_penalty = short_line_data2[5]
                    # -- to prevent connections between lines that are close to each other
                    if ls_index1 == ls_index2 and abs(l_index1-l_index2) < self.min_contour_difference:
                        distance = self.iterative_obj.get_contour_length_between_lines(line_lists[ls_index1],
                                                                            l_index1,l_index2)
                        #---even for cont lines with less than 8 conts between them
                        #---take if length of lines between them is high
                        if distance > self.avg_door_width*5:
                            paired_shortest_segs, pair_map, connection_data = self.make_connections(short_line_data1,
                                                                                                    short_line_data2,
                                                                                                    line_lists,
                                                                                                    simplified_contours,
                                                                                                    paired_shortest_segs,
                                                                                                    pair_map,
                                                                                                    connection_data, l1,
                                                                                                    l2)
                        else:
                            continue

                    else:
                        paired_shortest_segs, pair_map, connection_data = self.make_connections(short_line_data1,
                                                                                                short_line_data2,
                                                                                                line_lists,
                                                                                                simplified_contours,
                                                                                                paired_shortest_segs,
                                                                                                pair_map,
                                                                                                connection_data, l1, l2)
            # --- when theres >1 candidate door for same shortest seg
            # --- find candidate door line that is closest in length to avg door width and take only that
            # if len(candidate_doors_2)>0:
            #     candidate_doors_2.sort(key=itemgetter(3))
            #     candidate_doors.append(candidate_doors_2[0])
        # for d_line in paired_shortest_segs:
        #     centre_line = d_line[0]
        #     x1, y1 = centre_line[0]  # -- p1
        #     x2, y2 = centre_line[1]  # -- p2
        #     cv2.line(shortest_conts, (x1, y1), (x2, y2), (255, 0, 0), 5, cv2.CV_AA)
        #
        #
        # cv2.imwrite(path_output + '-doors_v0.png', shortest_conts)

        return paired_shortest_segs,pair_map,connection_data

    #--used
    def make_connections(self,short_line_data1,short_line_data2,line_lists,simplified_contours,
                         paired_shortest_segs,pair_map,connection_data,l1,l2):
        short_line1 = short_line_data1[0]
        cx1, cy1 = short_line_data1[1]
        ls_index1, l_index1 = short_line_data1[2]
        cont1_line_count = short_line_data1[3]
        short_line1_len = short_line_data1[4]
        short_line1_penalty = short_line_data1[5]

        short_line2 = short_line_data2[0]
        cx2, cy2 = short_line_data2[1]
        ls_index2, l_index2 = short_line_data2[2]
        cont2_line_count = short_line_data2[3]
        short_line2_len = short_line_data2[4]
        short_line2_penalty = short_line_data2[5]


        current_lines = [short_line1, short_line2]
        distance = math.hypot(cx2 - cx1, cy2 - cy1)
        if distance < self.door_threshold:
            possible_line = [[cx1, cy1], [cx2, cy2]]
            cont_line_intersect_count = 0

            intersect_flag, intersect_lines = self.check_LOS_for_line(possible_line,
                                                line_lists, -1, -1, [], all_lines=True)
            if intersect_flag:
                for int_line in intersect_lines:
                    i_x1, i_y1 = int_line[0]
                    i_x2, i_y2 = int_line[1]
                    invalid_line_flag = False
                    for sline in current_lines:
                        x1, y1 = sline[0]
                        x2, y2 = sline[1]
                        if (i_x1 == x1 and i_y1 == y1 and i_x2 == x2 and i_y2 == y2):
                            invalid_line_flag = True
                            break
                    if invalid_line_flag == False:
                        cont_line_intersect_count = cont_line_intersect_count + 1

            # --- check if new possible line is inside a contour:
            if cont_line_intersect_count == 0:
                num_of_points_inside_contour = 0
                for cnt in simplified_contours:
                    num_of_points_inside_contour = self.iterative_obj.check_line_inside_contour(cx1, cy1, cx2,
                                                                                                cy2, cnt)
                    if num_of_points_inside_contour > 0:
                        break

                if num_of_points_inside_contour == 0:
                    short_line1_data = [short_line1, ls_index1, l_index1,
                                        cont1_line_count, short_line1_len, short_line1_penalty]
                    short_line2_data = [short_line2, ls_index2,
                                        l_index2, cont2_line_count, short_line2_len, short_line2_penalty]
                    paired_shortest_segs.append([possible_line, short_line1_data,
                                                 short_line2_data, abs(self.avg_door_width - distance)])
                    pair_map[l1][l2] = len(paired_shortest_segs) - 1
                    pair_map[l2][l1] = len(paired_shortest_segs) - 1
                    connection_data[l1], connection_data[l2] = 1, 1

        return paired_shortest_segs,pair_map,connection_data


    #--used
    def filter_doors(self, paired_shortest_segs, line_lists, pair_map,sorted_shortest_lines,
                     shortest_conts, path_output,debug_mode):

        door_line_list_v1,pair_map2 = self.filter_multiple_doors_from_same_wall(pair_map,
                                            paired_shortest_segs,line_lists,sorted_shortest_lines)

        door_line_list_v2 = self.filter_multiple_doors_from_same_wall_2(door_line_list_v1,pair_map2,
                       paired_shortest_segs,sorted_shortest_lines,shortest_conts, path_output,line_lists)

        door_line_list_v3 = self.filter_doors_from_closeby_walls(door_line_list_v2,
                                       line_lists,self.closeby_cont_pair_diff)

        if debug_mode:
            shortest_conts_copy1 = shortest_conts.copy()
            for d_line in paired_shortest_segs:
                centre_line = d_line[0]
                x1, y1 = centre_line[0]  # -- p1
                x2, y2 = centre_line[1]  # -- p2
                cv2.line(shortest_conts_copy1, (x1, y1), (x2, y2), (0, 0, 255), 5, cv2.CV_AA)
            cv2.imwrite(path_output + '-doors_v0.png', shortest_conts_copy1)

            shortest_conts_copy1 = shortest_conts.copy()
            for d_line in door_line_list_v1:
                centre_line = d_line[0]
                x1, y1 = centre_line[0]  # -- p1
                x2, y2 = centre_line[1]  # -- p2
                cv2.line(shortest_conts_copy1, (x1, y1), (x2, y2), (0, 0, 255), 5, cv2.CV_AA)
            cv2.imwrite(path_output + '-doors_v1.png', shortest_conts_copy1)
            #
            shortest_conts_copy2 = shortest_conts.copy()
            for d_line in door_line_list_v2:
                centre_line = d_line[0]
                x1, y1 = centre_line[0]  # -- p1
                x2, y2 = centre_line[1]  # -- p2
                cv2.line(shortest_conts_copy2, (x1, y1), (x2, y2), (0, 0, 255), 5, cv2.CV_AA)
            cv2.imwrite(path_output + '-doors_v2.png', shortest_conts_copy2)
            #
            for d_line in door_line_list_v3:
                centre_line = d_line[0]
                x1, y1 = centre_line[0]  # -- p1
                x2, y2 = centre_line[1]  # -- p2
                cv2.line(shortest_conts, (x1, y1), (x2, y2), (0, 0, 255), 5, cv2.CV_AA)
            cv2.imwrite(path_output + '-doors-M1.png', shortest_conts)

        return door_line_list_v3

    #--used
    def filter_multiple_doors_from_same_wall(self,pair_map,paired_shortest_segs,
                                             line_lists,sorted_shortest_lines):
        door_line_list_v1 = []
        pair_map2 = [[-1 for column in range(len(pair_map))
                     ] for row in range(len(pair_map))]
        for row_num, pair_data in enumerate(pair_map):
            col_count = row_num+1#--since we need par_map[x:] to not to include 'x'
            # --count connections in columns > row number - to get only half of list
            row_range_after = pair_data[row_num:].count(-1)
            no_of_connections_after = len(pair_data[row_num:]) - row_range_after
            indexes_int_row_after = [[ind+row_num,row] for ind,row in enumerate(pair_data[row_num:]) if row != -1]

            if no_of_connections_after > 1:
                # print sorted_shortest_lines[row_num][1]
                # print no_of_connections_after,'no_of_connections_after'
                penalty_list = []
                for each_int_row in indexes_int_row_after:
                    index, row = each_int_row
                    current_row = paired_shortest_segs[row]
                    current_penalty = self.calculate_penalty(current_row, line_lists)
                    penalty_list.append(current_penalty)
                    # print '-----',sorted_shortest_lines[index][1],index, current_penalty

                min_penalty = min(penalty_list)
                min_penalty_indices = [index for index, element in enumerate(penalty_list)
                                       if min_penalty == element]
                min_index = min_penalty_indices[0]
                pair_map_index,pair_map_value = indexes_int_row_after[min_index]
                best_line_data = paired_shortest_segs[pair_map_value]
                door_line_list_v1.append(best_line_data)
                pair_map2[row_num][pair_map_index] = pair_map_value
                pair_map2[pair_map_index][row_num] = pair_map_value
                # print 'bl- ',best_line_data[0]

            else:
                interested_rows = [[ind+row_num,row] for ind,row in enumerate(pair_data[row_num:]) if row != -1]
                if len(interested_rows) > 0:
                    # print sorted_shortest_lines[row_num][1]
                    # print 'only 1- ',no_of_connections_after
                    pair_map_index, pair_map_value = interested_rows[0]
                    best_line_data = paired_shortest_segs[pair_map_value]
                    door_line_list_v1.append(best_line_data)
                    pair_map2[row_num][pair_map_index] = pair_map_value
                    pair_map2[pair_map_index][row_num] = pair_map_value
                    # print '*****',sorted_shortest_lines[pair_map_index][1],pair_map_index
                    # print 'bl- ', best_line_data[0]

        return door_line_list_v1,pair_map2

    # --used
    def filter_multiple_doors_from_same_wall_2(self,door_line_list_v1,pair_map2,
                      paired_shortest_segs,sorted_shortest_lines,shortest_conts, path_output,line_lists):
        # print '***********************attempt 2'
        door_line_list_v2 = []
        checked_row = []
        for row_num, pair_data in enumerate(pair_map2):
            if row_num in checked_row:
                continue
            else:
                # --count connections in columns > row number - to get only half of list
                row_range_after = pair_data[row_num:].count(-1)
                no_of_connections_after = len(pair_data[row_num:]) - row_range_after
                indexes_int_row_after = [[ind+row_num, row] for ind, row in enumerate(pair_data[row_num:]) if row != -1]

                if no_of_connections_after == 1:
                    # print 's1 ',sorted_shortest_lines[row_num][1]
                    # print no_of_connections_after,' no_of_connections_after'
                    index, value = indexes_int_row_after[0]
                    # print 's2 ',sorted_shortest_lines[index][1],index
                    checked_row.append(index)
                    other_row = pair_map2[index]
                    other_row_connections = len(other_row) - other_row.count(-1)
                    other_row_data = [[ind, row] for ind, row in enumerate(other_row) if row != -1]

                    # print other_row_connections
                    if other_row_connections > 1:
                        penalty_list = []
                        for each_row in other_row_data:
                            oth_index, oth_row = each_row
                            current_row = paired_shortest_segs[oth_row]
                            current_penalty = self.calculate_penalty(current_row, line_lists)
                            penalty_list.append(current_penalty)
                            #--remove connection from other mirrored lines,
                            #--since we don't want the connection to be made again
                            pair_map2[oth_index][index] =-1
                            # print '-----',sorted_shortest_lines[oth_index][1],oth_index, current_penalty
                        min_penalty = min(penalty_list)
                        min_penalty_indices = [index for index, element in enumerate(penalty_list)
                                               if min_penalty == element]
                        min_index = min_penalty_indices[0]
                        pair_map_index, pair_map_value = other_row_data[min_index]
                        best_line_data = paired_shortest_segs[pair_map_value]
                        door_line_list_v2.append(best_line_data)
                        #--if best line connects current line with line_x,
                        #-- we want to consider line_x to be fixed ( already has a connection fixed)
                        checked_row.append(pair_map_index)
                        # print 'bl- ', best_line_data[0]
                    else:
                        interested_rows = [[ind+row_num, row] for ind, row in enumerate(pair_data[row_num:]) if row != -1]
                        if len(interested_rows) > 0:
                            pair_map_index, pair_map_value = interested_rows[0]
                            best_line_data = paired_shortest_segs[pair_map_value]
                            door_line_list_v2.append(best_line_data)
                            # print sorted_shortest_lines[row_num][1]
                            # print '*****',sorted_shortest_lines[pair_map_index][1],pair_map_index
                            # print 'bl- ', best_line_data[0]
        return door_line_list_v2

    # --used
    def filter_doors_from_closeby_walls(self,door_line_list_v2,line_lists,cont_thresh):
        # --check to remove doors that connect very closeby contour lines
        door_line_list_v3,options_index = [],[]
        # ln_diff_thresh = 6
        for r1, cand_row1 in enumerate(door_line_list_v2):
            if r1 in options_index:
                continue
            else:
                options = []
                c1_line1_contnum = cand_row1[1][1]
                c1_line2_contnum = cand_row1[2][1]
                # print '--------'
                # print cand_row1[1][0], c1_line1_contnum, cand_row1[1][2]
                # print cand_row1[2][0], c1_line2_contnum, cand_row1[2][2]
                # print '**'
                for r2, cand_row2 in enumerate(door_line_list_v2):
                    if r2 <= r1 or (r2 in options_index):
                        continue
                    else:
                        c2_line1_contnum = cand_row2[1][1]
                        c2_line2_contnum = cand_row2[2][1]

                        # print cand_row2[1][0], c2_line1_contnum, cand_row2[1][2]
                        # print cand_row2[2][0], c2_line2_contnum, cand_row2[2][2]

                        # --if both line sets come from same contour pairs
                        if (c1_line1_contnum == c2_line1_contnum and c1_line2_contnum == c2_line2_contnum):
                            options,optional_flag = self.find_closeby_doors(cand_row1[1],
                                                         cand_row2[1],cand_row1[2],
                                                         cand_row2[2],options,
                                                         cont_thresh,
                                                         cand_row1, cand_row2,line_lists)
                            if optional_flag:
                                options_index.append(r2)
                        elif (c1_line1_contnum == c2_line2_contnum and c1_line2_contnum == c2_line1_contnum):
                            options,optional_flag = self.find_closeby_doors(cand_row1[1],
                                                            cand_row2[2],cand_row1[2],
                                                            cand_row2[1],options,
                                                            cont_thresh,
                                                            cand_row1, cand_row2,line_lists)
                            if optional_flag:
                                options_index.append(r2)
                        #--- special cases since we don't want any 2 lines
                        #--- that are close by in same cont to create door
                        elif c1_line1_contnum == c2_line1_contnum:
                            options, optional_flag =self.single_line_difference(line_lists,
                                cand_row1[1], cand_row2[1],options,cand_row1,cand_row2)
                            if optional_flag:
                                options_index.append(r2)

                        elif c1_line1_contnum == c2_line2_contnum:
                            options, optional_flag =self.single_line_difference(line_lists,
                                cand_row1[1], cand_row2[2],options,cand_row1,cand_row2)
                            if optional_flag:
                                options_index.append(r2)

                        elif c1_line2_contnum == c2_line1_contnum:
                            options, optional_flag =self.single_line_difference(line_lists,
                                cand_row1[2], cand_row2[1],options,cand_row1,cand_row2)
                            if optional_flag:
                                options_index.append(r2)

                        elif c1_line2_contnum == c2_line2_contnum:
                            options, optional_flag =self.single_line_difference(line_lists,
                                cand_row1[2], cand_row2[2],options,cand_row1,cand_row2)
                            if optional_flag:
                                options_index.append(r2)

                # ---sort by door length and take sline closest to avg-door width in length
                if len(options) > 0:
                    options.sort(key=itemgetter(3))
                    door_line_list_v3.append(options[0])
                # --if no options found add the current line
                else:
                    door_line_list_v3.append(cand_row1)

        return door_line_list_v3

    #--used
    def find_closeby_doors(self,line1_data, line2_data, line3_data, line4_data, options,
                           ln_diff_thresh,cand_row1,cand_row2,line_lists):
        self.dw_thresh = self.avg_door_width*4
        line1, line1_contnum, line1_lnum, line1_cont_count = line1_data[0],line1_data[1],line1_data[2],line1_data[3]
        line2, line2_contnum, line2_lnum, line2_cont_count = line2_data[0], line2_data[1], line2_data[2], line2_data[3]
        line3, line3_contnum, line3_lnum, line3_cont_count = line3_data[0], line3_data[1], line3_data[2], line3_data[3]
        line4, line4_contnum, line4_lnum, line4_cont_count = line4_data[0], line4_data[1], line4_data[2], line4_data[3]

        # pair1_length = self.iterative_obj.get_contour_length_between_lines(line_lists[line1_contnum],
        #                                                       line1_lnum,line2_lnum)
        # pair2_length = self.iterative_obj.get_contour_length_between_lines(line_lists[line3_contnum],
        #                                                       line3_lnum, line4_lnum)

        optional_flag = False
        pair1_diff = abs(line1_lnum - line2_lnum)
        pair2_diff = abs(line3_lnum - line4_lnum)

        # if ((pair1_diff < ln_diff_thresh) and (pair2_diff < ln_diff_thresh)) and (
        #     (pair1_length<self.dw_thresh) and (pair2_length<self.dw_thresh)):
            # optional_flag = True
        # elif (abs(line1_cont_count-pair1_diff)<ln_diff_thresh) and (pair2_diff<ln_diff_thresh) and (
        #     (pair1_length<self.dw_thresh) and (pair2_length<self.dw_thresh)
        # ):
        #     optional_flag = True
        # elif pair1_diff < ln_diff_thresh and abs(line3_cont_count-pair2_diff) < ln_diff_thresh and (
        #     (pair1_length<self.dw_thresh) and (pair2_length<self.dw_thresh)
        # ):
        #     optional_flag = True
        # elif abs(line1_cont_count - pair1_diff) < ln_diff_thresh and abs(line3_cont_count - pair2_diff) < ln_diff_thresh and (
        #     (pair1_length<self.dw_thresh) and (pair2_length<self.dw_thresh)
        # ):
        #     optional_flag = True
        # elif (
        #         (pair1_diff<3 and pair1_length<self.dw_thresh) or (
        #         pair2_diff<3 and pair2_length<self.dw_thresh) or (
        #         abs(line1_cont_count - pair1_diff)<3 and pair1_length<self.dw_thresh) or(
        #         abs(line3_cont_count - pair2_diff)<3 and pair2_length<self.dw_thresh)
        #     ):
        #     optional_flag = True
        if (pair1_diff < ln_diff_thresh and line1_cont_count>ln_diff_thresh) and (
                pair2_diff < ln_diff_thresh and line3_cont_count>ln_diff_thresh):
            optional_flag = True
        elif (abs(line1_cont_count-pair1_diff)<ln_diff_thresh and line1_cont_count>ln_diff_thresh) and (
                pair2_diff<ln_diff_thresh and line3_cont_count>ln_diff_thresh):
            optional_flag = True
        elif (pair1_diff < ln_diff_thresh and line1_cont_count>ln_diff_thresh) and (
                abs(line3_cont_count-pair2_diff) < ln_diff_thresh and line3_cont_count>ln_diff_thresh) :
            optional_flag = True
        elif (abs(line1_cont_count - pair1_diff) < ln_diff_thresh and line1_cont_count>ln_diff_thresh) and (
                abs(line3_cont_count - pair2_diff) < ln_diff_thresh and line3_cont_count>ln_diff_thresh):
            optional_flag = True
        elif (pair1_diff<3 and line1_cont_count>ln_diff_thresh) or (
                pair2_diff<3 and line3_cont_count>ln_diff_thresh) or (
                abs(line1_cont_count - pair1_diff)<3 and line1_cont_count>ln_diff_thresh) or (
                abs(line3_cont_count - pair2_diff)<3 and line3_cont_count>ln_diff_thresh):
            optional_flag = True

        if optional_flag:
            if len(options) == 0:
                options.append(cand_row1)
            options.append(cand_row2)

        return options,optional_flag

    #--used
    def single_line_difference(self,line_lists,line1_data,line2_data,options,
                               cand_row1,cand_row2):
        line1, line1_contnum, line1_lnum, line1_cont_count = line1_data[0], line1_data[1], line1_data[2], \
                                                             line1_data[3]
        line2, line2_contnum, line2_lnum, line2_cont_count = line2_data[0], line2_data[1], line2_data[2], \
                                                             line2_data[3]
        optional_flag = False
        pair_diff = abs(line1_lnum - line2_lnum)

        if (pair_diff<2) or abs(line1_cont_count-pair_diff)<2 or abs(
            line2_cont_count-pair_diff)<2:
            optional_flag = True

            if len(options) == 0:
                options.append(cand_row1)
            options.append(cand_row2)

        return options,optional_flag

    #--used
    def calculate_penalty(self,paired_seg_row,line_lists):
        door_line = paired_seg_row[0]
        wall1_data = paired_seg_row[1]
        wall2_data = paired_seg_row[2]
        angle_weight, wall_len_weight = 0.8, 0.2

        # ---get angle differences between walls beside door connectors and door line
        # print '---wall 1--',wall1_data[0]
        p_door_wall1_angle_difference = self.get_angle_diff_penalty(line_lists,
                                                  wall1_data,door_line)
        p_wall1_length = self.get_wall_length_penalty(wall1_data)
        p_wall_1 = (p_door_wall1_angle_difference*angle_weight)+(p_wall1_length*wall_len_weight)

        # print 'angle ',p_door_wall1_angle_difference
        # print 'wall ', p_wall1_length
        # print 'penalties ',p_door_wall1_angle_difference*angle_weight,p_wall1_length*wall_len_weight
        # print 'total', p_wall_1

        # print '---wall 2--',wall2_data[0]
        p_door_wall2_angle_difference = self.get_angle_diff_penalty(line_lists,
                                                     wall2_data,door_line)
        p_wall2_length = self.get_wall_length_penalty(wall2_data)
        p_wall_2 = (p_door_wall2_angle_difference * angle_weight) + (p_wall2_length * wall_len_weight)
        # print 'angle ', p_door_wall2_angle_difference
        # print 'wall ', p_wall2_length
        # print 'penalties ', p_door_wall2_angle_difference * angle_weight, p_wall2_length * wall_len_weight
        # print 'total', p_wall_2

        door_penalty = (p_wall_1+p_wall_2)/2

        # lge_wall_weight, sml_wall_weight = 0.8, 0.2
        # if p_wall_1 < p_wall_2:
        #     door_penalty = p_wall_1*lge_wall_weight + p_wall_2*sml_wall_weight
        # else:
        #     door_penalty = p_wall_1 * sml_wall_weight + p_wall_2 * lge_wall_weight
        # print '-----------------', door_penalty

        return door_penalty

    #--used
    def get_angle_diff_penalty(self,line_lists,wall_data,door_line):
        # --get wall data
        door_wall_angle_difference = -1
        # door = door_line
        wall_cont = wall_data[1]
        wall_line = wall_data[2]
        beside_lines = self.find_immediate_lines(line_lists, wall_cont, wall_line)
        beside_angles = []
        for l1, each_side_line in enumerate(beside_lines):
            angle = self.iterative_obj.find_angle_of_line(each_side_line)
            beside_angles.append(angle)

        angle_diff = abs(beside_angles[0]-beside_angles[1])
        angle_diff_no_direction = abs(angle_diff-180)

        if angle_diff < 5 or angle_diff_no_direction <5:
            door_angle = self.iterative_obj.find_angle_of_line(door_line)
            door_angle_diff1 = abs(beside_angles[0] - door_angle)
            door_angle_diff2 = abs(beside_angles[1] - door_angle)
            if door_angle_diff1< door_angle_diff2:
                door_angle_diff = door_angle_diff1
            else:
                door_angle_diff = door_angle_diff2

            door_angle_diff_no_direction = abs(door_angle_diff - 180)

            if door_angle_diff < 15 or door_angle_diff_no_direction < 15:
                door_wall_angle_difference = min(door_angle_diff,door_angle_diff_no_direction)

        #--calculate penalty
        if door_wall_angle_difference==-1:
            return 1
        #---/90 to make it unit
        else:
            return door_wall_angle_difference/90

    #--used
    def find_immediate_lines(self,line_list, cont_num, line_num):
        current_cont_lines = line_list[cont_num]
        if line_num==0:
            next_line = current_cont_lines[line_num+1]
            earlier_line = current_cont_lines[-1]
        elif line_num == len(current_cont_lines)-1:
            next_line = current_cont_lines[0]
            earlier_line = current_cont_lines[line_num-1]
        else:
            next_line = current_cont_lines[line_num+1]
            earlier_line = current_cont_lines[line_num-1]
        beside_lines = [earlier_line,next_line]

        return beside_lines

    #--used
    def get_wall_length_penalty(self,wall_data):
        wall_length = wall_data[4]
        wall_length_diff = abs(wall_length-(2/3.0*self.avg_wall_width))
        p_wall_length = wall_length_diff/self.avg_wall_width
        return p_wall_length

    def find_valid_doors(self,candidate_doors,shortest_conts,path_output):
        # --remove doors connecting same lines for candidate doors
        candidate_doors_v1 = []
        for r1, candidate_data1 in enumerate(candidate_doors):
            c1_line = candidate_data1[2][0]
            # --if candidate_data1 is already checked for similarity continue
            if len(candidate_doors_v1) > 0:
                exists_flag = False
                for row in candidate_doors_v1:
                    line1 = row[2][0]
                    exists_flag = self.iterative_obj.find_similarity_two_lines_ordered(c1_line, line1)
                    if exists_flag:
                        break
                if exists_flag:
                    continue
            # ---if candidate_data1 is not already checked for
            similar_list = []
            for r2, candidate_data2 in enumerate(candidate_doors):
                c2_line = candidate_data2[2][0]
                if r2 > r1:
                    similarity_flag = self.iterative_obj.find_similarity_two_lines_ordered(c1_line,
                                                                                           c2_line)
                    if similarity_flag:
                        if len(similar_list) == 0:
                            similar_list.append(candidate_data1)
                        similar_list.append(candidate_data2)
            # --sort similar line seg connecting doors by abs(length-avg_doorwidth)
            if len(similar_list) > 0:
                similar_list.sort(key=itemgetter(3))
                candidate_doors_v1.append(similar_list[0])
            else:
                candidate_doors_v1.append(candidate_data1)


        #--check to remove doors that connect very closeby contour lines
        door_line_list = []
        # cont_ln_diff_thresh = 6
        for r1, cand_row1 in enumerate(candidate_doors_v1):
            options = []
            for r2, cand_row2 in enumerate(candidate_doors_v1):
                if r2 > r1:
                    c1_line1,c1_line1_contnum, c1_line1_lnum,c1_line1_cont_count = cand_row1[1]
                    c1_line2,c1_line2_contnum, c1_line2_lnum,c1_line2_cont_count = cand_row1[2]
                    c2_line1,c2_line1_contnum, c2_line1_lnum,c2_line1_cont_count = cand_row2[1]
                    c2_line2,c2_line2_contnum, c2_line2_lnum,c2_line2_cont_count = cand_row2[2]

                    #--if both line sets come from same contour pairs
                    if c1_line1_contnum==c2_line1_contnum and c1_line2_contnum==c2_line2_contnum:
                        #--if line pairs in original contours are close to each other
                        optional_flag = False
                        line1_diff = abs(c1_line1_lnum-c2_line1_lnum)
                        line2_diff = abs(c1_line2_lnum-c2_line2_lnum)
                        if (line1_diff < self.min_contour_difference) and (
                                line2_diff < self.min_contour_difference):
                            optional_flag = True
                        elif abs(c1_line1_cont_count-line1_diff)<self.min_contour_difference and (
                                line2_diff < self.min_contour_difference):
                            optional_flag = True
                        elif line1_diff<self.min_contour_difference and abs(
                                c1_line2_cont_count - line2_diff)<self.min_contour_difference:
                            optional_flag= True
                        elif abs(c1_line1_cont_count-line1_diff)<self.min_contour_difference and abs(
                                c1_line2_cont_count - line2_diff)<self.min_contour_difference:
                            optional_flag=True

                        if optional_flag:
                            if len(options)==0:
                                options.append(cand_row1)
                            options.append(cand_row2)

            if len(options)>0:
                options.sort(key=itemgetter(3))
                door_line_list.append(options[0])
            else:
                door_line_list.append(cand_row1)



        for d_line in door_line_list:
            # print d_line[1],'--',d_line[2],'--',d_line[3]
            centre_line = d_line[0]
            x1, y1 = centre_line[0]  # -- p1
            x2, y2 = centre_line[1]  # -- p2
            cv2.line(shortest_conts, (x1, y1), (x2, y2), (255, 0, 0), 5, cv2.CV_AA)

        cv2.imwrite(path_output + '-doors.png', shortest_conts)

        return door_line_list

    #---used
    def create_doors(self,door_line_list,door_rect_as_points):
        for row in door_line_list:
            centre_line = row[0]
            cx,cy = self.iterative_obj.find_centre_of_line(centre_line)
            shape_points = []
            line1, line2 = row[1][0], row[2][0]
            shape_points.append(line1[0])
            shape_points.append(line1[1])
            shape_points.append(line2[0])
            shape_points.append(line2[1])

            sorted_points = self.iterative_obj.order_polygon_points(shape_points,[cx,cy])
            door_rect_as_points.append(sorted_points)

        return door_rect_as_points


    def get_union_bbx(self, rotated_bbxes):
        # --get union of all boxes
        xs = [row[0] for row in rotated_bbxes]
        ys = [row[1] for row in rotated_bbxes]

        min_x = min(xs)
        min_y = min(ys)
        max_x = max(xs)
        max_y = max(ys)
        union_box = [min_x, min_y, max_x, max_y]
        return union_box


    #--used
    def find_connected_wall_ends(self,sorted_shortest_lines,
                                 door_line_list,shortest_conts,path_output):
        connected_walls =[0 for row in range(len(sorted_shortest_lines))]
        for w,wall in enumerate(sorted_shortest_lines):
            current_cont, current_line_no = wall[2]
            max_lines = wall[3]
            door_flag = False
            for door_row in door_line_list:
                wall1 = door_row[1]
                wall2 = door_row[2]

                wall1_cont, wall1_line_no = wall1[1],wall1[2]
                wall2_cont, wall2_line_no = wall2[1], wall2[2]
                #--if door cont no == to current line from sorted_shortest_lines
                if current_cont == wall1_cont:
                    #--if max no of lines in current cont is <4 then
                    # we need to only see of door line==current line
                    #--coz otherwise all 4 lines in current cont will get invalid
                    if max_lines>4:
                        door_flag = self.check_wall_connected_to_door(current_line_no,
                                                                  wall1_line_no,max_lines)
                    else:
                        if current_line_no == wall1_line_no:
                            door_flag = True
                        else:
                            door_flag = False
                    if door_flag:
                        break
                elif current_cont == wall2_cont:
                    if max_lines>4:
                        door_flag = self.check_wall_connected_to_door(current_line_no,
                                                                  wall2_line_no, max_lines)
                    else:
                        if current_line_no == wall2_line_no:
                            door_flag = True
                        else:
                            door_flag = False
                    if door_flag:
                        break

            if door_flag:
                connected_walls[w] = 1

        return connected_walls

    #--used
    def check_wall_connected_to_door(self,current_line,
                                     wall_line,max_lines):
        if current_line == wall_line:
            return True
        elif abs(current_line - wall_line) == 1:
            return True
        elif max_lines - abs(current_line - wall_line) == 1:
            return True
        else:
            return False

    #--used
    def check_conotour_close_bb_area_measure(self,door_rect_as_points,
                                             simplified_contours,img_height,img_width,
                                             path_output):
        closed_contour_flag = False
        walls_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
        for cnt in simplified_contours:
            cv2.drawContours(walls_image, [cnt], -1, (0, 0, 0), -1)
        for rect in door_rect_as_points:
            rect_cont = self.iterative_obj.convert_points_to_contour(rect)
            cv2.drawContours(walls_image, [rect_cont], -1, (0, 0, 0), -1)



        gray_image = cv2.cvtColor(walls_image, cv2.COLOR_RGB2GRAY)
        ret, thresh = cv2.threshold(gray_image, 127, 255, 1)
        contours, hierachy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_TC89_KCOS)
        cnt_areas, hull_areas = [],[]

        walls_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
        for r_num, row in enumerate(hierachy[0]):
            if row[3] == -1:
                c = r_num
                cnt = contours[c]

                cv2.drawContours(walls_image,[cnt],-1,(0,0,0),-1)
                hull = cv2.convexHull(cnt)
                cnt_areas.append(cv2.contourArea(cnt))
                hull_areas.append(cv2.contourArea(hull))
                cv2.drawContours(walls_image, [hull], -1, (0, 0, 255), 5)

        ratio = sum(cnt_areas)/sum(hull_areas)
        if ratio > 0.75:
            closed_contour_flag = True

        cv2.imwrite(path_output + '_detect.png', walls_image)
        return closed_contour_flag




    #--used
    def check_if_contour_closed(self, door_rect_as_points, simplified_contours,
                                connected_walls,sorted_shortest_lines,
                                shortest_conts,path_output,
                                img_height, img_width):
        closed_contour_flag = False
        walls_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
        for cnt in simplified_contours:
            cv2.drawContours(walls_image, [cnt], -1, (0, 0, 0), -1)
        for rect in door_rect_as_points:
            rect_cont = self.iterative_obj.convert_points_to_contour(rect)
            cv2.drawContours(walls_image, [rect_cont], -1, (0, 0, 0), -1)

        gray_image = cv2.cvtColor(walls_image, cv2.COLOR_RGB2GRAY)
        ret, thresh = cv2.threshold(gray_image, 127, 255, 1)
        # ----
        contours, hierachy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_TC89_KCOS)

        # walls_image2 = ~(np.zeros((img_height, img_width, 3), np.uint8))
        outer_cont_as_points= []
        for r_num, row in enumerate(hierachy[0]):
            if row[3] == -1:
                c = r_num
                cnt = contours[c]
                outer_cont_as_points.append(self.iterative_obj.get_points_from_contour(cnt))
        #         cv2.drawContours(walls_image2,[cnt],-1,(0,0,0),-1)
        # cv2.imwrite(path_output+'outer_cont.png',walls_image2)


        wall_ends_on_outer_cont = []
        point_distance = self.avg_wall_width/10
        connected_wall_end_data = []
        for r,connection in enumerate(connected_walls):
            wall_end_data = sorted_shortest_lines[r]
            if connection==0:
                wall= wall_end_data[0]
                x1,y1 = wall[0]
                x2,y2 = wall[1]
                for cont_point_set in outer_cont_as_points:
                    for point in cont_point_set:
                        x3,y3 = point
                        if (abs(x1-x3)<point_distance and abs(y1-y3)<point_distance) or (
                           abs(x2 - x3) < point_distance and abs(y2 - y3) < point_distance):
                            wall_ends_on_outer_cont.append(wall_end_data)
                            break
            else:
                connected_wall_end_data.append(wall_end_data)

        # unconnected_wals = []
        # for outer_wall in wall_ends_on_outer_cont:
        #     cont1, line1= outer_wall[2]
        #     cont1_count = outer_wall[3]
        #     unconnectable = False
        #     for connected_wall_end in connected_wall_end_data:
        #         cont2, line2 =connected_wall_end[2]
        #         if (cont1 == cont2) and (abs(line1-line2)==1 or
        #             cont1_count-abs(line1-line2)==1):
        #             unconnectable = True
        #             break
        #     if unconnectable==False:
        #         unconnected_wals.append(outer_wall)




        # shortest_conts_copy = shortest_conts.copy()
        # for row in wall_ends_on_outer_cont:
        #     wall = row[0]
        #     # print wall
        #     cv2.line(shortest_conts_copy,(tuple(wall[0])),(tuple(wall[1])),(255,255,0),10)
        # cv2.imwrite(path_output+'_leftover.png',shortest_conts_copy)

        if len(wall_ends_on_outer_cont) == 0:
            closed_contour_flag = True

        return closed_contour_flag

    #--used
    def close_contour_method_02(self,connection_data,line_lists,sorted_shortest_lines,
                                simplified_contours,door_rect_as_points,
                                door_line_list_main,shortest_conts,path_output,debug_mode):
        door_line_list_addt = self.generate_additional_doors(connection_data, line_lists,
                                                             sorted_shortest_lines, simplified_contours,
                                                             door_rect_as_points,
                                                             shortest_conts, path_output)
        door_line_list_addt_v3 = self.filter_doors_additional(door_line_list_addt,
                                                              line_lists, door_line_list_main, shortest_conts,
                                                              path_output,debug_mode)
        return door_line_list_addt_v3

    #--used
    def generate_additional_doors(self,connection_data,line_lists,sorted_shortest_lines,
                                  simplified_contours,door_rect_as_points,
                                  shortest_conts, path_output):
        door_line_list = []
        for r,connect_row in enumerate(connection_data):
            #---connect_row==0 means no connection
            if connect_row==0:
                wall_end_data = sorted_shortest_lines[r]
                extension_line = self.generate_extension_line(line_lists,wall_end_data)
                valid_door_details = self.find_valid_door(extension_line,line_lists,
                                                          wall_end_data,simplified_contours)
                if len(valid_door_details)>0:
                    door_lines = self.generate_wall_end_for_long_cont_lines(valid_door_details)
                    door_line_list.append(door_lines)

        return door_line_list

    #---used
    def generate_extension_line(self,line_lists,wall_end_data):
        wall_end_lne = wall_end_data[0]
        # print '----', wall_end_lne
        x1, y1 = wall_end_lne[0]
        x2, y2 = wall_end_lne[1]
        x0, y0 = wall_end_data[1]
        cont_num = wall_end_data[2][0]
        line_num = wall_end_data[2][1]

        beside_lines = self.find_immediate_lines(line_lists, cont_num, line_num)
        beside_angles = []
        for side_line in beside_lines:
            x3, y3 = side_line[0]
            x4, y4 = side_line[1]

            # ---check if lines are going the same way as wall end connection
            if x1 == x4 and y1 == y4:
                new_side_line = [[x4, y4], [x3, y3]]
            elif x2 == x4 and y2 == y4:
                new_side_line = [[x4, y4], [x3, y3]]
            else:
                new_side_line = side_line

            angle = self.iterative_obj.find_angle_of_line(new_side_line)
            beside_angles.append(angle)

        average_angle = sum(beside_angles) / len(beside_angles)

        if abs(90-average_angle) < 5 or abs(270-average_angle) <5:
            m_average_line = 'a'
        else:
            m_average_line = math.tan(math.radians(average_angle))

        # --generate new line
        if m_average_line == 'a':
            x, y = x0, (y0 + 50)
        else:
            c = y0 - (m_average_line * x0)
            x = x0 + 50
            y = int((m_average_line * x) + c)

        extension_line = [[x0, y0], [x, y]]

        return extension_line

    #--used
    def find_valid_door(self,extension_line,line_lists,wall_end_data,simplified_contours):
        valid_door_details = []
        #---when creating line 1st point was centre point on line
        x0,y0 = extension_line[0]
        #--wall_end_data = [short_wall,[wallcx,wallcy],[contNo,lneNo],maxContLines,wallLen,penalty]
        # short_wall = wall_end_data[0]
        wall_end_cont_no = wall_end_data[2][0]
        wal_end_line_no = wall_end_data[2][1]

        new_paired_shortest_segs = []
        for c,cont_line_set in enumerate(line_lists):
            for l,single_line in enumerate(cont_line_set):
                if c == wall_end_cont_no and l == wal_end_line_no:
                    continue
                else:
                    x1, y1 = single_line[0]
                    x2, y2 = single_line[1]

                    max_sx = max(x1, x2)
                    min_sx = min(x1, x2)
                    max_sy = max(y1, y2)
                    min_sy = min(y1, y2)
                    intersect_x, intersect_y=self.iterative_obj.find_lines_intersection(extension_line,single_line)
                    #---1. if intersects and intersect point is inside cont line
                    if (intersect_x!=0 and intersect_y!= 0) and (
                            intersect_x >= min_sx and intersect_x <= max_sx and intersect_y >= min_sy and intersect_y <= max_sy):
                        possible_door = [[x0,y0],[intersect_x,intersect_y]]

                        #---2. if possible_door has LOS to selected cont line
                        intersection_flag, intersect_lines = self.check_LOS_for_line(
                            possible_door,line_lists,c,l,[],[])
                        if intersection_flag and len(intersect_lines)==1:

                            #---3. if possible door is NOT inside a contour
                            num_of_points_inside_contour = 0
                            for cnt in simplified_contours:
                                num_of_points_inside_contour = self.iterative_obj.check_line_inside_contour(x0,y0,
                                                                                     intersect_x,intersect_y, cnt)
                                if num_of_points_inside_contour > 0:
                                    break
                            if num_of_points_inside_contour == 0:
                                # --wall_end_data = [short_wall,[wallcx,wallcy],[contNo,lneNo],maxContLines,wallLen,penalty]
                                short_cont_no, short_lne_no = wall_end_data[2]
                                line1_data = [wall_end_data[0],short_cont_no, short_lne_no,wall_end_data[3],
                                              wall_end_data[4],wall_end_data[5]]
                                #--not adding wall len and penalty coz this is
                                #-- a big cont line and doesn't make send to
                                #-- add those data
                                line2_data = [single_line,c,l,
                                              len(cont_line_set),-1,-1]
                                #---4. all condtions ok -> add to list
                                candidate_length = math.hypot(x0-intersect_x,y0-intersect_y)
                                new_paired_shortest_segs.append([possible_door,line1_data,
                                         line2_data,abs(self.avg_door_width-candidate_length),
                                         candidate_length])
                                # short_line1_data = [short_line1, ls_index1, l_index1,
                                #                     cont1_line_count, short_line1_len, short_line1_penalty]
                                # short_line2_data = [short_line2, ls_index2,
                                #                     l_index2, cont2_line_count, short_line2_len, short_line2_penalty]
                                # paired_shortest_segs.append([possible_line, short_line1_data,
                                #                              short_line2_data, abs(self.avg_door_width - distance)])
                                # pair_map[l1][l2] = len(paired_shortest_segs) - 1
                                # pair_map[l2][l1] = len(paired_shortest_segs) - 1

        new_paired_shortest_segs.sort(key=itemgetter(4))

        if len(new_paired_shortest_segs) > 0 and new_paired_shortest_segs[0][-1] < self.door_threshold:
            #-- remove wall length that was added only to get the best door
            valid_door_details.append(new_paired_shortest_segs[0][:-1])

        return valid_door_details

    #--used
    def generate_wall_end_for_long_cont_lines(self,valid_door_detail):
        current_door_data = valid_door_detail[0]
        # --current_door_data = [possible_door,line1_data,line2_data,abs(self.avg_door_width-candidate_length)]
        door = current_door_data[0]
        # short_line1_data = [short_line1, ls_index1, l_index1,
        #                     cont1_line_count, short_line1_len, short_line1_penalty]
        short_wall_data = current_door_data[1]
        short_wall_length = short_wall_data[4]
        long_wall_data = current_door_data[2]
        long_wall= long_wall_data[0]
        d = short_wall_length/2
        #---find m,c of long_wall
        x1,y1 = long_wall[0]
        x2,y2 = long_wall[1]
        m,c = self.iterative_obj.find_mc_of_line(x1,y1,x2,y2)

        # long_wall_door_point is == door[1]
        x0,y0 = door[1]
        if m=='a':
            x3 = x0
            y3 = int(y0+d)

            x4 = x0
            y4 = int(y0-d)

        else:
            p1 = m**2 + 1
            p2 = (2*m)*(c-y0) - (2*x0)
            p3 = ((c-y0)**2) + (x0**2) - (d**2)

            x3 = int(((-1*p2) + math.sqrt((p2**2) - (4*(p1*p3)))) / 2*p1)
            y3 = int((m*x3) + c)

            x4 = int(((-1*p2) - math.sqrt((p2**2) - (4*(p1*p3)))) / 2*p1)
            y4 = int((m*x4) + c)

        new_long_wall = [[x3,y3],[x4,y4]]
        new_wall_data = [new_long_wall,long_wall_data[1],long_wall_data[2],long_wall_data[3],
                         long_wall_data[4],long_wall_data[5]]
        door_lines = [door,short_wall_data,new_wall_data,current_door_data[3]]

        return door_lines


    #--used
    def filter_doors_additional(self,door_line_list_addt_v1,line_lists,door_line_list_main,
                                shortest_conts,path_output,debug_mode):
        door_line_list_addt_v2 = self.filter_doors_from_closeby_walls_2_lists(door_line_list_addt_v1,
                                                     door_line_list_main,line_lists)
        door_line_list_addt_v3 = self.filter_doors_from_closeby_walls(door_line_list_addt_v2,
                                        line_lists,self.closeby_cont_pair_diff)

        if debug_mode:
            # shortest_conts_copy1 = shortest_conts.copy()
            for d_line in door_line_list_addt_v3:
                centre_line = d_line[0]
                x1, y1 = centre_line[0]  # -- p1
                x2, y2 = centre_line[1]  # -- p2
                cv2.line(shortest_conts, (x1, y1), (x2, y2), (255, 0, 0), 5, cv2.CV_AA)
            cv2.imwrite(path_output + '-doors-M2.png', shortest_conts)

        return door_line_list_addt_v3

    #--used
    def filter_doors_from_closeby_walls_2_lists(self,door_line_list_addt_v1,
                                                     door_line_list_main,line_lists):
        door_line_list_addt_v2 = []

        for additional_door in door_line_list_addt_v1:
            cand_row1 = additional_door
            c1_line1_contnum = cand_row1[1][1]
            c1_line2_contnum = cand_row1[2][1]
            #---check 'additional_door' is optional compared to 'main_door'
            optional_flag = False
            for main_door in door_line_list_main:
                cand_row2 = main_door
                c2_line1_contnum = cand_row2[1][1]
                c2_line2_contnum = cand_row2[2][1]

                # --if both line sets come from same contour pairs
                if (c1_line1_contnum == c2_line1_contnum and c1_line2_contnum == c2_line2_contnum):
                    options, optional_flag = self.find_closeby_doors(cand_row1[1],
                                                                     cand_row2[1], cand_row1[2],
                                                                     cand_row2[2], [],
                                                                     self.closeby_cont_pair_diff,
                                                                     cand_row1, cand_row2, line_lists)
                    if optional_flag:
                        break
                elif (c1_line1_contnum == c2_line2_contnum and c1_line2_contnum == c2_line1_contnum):
                    options, optional_flag = self.find_closeby_doors(cand_row1[1],
                                                                     cand_row2[2], cand_row1[2],
                                                                     cand_row2[1], [],
                                                                     self.closeby_cont_pair_diff,
                                                                     cand_row1, cand_row2, line_lists)
                    if optional_flag:
                        break
                # --- special cases since we don't want any 2 lines
                # --- that are close by in same cont to create door
                elif c1_line1_contnum == c2_line1_contnum:
                    options, optional_flag = self.single_line_difference(line_lists,
                                             cand_row1[1], cand_row2[1], [],
                                             cand_row1,cand_row2)
                    if optional_flag:
                        break

                elif c1_line1_contnum == c2_line2_contnum:
                    options, optional_flag = self.single_line_difference(line_lists,
                                               cand_row1[1], cand_row2[2], [],
                                               cand_row1,cand_row2)
                    if optional_flag:
                        break

                elif c1_line2_contnum == c2_line1_contnum:
                    options, optional_flag = self.single_line_difference(line_lists,
                                                 cand_row1[2], cand_row2[1], [],
                                                 cand_row1,cand_row2)
                    if optional_flag:
                        break

                elif c1_line2_contnum == c2_line2_contnum:
                    options, optional_flag = self.single_line_difference(line_lists,
                                                  cand_row1[2], cand_row2[2], [],
                                                  cand_row1,cand_row2)
                    if optional_flag:
                        break

            #--only add if 'additional_door' is not optional
            if optional_flag==False:
                door_line_list_addt_v2.append(additional_door)

        return door_line_list_addt_v2

    #--used
    def check_valid_door(self,short_line_data1,short_line_data2,cont_line_intersect_count,
                         possible_line, line_lists):
        intersect_flag, intersect_lines = self.check_LOS_for_line(possible_line,
                                        line_lists, -1, -1, [], all_lines=True)
        if intersect_flag:
            for int_line in intersect_lines:
                i_x1, i_y1 = int_line[0]
                i_x2, i_y2 = int_line[1]
                invalid_line_flag = False
                for sline in short_line_data1+short_line_data2:
                    x1,y1 = sline[0]
                    x2,y2 = sline[1]
                    if (i_x1 == x1 and i_y1 == y1 and i_x2 == x2 and i_y2 == y2):
                        invalid_line_flag = True
                if invalid_line_flag == False:
                    cont_line_intersect_count = cont_line_intersect_count + 1
        return cont_line_intersect_count

    #--used
    def closing_method_03(self,connected_walls_2,sorted_shortest_lines,line_lists,
                          simplified_contours,shortest_conts,path_output,debug_mode):
        # cv2.imwrite(path_output+'input-M3.png',shortest_conts)

        new_wall_ends = []
        for c, connection in enumerate(connected_walls_2):
            if connection==0:
                new_wall_ends.append(sorted_shortest_lines[c])


        new_doors,invalid_walls = [],[]
        for w1, wall1 in enumerate(new_wall_ends):
            short_line1,centre,cnt_data,cont1_line_count,wall1_len, wall1_penalty = wall1
            cx1, cy1 = centre
            cnt1, l1 = cnt_data
            # if cnt_data in invalid_walls:
            #     continue
            # else:
            pairs_sets = []
            for w2, wall2 in enumerate(new_wall_ends):
                if w2 > w1:
                    short_line2, centre2, cnt_data2, cont2_line_count, wall2_len, wall2_penalty = wall2
                    cx2,cy2 = centre2
                    cnt2,l2 = cnt_data2

                    # if cnt_data2 in invalid_walls:
                    #     continue
                    # else:
                    current_lines = [short_line1, short_line2]
                    possible_door = [[cx1,cy1],[cx2,cy2]]
                    distance = math.hypot(cx2 - cx1, cy2 - cy1)
                    cont_line_intersect_count = 0
                    intersect_flag, intersect_lines = self.check_LOS_for_line(possible_door,
                                                       line_lists, -1, -1, [], all_lines=True)
                    if intersect_flag:
                        for int_line in intersect_lines:
                            i_x1, i_y1 = int_line[0]
                            i_x2, i_y2 = int_line[1]
                            invalid_line_flag = False
                            for sline in current_lines:
                                x1, y1 = sline[0]
                                x2, y2 = sline[1]
                                if (i_x1 == x1 and i_y1 == y1 and i_x2 == x2 and i_y2 == y2):
                                    invalid_line_flag = True
                                    break
                            if invalid_line_flag == False:
                                cont_line_intersect_count = cont_line_intersect_count + 1

                    # --- check if new possible line is inside a contour:
                    num_of_min_points = (distance / 3) / 10
                    if cont_line_intersect_count == 0:
                        num_of_points_inside_contour = 0
                        for cnt in simplified_contours:
                            num_of_points_inside_contour = self.iterative_obj.check_line_inside_contour(cx1,
                                                                      cy1,cx2,cy2,cnt)
                            if num_of_points_inside_contour > num_of_min_points:
                                break

                        if num_of_points_inside_contour < num_of_min_points:
                            short_line1_data = [short_line1, cnt1, l1,
                                                cont1_line_count, wall1_len, wall1_penalty]
                            short_line2_data = [short_line2, cnt2,
                                                l2, cont2_line_count, wall2_len, wall2_penalty]
                            pairs_sets.append([possible_door, short_line1_data,
                                              short_line2_data, abs(self.avg_door_width - distance)])

            if len(pairs_sets)>0:
                # for row in pairs_sets:
                #     print row
                pairs_sets.sort(key=itemgetter(3))
                new_doors.append(pairs_sets[0])

                # selected_element = pairs_sets[0]
                # c1,ln1,mx1 = selected_element[1][1],selected_element[1][2],selected_element[1][3]
                # c2,ln2,mx2 = selected_element[2][1],selected_element[2][2],selected_element[2][3]
                #
                # invalid_walls.append([c1,ln1])
                # invalid_walls.append([c2, ln2])
                #
                # if ln1 ==0:
                #     invalid_walls.append([c1, mx1-1])
                #     invalid_walls.append([c1, ln1+1])
                # elif ln1==mx1-1:
                #     invalid_walls.append([c1, ln1 - 1])
                #     invalid_walls.append([c1, 0])
                # else:
                #     invalid_walls.append([c1, ln1 - 1])
                #     invalid_walls.append([c1, ln1 + 1])
                #
                # if ln2 == 0:
                #     invalid_walls.append([c2, mx2 - 1])
                #     invalid_walls.append([c2, ln2 + 1])
                # elif ln1 == mx2 - 1:
                #     invalid_walls.append([c2, ln2 - 1])
                #     invalid_walls.append([c2, 0])
                # else:
                #     invalid_walls.append([c2, ln2 - 1])
                #     invalid_walls.append([c2, ln2 + 1])

        if debug_mode:
            # shortest_conts_copy = shortest_conts.copy()
            for d_line in new_doors:
                centre_line = d_line[0]
                x1, y1 = centre_line[0]  # -- p1
                x2, y2 = centre_line[1]  # -- p2
                cv2.line(shortest_conts, (x1, y1), (x2, y2), (255, 0, 255), 5, cv2.CV_AA)
            cv2.imwrite(path_output + '-doors-M3.png', shortest_conts)

        return new_doors

    # --used
    def closing_method_04(self,connected_walls,sorted_shortest_lines,line_lists,
                          simplified_contours,shortest_conts,path_output,debug_mode):
        new_wall_ends = []
        for c, connection in enumerate(connected_walls):
            if connection == 0:
                new_wall_ends.append(sorted_shortest_lines[c])

        new_doors, invalid_walls = [], []
        for w1, wall1 in enumerate(new_wall_ends):
            short_line1, centre, cnt_data, cont1_line_count, wall1_len, wall1_penalty = wall1
            cx1, cy1 = centre
            cnt1, l1 = cnt_data
            pairs_sets = []
            for w2, wall2 in enumerate(new_wall_ends):
                if w2 > w1:
                    short_line2, centre2, cnt_data2, cont2_line_count, wall2_len, wall2_penalty = wall2
                    cx2, cy2 = centre2
                    cnt2, l2 = cnt_data2

                    ignorable_lines = [short_line1, short_line2]
                    beside_line_set1 = self.find_immediate_lines(line_lists,cnt1,l1)
                    beside_line_set2 = self.find_immediate_lines(line_lists, cnt2, l2)
                    ignorable_lines = ignorable_lines+beside_line_set1+beside_line_set2

                    possible_door = [[cx1, cy1], [cx2, cy2]]
                    distance = math.hypot(cx2 - cx1, cy2 - cy1)
                    cont_line_intersect_count = 0
                    intersect_flag, intersect_lines = self.check_LOS_for_line(possible_door,
                                                                              line_lists, -1, -1, [], all_lines=True)
                    if intersect_flag:
                        for int_line in intersect_lines:
                            i_x1, i_y1 = int_line[0]
                            i_x2, i_y2 = int_line[1]
                            invalid_line_flag = False
                            for sline in ignorable_lines:
                                x1, y1 = sline[0]
                                x2, y2 = sline[1]
                                if (i_x1 == x1 and i_y1 == y1 and i_x2 == x2 and i_y2 == y2):
                                    invalid_line_flag = True
                                    break
                            if invalid_line_flag == False:
                                cont_line_intersect_count = cont_line_intersect_count + 1

                    # --- check if new possible line is inside a contour:
                    num_of_min_points = (distance / 3) / 10
                    if cont_line_intersect_count == 0:
                        num_of_points_inside_contour = 0
                        for cnt in simplified_contours:
                            num_of_points_inside_contour = self.iterative_obj.check_line_inside_contour(cx1,
                                                                                                        cy1,
                                                                                                        cx2,
                                                                                                        cy2,
                                                                                                        cnt)
                            if num_of_points_inside_contour > num_of_min_points:
                                break

                        if num_of_points_inside_contour < num_of_min_points:
                            short_line1_data = [short_line1, cnt1, l1,
                                                cont1_line_count, wall1_len, wall1_penalty]
                            short_line2_data = [short_line2, cnt2,
                                                l2, cont2_line_count, wall2_len, wall2_penalty]
                            pairs_sets.append([possible_door, short_line1_data,
                                               short_line2_data, abs(self.avg_door_width - distance)])
            if len(pairs_sets) > 0:
                pairs_sets.sort(key=itemgetter(3))
                new_doors.append(pairs_sets[0])


        if debug_mode:
            for d_line in new_doors:
                centre_line = d_line[0]
                x1, y1 = centre_line[0]  # -- p1
                x2, y2 = centre_line[1]  # -- p2
                cv2.line(shortest_conts, (x1, y1), (x2, y2), (0, 255, 255), 5, cv2.CV_AA)
            cv2.imwrite(path_output + '-doors-M4.png', shortest_conts)

        return new_doors

    # --used
    def keep_only_outer_doors(self,all_doors,simplified_contours,door_rect_as_points,
                              img_height,img_width,path_output):
        walls_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
        for cnt in simplified_contours:
            cv2.drawContours(walls_image,[cnt],-1,(0,0,0),-1)
        for d_line in all_doors:
            centre_line = d_line[0]
            x1, y1 = centre_line[0]  # -- p1
            x2, y2 = centre_line[1]  # -- p2
            cv2.line(walls_image, (x1, y1), (x2, y2), (0, 0, 0), 1, cv2.CV_AA)
        # cv2.imwrite(path_output + '-closed.png', walls_image)
        # for rect in door_rect_as_points:
        #     rect_cont = self.iterative_obj.convert_points_to_contour(rect)
        #     cv2.drawContours(walls_image,[rect_cont],-1,(0,0,0),-1)


        gray_image = cv2.cvtColor(walls_image, cv2.COLOR_RGB2GRAY)
        ret, thresh = cv2.threshold(gray_image, 127, 255, 1)
        contours, hierachy = cv2.findContours(thresh, cv2.RETR_TREE, 2)

        outer_rect_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
        outer_cont,outer_lines = [],[]
        for r_num, row in enumerate(hierachy[0]):
            if row[3] == -1:
                current_cont = contours[r_num]
                cv2.drawContours(outer_rect_image, [current_cont], -1, (0, 0, 0), -1,cv2.cv.CV_AA)
                outer_cont = contours[r_num]
        #
        # gray = cv2.cvtColor(outer_rect_image, cv2.COLOR_RGB2GRAY)
        # corners = cv2.goodFeaturesToTrack(gray, 200, 0.27, self.avg_wall_width / 10)
        # corners = np.int0(corners)
        # new_corners = []
        # for i in corners:
        #     x, y = i.ravel()
        #     new_corners.append([x, y])
        #
        # cont_point_list = self.iterative_obj.get_points_from_contour(outer_cont)
        # for num, r in enumerate(cont_point_list):
        #     cont_point_list[num].append([])
        #
        # # ----4. order corner points based on contour
        # for p_index, point in enumerate(new_corners):
        #     x1, y1 = point
        #     closest_index, min_distance = -1, 1000
        #     for index, cont_point in enumerate(cont_point_list):
        #         x2, y2 = cont_point[0], cont_point[1]
        #         distance = math.hypot(x2 - x1, y2 - y1)
        #         if distance < min_distance:
        #             closest_index = index
        #             min_distance = distance
        #     new_corners[p_index].append(closest_index)
        # new_corners.sort(key=itemgetter(2))
        # contable_corner_points = []
        # for row in new_corners:
        #     contable_corner_points.append([row[0], row[1]])
        #
        # simple_contour = self.iterative_obj.convert_points_to_contour(contable_corner_points)

        # outer_lines = self.iterative_obj.find_lines_from_contour(simple_contour)
        #
        # image_border_lines = [
        #     [[0,0],[img_width,0]],
        #     [[img_width,0],[img_width,img_height]],
        #     [[img_width,img_height],[0,img_height]],
        #     [[0,img_height],[0,0]]
        # ]


        #---get distance from each door to contour
        for d_line in all_doors:
            centre_line = d_line[0]
            cx,cy = self.iterative_obj.find_centre_of_line(centre_line)
            distance_from_door = cv2.pointPolygonTest(outer_cont,(cx,cy),True)
            d_line.append(distance_from_door)
        all_doors.sort(key=itemgetter(4))

        outer_doors = []
        for d_line in all_doors:
            # print d_line[0]
            # door_line = d_line[0]
            # cx,cy = self.iterative_obj.find_centre_of_line(door_line)
            # print cx,cy,'--', d_line[-1]
            if d_line[-1] < 2:
                outer_doors.append(d_line[:-1])

        # for door_row,d_line in enumerate(all_doors):
        #     outer_rect_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
        #     for row in outer_lines:
        #         cv2.line(outer_rect_image,(tuple(row[0])),(tuple(row[1])),(0,0,0),5,cv2.cv.CV_AA)
        #
        #     door_line = d_line[0]
        #     cx,cy = self.iterative_obj.find_centre_of_line(door_line)
        #     for border_line in image_border_lines:
        #         direct_line = self.iterative_obj.get_shortest_path_from_point_to_line((cx,cy),border_line)
        #     #     cv2.line(outer_rect_image, (tuple(direct_line[0])), (tuple(direct_line[1])), (0, 0, 255), 3, cv2.cv.CV_AA)
        #     # cv2.imwrite(path_output + str(door_row)+'-outer_lines.png', outer_rect_image)
        #         intersect_flag, intersect_lines = self.check_LOS_for_line(direct_line,
        #                  [outer_lines], -1, -1, [], all_lines=True)
        #         print intersect_flag,cx,cy,'-',border_line,len(intersect_lines)
        #         for row in intersect_lines:
        #             print row
        #         print '------'

        # outer_rect_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
        # for row in outer_lines:
        #     cv2.line(outer_rect_image,(tuple(row[0])),(tuple(row[1])),(0,0,0),5,cv2.cv.CV_AA)
        # cv2.imwrite(path_output+'-outer_lines.png',outer_rect_image)

        return outer_doors

    # --used
    def find_outermost_contour(self,simplified_contours,rect_conts,img_height, img_width):
        contour_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
        for cnt in simplified_contours:
            cv2.drawContours(contour_image,[cnt],-1,(0,0,0),-1)
        for rect_cont in rect_conts:
            cv2.drawContours(contour_image, [rect_cont], -1, (0, 0, 0), -1)

        gray_image = cv2.cvtColor(contour_image, cv2.COLOR_RGB2GRAY)
        ret, thresh = cv2.threshold(gray_image, 127, 255, 1)
        contours, hierachy = cv2.findContours(thresh, cv2.RETR_TREE, 2)

        outer_cont = 0
        for r_num, row in enumerate(hierachy[0]):
            if row[3] == -1:
                outer_cont = contours[r_num]
                break

        return outer_cont

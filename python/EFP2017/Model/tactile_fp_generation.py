import cv2, numpy as np, os,math,random,Image

from iterative_functions import iterative_functions_class
from tactile_page_2 import tactile_page_2_class

class tactile_floorplan_generation_class():
    def __init__(self,path_gravvitas,current_directory,orginal_img_height,
                 orginal_img_width,avg_door_width):
        self.path_gravvitas = path_gravvitas
        self.current_directory = current_directory
        self.avg_door_width = avg_door_width
        self.font_size = int(round(avg_door_width/100))
        self.font_width = int(round(avg_door_width/5))
        self.orginal_img_height, self.orginal_img_width = orginal_img_height,orginal_img_width
        self.iterative_obj = iterative_functions_class()
        self.tactile_page_2_obj = tactile_page_2_class()

    def generate_tactile_floorplan(self,file_name, tactile_path, cropped_img_height,
                                    cropped_img_width,all_floorplan_components,fp_text,
                                    modified_room_cordinates):
        tactile_image_path = tactile_path + file_name[:-4]
        braille_path = self.current_directory + '/Model/floor_plan_text/braille/'
        self.generate_whole_fp(file_name, tactile_image_path,cropped_img_height,
                                cropped_img_width,all_floorplan_components,fp_text,
                                braille_path)
        # self.generate_room_fps(modified_room_cordinates,tactile_image_path,braille_path)


    def generate_whole_fp(self,file_name, tactile_image_path,cropped_img_height,
                          cropped_img_width,all_floorplan_components,fp_text,braille_path):
        self.whole_fp_generate_page1(file_name, tactile_image_path,cropped_img_height,
                          cropped_img_width, all_floorplan_components,braille_path)
        self.tactile_page_2_obj.generate_page2(fp_text,tactile_image_path,braille_path,self.current_directory,self.avg_door_width)


    def generate_room_fps(self,modified_room_cordinates,tactile_image_path,braille_path):
        #--- generate tactile for each room
        for room_row in modified_room_cordinates:
            rooms_point_list, object_point_list, windows_point_list, doors_rect_list, wall_data, dimensions = room_row
            # ---find if is single use room or overall open plan and if has objects inside
            if rooms_point_list[0]=='single' or rooms_point_list[0]=='OP': # and len(object_point_list) >0:
                no_object_rooms = ['closet','0']#-- since wont have objects-temp fix
                if rooms_point_list[1] not in no_object_rooms:
                    image_width, image_height = dimensions
                    room_image = self.room_generate_map(object_point_list,
                                windows_point_list,doors_rect_list,wall_data,
                                rooms_point_list[2], image_height,image_width,
                                rooms_point_list[1], tactile_image_path)


                    room_text = self.room_generate_text_description(rooms_point_list[1],object_point_list,
                                                                    windows_point_list,tactile_image_path)
                    self.room_generate_A4_page(tactile_image_path,room_image,room_text,room_row,
                                       rooms_point_list[1],image_width, image_height,braille_path)

    def whole_fp_generate_page1(self,file_name, tactile_image_path,cropped_img_height,
                       cropped_img_width,all_floorplan_components,braille_path):
        tactile_fp_image = self.whole_fp_generate_tactile_map(file_name, tactile_image_path,
                          cropped_img_height,cropped_img_width, all_floorplan_components)
        self.whole_fp_map_to_A4(tactile_fp_image, cropped_img_height, cropped_img_width,
                       tactile_image_path,braille_path)




    def whole_fp_generate_tactile_map(self, file_name, tactile_image_path, cropped_img_height,cropped_img_width,all_floorplan_components):

        tactile_floorplan_path = tactile_image_path + '_tactile.png'
        tactile_fp_image = ~(np.zeros((cropped_img_height, cropped_img_width, 3), np.uint8))

        # --extract floorplan_data
        rooms_point_list = all_floorplan_components[0]
        stairs_point_list = all_floorplan_components[2]
        windows_point_list = all_floorplan_components[3]
        # doors_rect_list = all_floorplan_components[4]
        wall_point_list = all_floorplan_components[5]
        text_data_list = all_floorplan_components[6]


        for wall_num, wall_row in enumerate(wall_point_list):
            # x1, y1 = wall_row[0]
            # x2, y2 = wall_row[1]
            # cv2.rectangle(tactile_fp_image, (x1, y1), (x2, y2), (0, 0, 0), -1)
            wall_contour = self.iterative_obj.convert_points_to_contour(wall_row)
            cv2.drawContours(tactile_fp_image, [wall_contour], -1, (0, 0, 0), -1)


        # for each_object_row in object_point_list:
        #     for each_object in each_object_row[1:]:
        #         object_contour = self.iterative_obj.convert_points_to_contour(each_object)
        #         cv2.drawContours(tactile_fp_image, [object_contour], -1, (0, 0, 0), -1)

        for stair_num,stair_row in enumerate(stairs_point_list):
            stair_contour = self.iterative_obj.convert_points_to_contour(stair_row)
            cv2.drawContours(tactile_fp_image, [stair_contour], -1, (0, 0, 255), -1)
            #get BB of stair_contour - width, height
            #draw new canvas using bb-w and bb-h
            #draw lines on canvas in door_width/3 distance, have flag for vertical/horizontal

            #4.find minimum x and minimum y of stairs point list
            #get new cordinates for stairs using min_x, min_y
            #new_x=current_x-min_x, new_y=current_y-min_y

            #if new contour is drawn on canvas to get mask
            #get mask of stair contour with new canvas
            #contour detection on masked image

            #map contour points back to image by reverse method of 4.
            #---draw on new image
            # self.whole_fp_draw_stairs(tactile_fp_image,stair_row,stair_contour,self.avg_door_width,
            #                           tactile_image_path)
        #-- leave door space empty
        # for door_num, door_row in enumerate(doors_rect_list):
        #     x1, y1 = door_row[0]
        #     x2, y2 = door_row[1]
        #     cv2.rectangle(tactile_fp_image, (x1, y1), (x2, y2), (0, 255, 0), -1)

        for win_num, window_row in enumerate(windows_point_list):
            #--- find middle line n window rect and draw tactile presentation ther
            line_list = self.whole_fp_draw_windows(window_row,line=False)
            line_thickness = int(self.avg_door_width / 10)
            for line in line_list:
                cv2.line(tactile_fp_image, (tuple(line[0])), (tuple(line[1])), (0, 0, 0), line_thickness,cv2.cv.CV_AA)


        #---get braille text image word list
        braille_names_list = []
        braille_text_path = self.current_directory + '/Model/floor_plan_text/braille/words/room_names/'
        for braille_file in os.listdir(braille_text_path):
            braille_names_list.append(braille_file[:-4].strip())

        label_enlarge_factor = self.avg_door_width*0.02

        #-- get room names
        for room_row in rooms_point_list:
            room_name = room_row[0]
            room_contour = self.iterative_obj.convert_points_to_contour(room_row[2])

            for text_row in text_data_list:
                text = text_row[0]
                if room_name == text:
                    # only_letter_name = ''.join(x for x in room_name if x.isalpha())
                    if room_name in braille_names_list:
                        # print room_name
                        #--enlarge or reduce braille label based on self.door_width
                        new_br_image_path = self.resize_braille_label(braille_text_path,room_name+'.png')
                        braile_text_image = cv2.imread(new_br_image_path)
                        br_text_height, br_text_width,br_text_depth = braile_text_image.shape
                        # ---decide text position place
                        new_t_x, new_t_y = self.whole_fp_find_best_text_position(room_contour, text_row[1],br_text_width,br_text_height)
                        if new_t_x < 0:
                            new_t_x=0
                        if new_t_y < 0:
                            new_t_y=0

                        tactile_fp_image[new_t_y:new_t_y + braile_text_image.shape[0],new_t_x:new_t_x + braile_text_image.shape[1]] = braile_text_image
                        os.remove(new_br_image_path)
                    # cv2.putText(tactile_fp_image,room_name,(cx,cy),
                    #             cv2.FONT_HERSHEY_COMPLEX,self.font_size,(0,0,0),2)

        cv2.imwrite(tactile_floorplan_path, tactile_fp_image)

        return tactile_fp_image

    def whole_fp_find_best_text_position(self,room_contour,text_centre_point,
                                         br_text_width,br_text_height):
        cont_cx, cont_cy = self.iterative_obj.get_centre_of_contour(room_contour)
        c_bb_x1, c_bb_y1 = cont_cx - (br_text_width/2),cont_cy - (br_text_height/2)
        c_bb_x2, c_bb_y2 = cont_cx + (br_text_width/2),cont_cy + (br_text_height/2)

        text_x, text_y = text_centre_point
        t_bb_x1, t_bb_y1 = text_x - (br_text_width / 2), text_y - (br_text_height / 2)
        t_bb_x2, t_bb_y2 = text_x + (br_text_width / 2), text_y + (br_text_height / 2)

        if cv2.pointPolygonTest(room_contour, (c_bb_x1, c_bb_y1),
           False) == 1 and cv2.pointPolygonTest(room_contour,(c_bb_x2, c_bb_y2), False) == 1:
            new_t_x, new_t_y = c_bb_x1, c_bb_y1
        elif cv2.pointPolygonTest(room_contour, (t_bb_x1, t_bb_y1),
           False) == 1 and cv2.pointPolygonTest(room_contour,(t_bb_x2, t_bb_y2), False) == 1:
            new_t_x, new_t_y = t_bb_x1, t_bb_y1
        else:
            new_t_x, new_t_y = text_x-(br_text_width/3)*2, text_y-(br_text_height/3)*2


        return new_t_x, new_t_y

    def resize_braille_label(self,path,image_name):
        braile_text_image_old = cv2.imread(path+image_name, cv2.IMREAD_COLOR)
        old_height, old_width, depth = braile_text_image_old.shape
        braille_img = Image.open(path+image_name)
        new_height = int(old_height*(self.avg_door_width*0.018))
        new_width = int(old_width*(self.avg_door_width*0.018))
        new_braille_img = braille_img.resize((new_width,new_height),Image.ANTIALIAS)
        new_braille_image_name = image_name[:-4]+'_2.png'
        new_braille_img.save(path+new_braille_image_name)
        return path+new_braille_image_name



    def whole_fp_map_to_A4(self,tactile_fp_image, cropped_img_height, cropped_img_width,
                                    tactile_image_path,braille_path):
        A4_dimension_ratio = 297/210.0
        title_list = os.listdir(braille_path + 'words/fp_titles/')
        random_title = random.choice(title_list)
        title_image = cv2.imread(braille_path + 'words/fp_titles/' +str(random_title), cv2.IMREAD_COLOR)
        title_height, title_width, depth = title_image.shape
        extra_height_space = cropped_img_height/10
        title_total_height = title_height +extra_height_space
        img_start_x=0
        img_start_y = 0
        # print cropped_img_height,cropped_img_height / 10

        #--- 1. find if landscape or potrait
        if cropped_img_height < cropped_img_width * 1.2:
            # orientation = 'landscape'
            A4_width = cropped_img_width
            # print A4_width, A4_dimension_ratio
            A4_height = int(A4_width / A4_dimension_ratio)
            #--- since we have to leave space for title: then A44 might get too small
            if (cropped_img_height+title_total_height) > A4_height:
                A4_height = cropped_img_height+title_total_height
                A4_width = int(A4_height*A4_dimension_ratio)

                left_over_x_space = A4_width - cropped_img_width
                img_start_x = left_over_x_space / 2
            else:
                left_over_y_space = A4_height - (cropped_img_height+title_total_height)
                img_start_y = left_over_y_space/2

        else:
            # orientation = 'portrait'
            A4_height = cropped_img_height + title_total_height
            A4_width = int(A4_height/ A4_dimension_ratio)

            if cropped_img_width > A4_width:
                A4_width = cropped_img_width
                A4_height = int(A4_width*A4_dimension_ratio)

                left_over_y_space = A4_height - (cropped_img_height + title_total_height)
                img_start_y = left_over_y_space / 2
            else:
                left_over_x_space = A4_width - cropped_img_width
                img_start_x = left_over_x_space/2

        A4_image = ~(np.zeros((A4_height, A4_width, 3), np.uint8))

        #---1. draw title
        title_x = (A4_width/2)-(title_width/2)
        title_y = img_start_y + extra_height_space/2
        A4_image[title_y:title_y + title_image.shape[0],
        title_x:title_x + title_image.shape[1]] = title_image
        img_start_y = title_y + title_height + extra_height_space/2

        # ---2. draw image on bottom left corner
        A4_image[img_start_y:img_start_y + tactile_fp_image.shape[0],
        img_start_x:img_start_x + tactile_fp_image.shape[1]] = tactile_fp_image

        cv2.imwrite(tactile_image_path + '_A4.png', A4_image)



    def whole_fp_generate_page2(self,all_floorplan_components,fp_text,tactile_image_path,braille_path):
        symbols_images_list = self.whole_fp_get_legend_symbols(all_floorplan_components,braille_path)

        self.whole_fp_map_page2_to_A4(symbols_images_list,tactile_image_path,
                                      braille_path,fp_text)

    def whole_fp_get_legend_symbols(self,all_floorplan_components,braille_path):
        # --extract floorplan_data
        stairs_point_list = all_floorplan_components[2]
        windows_point_list = all_floorplan_components[3]
        doors_rect_list = all_floorplan_components[4]

        symbols_images_list = []
        if len(doors_rect_list) > 0:
            # door_image = cv2.imread(braille_path + 'symbols/door.png', cv2.IMREAD_COLOR)
            door_image = self.generate_door_symbol(int(self.avg_door_width/10))
            door_text = cv2.imread(braille_path + 'words/legend_words/door.png', cv2.IMREAD_COLOR)
            symbols_images_list.append([door_image, door_text])
        # if len(windows_point_list) > 0:
        #     # window_image = cv2.imread(braille_path + 'symbols/window.png', cv2.IMREAD_COLOR)
        #     # window_image = self.generate_window_symbol(int(self.avg_door_width/10))
        #     window_text = cv2.imread(braille_path + 'words/legend_words/door.png', cv2.IMREAD_COLOR)
        #     # symbols_images_list.append([window_image, window_text])
        # # if len(stairs_point_list) > 0:
        # #     stairs_image = cv2.imread(braille_path + 'symbols/stairs.png', cv2.IMREAD_COLOR)
        # #     stairs_text = cv2.imread(braille_path + 'words/legend_words/door.png', cv2.IMREAD_COLOR)
        # #     symbols_images_list.append([stairs_image, stairs_text])

        return symbols_images_list

    def generate_door_symbol(self,line_thickness):
        width = int(self.avg_door_width * 2)
        height = int(self.avg_door_width/2)
        door_image = ~(np.zeros((height, width, 3), np.uint8))

        unit_length = width/4
        line_y = height/2
        # line_thickness = int(self.avg_door_width/10)
        #--- draw line from start to 1/4 of width
        cv2.line(door_image,(0,line_y),(unit_length,line_y),(0,0,0),line_thickness,cv2.cv.CV_AA)
        #--- draw line from 3/4 width to end
        cv2.line(door_image, (unit_length*3, line_y), (width, line_y), (0, 0, 0), line_thickness, cv2.cv.CV_AA)

        return door_image

    def generate_window_symbol(self,line_thickness):
        width = self.avg_door_width * 2
        height = self.avg_door_width / 2
        window_image = ~(np.zeros((height, width, 3), np.uint8))
        line_y = height/2
        x1,y1 = 0, line_y
        x2,y2 = width, line_y
        line_list = self.whole_fp_draw_windows([x1,y1,x2,y2], line=True)
        # line_thickness = int(self.avg_door_width / 10)
        for line in line_list:
            cv2.line(window_image, (tuple(line[0])), (tuple(line[1])), (0, 0, 0), line_thickness,
                     cv2.cv.CV_AA)
        return window_image


    def whole_fp_map_page2_to_A4(self,symbols_images_list,tactile_image_path,
                                 braille_path,fp_text):
        #--get A4 dimesnions
        A4_image_height = 1500
        A4_image_width = int(A4_image_height / (297 / 210.0))
        A4_image = ~(np.zeros((A4_image_height, A4_image_width, 3), np.uint8))

        #-- height parameters setting
        extra_space = A4_image_height*0.4
        legend_start_y = extra_space/4
        legend_end_y = (A4_image_height-extra_space/25) * 15
        text_start_y = legend_end_y + extra_space/4
        # text_end_y = (A4_image_height-extra_space/25) * 10

        #--write braille word 'legend'
        legend_word = cv2.imread(braille_path + 'words/legend_words/legend.png', cv2.IMREAD_COLOR)
        legend_w_height, legend_w_width, depth = legend_word.shape
        legend_x = (A4_image_width / 2) - (legend_w_width / 2)
        A4_image[legend_start_y:legend_start_y + legend_word.shape[0],
        legend_x:legend_x + legend_word.shape[1]] = legend_word

        max_symbol_w = self.avg_door_width * 2
        legend_ex_space = self.avg_door_width/2
        legend_x = (A4_image_width / 2) - (max_symbol_w + legend_ex_space)
        legend_y = legend_start_y+ legend_w_height + extra_space/4

        #-- draw each symbol in legend and it's text word
        for symbol_image_row in symbols_images_list:
            #-- draw symbol
            symbol_image = symbol_image_row[0]
            sym_height, sym_width, sym_depth = symbol_image.shape
            A4_image[legend_y:legend_y + symbol_image.shape[0],
            legend_x:legend_x + symbol_image.shape[1]] = symbol_image

            #-- draw symbol word
            symbol_image_word = symbol_image_row[1]
            symbol_word_x = legend_x + (max_symbol_w + legend_ex_space)
            symbol_word_y = legend_y
            A4_image[symbol_word_y:symbol_word_y + symbol_image_word.shape[0],
            symbol_word_x:symbol_word_x + symbol_image_word.shape[1]] = symbol_image_word

            legend_y = legend_y + sym_height + 20

        #---calculate text start point
        brf_start_row_number = int(text_start_y/(A4_image_height/25))

        self.whole_fp_generate_brf_text(fp_text, tactile_image_path,
                                         brf_start_row_number,capital=False)

        cv2.imwrite(tactile_image_path + 'page_2.png', A4_image)

    def whole_fp_generate_brf_text(self,fp_text,tactile_image_path,brf_start_row_number,
                                   capital):
        # print fp_text
        #-- 1. if NOT room text, remove first line since it is not needed for tactile
        if capital== False:
            fp_text_list = fp_text.split('.')[1:]
            fp_text_stp_1 = '.'.join(fp_text_list)
        else:
            fp_text_stp_1 = fp_text

        #-- 2. add '#' to infront of numbers
        fp_text_stp_1_list = fp_text_stp_1.split()
        for r, word in enumerate(fp_text_stp_1_list):
            if word.strip().isdigit():
                fp_text_stp_1_list[r] = '#'+word
        fp_text_stp_2 = ' '.join(fp_text_stp_1_list)

        #-- 3. add 2 spaces to replace '..' since '..' means a para break
        fp_text_stp_2_list = fp_text_stp_2.split('..')
        fp_text_stp_3 = '.  '.join(fp_text_stp_2_list)

        #-- 4. replace numbers by ascii character
        arabic_numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
        ascii_numbers = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
        for n_row, num in enumerate(arabic_numbers):
            fp_text_stp_3 = fp_text_stp_3.replace(num, ascii_numbers[n_row])

        #-- 5. replace '.' and ',' with relevant ascii character
        fp_text_stp_4 = fp_text_stp_3.replace('.','4')
        fp_text_stp_5 = fp_text_stp_4.replace(',', '1')

        #-- 6. get english phrases, words and their ascii representation
        english_word_list,ascii_word_list = [],[]
        brf_file = open(self.current_directory+'/Model/floor_plan_text/braille/brf_conversion.txt')
        for text_line in brf_file:
            if len(text_line)>0:
                mapping = (text_line.strip()).split(':::')
                english_word = mapping[0].strip()
                ascii_word = mapping[1].strip()
                english_word_list.append(english_word)
                ascii_word_list.append(ascii_word)

        #-- 7. replace each phrase and variable with ascii phrases and words
        for eng_row, eng_word in enumerate(english_word_list):
            if eng_word in fp_text_stp_5:
                index_range = [fp_text_stp_5.find(eng_word),fp_text_stp_5.find(eng_word)+len(eng_word)]
                fp_text_list = list(fp_text_stp_5)
                fp_text_list[index_range[0]:index_range[1]]=list(ascii_word_list[eng_row])
                fp_text_stp_5 = ''.join(fp_text_list)

        #-- 8. for room_text instances make first letter of sentence captial(room name)
        if capital:
            fp_text_stp_8 = ',' + fp_text_stp_5
        else:
            fp_text_stp_8 =  fp_text_stp_5


        #-- 8. break to new lines by para break '  ' - 2 spaces
        fp_text_stp_8_list = fp_text_stp_8.split('   ')
        # -- 9. break each para to lines to match A4 width and write each line to file
        brf_file_to_write = open(tactile_image_path+'.brf','w')
        #--- enter new lines until we get to the point I need to have brf text
        for r in xrange(brf_start_row_number):
            brf_file_to_write.write('\n')
        for chun_no, text_chunk in enumerate(fp_text_stp_8_list):
            word_list = text_chunk.split(' ')
            letter_count,new_line_words = 0,'  '
            written_last_line = False
            for each_word in word_list:
                if len(new_line_words) + len(each_word) < 30:
                    new_line_words = new_line_words + each_word +' '
                    written_last_line = False
                else:
                    brf_file_to_write.write(new_line_words + '\n')
                    new_line_words = each_word+' '
                    written_last_line = True
            if written_last_line==False:
                brf_file_to_write.write(new_line_words + '\n')
        brf_file_to_write.close()

    def whole_fp_draw_stairs(self,tactile_fp_image,stair_row,stair_contour,avg_door_width,
                             tactile_image_path):
        # get BB of stair_contour - width, height
        x, y, stair_bb_w, stair_bb_h = cv2.boundingRect(stair_contour)
        # draw new canvas using bb-w and bb-h
        stair_image = ~(np.zeros((stair_bb_h, stair_bb_w, 3), np.uint8))

        # draw lines on canvas in door_width/3 distance, have flag for vertical/horizontal
        step_line_thickness = int(avg_door_width/15)
        step_space = int(avg_door_width/5)
        line_y = step_space
        while line_y < stair_bb_h:
            cv2.line(stair_image,(0,line_y),(stair_bb_w,line_y),(0,0,0),step_line_thickness,cv2.CV_AA)
            line_y = line_y + step_space
        cv2.imwrite(tactile_image_path+'-stairs.png',stair_image)


    def whole_fp_draw_windows(self,window_row,line):
        line_list = []
        if len(window_row) == 4 or line==True:
            if line == False:
                p1,p2,p3,p4 = window_row
                x1,y1 = p1
                x2,y2 = p3
                if abs(x1-x2) > abs(y1-y2):
                    cx1,cy1 = self.iterative_obj.find_centre_of_line([p1,p4])
                    cx2, cy2 = self.iterative_obj.find_centre_of_line([p2, p3])
                else:
                    cx1, cy1 = self.iterative_obj.find_centre_of_line([p1, p2])
                    cx2, cy2 = self.iterative_obj.find_centre_of_line([p3, p4])
            else:
                cx1, cy1, cx2, cy2 = window_row
                
            c_p1, c_p2 = [cx1, cy1],[cx2, cy2]
            window_line_length = math.hypot(cx2-cx1,cy2-cy1)
            # print window_line_length

            unit_length = self.avg_door_width/10
            no_of_segments = int(math.floor(window_line_length/(4*unit_length)))

            left_over_space = window_line_length - no_of_segments * (4*unit_length)

            shift = (left_over_space+unit_length * 3) / 2

            new_p = self.shift_window_point(c_p1,c_p2,shift)
            n_cx1, n_cy1 = new_p

            line_list = self.get_lines(n_cx1,n_cy1,cx2, cy2,unit_length,no_of_segments)
            # line_thickness = int(self.avg_door_width/10)
            #
            # for line in line_list:
            #     cv2.line(tactile_fp_image,(tuple(line[0])),(tuple(line[1])),(0,0,255),line_thickness,cv2.cv.CV_AA)

            # cv2.imwrite(tactile_image_path+'-windows.png',tactile_fp_image)
            # # print tactile_image_path+'-windows.png'
        return line_list


    def shift_window_point(self,c_p1,c_p2,shift):
        cx1, cy1 = c_p1
        cx2, cy2 = c_p2
        line_length = math.hypot(cx2-cx1,cy2-cy1)
        delta_x = ((cx2-cx1)/line_length) * shift
        delta_y = ((cy2 - cy1) / line_length) * shift

        return [int(cx1+delta_x), int(cy1+delta_y)]

    def get_lines(self,n_cx1,n_cy1,cx2, cy2,unit_length,no_of_segments):
        line_list = []
        for x in xrange(no_of_segments):
            if x==0:
                p1 = [n_cx1, n_cy1]
                p2 = self.shift_window_point(p1,[cx2, cy2],unit_length)
                line_list.append([p1,p2])
            else:
                p1 = self.shift_window_point(line_list[-1][1], [cx2, cy2], unit_length*3)
                p2 = self.shift_window_point(p1, [cx2, cy2], unit_length)
                line_list.append([p1, p2])

        return line_list


    # --rooms fps methods
    def room_generate_map(self, object_point_list, windows_point_list, doors_rect_list,
                          wall_data, rooms_point_list,image_height, image_width,
                          room_name, tactile_image_path):
        wall_thickness = int(self.avg_door_width/25)
        # --generate image
        room_image = ~(np.zeros((image_height, image_width, 3), np.uint8))
        #--draw walls
        # outside_contour, reveresed_inside_contour = wall_data
        # cv2.drawContours(room_image, [outside_contour], -1, (0, 0, 0), -1)
        # cv2.drawContours(room_image, [reveresed_inside_contour], -1, (0, 0, 0), 10)
        room_contour = self.iterative_obj.convert_points_to_contour(rooms_point_list)
        cv2.drawContours(room_image, [room_contour], -1, (0,0,0),wall_thickness)

        #-- draw objects
        for objrow_num, each_object_row in enumerate(object_point_list):
            # object_text = str(each_object_row[0])
            object_contour = self.iterative_obj.convert_points_to_contour(each_object_row[1])
            cv2.drawContours(room_image, [object_contour], -1, (255, 0, 255), -1)
        # -- draw doors
        for door_num, door_contour in enumerate(doors_rect_list):
            x1, y1 = door_contour[0]
            x2, y2 = door_contour[1]
            cv2.rectangle(room_image, (x1-2, y1-2), (x2+2, y2+2), (255, 255, 255), -1)
        # -- draw windows
        # for win_num, window_row in enumerate(windows_point_list):
        #     if len(window_row)==4:
        #         p1,p2,p3,p4 = window_row
        #         x1, y1 = p1
        #         x2, y2 = p3
        #         cv2.rectangle(room_image, (x1 - 2, y1 - 2), (x2 + 2, y2 + 2), (255, 255, 255), -1)
        #     #--- get new window positin since original windw is thicker than wall
        #     #--- find window line closest to contour and use that asssss the new window
        #     new_line = self.room_edit_windows(window_row,room_contour)
        #     line_list = self.whole_fp_draw_windows(new_line,line=True)
        #     for line in line_list:
        #         cv2.line(room_image, (tuple(line[0])), (tuple(line[1])), (0, 0, 255),
        #                  wall_thickness,cv2.cv.CV_AA)
        #     # for rect in new_rect_points:
        #     #     rect_contour = self.iterative_obj.convert_points_to_contour(rect)
        #     #     cv2.drawContours(room_image, [rect_contour], -1, (0, 0, 255), -1)

        # cv2.imwrite(tactile_image_path+room_name+'.png',room_image)
        return room_image

    def room_edit_windows(self,window_row,room_contour):
        new_line = []
        if len(window_row) == 4:
            # print window_row
            p1,p2,p3,p4 = window_row
            L1, L2, L3, L4 = self.iterative_obj.find_lines_rectangle(p1[0],p1[1],p3[0],p3[1])
            closest_dist = -1000
            line_list = [L1, L2, L3, L4]
            x1 = y1 = x2 = y2 = 0
            for r,each_line in enumerate(line_list):
                cx,cy = self.iterative_obj.find_centre_of_line(each_line)
                distance = cv2.pointPolygonTest(room_contour,(cx,cy),True)
                #---distance is + or 0 when its inside or close to cont, - when outside
                if distance > closest_dist:
                    # if r == 3:
                    #     next_line = L1
                    # else:
                    #     next_line = line_list[r+1]
                    # x2, y2 = self.iterative_obj.find_centre_of_line(next_line)
                    x1,y1 = each_line[0]
                    x2,y2 = each_line[1]
                    closest_dist = distance
            new_line=[x1,y1,x2,y2]
            # p1, p2, p3, p4 = self.iterative_obj.find_points_rectangle(x1,y1,x2,y2)
            # new_rect_points.append([p1, p2, p3, p4])
            # print x1,y1,x2,y2
            # print p1, p2, p3, p4
        return new_line



    def room_generate_text_description(self, room_name, object_point_list,
                                       windows_point_list,tactile_image_path):
        room_text = room_name + ' with '

        # --- 1. categorize objects
        object_existence_room_list = categories = []
        for each_object_row in object_point_list:
            object_existence_room_list.append(each_object_row[0])

        for r, obj in object_existence_room_list:
            if r == 0:
                categories.append([obj, 1])
            else:
                # --- extract column from categories
                obj_types = [row[0] for row in categories]
                if obj in obj_types:
                    # --since len(categories)==len(roomtype), indexes are also same
                    categories[obj_types.index(obj)][1] = categories[obj_types.index(obj)][1] + 1
                else:
                    categories.append([obj, 1])

        # --- 2. setting grammar correct
        for cr, cat_row in enumerate(categories):
            # --add a 's' if plural
            if cat_row[1] > 1:
                name = str(cat_row[1]) + ' ' + cat_row[0] + 's'
            else:
                name = str(cat_row[1]) + ' ' + cat_row[0]

            # --generate string for case when list has only 1 element
            if len(categories) == 1:
                room_text = room_text + name
                break
            # --normal cases with >1 room names
            else:
                if cr == len(categories) - 1:
                    room_text = room_text[:-2] + ' and ' + name
                else:
                    room_text = room_text + name + ', '

        # --- 3. add windows count
        if len(windows_point_list) > 0:
            if len(windows_point_list) == 1:
                room_text = room_text + ' and ' + str(len(windows_point_list)) + ' window.'
            else:
                room_text = room_text + ' and ' + str(len(windows_point_list)) + ' windows.'
        else:
            room_text = room_text + '.'

        return room_text

    def room_generate_A4_page(self, tactile_image_path, room_image, room_text, room_row,
                              room_name, cropped_img_width,
                              cropped_img_height, braille_path):
        symbols_images_list, max_leg_text_width = self.room_get_legend_symbols(room_row, braille_path)
        if len(symbols_images_list) > 0: #--- can be removed later when object check is added
            A4_dimension_ratio = 297 / 210.0
            #-- height parameters
            title_word = cv2.imread(braille_path + 'words/room_titles/' + room_name + '.png', cv2.IMREAD_COLOR)
            title_height, title_width, depth = title_word.shape
            text_height = title_height * 3
            extra_height_space = title_height * 4
            top_area_space = text_height + title_height+extra_height_space

            # -- legend parameters
            legend_word = cv2.imread(braille_path + 'words/legend_words/legend.png', cv2.IMREAD_COLOR)
            legend_w_height, legend_w_width, depth = legend_word.shape
            legend_extra_height_space = title_height*2
            max_symbol_height = self.avg_door_width /2
            legend_max_height = (len(symbols_images_list) * max_symbol_height) + legend_w_height + legend_extra_height_space# --500 is max symbol height
            min_a4_height = max(cropped_img_height,legend_max_height)+top_area_space

            # -- width parameters
            extra_legend_width_space = cropped_img_width * 0.3
            max_sym_width = self.avg_door_width * 2
            total_legend_width = max_sym_width+max_leg_text_width+extra_legend_width_space
            lower_area_width = cropped_img_width + total_legend_width
            extra_title_width_space = cropped_img_width * 0.2
            min_a4_width = max(lower_area_width, title_width+extra_title_width_space)

            img_start_x = 0
            img_start_y = 0

            # --- 1. define A4 height and A4 width based on room image size
            if cropped_img_height < cropped_img_width * 1.2:
                # orientation = 'landscape'
                A4_width = min_a4_width
                A4_height = int(A4_width / A4_dimension_ratio)
                # --- since we have to leave space for title: then A44 might get too small
                if min_a4_height > A4_height:
                    A4_height = min_a4_height
                    A4_width = int(A4_height * A4_dimension_ratio)

                    left_over_x_space = A4_width - min_a4_width
                    img_start_x = left_over_x_space / 2
                else:
                    left_over_y_space = A4_height - min_a4_height
                    img_start_y = left_over_y_space / 2

            else:
                # orientation = 'portrait'
                A4_height = min_a4_height
                A4_width = int(A4_height / A4_dimension_ratio)

                if min_a4_width > A4_width:
                    A4_width = min_a4_width
                    A4_height = int(A4_width * A4_dimension_ratio)

                    left_over_y_space = A4_height - min_a4_height
                    img_start_y = left_over_y_space / 2
                else:
                    left_over_x_space = A4_width - min_a4_width
                    img_start_x = left_over_x_space / 2

            A4_image = ~(np.zeros((A4_height, A4_width, 3), np.uint8))

            # ---2. write title
            title_x = (A4_width/2) - (title_width/2)
            title_y = img_start_y + (extra_height_space/4)
            A4_image[title_y:title_y + title_word.shape[0],title_x:title_x + title_word.shape[1]] = title_word
            img_start_y = img_start_y + title_height + (extra_height_space / 4)

            # # ---3. write text desc -- convert to .brf
            brf_row_number = int(img_start_y/ (A4_height/25))
            self.whole_fp_generate_brf_text(room_text, tactile_image_path + '_' + room_name,
                                            brf_row_number,capital=True)
            img_start_y = img_start_y + text_height + (extra_height_space / 4) #-- leave space for the text from embosser
            #-- extra +(title_height) is for spacing

            # ---4. draw image
            A4_image[img_start_y:img_start_y + room_image.shape[0],
            img_start_x:img_start_x + room_image.shape[1]] = room_image

            # ---5. draw legend
            # --4.1 write braille word 'legend'
            legend_x = img_start_x + cropped_img_width + (total_legend_width/ 2 - (legend_w_width / 2))
            A4_image[img_start_y:img_start_y + legend_word.shape[0],
            legend_x:legend_x + legend_word.shape[1]] = legend_word
            img_start_y = img_start_y + (legend_extra_height_space)

            # --- 4.2 write rest of the symbols
            legend_symbol_start_x = img_start_x + cropped_img_width + (extra_legend_width_space/3)
            legend_text_start_x = legend_symbol_start_x + max_sym_width + (extra_legend_width_space/3)


            # -- draw each symbol in legend and it's text word
            for symbol_image_row in symbols_images_list:
                # -- draw symbol
                symbol_image = symbol_image_row[0]
                sym_height, sym_width, sym_depth = symbol_image.shape
                A4_image[img_start_y:img_start_y + symbol_image.shape[0],
                legend_symbol_start_x:legend_symbol_start_x + symbol_image.shape[1]] = symbol_image

                # -- draw symbol word
                symbol_image_word = symbol_image_row[1]
                symbol_word_y = img_start_y + (sym_height / 2)
                A4_image[symbol_word_y:symbol_word_y + symbol_image_word.shape[0],
                legend_text_start_x:legend_text_start_x + symbol_image_word.shape[1]] = symbol_image_word

                img_start_y = img_start_y + sym_height + (max_symbol_height/20)

            # ---6. write image to system
            cv2.imwrite(tactile_image_path + '_' + room_name + '_A4.png', A4_image)

    def room_get_legend_symbols(self,room_row,braille_path):
        max_sym_width, max_leg_text_width = 0,0
        object_point_list = room_row[1]
        windows_point_list = room_row[2]
        doors_rect_list = room_row[3]

        symbols_images_list = []
        if len(doors_rect_list) > 0:
            door_image = self.generate_door_symbol(int(self.avg_door_width/25))
            door_text = cv2.imread(braille_path + 'words/legend_words/door.png', cv2.IMREAD_COLOR)
            symbols_images_list.append([door_image, door_text])
            # sym_height, sym_width, sym_depth = door_image.shape
            sym_tx_height, sym_tx_width, sym_tx_depth = door_text.shape
            # if sym_width > max_sym_width: max_sym_width = sym_width
            if sym_tx_width > max_leg_text_width: max_leg_text_width = sym_tx_width
        # if len(windows_point_list) > 0:
        #     window_image = self.generate_window_symbol(int(self.avg_door_width/25))
        #     window_text = cv2.imread(braille_path + 'words/legend_words/door.png', cv2.IMREAD_COLOR)
        #     symbols_images_list.append([window_image, window_text])
        #     # sym_height, sym_width, sym_depth = window_image.shape
        #     sym_tx_height, sym_tx_width, sym_tx_depth = window_text.shape
        #     # if sym_width > max_sym_width: max_sym_width = sym_width
        #     if sym_tx_width > max_leg_text_width: max_leg_text_width = sym_tx_width
        #--categorize objects and add them to list
        object_list = []
        for each_object_row in object_point_list:
            obj_name = each_object_row[0]
            if len(object_list) > 0 and obj_name not in object_list:
                object_list.append(obj_name)

        for obj in object_list:
            obj_image = cv2.imread(braille_path + 'symbols/'+obj+'.png', cv2.IMREAD_COLOR)
            obj_text = cv2.imread(braille_path + 'words/legend_words/'+obj+'.png', cv2.IMREAD_COLOR)
            symbols_images_list.append([obj_image, obj_text])
            sym_height, sym_width, sym_depth = obj_image.shape
            sym_tx_height, sym_tx_width, sym_tx_depth = obj_text.shape
            # if sym_width > max_sym_width: max_sym_width = sym_width
            if sym_tx_width > max_leg_text_width: max_leg_text_width = sym_tx_width

        return symbols_images_list, max_leg_text_width
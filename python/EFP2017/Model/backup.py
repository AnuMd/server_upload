# def find_angles(self,p1,ref,p2):
#     a = np.array(p1)
#     b = np.array(ref)
#     c = np.array(p2)
#
#     ba = a - b
#     bc = c - b
#
#     cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
#     angle = np.arccos(cosine_angle)
#
#     return np.degrees(angle)
#
# def find_angles_3(self,p1, ref_pt, p2):
#     lineA = [p1,ref_pt]
#     lineB = [ref_pt, p2]
#     # Get nicer vector form
#     vA = [(lineA[0][0] - lineA[1][0]), (lineA[0][1] - lineA[1][1])]
#     vB = [(lineB[0][0] - lineB[1][0]), (lineB[0][1] - lineB[1][1])]
#     # Get dot prod
#     dot_prod = self.dot(vA, vB)
#     # Get magnitudes
#     magA = self.dot(vA, vA) ** 0.5
#     magB = self.dot(vB, vB) ** 0.5
#     # # Get cosine value
#     # cos_ = dot_prod / magA / magB
#     # Get angle in radians and then convert to degrees
#     angle = math.acos(dot_prod / magB / magA)
#     # Basically doing angle <- angle mod 360
#     ang_deg = math.degrees(angle) % 360
#
#     if ang_deg - 180 >= 0:
#         # As in if statement
#         print 360 - ang_deg
#     else:
#
#         print ang_deg
#
# def dot(self,vA, vB):
#     return vA[0] * vB[0] + vA[1] * vB[1]
__author__ = 'stefano'

import os,re,cv2,math,shutil,random,pickle
from pprint import pprint
import numpy as np
from scipy import ndimage
from connectedcomponent import CC, bins1, bins2
from rectangle_in_cc import Rectangle
from itertools import *
from graph import Graph
from edge import Edge
from PIL import Image
import PIL.ImageOps
from graphCC import GraphCC
import imageOperations as imop
from room import Room
from copy import deepcopy
import mathMethods as mm
from natsort import natsorted
from operator import itemgetter

from line_optimization import line_optimization_class
from iterative_functions import iterative_functions_class
from text_functions import text_funcitons_class



class Image_class():
    def __init__(self,current_directory,f,path_output,path_debug,img,image_color):
        self.current_directory = current_directory
        self.list_cc = []
        self.width = 0
        self.height = 0
        self.file_name = f
        self.original_image = img
        #self.color_image = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        self.color_image = image_color
        self.path_debug = path_debug 
        self.path_output = path_output
        self.labels = None
        self.numlabels = 0
        self.list_cc = []
        self.list_cc_text = []
        self.list_rect = []
        self.image_rect = None
        self.max_tink_wall = 0
        self.image_run_pixel=None
        self.graph_rect = Graph()
        self.graph_wall = Graph()
        self.graph_image_output = Graph()
        self.numberDoors = 0
        self.list_door = []
        ###
        self.list_door_2 = []
        ###
        self.numberWindows = 0
        self.list_windows = []
        self.list_wwd=[]
        self.list_scale=[]
        self.dic_scale={}
        self.length_door = 0
        self.image_remove_text=np.array(img)
        h,w = img.shape
        self.final_image=np.zeros((h,w,3),dtype=np.uint8)

        #Pierpaolo's:
        self.graph_cc = GraphCC()
        self.avg_width = 0
        self.img_walls_and_doors = None
        self.img_walls_and_doors_color = None
        self.rooms = []
        self.room_index = {}
        self.num_cc_inside = 0
        self.text_coordinates = []
       # self.image_clean=None

        # self.text_functions_obj = text_functions(self.path_output,self.file_name,self.current_directory)
        self.text_functions_obj = text_funcitons_class(self.path_output,self.path_debug,
                                                       self.current_directory,self.file_name)
        self.iterative_obj = iterative_functions_class()



    #----method 01
    def find_CC_and_classify(self):
        self.find_connected_componet()
        list_cc_text, list_cc_no_text = self.categorize_CC()
        return list_cc_text, list_cc_no_text

    def find_connected_componet(self):
        # here convert in gray-scale becouse the function  ndimage.label
        img=self.original_image
        self.height,self.width=img.shape
        s1 = [[0, 1, 0], [1,1,1], [0,1,0]] # elemento strutturato
        s2 = [[1, 1, 1], [1,1,1], [1,1,1]]
        # plt.imshow(self.labels_background*10, cmap = 'gray', interpolation = 'bicubic')
        # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
        # plt.show()
        self.labels, self.numlabels = ndimage.label(img<127,s2)
        # print 'number of connected components: '+str(self.numlabels)
        s=self.file_name[0:-4]
        for i in range(1,self.numlabels+1):
            cc=CC(i)
            point= np.argwhere(self.labels == i)
            # find x_max, x_min, y_max, y_min
            cc.y_min=point[:,0].min()
            cc.x_min=point[:,1].min()
            cc.y_max=point[:,0].max()
            cc.x_max=point[:,1].max()
            cc.height= cc.y_max-cc.y_min+1
            cc.width= cc.x_max-cc.x_min+1
            # I create image of the connected compis an open image with black background and white lines, i want as output an image with white background and black linesonent
            cc.image=(np.where(self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)

            cc.numPixel=(cc.image.sum()/255)
            # add padding to cc.image
            h,w=cc.image.shape
            z1=np.zeros((1,w))
            z2=np.zeros((h+2,1))
            cc.image = np.concatenate((z1,cc.image),axis=0)
            cc.image = np.concatenate((cc.image,z1),axis=0)
            cc.image = np.concatenate((z2,cc.image),axis=1)
            cc.image = np.concatenate((cc.image,z2),axis=1)

            cc.image_dt_1 = ndimage.morphology.distance_transform_cdt(cc.image)
            cc.image_dt_2 = np.array(cc.image_dt_1)
            #cv2.imwrite(self.path_debug+'preprocessing/'+s+'/'+str(i)+'.png', np.abs(cc.image-255))
            # I add the connected component to the list
            self.list_cc.append(cc)

    def categorize_CC(self):
        list_cc_text, list_cc_no_text=[],[]

        # selected cc for area dimension
        list_area=[]
        for cc_a in self.list_cc:
            list_area.append(cc_a.getArea())
        media=sum(list_area)/len(list_area)

        for cc_a2 in self.list_cc:
            if(cc_a2.getArea()<float(media)):# and cc.height*cc.width>float(media)*0.1 ):
                list_cc_text.append(cc_a2)
            else:
                list_cc_no_text.append(cc_a2)

        return list_cc_text,list_cc_no_text





    def remove_thin_lines_and_clean_image(self,list_cc_no_text,output_path):
        img_no_text = np.zeros((self.height, self.width))
        for cc in list_cc_no_text:
            img_no_text[cc.y_min:cc.y_max + 1, cc.x_min:cc.x_max + 1] = img_no_text[cc.y_min:cc.y_max + 1,
                                                                        cc.x_min:cc.x_max + 1] + cc.image[
                                                                                                 1:cc.height + 1,
                                                                                                 1:cc.width + 1]
        # cv2.imwrite(output_path+'_img_no_text.png',img_no_text)

        kernel_2 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3), (0, 2))
        open = cv2.morphologyEx(img_no_text, cv2.MORPH_OPEN, kernel_2)
        # cv2.imwrite(output_path + '_open.png', open)

        #--invert colors
        bw = np.ones((open.shape)) * 255
        cleaned_image = np.where(open == 0, 510, open) - bw
        # cv2.imwrite(output_path + '_cleaned_image.png', cleaned_image)

        return cleaned_image

    def erase_all_thin_lines(self,list_cc_no_text,output_path):
        img_no_text = np.zeros((self.height, self.width))
        for cc in list_cc_no_text:
             img_no_text[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=img_no_text[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+cc.image[1:cc.height+1,1:cc.width+1]
        # cv2.imwrite(output_path+'_img_no_text.png',img_no_text)

        # # --testing image
        # img_no_text = cv2.imread(self.current_directory + '/input_fps/fp_1_img_no_text.png')
        # cv2.imwrite(output_path + '_img_no_text.png', img_no_text)
        # self.height, self.width = self.original_image.shape
        # # ---end test path

        kernel_2 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3), (0, 2))
        open = cv2.morphologyEx(img_no_text, cv2.MORPH_OPEN, kernel_2)
        cv2.imwrite(output_path + '_open.png', open)

        return open,img_no_text

    def find_erased_thin_lines(self,img_no_text,open,output_path):
        # absdiff = cv2.absdiff(img_no_text, open)
        # open = cv2.imread(output_path + '_open.png',cv2.IMREAD_GRAYSCALE)
        # absdiff = ~(np.zeros((self.height, self.width, 3), np.uint8))
        # cv2.bitwise_and(img_no_text, open, absdiff, mask=None)
        absdiff = img_no_text - open
        kernel = np.ones((3, 3), np.uint8)
        dilation = cv2.dilate(absdiff, kernel, iterations=1)
        # cv2.imwrite(output_path + '_diff.png', absdiff)
        cv2.imwrite(output_path + '_dilation.png', dilation)

        # -----
        gray_image = cv2.imread(output_path + '_dilation.png', cv2.IMREAD_GRAYSCALE)
        ret, thresh = cv2.threshold(gray_image, 0, 255, 1)
        contours, hierachy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_TC89_KCOS)
        cont_list = []
        for c, cnt in enumerate(contours):
            cont_list.append([cnt, cv2.contourArea(cnt)])
        cont_list.sort(key=itemgetter(1), reverse=True)

        return cont_list

    def find_lines_hough_lines(self,output_path):
        gray = cv2.imread(output_path + '_img_no_text.png', cv2.IMREAD_GRAYSCALE)
        kernel = np.ones((3, 3), np.uint8)
        dilation = cv2.dilate(gray, kernel, iterations=1)
        # cv2.imwrite(output_path + '_dilation.png', dilation)
        colored = cv2.imread(output_path + '_img_no_text.png', cv2.IMREAD_COLOR)
        # # gray = cv2.imread(output_path + '_img_no_text.png', cv2.IMREAD_GRAYSCALE)
        # edges = cv2.Canny(dilation, 100, 400, apertureSize=5)
        # cv2.imwrite(output_path + '_edges.png', edges)

        minLineLength = 100
        maxLineGap = 50
        lines = cv2.HoughLinesP(dilation, 1, np.pi / 180, 100, minLineLength, maxLineGap)
        hough_lines = []
        for x1, y1, x2, y2 in lines[0]:
            # if math.hypot(x1-x2,y1-y2)>200:
            cv2.line(colored, (x1, y1), (x2, y2), (0, 255, 0), 2)
            hough_lines.append([[x1, y1], [x2, y2]])

        cv2.imwrite(output_path + '_houghlines.jpg', colored)

        return hough_lines

    def find_roof_border_lines(self,output_path,cont_list):
        cont_image = ~(np.zeros((self.height, self.width, 3), np.uint8))
        cv2.drawContours(cont_image, [cont_list[1][0]], -1, (0, 0, 0), -1)
        cv2.imwrite(output_path + '_roof border.png', cont_image)

        gray = cv2.cvtColor(cont_image, cv2.COLOR_RGB2GRAY)
        corners = cv2.goodFeaturesToTrack(gray, 200, 0.27, 10)
        corners = np.int0(corners)
        new_corners = []
        for i in corners:
            x, y = i.ravel()
            new_corners.append([x, y])

        cont_point_list = self.iterative_obj.get_points_from_contour(cont_list[1][0])
        for num, r in enumerate(cont_point_list):
            cont_point_list[num].append([])

        # ----order corner points based on contour
        for p_index, point in enumerate(new_corners):
            x1, y1 = point
            closest_index, min_distance = -1, 1000
            for index, cont_point in enumerate(cont_point_list):
                x2, y2 = cont_point[0], cont_point[1]
                distance = math.hypot(x2 - x1, y2 - y1)
                if distance < min_distance:
                    closest_index = index
                    min_distance = distance
            new_corners[p_index].append(closest_index)
        new_corners.sort(key=itemgetter(2))
        contable_corner_points = []
        for row in new_corners:
            contable_corner_points.append([row[0], row[1]])

        # ----get lines based on new points
        border_line_list = self.iterative_obj.convert_points_to_lines(contable_corner_points)

        simple_cont2 = ~(np.zeros((self.height, self.width, 3), np.uint8))
        for ls in border_line_list:
            x1, y1 = ls[0]
            x2, y2 = ls[1]
            cv2.line(simple_cont2, (x1, y1), (x2, y2), (0, 0, 0), 5)
        cv2.imwrite(output_path + 'selected.png', simple_cont2)

        return border_line_list

    def find_all_roof_lines(self,hough_lines,border_line_list,output_path):
        simple_cont2 = ~(np.zeros((self.height, self.width, 3), np.uint8))
        for l_num, hough_l in enumerate(hough_lines):
            if l_num == 772:
                a = 2
                for point in hough_l:
                    inside_flag = False
                    for cont_line in border_line_list:
                        data = self.iterative_obj.calculate_distance_from_point_to_line(point, cont_line)
                        intersect_x, intersect_y = data[1], data[2]
                        x1, y1 = cont_line[0]
                        x2, y2 = cont_line[1]
                        distance_1 = math.hypot(intersect_x - x1, intersect_y - y1)
                        distance_2 = math.hypot(intersect_x - x2, intersect_y - y2)
                        # inside_flag = self.iterative_obj.check_point_approx_inside_line(point,cont_line,50)
                        if data[0] < 50 and data[0] > 0 or distance_1 < 5 or distance_2 < 5:
                            print l_num, hough_l, '-', cont_line, data
                            print distance_1, distance_2
                            print '----------'
                            cv2.line(simple_cont2, (tuple(hough_l[0])), (tuple(hough_l[1])), (0, 0, 0), 5)
                #         # print data[0]
                #         break
                # if inside_flag:
                #     break
        cv2.imwrite(output_path + 'selected--2.png', simple_cont2)



    def improve_textCC_accuracy(self,cleaned_image,list_cc_text):
        ni = cleaned_image.astype(np.uint8)

        # find cc in new image
        n_i = Image_class(self.current_directory, self.file_name, self.path_output,
                          self.path_debug, ni, ni)
        n_i.find_connected_componet()

        list_area_2 = []
        for cc_a in n_i.list_cc:
            list_area_2.append(cc_a.getArea())
        media = sum(list_area_2) / len(list_area_2)

        for cc in n_i.list_cc:
            if (cc.getArea() < float(media) * 0.1):  # and cc.height*cc.width>float(media)*0.1 ):
                if ((cc.getAspectRatio() > 0.1) and (cc.getAspectRatio() < 2.5)):
                    if (cc.getDensity() > 0.2):
                        list_cc_text.append(cc)

        return list_cc_text

    def generate_and_improve_text_image(self,list_cc_text):
        path_extract_text = self.path_debug + 'Extract_text/'

        #--1. generate text image
        img_dy = np.zeros((self.height, self.width))
        for cc in list_cc_text:
            img_dy[cc.y_min:cc.y_max + 1, cc.x_min:cc.x_max + 1] = cc.image[1:cc.height + 1, 1:cc.width + 1]
        cv2.imwrite(path_extract_text + 'density/' + str(self.file_name), img_dy)

        #--2. improve image quality
        # ---- 2.1 invert image colors
        image_density = Image.open(path_extract_text + 'density/' + str(self.file_name))
        inverted_image = PIL.ImageOps.invert(image_density)
        inverted_image.save(path_extract_text + 'density/' + 'inverted_' + str(self.file_name))
        filename = path_extract_text + 'density/' + 'inverted_' + str(self.file_name)

        #---- 2.2 improve with image magick
        textcleaner_file = self.current_directory+ '/Model/text_clean_library/textcleaner'
        # print self.current_directory+ '/Model/text_clean_library/textcleaner'
        os.system(textcleaner_file + ' -g -e normalize -f 15 -o 10 -u -s 1 ' + filename + ' ' + path_extract_text + 'density/' + 'improved_' + str(self.file_name))
        cc_image_path = path_extract_text + 'density/' + 'improved_' + str(self.file_name)

        return cc_image_path

#     def detect_text_by_CC_2(self):
#
#         #This path is used to save the output images for each step of the analysis
#         path_extract_text= self.path_debug+'Extract_text/'
#         #------uncomment later
#         list_cc_text_1=[]
#         list_cc_no_text_1=[]
#         # selected cc for area dimension
#         list_area=[]
#         for cc_a in self.list_cc:
#             list_area.append(cc_a.getArea())
#         media=sum(list_area)/len(list_area)
#
#         for cc_a2 in self.list_cc:
#             if(cc_a2.getArea()<float(media)):# and cc.height*cc.width>float(media)*0.1 ):
#                 list_cc_text_1.append(cc_a2)
#             else:
#                 list_cc_no_text_1.append(cc_a2)
#
#
#         img_no_text=np.zeros((self.height,self.width))
#         # img_ad=np.zeros((self.height,self.width))
#
#         # for cc in list_cc_text_1:
#         #      img_ad[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=img_ad[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+cc.image[1:cc.height+1,1:cc.width+1]
#         # if debug_mode:
#             # cv2.imwrite(path_extract_text + 'area_dim/' + str(self.file_name) + '_step-1', img_ad)
#
#         # I  clean image, and extract the text that is overlapping on the lines
#
#         for cc in list_cc_no_text_1:
#              img_no_text[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=img_no_text[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+cc.image[1:cc.height+1,1:cc.width+1]
#         new_image= self.clean_image(img_no_text)
#         cv2.imwrite(path_extract_text + 'area_dim/' + str(self.file_name)+'_step-2', new_image)
#
#         ni = new_image.astype(np.uint8)
#         #find cc in new image
#         n_i=Image_class(self.current_directory,self.file_name,self.path_output,
#                         self.path_debug, ni,ni)
#         n_i.find_connected_componet()
#         #I repeat selection for area dimension
#
#         list_area_2=[]
#         for cc_a in n_i.list_cc:
#             list_area_2.append(cc_a.getArea())
#         media=sum(list_area_2)/len(list_area_2)
#
#         # list_cc_no_text_11 =[]
#         for cc_a2 in n_i.list_cc:
#             if(cc_a2.getArea()<float(media)*0.1):# and cc.height*cc.width>float(media)*0.1 ):
#                 list_cc_text_1.append(cc_a2)
#             else:
#                 list_cc_no_text_1.append(cc_a2)
#                 # list_cc_no_text_11.append(cc_a2)
#
#         # reconstruct image with the connected components obtained after analyzing area dimension
#         img_ad_2=np.zeros((self.height,self.width))
#         for cc in list_cc_text_1:
#              img_ad_2[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=img_ad_2[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+cc.image[1:cc.height+1,1:cc.width+1]
#         # if debug_mode:
#         #     cv2.imwrite(path_extract_text+'area_dim_2/'+str(self.file_name), img_ad_2)
#
#         #select cc for aspect ratio
#         list_cc_text_2=[]
#         for cc in list_cc_text_1:
#             if((cc.getAspectRatio()>0.1) and (cc.getAspectRatio()<2.5)):
#                 list_cc_text_2.append(cc)
#             # else:
#             #     list_cc_no_text_11.append(cc)
#         # reconstruct image with the connected components obtained after analyzing aspect ratio
#         # img_ar=np.zeros((self.height,self.width))
#         # for cc in list_cc_text_2:
#         #      img_ar[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=cc.image[1:cc.height+1,1:cc.width+1]
#         # if debug_mode:
#         #     cv2.imwrite(path_extract_text+'asp_ratio/'+str(self.file_name), img_ar)
#         # img_ar=np.zeros((self.height,self.width))
#         # for cc in list_cc_no_text_11:
#         #      img_ar[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=cc.image[1:cc.height+1,1:cc.width+1]
#         # cv2.imwrite(path_extract_text+'asp_ratio/NO TEXT'+str(self.file_name), img_ar)
#
#         # select cc for density
#         list_cc_text_3=[]
#         for cc in list_cc_text_2:
#             if( cc.getDensity()> 0.2 ):# and cc.height*cc.width>float(media)*0.1 ):
#                 list_cc_text_3.append(cc)
#             # else:
#             #     list_cc_no_text_11.append(cc)
#
#
#         # reconstruct image with the connected components obtained after analyzing density
#         img_dy=np.zeros((self.height,self.width))
#         for cc in list_cc_text_3:
#              img_dy[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=cc.image[1:cc.height+1,1:cc.width+1]
#         cv2.imwrite(path_extract_text+'density/'+str(self.file_name), img_dy)
#
#         # img_dy=np.zeros((self.height,self.width))
#         # for cc in list_cc_no_text_11:
#         #      img_dy[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]=cc.image[1:cc.height+1,1:cc.width+1]
#         # cv2.imwrite(path_extract_text+'density/NO TEXT'+str(self.file_name), img_dy)
#
# #------text improve attempt march 31st
# #----- invert image colors
#         image_density = Image.open(path_extract_text+'density/'+str(self.file_name))
#         inverted_image = PIL.ImageOps.invert(image_density)
#         inverted_image.save(path_extract_text+'density/'+'inverted_'+str(self.file_name))
#         filename = path_extract_text+'density/'+'inverted_'+str(self.file_name)
#         # print path_extract_text+'density/'+'inverted_'+str(self.file_name)
#         # print filename
#         # print path_extract_text+'density/'+'improved_'+str(self.file_name)
#         textcleaner_file = 'text_clean_library/textcleaner'
#         os.system(textcleaner_file+' -g -e normalize -f 15 -o 10 -u -s 1 ' + filename + ' '+path_extract_text+'density/'+'improved_'+str(self.file_name))
#         # image=cv.LoadImage(path_extract_text+'density/'+'improved_'+str(self.file_name), cv.CV_LOAD_IMAGE_GRAYSCALE)
#         cc_image_path= path_extract_text+'density/'+'improved_'+str(self.file_name)
#         # text_img_height, text_img_width = self.height,self.width
#
#         return cc_image_path
#
#     def clean_image(self, i):
#         # original_image = np.array(self.original_image)
#         # cv2.imwrite(self.path_debug + 'Extract_text/clean_image/Canny/1_' + str(self.file_name),
#         #             i)
#         # kernel_1 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3), (0, 2))
#         # closing_2 = cv2.morphologyEx(i, cv2.MORPH_CLOSE, kernel_1)
#         # c2_copy = np.uint8(closing_2)
#         # edges = cv2.Canny(c2_copy, 5, 500, apertureSize=3)
#         # cv2.imwrite(self.path_debug + 'Extract_text/clean_image/Canny/2_' + str(self.file_name),
#         #             edges)
#         # minLineLength = 10
#         # maxLineGap = 5
#         # self.lines = cv2.HoughLinesP(edges, 1, np.pi / 360, 100, minLineLength, maxLineGap)
#         # hough_I = np.array(self.color_image)
#         # for x1, y1, x2, y2 in self.lines[0]:
#         #     cv2.line(hough_I, (x1, y1), (x2, y2), (0, 255, 0), 2)
#         # cv2.imwrite(self.path_debug + '/Extract_text/clean_image/Hough/' + str(self.file_name),
#         #             hough_I)
#         # # considering the line in the form y=mx+q , i find m and q
#         # for x1, y1, x2, y2 in self.lines[0]:
#         #     x = float(min(x1, x2))
#         #     y = float(min(y1, y2))
#         #
#         #     if (x1 != x2):
#         #         m = float(y2 - y1) / float(x2 - x1)
#         #         q = m * (-x1) + y1
#         #
#         #         while (((m == 0) and (x != max(x1, x2)))):
#         #             x = x + 1
#         #             # print 'x2: '+str(x2)+' x: '+str(x)
#         #             # I find the pixels that are on the perpendicular line, far more maximum of 3
#         #             #       p3
#         #             #       p2
#         #             #       p1
#         #             # p  p  P  p  p  p...
#         #             #       p4
#         #             #       p5
#         #             #       p6
#         #             if (np.sum(closing_2[y - 3:y + 1, x]) == 0):
#         #                 closing_2[y:y + 4, x] = 0
#         #
#         #             elif (np.sum(closing_2[y:y + 4, x]) == 0):
#         #                 closing_2[y - 3:y + 1, x] = 0
#         #                 # I find the pixels that are on the perpendicular line, far more maximum of 3
#         #                 #                   p3
#         #                 #                p2
#         #                 #             p1
#         #                 # p  p  p  P  p  p  p
#         #                 #       p4
#         #                 #    p5
#         #                 # p6
#         #                 #
#         #         if (m != 0):
#         #
#         #             while (x != max(x1, x2) and y != max(y1, y2)):
#         #                 if (abs(m) <= 1):
#         #                     # print 'ciao'
#         #                     x = x + 1
#         #                     y = int(m * x + q)
#         #
#         #                     closing_2[y, x - 3:x + 4] = 0
#         #
#         #                 else:
#         #                     # da rivedere nel caso y<q e m negativo
#         #                     y = y + 1
#         #
#         #                     x = abs(int((y - q) / m))
#         #                     closing_2[y - 1, x - 3:x + 4] = 0
#         #     else:
#         #         while (y != max(y1, y2)):
#         #             y = y + 1
#         #             # I find the pixels that are on the perpendicular line, far more maximum of 3
#         #             #            p
#         #             #            p
#         #             #            p
#         #             #  p1 p2 p3  P p4 p5 p6
#         #             #            p
#         #             #            p
#         #             #            p
#         #             #
#         #
#         #             if (np.sum(closing_2[y, x - 3:x + 1]) == 0):
#         #                 closing_2[y, x:x + 4] = 0
#         #
#         #             elif (np.sum(closing_2[y, x:x + 4]) == 0):
#         #                 closing_2[y, x - 3:x + 1] = 0
#         #
#         #
#         #
#         #                 # morfologic open
#
#         kernel_2 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3), (0, 2))
#         open = cv2.morphologyEx(i, cv2.MORPH_OPEN, kernel_2)
#         # open is image with black background and white lines, i want as output an image with white background and black lines
#         bw = np.ones((open.shape)) * 255
#         output = np.where(open == 0, 510, open) - bw
#         # cv2.imwrite(self.path_debug + 'Extract_text/clean_image/Open/' + str(self.file_name), open)
#         return output



    def test_image_process(self):
        # --word recognition testing start
        # --english png
        # new_walls_path = self.current_directory+'/input_fps/improved_text/most-png/'
        # --english_apghomes
        # new_walls_path = self.current_directory+'/input_fps/improved_text/apg_homes/'
        # --french
        new_walls_path = self.current_directory + '/input_fps/improved_text/french/'
        new_walls_list = os.listdir(new_walls_path)
        cc_image_path = 0
        for wall_room_num, wall_room in enumerate(natsorted(new_walls_list)):
            wall_image_name = str(wall_room)
            # --english
            # wall_name = wall_image_name[9:-4]
            # --french
            wall_name = wall_image_name[9:-4]
            if wall_name == self.file_name[0:-4]:
                # --english
                # orginal_image_path = new_walls_path+'improved_'+wall_name+'.png'
                # --french
                cc_image_path = new_walls_path+'improved_' + wall_name + '.png'
                break
        # image = cv.LoadImage(orginal_image_path, cv.CV_LOAD_IMAGE_GRAYSCALE)
        # text_image = cv2.imread(orginal_image_path, cv2.IMREAD_GRAYSCALE)
        # text_img_height, text_img_width = text_image.shape
        # return orginal_image_path,image,text_img_height, text_img_width
        return cc_image_path



    # ----method 03: find stairs functions
    def find_stairs(self,path_gravvitas,only_image_name):
        stairs_path = self.path_debug+'stairs/'
        if os.path.exists(stairs_path):
            shutil.rmtree(stairs_path)
        os.mkdir(stairs_path)

        # the first part of the code I initialize the images to view the step of function
        h,w=self.original_image.shape
        img =  np.array(self.original_image)
        #img2 =  np.array(self.color_image)
        img2 = np.zeros((h,w,3))
        img3 =  np.zeros((h,w,3))
        img4 = np.zeros((h,w,3))
        img5 =  np.zeros((h,w,3))
        img6 =  np.zeros((h,w,3))
        img7 =  np.zeros((h,w),dtype=np.uint8)
        img8 =  np.zeros((h,w),dtype=np.uint8)

        # I find connected component
        s1 = [[0, 1, 0], [1,1,1], [0,1,0]]
        s2 = [[1, 1, 1], [1,1,1], [1,1,1]]
        labels,numlabels =ndimage.label(img>127,s1)
        list_initial_cc=[]
        for i in range(1,numlabels+1):
            point = np.argwhere(labels == i)
            cc = CC(i)
            if len(point)>0:
                cc.y_min=point[:,0].min()
                cc.x_min=point[:,1].min()
                cc.y_max=point[:,0].max()
                cc.x_max=point[:,1].max()
                cc.height= cc.y_max-cc.y_min+1
                cc.width= cc.x_max-cc.x_min+1
                cc.image=(np.where(labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
                cc.numPixel=(cc.image.sum()/255)
                list_initial_cc.append(cc)
        # I save the original image
        cv2.imwrite(self.path_debug+'stairs/'+str(self.file_name)+'_1.png', self.color_image )


        # I save the image whit colored cc
        for l in list_initial_cc:
            r=random.randint(0, 255)
            g=random.randint(0, 255)
            b=random.randint(0, 255)
            cc_rgb = cv2.cvtColor(l.image.astype(np.uint8),cv2.COLOR_GRAY2RGB)/255
            img2[l.y_min:l.y_max+1,l.x_min:l.x_max+1] = img2[l.y_min:l.y_max+1,l.x_min:l.x_max+1]+(cc_rgb*(r,g,b))

        cv2.imwrite(self.path_debug+'stairs/'+str(self.file_name)+'_2.png', img2 )

        # I create a dictionary
        #the key element of a dictionary is the cc area
        d_area = {}
        for cc in list_initial_cc:
            #area = ((cc.y_max - cc.y_min) + 1)*((cc.y_max - cc.y_min) + 1)
            area = cc.height*cc.width
            if area *0.6 < cc.numPixel:
                keys = d_area.keys()
                if len(keys) == 0:
                    d_area[area] = []
                    d_area[area].append(cc)
                else:
                    check_d = False
                    for k in keys:
                        if area>(k*0.80) and area<(k*1.2):
                            d_area[k].append(cc)
                            check_d = True
                            break
                    if not(check_d):
                        d_area[area] = []
                        d_area[area].append(cc)
        for k in d_area.keys():
            if len(d_area[k])>=3:
                r=random.randint(0, 255)
                g=random.randint(0, 255)
                b=random.randint(0, 255)
                for cc in d_area[k]:
                    cc_rgb = cv2.cvtColor(cc.image.astype(np.uint8),cv2.COLOR_GRAY2RGB)/255
                    img3[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] = img3[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+(cc_rgb*(r,g,b))
        cv2.imwrite(self.path_debug+'stairs/'+str(self.file_name)+'_3.png', img3 )

        # in this case i select the list cc that has cc near
        d2={}
        for k in d_area.keys():
            l_cc = []
            if len(d_area[k])>=3:
                for cc in d_area[k]:
                    for cc2 in d_area[k]:
                        if cc != cc2:
                            if (cc.x_min > cc2.x_min-20 and cc.x_min < cc2.x_min + 5) and (cc.x_max > cc2.x_max-5 and cc.x_max < cc2.x_max + 5) and ( np.abs(cc.y_max -cc2.y_min)<=5 or np.abs(cc.y_min -cc2.y_max)<=5   ):
                                l_cc.append(cc)
                                break

                            elif (cc.y_min > cc2.y_min-20 and cc.y_min < cc2.y_min + 5) and (cc.y_max > cc2.y_max-5 and cc.y_max < cc2.y_max + 5) and (  np.abs(cc.x_max -cc2.x_min)<=5 or np.abs(cc.x_min -cc2.x_max)<=5  ):
                                l_cc.append(cc)
                                break
                d2[k] = l_cc
                r=random.randint(0, 255)
                g=random.randint(0, 255)
                b=random.randint(0, 255)
                for cc in l_cc:
                    cc_rgb = cv2.cvtColor(cc.image.astype(np.uint8),cv2.COLOR_GRAY2RGB)/255
                    img4[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] = img4[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+(cc_rgb*(r,g,b))
        cv2.imwrite(self.path_debug+'stairs/'+str(self.file_name)+'_4.png', img4 )

        #  in una lista ci possono essere cc con la stessa area  ma che non sono tra loro vicine,
        #  per cui vado a dividere ogni lista del dizionario in liste di cc che sono vicine tra loro
        d_near = {}
        n = 0
        for k in d2.keys():
            ll = d2[k][:]
            while(len(ll)>0):
                cc = ll.pop()
                n = n + 1
                d_near[n] = [cc]
                stop = True
                while(stop):
                    stop = False
                    for cc in ll:
                        x,y = cc.getCentroid()
                        h = cc.getLengthSide()
                        for cc2 in d_near[n]:
                            x2,y2 = cc2.getCentroid()
                            if cc != cc2:
                                dist = math.sqrt((x-x2)**2 + (y-y2)**2)
                                if dist<h*2:
                                    if not( cc in d_near[n]):
                                        d_near[n].append(cc)
                                        stop=True
                for cc in d_near[n]:
                    if cc in ll:
                        ll.remove(cc)
        list_scale=[]
        d_final = {}
        for k in d_near.keys():
            if len(d_near[k])>2:
                d_final[k] = d_near[k]
                r=random.randint(0, 255)
                g=random.randint(0, 255)
                b=random.randint(0, 255)
                list_scale.extend(d_near[k])
                for cc in d_near[k]:
                    cc_rgb = cv2.cvtColor(cc.image.astype(np.uint8),cv2.COLOR_GRAY2RGB)/255
                    img5[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] = img5[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+(cc_rgb*(r,g,b))
        cv2.imwrite(self.path_debug+'stairs/'+str(self.file_name)+'_5.png', img5 )

        # a questo punto ho individuato le cc che formano gli scalini di una scala
        # in piu ci possono essere gruppi di cc piu piccole oppure gruppi di componenti cc che sono vicine a gruppi di due

        # 1) il primo passo e vedere quante cc compongono la mia lista, se son maggiori di 50 sicuramente ci saranno dei
        # gruppi di cc piccole, poi mi calcolo la area media e la lunghezza media delle cc che mi restano

        #devo andare a ritrovare le cc a loro vicine e che hanno le seguenti caratteristiche
        # vicinanza rispetto ad una cc
        # area non superiore a 10 volte la cc
        # area non inferiore al meta della cc
        # devono avere un lato in comune(sara dura)

        if len(list_scale)>0:
            v_a = []
            v_l = []
            for k in d_final.keys():
                mean_area = 0
                min_dist = max(cc.width,cc.height)
                for s in d_final[k]:
                    mean_area = mean_area + (s.width*s.height)
                    sx,sy=s.getCentroid()
                    for s2 in d_final[k]:
                        if s.id != s2.id:
                            s2x,s2y=s2.getCentroid()
                            dist = math.sqrt((sx-s2x)**2 + (sy-s2y)**2)
                            if dist < min_dist:
                                min_dist = dist

                v_a.append(mean_area/len(d_final[k]))
                v_l.append(min_dist)
            mean_area = sum(v_a)/len(v_a)

            keys = d_final.keys()
            for k in keys:
                kk = d_final.keys()
                i = kk.index(k)
                if v_a[i]< mean_area*0.5 or v_a[i]<20:
                    del(d_final[k])
                    del(v_a[i])
                    del(v_l[i])




            keys = d_final.keys()
            stop = True
            if len(keys)>0:
                while(stop):
                    stop=False
                    for cc in list_initial_cc:
                        isBreak=False
                       # if (cc.width*cc.height)<(mean_area*10) and (cc.width*cc.height)>mean_area*0.2:
                        for i in range(0,len(keys)):
                            if (cc.width*cc.height)<(v_a[i]*10) and (cc.width*cc.height)>v_a[i]*0.5:
                                ccx,ccy=cc.getCentroid()
                                for s in d_final[keys[i]]:
                                    if cc.id!=s.id:
                                        sx,sy=s.getCentroid()
                                        dist = math.sqrt((ccx-sx)**2 + (ccy-sy)**2)
                                        # leng = max(max(cc.width,cc.height),max(s.width,s.height))

                                        if dist<v_l[i]*4:
                                            #list_scale2.append(cc)
                                            list_initial_cc.remove(cc)
                                            d_final[keys[i]].append(cc)

                                            isBreak=True
                                            stop=True
                                            break
                                if isBreak:
                                    break
                        if isBreak:
                            break



            for k in d_final.keys():
                self.dic_scale[k]= d_final[k]
                for cc in d_final[k]:
                    r=random.randint(0, 255)
                    g=random.randint(0, 255)
                    b=random.randint(0, 255)
                    cc_rgb = cv2.cvtColor(cc.image.astype(np.uint8),cv2.COLOR_GRAY2RGB)/255
                    img6[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] = img6[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+(cc_rgb*(r,g,b))
                cv2.imwrite(self.path_debug+'stairs/'+str(self.file_name)+'_6.png', img6 )


        for k in d_final.keys():
            self.dic_scale[k]= d_final[k]
            for cc in d_final[k]:
                img7[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] = img7[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1]+cc.image.astype(np.uint8)
        kernel = np.ones((5,5),np.uint8)
        closing = cv2.morphologyEx(img7, cv2.MORPH_CLOSE, kernel)
        cv2.imwrite(self.path_debug+'stairs/'+str(self.file_name)+'_7.png', closing )


        cnt,hierarchy = cv2.findContours(closing, 1, 2)
        max_area = 0
        for c in cnt:
            area = cv2.contourArea(c)
            if max_area < area:
                max_area = area
                scala = c
        if max_area>0:
            epsilon = 0.01*cv2.arcLength(scala,True)
            approx = cv2.approxPolyDP(scala,epsilon,True)
            self.list_scale.append(approx)
            cv2.drawContours(img8, [approx], -1, 255, -1)
            cv2.imwrite(self.path_debug+'stairs/'+str(self.file_name)+'_8.png', img8 )

        stairs_path = self.path_debug+'stairs/'
        stairs_image_list = os.listdir(stairs_path)
        last_image_name = natsorted(stairs_image_list)[-1]

        stairs_details_file = open(path_gravvitas+only_image_name+'_3_stairs_details.txt', 'w')

        gray_room = cv2.imread(stairs_path+last_image_name,cv2.IMREAD_GRAYSCALE)
        ret,thresh = cv2.threshold(gray_room,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        for count, cnt in enumerate(contours):
            if count != len(contours)-1:
                contour_data = ''
                for c,cont_point in enumerate(cnt):
                    if c==0:
                        contour_data  = contour_data+'['+str(cont_point[0][0])+' '+str(cont_point[0][1])+']'
                    else:
                        contour_data  = contour_data+',['+str(cont_point[0][0])+' '+str(cont_point[0][1])+']'
                stairs_details_file.write('Stairs : '+contour_data + ' : [1] \n')
        stairs_details_file.close()

        # self.save_object(self.list_scale, self.path_debug + 'stairs/stairs.pkl')

    # ----method 04: find no_text_CC functions
    def find_connected_componet_no_text(self,only_image_name,orginal_img_path):
        #--testing
        # no_text_path = orginal_img_path+'../new_walls/no_text/'+only_image_name+'_no_text.png'
        #--non testing
        no_text_path = self.path_output + only_image_name + '/no_text.png'
        self.image_remove_text = cv2.imread(no_text_path, cv2.IMREAD_GRAYSCALE)
        # I add it as I have to reset the lists once the text is extracted
        del self.list_cc[:]
        img = self.image_remove_text
        self.height, self.width = img.shape
        # s1 = [[0, 1, 0], [1, 1, 1], [0, 1, 0]]  # elemento strutturato
        s2 = [[1, 1, 1], [1, 1, 1], [1, 1, 1]]
        # plt.imshow(self.labels_background*10, cmap = 'gray', interpolation = 'bicubic')
        # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
        # plt.show()
        self.labels, self.numlabels = ndimage.label(img < 127, s2)
        s = self.file_name[0:-4]
        for i in range(1, self.numlabels + 1):
            cc = CC(i)
            point = np.argwhere(self.labels == i)
            # find x_max, x_min, y_max, y_min
            cc.y_min = point[:, 0].min()
            cc.x_min = point[:, 1].min()
            cc.y_max = point[:, 0].max()
            cc.x_max = point[:, 1].max()
            cc.height = cc.y_max - cc.y_min + 1
            cc.width = cc.x_max - cc.x_min + 1
            # I create image of the connected compis an open image with black background and white lines, i want as output an image with white background and black linesonent
            cc.image = (np.where(self.labels[cc.y_min:cc.y_max + 1, cc.x_min:cc.x_max + 1] != i, 0,
                                 self.labels[cc.y_min:cc.y_max + 1, cc.x_min:cc.x_max + 1]) * 255 / i)
            cc.numPixel = (cc.image.sum() / 255)
            # add padding to cc.image
            h, w = cc.image.shape
            z1 = np.zeros((1, w))
            z2 = np.zeros((h + 2, 1))
            cc.image = np.concatenate((z1, cc.image), axis=0)
            cc.image = np.concatenate((cc.image, z1), axis=0)
            cc.image = np.concatenate((z2, cc.image), axis=1)
            cc.image = np.concatenate((cc.image, z2), axis=1)
            cc.image_dist_tran = ndimage.morphology.distance_transform_cdt(cc.image)

            cc.image_dt_1 = ndimage.morphology.distance_transform_cdt(cc.image)
            cc.image_dt_2 = np.array(cc.image_dt_1)
            # cv2.imwrite(self.path_debug+'preprocessing/'+s+'/'+str(i)+'.png', np.abs(cc.image-255))
            # I add the connected component to the list
            self.list_cc.append(cc)


    # ----method 05: find wall functions
    def classification_walls(self,debug_mode):
        temp_wall_list,avg_wall_width = self.classification_external_walls(debug_mode)
        self.recostructed_image_whit_rectangle_oblique(debug_mode)
        temp_wall_list = self.classification_interior_walls(debug_mode,temp_wall_list)

        # self.save_object(self.graph_wall.nodes(),self.path_debug+'wall/wall.pkl')
        # self.save_object(self.list_rect, self.path_debug + 'wall/rect.pkl')
        # self.save_object(self.graph_image_output.nodes(), self.path_debug + 'wall/output.pkl')

        return temp_wall_list,avg_wall_width

    def save_object(self,obj_list, filename):
        with open(filename, 'wb') as output:
            # for wall in obj_list:
            pickle.dump(obj_list, output, pickle.HIGHEST_PROTOCOL)


    def classification_external_walls(self,debug_mode):
        index_inc = 0
        for cc in self.list_cc:
             self.list_rect.extend(cc.extract_rectangle_in_cc_3(index_inc))
             index_inc = len(self.list_rect)
        avg_wall_width = self.find_run_pixel(debug_mode)
        self.find_adjacent_rectangles()
        self.create_graph()
        temp_wall_list = self.find_external_wall(debug_mode)
        return temp_wall_list,avg_wall_width

    def create_graph(self):
        for r in self.list_rect:
            self.graph_rect.add_node(r)


        id_edge=0
        for r in self.list_rect:
            lll = r.get_max_len()
            n = self.graph_rect.get_node(Rectangle(r.id))
            if len(r.list_adjacent_rect)>0:
                for a in r.list_adjacent_rect:
                    n2 = self.graph_rect.get_node(Rectangle(a[1].id))
                    id_edge = str(max(n.id,n2.id)) + str(min(n.id,n2.id)) + str(min(n.id,n2.id)) + str(max(n.id,n2.id))
                    a1 = Edge(id_edge,n2,a[0])
                    a2 = Edge(id_edge,n,a[0])
                    n.add_edge(a1)
                    n2.add_edge(a2)


        # for r in self.list_rect:
        #      print 'ad: '+str(len(r.list_adjacent_rect))
        #      n = self.graph_rect.get_node(Rectangle(r.id))
        #      print 'archi: '+str(len(n.edges()))

    def find_run_pixel(self, debug_mode):
        l=[cc for cc in self.list_cc if cc.getArea()>(self.height*self.width)/10]
        run_hist=[]
        i=0
        limit=max(self.width,self.height)
        m=0

        if(len(l)==0):
            for cc in self.list_cc:
                if cc.getArea()>m:
                    maxi=cc
            l.append(maxi)



        #find run pixel
        for cc in l:
            i=i+1
            #cv2.imwrite(path+self.file_name+'_CC_num_'+str(i)+'.png', cc.image)
            run=[]
            a=[]
            h,w=cc.image.shape

            for i in range(0,h):
                row=cc.image[i,:]
                a.extend([(len(list(group)),name) for name, group in groupby(row)])
            run.extend([item for item in a if item[1] == 255])

            b=[]
            for i in range(0,w):
                col=cc.image[:,i].transpose()
                b.extend([(len(list(group)),name) for name, group in groupby(col)])

            run.extend([item for item in b if item[1] == 255])

            run_np = np.asarray(run)
            run_hist.extend(run_np[:,0])



            if limit>min(h,w):
                limit = min(h,w)


        #plot histogram
        hist, bins =np.histogram(run_hist,bins= range(1,int(max(run_hist))+2), range=(int(min(run_hist)), int(max(run_hist))))

        max_first_wall=1
        max_tink_wall=1
        first=1
        for i in range(0,len(hist)):

            if hist[i] >= (limit*0.8):
                if i==0:
                    if hist[0]>hist[1]:
                        max_first_wall=1
                        max_tink_wall=1
                        first=0
                    else:
                        max_tink_wall=2
                        max_first_wall=2
                        first=0
                else:
                    if hist[i]>hist[i+1] and hist[i]>hist[i-1]:
                        max_tink_wall=i+1
                        if first:
                            max_first_wall=i+1
                            first=0
        list_first_wall=[]
        list_tink_wall=[]
        self.max_tink_wall=max_tink_wall
        if (max_tink_wall-max_first_wall)>4:
            if max_first_wall==1:
                list_first_wall.append(1)
                list_first_wall.append(2)
            else:
                lim=int(max_first_wall*1.5+1)-int(max_first_wall*0.9)
                for j in range(0,lim+1):
                    list_first_wall.append(int(max_first_wall*0.9)+j)

            if max_tink_wall==1:
                list_tink_wall.append(1)
                list_tink_wall.append(2)
            else:
                lim=int(max_tink_wall*1.5+1)-int(max_tink_wall*0.9)
                for j in range(0,lim+1):
                    list_tink_wall.append(int(max_tink_wall*0.9)+j)

        elif (max_tink_wall-max_first_wall)<=4 and (max_tink_wall-max_first_wall)>=2:

            if max_first_wall==1:
                list_first_wall.append(1)
                list_first_wall.append(2)
            else:
                list_first_wall.append(max_first_wall-1)
                list_first_wall.append(max_first_wall)
                list_first_wall.append(max_first_wall+1)

            if max_tink_wall==1:
                list_tink_wall.append(1)
                list_tink_wall.append(2)
            else:
                list_tink_wall.append(max_tink_wall-1)
                list_tink_wall.append(max_tink_wall)
                list_tink_wall.append(max_tink_wall+1)


        else:
            list_tink_wall.append(max_tink_wall)
            list_first_wall.append(max_first_wall)

        self.max_tink_wall = max_tink_wall
       #  width =  (bins[1] - bins[0])
       #  center = (bins[:-1] + bins[1:]) / 2
       #  barlist=plt.bar(range(1,61), hist[0:60], align='center', width=width)
       #  if max_tink_wall< 60:
       #      for item in list_tink_wall:
       #          barlist[item-1].set_color('r')
       #      for item in list_first_wall:
       #          barlist[item-1].set_color('g')
       #          #
       # # plt.savefig(self.path_debug+self.file_name+'_hist_runpixel.png')
       #  print hist
       #  plt.show()



        # plot image color
        image = np.zeros((self.height,self.width,3),dtype=np.uint8)
        self.image_run_pixel =  np.zeros((self.height,self.width,3),dtype=np.uint8)
        img_or = self.original_image[:]
        img_or.astype(np.float32)
        for i in range(0,self.height):
            a2=[]

            row=np.abs(img_or[i,:]-255.0)
            a2.extend([(len(list(group)),name) for name, group in groupby(row)])

            index=0
            for p in a2:
                if (p[1]==255):
                    #255,0,0 rosso
                    #0,255,0 verde
                    #if (p[0]==max_tink_wall or p[0]==check_mtw_dx or p[0]==check_mtw_sx):
                    if (p[0] in list_tink_wall ):

                        image[i,index:index+p[0]]=(0,0,255)
                        self.image_run_pixel[i,index:index+p[0]]=(0,0,255)

                    # elif (p[0]==max_freq_wall  or p[0]==check_mfw_dx or p[0]==check_mfw_sx):
                    elif (p[0] in list_first_wall):
                        # if not((0,0,255) in image[i,index:index+p[0]]):
                        image[i,index:index+p[0]]=(0,255,0)
                    else:

                        if ( np.sum(image[i,index:index+p[0]])==0):
                            image[i,index:index+p[0]]=(255,255,255)
                index=index+p[0]


        for i in range(0,self.width):
            b2=[]
            col=np.abs(img_or[:,i].transpose()-255.0)
            b2.extend([(len(list(group)),name) for name, group in groupby(col)])
            index=0
            for p in b2:
                if (p[1]==255):
                    #255,0,0 rosso
                    #0,255,0 verde
                    if(p[0] in list_tink_wall ):
                        # if(p[0]==max_tink_wall  or p[0]==check_mtw_dx or p[0]==check_mtw_sx):

                        image[index:index+p[0],i]=(0,0,255)
                        self.image_run_pixel[index:index+p[0],i]=(0,0,255)
                    #elif(p[0]==max_freq_wall or p[0]==check_mfw_dx or p[0]==check_mfw_sx):
                    elif(p[0] in list_first_wall):
                        #  if not( (0,0,255) in image[index:index+p[0],i]):
                        image[index:index+p[0],i]=(0,255,0)

                    else:

                        if(np.sum(image[index:index+p[0],i])==0):

                            image[index:index+p[0],i]=(255,255,255)
                index=index+p[0]
        # if debug_mode:
        #     cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_1_RunPixel.png', image)
        return max_tink_wall


    def find_adjacent_rectangles(self):
        list_of_serch=self.list_rect[:]# create new list
        #print len(self.list_rect)
        while(len(list_of_serch)>0):

            rect_1=list_of_serch.pop()


            merge=False
            for rect_2 in list_of_serch:
                # adjacent l1,l3
                # if rect_2.get_area>10:
                # adjacent l1,l3es che ritorna una copia della lista di tutti i
                # node_2 = Node(rect_2.id)
                #    if rect_1 is not rect_2:
                # qui potrei mettere il primo membro |y2-y1|<3 per includere anche quelle vicini ma non attaccati anche se probabilmente fanno parte di un altra cc
                # if ((rect_2.list_sides[2].y2==rect_1.list_sides[0].y1-1) and (rect_2.list_sides[2].x2<=rect_1.list_sides[0].x2) and (rect_2.list_sides[2].x1>=rect_1.list_sides[0].x1)) :
                if (np.abs(rect_2.y4-rect_1.y1)<4 and (rect_2.x4<=rect_1.x2+3) and (rect_2.x3>=rect_1.x1-3)) :
                    a=np.abs(rect_1.x2-rect_1.x1)
                    b=np.abs(rect_2.x2-rect_2.x1)
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)


                    if(percent_adjacency<0.7):# questo mi basta per capire se i due rettangoli sono allineati

                        thinckness_r1=min( (a) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor

                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 1, 3])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 3, 1])
                        else:
                            if ['ADIACENT',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT',rect_2, 1, 3])
                                rect_2.list_adjacent_rect.append(['ADIACENT',rect_1, 3, 1])


                    else:

                        if np.abs((rect_1.x2-rect_2.x2))<=1 and  np.abs(rect_1.x1-rect_2.x1)<=1:
                            r=Rectangle(rect_1.id)
                            r.x2=max(rect_1.x2,rect_2.x2)
                            r.x1=min(rect_1.x1,rect_2.x1)
                            r.y1=rect_2.y1
                            r.y2=rect_2.y2
                            r.x3=max(rect_1.x2,rect_2.x2)
                            r.x4=min(rect_1.x1,rect_2.x1)
                            r.y3=rect_1.y3
                            r.y4=rect_1.y4
                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                            # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0: #and len(rect_1.list_adjacent_rect)>0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                        #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente
                                        a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0: #and len(rect_2.list_adjacent_rect)>0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                       # a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))
                                         a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])
                            elif len(rect_1.list_adjacent_rect)>0: # and len(rect_2.list_adjacent_rect)>0:

                                for a in rect_1.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])


                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            #print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                            merge=True
                        else:
                            if ['ALIGNED',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 1, 3])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 3, 1])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                #list_new.append(rect_1)

                                # if merge:
                                #     break

                                # adjacent l3,l1
                                #    if((rect_2.list_sides[0].y2==rect_1.list_sides[2].y1+1) and (rect_2.list_sides[0].x1<=rect_1.list_sides[2].x1) and (rect_2.list_sides[0].x2>=rect_1.list_sides[2].x2) ):
                elif(np.abs(rect_2.y2-rect_1.y3)<4 and (rect_2.x1<=rect_1.x3+3) and (rect_2.x2>=rect_1.x4-3)):
                    a=np.abs(rect_1.x2-rect_1.x1)
                    b=np.abs(rect_2.x2-rect_2.x1)
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)
                    #percent_adjacency=min(float(rect_1.x2-rect_1.x1),float(rect_2.x2-rect_2.x1))/(max(float(rect_1.x2-rect_1.x1),float(rect_2.x2-rect_2.x1))+1)


                    if(percent_adjacency<0.7):
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor

                        # if(rect_1.x2-rect_1.x1>rect_2.x2-rect_1.x2):
                        #  if( (min( (rect_2.x2-rect_2.x1) , (rect_2.y4-rect_2.y1) ) == (rect_2.x2-rect_2.x1)) and ( min( (rect_1.x3-rect_1.x4) , (rect_1.y4-rect_1.y1) ) == (rect_1.x3-rect_1.x4)) ):
                        #      rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 3, 1])
                        #      rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 1, 3])

                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 3, 1])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 1, 3])

                        else:
                            if ['ADIACENT',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT',rect_2, 3, 1])
                                rect_2.list_adjacent_rect.append(['ADIACENT',rect_1, 1, 3])

                    else:
                        if np.abs((rect_1.x2-rect_2.x2))<=1 and  np.abs(rect_1.x1-rect_2.x1)<=1:
                            r=Rectangle(rect_1.id)
                            r.x2=max(rect_1.x2,rect_2.x2)
                            r.x1=min(rect_1.x1,rect_2.x1)
                            r.y1=rect_1.y1
                            r.y2=rect_1.y2
                            r.x3=max(rect_1.x2,rect_2.x2)
                            r.x4=min(rect_1.x1,rect_2.x1)
                            r.y3=rect_2.y3
                            r.y4=rect_2.y4

                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                           # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                        #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente
                                        a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                        #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))
                                        a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            else:

                                for a in rect_1.list_adjacent_rect:
                                    #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    #a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            #print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                            merge=True
                        else:
                            if ['ALIGNED',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 3, 1])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 1, 3])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                #list_new.append(rect_1)
                                # if merge:
                                #     break

                                # adjacent l2,l4
                                #    if((rect_2.list_sides[3].x1==rect_1.list_sides[1].x2+1) and (rect_2.list_sides[3].y1>=rect_1.list_sides[1].y1) and (rect_2.list_sides[3].y2<=rect_1.list_sides[1].y2) ):

                elif(np.abs(rect_2.x4-rect_1.x3)<4 and (rect_2.y4>=rect_1.y2-3) and (rect_2.y1<=rect_1.y3+3) ):
                    a=np.abs(rect_1.y3-rect_1.y2)
                    b=np.abs(rect_2.y3-rect_2.y2)
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)


                    if(percent_adjacency<0.7):
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor
                        # if( (min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) ) == (rect_1.y4-rect_1.y1)) and ( min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) ) == (rect_2.y4-rect_2.y1)) ):
                        #     rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 2, 4])
                        #     rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 4, 2])

                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 2, 4])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 4, 2])

                        else:
                            if ['ADIACENT', rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT', rect_2, 2, 4])
                                rect_2.list_adjacent_rect.append(['ADIACENT', rect_1, 4, 2])
                    else:
                        if np.abs((rect_1.y1-rect_2.y1))<=1 and  np.abs(rect_1.y4-rect_2.y4)<=1:
                            r=Rectangle(rect_1.id)
                            r.y4=max(rect_1.y4,rect_2.y4)
                            r.y1=min(rect_1.y1,rect_2.y1)
                            r.x1=rect_1.x1
                            r.x4=rect_1.x4
                            r.y3=max(rect_1.y4,rect_2.y4)
                            r.y2=min(rect_1.y1,rect_2.y1)
                            r.x3=rect_2.x3
                            r.x2=rect_2.x2
                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                           # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                       # a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente
                                         a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                        a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            else:

                                for a in rect_1.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            merge=True
                            # print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)

                        else:
                            if ['ALIGNED',rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 2, 4])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 4, 2])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]
                                #list_new.append(rect_1)
                                # if merge:
                                #     break
                                # adjacent l4,l2
                                #    if((rect_2.list_sides[1].x1==rect_1.list_sides[3].x2-1) and (rect_2.list_sides[1].y1<=rect_1.list_sides[3].y1) and (rect_2.list_sides[1].y2>=rect_1.list_sides[3].y2) ):

                elif(np.abs(rect_2.x2-rect_1.x1)<4 and (rect_2.y2<=rect_1.y4+3) and (rect_2.y3>=rect_1.y1-3) ):
                    a=np.abs(rect_1.y3-rect_1.y2)
                    b=np.abs(rect_2.y3-rect_2.y2)
                    percent_adjacency=min(float(a),float(b))/(max(float(a),float(b))+1)
                    #percent_adjacency=min(float(rect_1.y3-rect_1.y2),float(rect_2.y3-rect_2.y2))/(max(float(rect_1.y3-rect_1.y2),float(rect_2.y3-rect_2.y3))+1)


                    if(percent_adjacency<0.7):
                        # if( (min( (rect_2.x2-rect_2.x1) , (rect_2.y4-rect_2.y1) ) == (rect_2.y4-rect_2.y1)) and ( min( (rect_1.x3-rect_1.x4) , (rect_1.y4-rect_1.y1) ) == (rect_1.y4-rect_1.y1)) ):
                        #     rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 4, 2])
                        #     rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 2, 4])
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        divisor = float(max(thinckness_r1,thinckness_r2))
                        if divisor == 0:
                            ratio_of_thicknesses=0
                        else:
                            ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor
                        if (ratio_of_thicknesses<0.5):
                            if ['TANGENT',rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 4, 2])
                                rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 2, 4])

                        else:
                            if ['ADIACENT', rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ADIACENT', rect_2, 4, 2])
                                rect_2.list_adjacent_rect.append(['ADIACENT', rect_1, 2, 4])


                    else:
                        if np.abs((rect_1.y1-rect_2.y1))<=1 and  np.abs(rect_1.y4-rect_2.y4)<=1:
                            r=Rectangle(rect_1.id)
                            r.y4=max(rect_1.y4,rect_2.y4)
                            r.y1=min(rect_1.y1,rect_2.y1)
                            r.x1=rect_2.x1
                            r.x4=rect_2.x4
                            r.y3=max(rect_1.y4,rect_2.y4)
                            r.y2=min(rect_1.y1,rect_2.y1)
                            r.x3=rect_1.x3
                            r.x2=rect_1.x2
                            r.set_sides()
                            if rect_2 in list_of_serch:
                                #list_of_serch.pop(list_of_serch.index(rect_2))
                                list_of_serch.remove(rect_2)
                            if rect_1 in list_of_serch:
                                list_of_serch.remove(rect_1)
                            #print rect_1
                            self.list_rect.remove(rect_2)
                            self.list_rect.remove(rect_1)
                           # r.id=rect_1.id

                            if len(rect_2.list_adjacent_rect)==0:

                                if len(rect_1.list_adjacent_rect)>0:
                                    for a in rect_1.list_adjacent_rect:
                                        a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])# elimino adiacenza nel rettangolo che prima era adiacente


                            elif len(rect_1.list_adjacent_rect)==0:

                                if len(rect_2.list_adjacent_rect)>0:
                                    for a in rect_2.list_adjacent_rect:
                                        a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            else:

                                for a in rect_1.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_1,a[3],a[2]])

                                for a in rect_2.list_adjacent_rect:
                                    a[1].list_adjacent_rect.remove([a[0],rect_2,a[3],a[2]])

                            self.list_rect.append(r)
                            list_of_serch.append(r)
                            #  print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                            merge=True
                        else:
                            if ['ALIGNED',rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 4, 2])
                                rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 2, 4])
                                # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]
                                #list_new.append(rect_1)
                                # else:
                                #     rect_2.num_set_of_rectangle=0

                    # if merge:
                    #     break            # if len(list_of_serch)==0 and len(rect_1.list_adjacent_rect)>0:
                #     self.list_new.append(rect_1)
                #vado a vedere se i due scheletri si incontrano
                elif (self.skeleton_intersect(rect_1,rect_2)):
                    a = np.abs(rect_1.get_min_len())
                    b = np.abs(rect_2.get_min_len())
                    thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                    thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                    divisor = float(max(thinckness_r1,thinckness_r2))
                    if divisor == 0:
                        ratio_of_thicknesses=0
                    else:
                        ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/divisor
                    if (ratio_of_thicknesses<0.5):
                        if ['TANGENT',rect_2, 0, 0] not in rect_1.list_adjacent_rect:
                            rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 0, 0])
                            rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 0, 0])

                    else:
                        if ['ADIACENT', rect_2, 0, 0] not in rect_1.list_adjacent_rect:
                            rect_1.list_adjacent_rect.append(['ADIACENT', rect_2, 0, 0])
                            rect_2.list_adjacent_rect.append(['ADIACENT', rect_1, 0, 0])

                if merge:
                    break

    def skeleton_intersect(self,r1,r2):
        # lll1 = r1.get_max_len()
        # lll2 = r2.get_max_len()
        # ll1 = r1.get_min_len()
        # ll2 = r2.get_min_len()

        x11 = r1.sk_x1
        y11 = r1.sk_y1
        x12 = r1.sk_x2
        y12 = r1.sk_y2

        x21 = r2.sk_x1
        y21 = r2.sk_y1
        x22 = r2.sk_x2
        y22 = r2.sk_y2
        # x21==613 and y21==1300 and x22==916
        # x21==613 and y21==1300 and x22==916
        a1 = self.ccw(x11, y11, x12, y12, x21, y21)
        a2 = self.ccw(x11, y11, x12, y12, x22, y22)
        a3 = self.ccw(x21, y21, x22, y22, x11, y11)
        a4 = self.ccw(x21, y21, x22, y22, x12, y12)
        # print r1.skeleton
        # print r2.skeleton
        # print '----------------------------------'
        if (a1*a2) <=0 and (a3*a4) <=0:
            return True
        else:
            return False

    def ccw(self,x1,y1,x2,y2,x3,y3):
        dx1 = x2-x1
        dy1 = y2-y1
        dx2 = x3-x1
        dy2 = y3-y1

        if (dx1*dy2 > dy1*dx2):
            return 1
        elif (dx1*dy2 < dy1*dx2):
            return -1
        elif (dx1*dx2 < 0 or dy1*dy2 < 0):
            return -1
        elif ((dx1*dx1 + dy1*dy1) < (dx2*dx2 + dy2*dy2)):
            return 1
        else:
            return 0

    def find_external_wall(self,debug_mode):
        temp_wall_list = []
        image=np.zeros((self.height,self.width,3),dtype=np.int32)
        # for rect in self.graph_rect.nodes():

        for rect in self.list_rect:
            rect.x_max=rect.x2
            rect.x_min=rect.x1
            rect.y_min=rect.y1
            rect.y_max=rect.y3


            if self.max_tink_wall>20 :
                if (rect.get_min_len()>(float(self.max_tink_wall)*0.4) and rect.get_min_len()<(float(self.max_tink_wall)*2 )):
                    rect.type = 'wall'
                    self.graph_wall.add_node(rect)
            else:
                if (rect.get_min_len()>(float(self.max_tink_wall)*0.6) and rect.get_min_len()<(float(self.max_tink_wall)*2)):
                    rect.type = 'wall'
                    self.graph_wall.add_node(rect)

        # for rec in self.graph_wall.nodes():
        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(0,0,255)
        # cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_2_ExternalWall.png', image)

        for rect in self.graph_wall.nodes():
            r = 0
            g = 0
            b = 255

            #if rect.type=="oblique":
            p = np.array([[[rect.x1, rect.y1],[rect.x2, rect.y2],[rect.x3, rect.y3],[rect.x4, rect.y4]]], dtype=np.int32)
            cv2.drawContours(image, [p], -1, (r,g,b), -1)
            temp_wall_list.append([[int(rect.x1), int(rect.y1)],[int(rect.x2), int(rect.y2)],
                                   [int(rect.x3), int(rect.y3)],[int(rect.x4), int(rect.y4)]])

        # if debug_mode:
        #     cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_2_ExternalWall.png', image)

        return temp_wall_list

    def recostructed_image_whit_rectangle_oblique(self,debug_mode):

        #image=np.zeros((self.height,self.width,3),dtype=np.uint8)
        image = np.array(self.color_image,dtype=np.int32)
        #image2=np.zeros((self.height,self.width),dtype=np.uint8)
        #self.color_image[:]
        list_id = []
        for rect in self.list_rect:
            #  print rect.id
            r=random.randint(0, 255)
            g=random.randint(0, 255)
            b=random.randint(0, 255)
            # if rect.id in list_id:
                # print 'ATTENZIONE!!!!!!!!!!!!'
                # print rect.id
            list_id.append(rect.id)
            if rect.type=="oblique":

                p = np.array([[[rect.x1, rect.y1],[rect.x2, rect.y2],[rect.x3, rect.y3],[rect.x4, rect.y4]]], dtype=np.int32)
                cv2.drawContours(image, [p], -1, (r,g,b), -1)

            else:
                image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)


        # if debug_mode:
        #     cv2.imwrite(self.path_debug+'wall/'+str(self.file_name[0:len(self.file_name)-4])+'_ricostruzione.png', image)
        #     cv2.imwrite(self.path_debug+'door/wall inspired_'+str(self.file_name), image)

    def classification_interior_walls(self, debug_mode,temp_wall_list):

        #-- this code bit can be here only if it's being called from mainlogic. Not from image.py
        # self.max_tink_wall = 43
        # with open(self.current_directory + '/Test_object_store/for_internal_wall/rect.pkl', 'rb') as input:
        #     self.list_rect = pickle.load(input)
        # with open(self.current_directory + '/Test_object_store/for_internal_wall/wall.pkl', 'rb') as input:
        #     for wall in pickle.load(input):
        #         self.graph_wall.add_node(wall)
        # with open(self.current_directory + '/Test_object_store/for_internal_wall/output.pkl', 'rb') as input:
        #     for output in pickle.load(input):
        #         self.graph_image_output.add_node(output)


        check = 1


        image = np.zeros((self.height, self.width, 3), dtype=np.uint8)

        if (len(self.graph_image_output.nodes()) > 0):
            list_walls = self.graph_image_output.nodes()
        else:
            list_walls = self.graph_wall.nodes()
        while (len(list_walls) > 0):

            list_new_walls = []
            if self.max_tink_wall >= 20:
                stop = True
                lim = float(self.max_tink_wall)

                lim = 5
                for n in list_walls:
                    for e in n.edges():
                        rect = e.get_node()
                        # if  rect.get_min_len()>=float(self.max_tink_wall)*0.1 and rect.get_min_len()<(float(self.max_tink_wall)*2 ) and not(self.graph_wall.isIn(rect)) :
                        if rect.get_min_len() >= lim and rect.get_min_len() < (float(
                                self.max_tink_wall) * 2) and rect.get_max_len() > self.max_tink_wall * 1.2 and not (
                        self.graph_wall.isIn(rect)):
                            stop = False
                            rect.type = 'wall'
                            list_new_walls.append(rect)
                            self.graph_wall.add_node(rect)
            else:
                for n in list_walls:
                    for e in n.edges():
                        rect = e.get_node()
                        if rect.get_min_len() >= float(self.max_tink_wall) * 0.3 and rect.get_min_len() < (
                            float(self.max_tink_wall) * 2) and not (self.graph_wall.isIn(rect)):
                            rect.type = 'wall'
                            list_new_walls.append(rect)
                            self.graph_wall.add_node(rect)

            list_walls = list_new_walls
            if len(list_new_walls) > 0:
                for r in list_new_walls:
                    self.graph_wall.add_node(r)
        # for rec in self.graph_wall.nodes():
        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(0,0,255)
        for rect in self.graph_wall.nodes():
            r = 0
            g = 0
            b = 255

            # if rect.type=="oblique":
            p = np.array([[[rect.x1, rect.y1], [rect.x2, rect.y2], [rect.x3, rect.y3], [rect.x4, rect.y4]]],
                         dtype=np.int32)
            cv2.drawContours(image, [p], -1, (r, g, b), -1)
            current_wall = [[int(rect.x1), int(rect.y1)], [int(rect.x2), int(rect.y2)],
                                       [int(rect.x3), int(rect.y3)], [int(rect.x4), int(rect.y4)]]
            if self.check_if_wall_exists(temp_wall_list,current_wall)== False:
                temp_wall_list.append(current_wall)

            # else:
            # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)

        # if debug_mode:
        #     if (len(self.graph_image_output.nodes()) > 0):
        #         cv2.imwrite(self.path_debug + 'wall/' + str(
        #             self.file_name[0:len(self.file_name) - 4]) + '_3_InternalWall_withDoor.png', image)
        #
        #     else:
        #         cv2.imwrite(self.path_debug + 'wall/' + str(
        #             self.file_name[0:len(self.file_name) - 4]) + '_3_InternalWall.png', image)


        # #---write methods
        # self.save_object(self.graph_wall.nodes(), self.path_debug + 'merge_all/wall.pkl')
        # self.save_object(self.list_rect, self.path_debug + 'merge_all/rect.pkl')
        # self.save_object(self.graph_image_output.nodes(), self.path_debug + 'merge_all/output.pkl')

        return temp_wall_list

    def check_if_wall_exists(self,wall_list,current_wall):
        x1,y1 = current_wall[0]
        x2,y2 = current_wall[1]
        x3,y3 = current_wall[2]
        x4, y4 = current_wall[3]
        exists = False
        for row in wall_list:
            x5, y5 = row[0]
            x6, y6 = row[1]
            x7, y7 = row[2]
            x8, y8 = row[3]
            if (x1==x5 and y1==y5) and (x2==x6 and y2==y6) and (
                    x3==x7 and y3==y7) and(x4==x8 and y4==y8):
                exists = True
                break
        if exists:
            return True
        else:
            return False



    # ----method 06: find doors functions
    def classification_door(self,debug_mode):
        # self.max_tink_wall = 43
        # with open(self.current_directory+'/Test_object_store/for_door/rect.pkl', 'rb') as input:
        #     self.list_rect = pickle.load(input)
        # with open(self.current_directory+'/Test_object_store/for_door/wall.pkl', 'rb') as input:
        #      for wall in pickle.load(input):
        #          self.graph_wall.add_node(wall)
        # with open(self.current_directory + '/Test_object_store/for_door/output.pkl', 'rb') as input:
        #     for output in pickle.load(input):
        #         self.graph_image_output.add_node(output)

        self.detect_door_arc(debug_mode)
        # --rotates detected doors to align with walls and merge with walls
        door_list = self.align_door_to_wall(debug_mode)



        # self.save_object(self.graph_wall.nodes(), self.path_debug + 'door/wall.pkl')
        # self.save_object(self.list_rect, self.path_debug + 'door/rect.pkl')
        # self.save_object(self.graph_image_output.nodes(), self.path_debug + 'door/output.pkl')

        return door_list

    def detect_door_arc(self,debug_mode):
        list_door = self.find_door_select_for_length(debug_mode)
        dic_rect = self.find_orientation_rectangle(list_door,debug_mode)
        self.find_left_arc_door(dic_rect)
        self.find_right_arc_door(dic_rect)
        self.find_down_arc_door(dic_rect)
        self.find_up_arc_door(dic_rect)
        image2 =  np.array(self.color_image)
        #print 'max tinck wall: '+str(self.max_tink_wall)
        for rec in self.list_door:
            image2[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(0,255,0)
        # if debug_mode:
        #     cv2.imwrite(self.path_debug+'door/'+str(self.file_name[0:len(self.file_name)-4])+'_final.png', image2)
        #     cv2.imwrite(self.path_debug+'merge_all/'+str(self.file_name[0:len(self.file_name)-4])+'_Door.png', image2)

    def find_door_select_for_length(self,debug_mode):
        list_door = []
        # if debug_mode:
        #     cv2.imwrite(self.path_debug+'door/ante/'+str(self.file_name[0:len(self.file_name)-4])+'.png', self.color_image)

        for r in self.list_rect:
            if self.max_tink_wall<=19:
                if r.get_max_len()>(self.max_tink_wall*3)and r.get_max_len()<(self.max_tink_wall*10) and r.get_min_len() < (self.max_tink_wall*0.8):
                   # if r.type!='wall':
                    list_door.append(r)
            if self.max_tink_wall>19:
                if r.get_max_len()>(self.max_tink_wall) and r.get_max_len()<(self.max_tink_wall*5)and r.get_min_len() < (self.max_tink_wall*0.4) :
                    #if r.type!='wall':
                        list_door.append(r)


        image =  np.array(self.color_image)
        for rec in list_door:
            r=0
            g=255
            b=0
            image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)
        # if debug_mode:
        #     cv2.imwrite(self.path_debug+'door/'+str(self.file_name[0:len(self.file_name)-4])+'_1.png', image)
        return list_door

    def find_orientation_rectangle(self,list_door,debug_mode):
        r=0
        g=255
        b=0
        image =  np.array(self.color_image)
        dic_rect = {'left':[],'right':[],'down':[],'up':[]}
        for rect in list_door:
            ori = rect.get_orientation()
            #thickness_door = rect.get_min_len()+1
            r_length = rect.get_max_len()
            if ori == 'vertical':
                if self.max_tink_wall<20:
                    # coordinate del riquadro a sinistra del rettangolo
                    x_min_left = rect.x2 - r_length - self.max_tink_wall
                    y_min_left = rect.y2 - int(self.max_tink_wall)
                    x_max_left = rect.x3 + 1
                    y_max_left = rect.y3 + int(self.max_tink_wall)
                    # coordinate del riquadro a destra del rettangolo
                    x_min_right = rect.x1 -1
                    y_min_right = rect.y1 - int(self.max_tink_wall)
                    x_max_right = rect.x4 + r_length + (self.max_tink_wall)
                    y_max_right = rect.y4 + int(self.max_tink_wall)

                    list_left_rect = []
                    list_right_rect = []
                    for rect2 in self.list_rect:
                        if rect2.x1 >= x_min_left and rect2.x3 <= x_max_left and rect2.y1 >= y_min_left and rect2.y3 <= y_max_left:
                            list_left_rect.append(rect2)
                        if rect2.x1 >= x_min_right and rect2.x3 <= x_max_right and rect2.y1 >= y_min_right and rect2.y3 <= y_max_right:
                            list_right_rect.append(rect2)

                    if len(list_left_rect)>(r_length/4):
                        dic_rect['left'].append(rect)
                        rect.list_left_rect.append(list_left_rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_left,y_min_left),(x_max_left,y_max_left),(255,255,0),1)
                        # for rec in list_left_rect:
                        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_right_rect)>(r_length/4):
                        dic_rect['right'].append(rect)
                        rect.list_right_rect.append(list_right_rect)
                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_right,y_min_right),(x_max_right,y_max_right),(255,255,0),1)
                else:
                    # coordinate del riquadro a sinistra del rettangolo
                    x_min_left = rect.x2 - r_length - self.max_tink_wall
                    y_min_left = rect.y2 - int(self.max_tink_wall*2/3)
                    x_max_left = rect.x3 + 1
                    y_max_left = rect.y3 + int(self.max_tink_wall*2/3)
                    # coordinate del riquadro a destra del rettangolo
                    x_min_right = rect.x1 -1
                    y_min_right = rect.y1 - int(self.max_tink_wall*2/3)
                    x_max_right = rect.x4 + r_length + (self.max_tink_wall)
                    y_max_right = rect.y4 + int(self.max_tink_wall*2/3)
                    # coordinate del riquadro a sinistra del rettangolo
                    x_min_left2 = rect.x2 - r_length - self.max_tink_wall
                    y_min_left2 = rect.y2 - int(self.max_tink_wall/2)
                    x_max_left2 = rect.x3 + 1
                    y_max_left2 = rect.y3 + int(self.max_tink_wall/2)
                    # coordinate del riquadro a destra del rettangolo
                    x_min_right2 = rect.x1 -1
                    y_min_right2 = rect.y1 - int(self.max_tink_wall/2)
                    x_max_right2 = rect.x4 + r_length + (self.max_tink_wall)
                    y_max_right2 = rect.y4 + int(self.max_tink_wall/2)


                    list_left_rect = []
                    list_right_rect = []
                    list_left_rect2 = []
                    list_right_rect2 = []
                    for rect2 in self.list_rect:
                        if rect2.x1 >= x_min_left and rect2.x3 <= x_max_left and rect2.y1 >= y_min_left and rect2.y3 <= y_max_left:
                            list_left_rect.append(rect2)
                        if rect2.x1 >= x_min_right and rect2.x3 <= x_max_right and rect2.y1 >= y_min_right and rect2.y3 <= y_max_right:
                            list_right_rect.append(rect2)
                    for rect2 in self.list_rect:
                        if rect2.x1 >= x_min_left2 and rect2.x3 <= x_max_left2 and rect2.y1 >= y_min_left2 and rect2.y3 <= y_max_left2:
                            list_left_rect2.append(rect2)
                        if rect2.x1 >= x_min_right2 and rect2.x3 <= x_max_right2 and rect2.y1 >= y_min_right2 and rect2.y3 <= y_max_right2:
                            list_right_rect2.append(rect2)

                    if len(list_left_rect)>(r_length/4):
                        if not(rect in dic_rect['left']):
                            dic_rect['left'].append(rect)
                            rect.list_left_rect.append(list_left_rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_left,y_min_left),(x_max_left,y_max_left),(255,255,0),1)
                        # for rec in list_left_rect:
                        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_right_rect)>(r_length/4):
                        if not(rect in dic_rect['right']):
                            dic_rect['right'].append(rect)
                            rect.list_right_rect.append(list_right_rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_right,y_min_right),(x_max_right,y_max_right),(255,255,0),1)
                        # for rec in list_right_rect:
                        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_left_rect2)>(r_length/4):
                        if not(rect in dic_rect['left']):
                            dic_rect['left'].append(rect)
                            rect.list_left_rect.append(list_left_rect2)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_left,y_min_left),(x_max_left,y_max_left),(255,255,0),1)
                        # for rec in list_left_rect:
                        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_right_rect2)>(r_length/4):
                        if not(rect in dic_rect['right']):
                            dic_rect['right'].append(rect)
                            rect.list_right_rect.append(list_right_rect2)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_right,y_min_right),(x_max_right,y_max_right),(255,255,0),1)

            else:
                # coordinate del riquadro sotto del rettangolo
                #   if self.max_tink_wall>19:
                if self.max_tink_wall<20:
                    x_min_down = rect.x1 - int(self.max_tink_wall)
                    y_min_down = rect.y1 - 1
                    x_max_down = rect.x2 + int(self.max_tink_wall)
                    y_max_down = rect.y2 + r_length + (self.max_tink_wall)
                    # coordinate del riquadro sopra del rettangolo
                    x_min_up = rect.x4 - int(self.max_tink_wall)
                    y_min_up = rect.y4 -  r_length - (self.max_tink_wall)
                    x_max_up = rect.x3 + int(self.max_tink_wall)
                    y_max_up = rect.y3 + 1
                    list_down_rect = []
                    list_up_rect = []
                    for rect2 in self.list_rect:
                        if rect2.x1 >= x_min_down and rect2.x3 <= x_max_down and rect2.y1 >= y_min_down and rect2.y3 <= y_max_down:
                            list_down_rect.append(rect2)
                        if rect2.x1 >= x_min_up and rect2.x3 <= x_max_up and rect2.y1 >= y_min_up and rect2.y3 <= y_max_up:
                            list_up_rect.append(rect2)

                    if len(list_down_rect)>(r_length/4):
                        rect.list_down_rect.append(list_down_rect)
                        dic_rect['down'].append(rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_down,y_min_down),(x_max_down,y_max_down),(255,255,0),1)
                        # for rec in list_down_rect:
                        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_up_rect)>(r_length/4):
                        rect.list_up_rect.append(list_up_rect)
                        dic_rect['up'].append(rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_up,y_min_up),(x_max_up,y_max_up),(255,255,0),1)
                else:
                    x_min_down = rect.x1 - int(self.max_tink_wall*2/3)
                    y_min_down = rect.y1 - 1
                    x_max_down = rect.x2 + int(self.max_tink_wall*2/3)
                    y_max_down = rect.y2 + r_length + (self.max_tink_wall)
                    # coordinate del riquadro sopra del rettangolo
                    x_min_up = rect.x4 - int(self.max_tink_wall*2/3)
                    y_min_up = rect.y4 -  r_length - (self.max_tink_wall)
                    x_max_up = rect.x3 + int(self.max_tink_wall*2/3)
                    y_max_up = rect.y3 + 1

                    x_min_down2 = rect.x1 - int(self.max_tink_wall*2/3)
                    y_min_down2 = rect.y1 - 1
                    x_max_down2 = rect.x2 + int(self.max_tink_wall*2/3)
                    y_max_down2 = rect.y2 + r_length + (self.max_tink_wall)
                    # coordinate del riquadro sopra del rettangolo
                    x_min_up2 = rect.x4 - int(self.max_tink_wall*2/3)
                    y_min_up2 = rect.y4 -  r_length - (self.max_tink_wall)
                    x_max_up2 = rect.x3 + int(self.max_tink_wall*2/3)
                    y_max_up2 = rect.y3 + 1


                    list_down_rect = []
                    list_up_rect = []
                    list_down_rect2 = []
                    list_up_rect2 = []
                    for rect2 in self.list_rect:
                        if rect2.x1 >= x_min_down and rect2.x3 <= x_max_down and rect2.y1 >= y_min_down and rect2.y3 <= y_max_down:
                            list_down_rect.append(rect2)
                        if rect2.x1 >= x_min_up and rect2.x3 <= x_max_up and rect2.y1 >= y_min_up and rect2.y3 <= y_max_up:
                            list_up_rect.append(rect2)
                    for rect2 in self.list_rect:
                        if rect2.x1 >= x_min_down2 and rect2.x3 <= x_max_down2 and rect2.y1 >= y_min_down2 and rect2.y3 <= y_max_down2:
                            list_down_rect2.append(rect2)
                        if rect2.x1 >= x_min_up2 and rect2.x3 <= x_max_up2 and rect2.y1 >= y_min_up2 and rect2.y3 <= y_max_up2:
                            list_up_rect2.append(rect2)


                    if len(list_down_rect)>(r_length/4):
                        if not(rect in dic_rect['down']):
                            rect.list_down_rect.append(list_down_rect)
                            dic_rect['down'].append(rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_down,y_min_down),(x_max_down,y_max_down),(255,255,0),1)
                        # for rec in list_down_rect:
                        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_up_rect)>(r_length/4):
                        if not(rect in dic_rect['up']):
                            rect.list_up_rect.append(list_up_rect)
                            dic_rect['up'].append(rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_up,y_min_up),(x_max_up,y_max_up),(255,255,0),1)
                    # for rec in list_up_rect:
                    #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_down_rect2)>(r_length/4):
                        if not(rect in dic_rect['down']):
                            rect.list_down_rect.append(list_down_rect2)
                            dic_rect['down'].append(rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_down,y_min_down),(x_max_down,y_max_down),(255,255,0),1)
                        # for rec in list_down_rect:
                        #     image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)

                    if len(list_up_rect2)>(r_length/4):
                        if not(rect in dic_rect['up']):
                            rect.list_up_rect.append(list_up_rect2)
                            dic_rect['up'].append(rect)

                        # image[rect.y1-1:rect.y4,rect.x1-1:rect.x2]=(r,g,b)
                        # cv2.rectangle(image,(x_min_up,y_min_up),(x_max_up,y_max_up),(255,255,0),1)
                    for rec in list_up_rect:
                        image[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)
        # if debug_mode:
        #     cv2.imwrite(self.path_debug+'door/'+str(self.file_name[0:len(self.file_name)-4])+'_2.png', image)
        return dic_rect

    def find_left_arc_door(self,dic_rect):
        for rect in dic_rect['left']:

            thickness_door = rect.get_min_len()+1
            if thickness_door<0:
                thickness_door=np.abs(thickness_door)
            elif thickness_door==0:
                thickness_door=1
            #aggiungo lo spessore del muro alla porta

            c1,o1,l1 = self.check_door_left( rect.x_min, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_left_rect, 'left')
            if(not(c1)):

                c2,o2,l2 = self.check_door_left( rect.x_min, rect.y_min, rect.x_max, rect.y_max+self.max_tink_wall, thickness_door, rect.list_left_rect, 'left')
                if(not(c2)):

                    c3,o3,l3 = self.check_door_left( rect.x_min, rect.y_min - self.max_tink_wall, rect.x_max, rect.y_max, thickness_door, rect.list_left_rect, 'left')
                    if(not(c3)):
                        c4,o4,l4 = self.check_door_left( rect.x_min, rect.y_min, rect.x_max, rect.y_max + int(self.max_tink_wall/2), thickness_door, rect.list_left_rect, 'left')
                        if(not(c4)):
                            c5,o5,l5 = self.check_door_left( rect.x_min, rect.y_min - int(self.max_tink_wall/2), rect.x_max, rect.y_max, thickness_door, rect.list_left_rect, 'left')
            #tolgo lo spessore del muro alla porta
                            if(not(c5)):
                                c6,o6,l6 = self.check_door_left( rect.x_min, rect.y_min, rect.x_max, rect.y_max - self.max_tink_wall, thickness_door, rect.list_left_rect, 'left')
                                if(not(c6)):
                                    c7,o7,l7 = self.check_door_left( rect.x_min, rect.y_min + self.max_tink_wall, rect.x_max, rect.y_max, thickness_door, rect.list_left_rect, 'left')
                                    if(c7):
                                        rect.opening = [o7,'left']
                                        self.list_door.append(rect)
                                        # for rd in l7:
                                        #     # for rr in self.list_rect:
                                        #     #     if rr.id==rd.id or rr.id==rect.id:
                                        #     rd.type = 'door'
                                else:
                                    rect.opening = [o6,'left']
                                    self.list_door.append(rect)
                                    # for rd in l6:
                                    #     # for rr in self.list_rect:
                                    #     #     if rr.id==rd.id or rr.id==rect.id:
                                    #     rd.type = 'door'
                            else:
                                rect.opening = [o5,'left']
                                self.list_door.append(rect)
                                # for rd in l5:
                                #     # for rr in self.list_rect:
                                #     #     if rr.id==rd.id or rr.id==rect.id:
                                #     rd.type = 'door'

                        else:
                            rect.opening = [o4,'left']
                            self.list_door.append(rect)
                            # for rd in l4:
                            #     # for rr in self.list_rect:
                            #     #     if rr.id==rd.id or rr.id==rect.id:
                            #     rd.type = 'door'
                    else:
                        rect.opening = [o3,'left']
                        self.list_door.append(rect)
                        # for rd in l3:
                        #     # for rr in self.list_rect:
                        #     #     if rr.id==rd.id or rr.id==rect.id:
                        #     rd.type = 'door'
                else:
                    rect.opening = [o2,'left']
                    self.list_door.append(rect)
                    # for rd in l2:
                    #     # for rr in self.list_rect:
                    #     #     if rr.id==rd.id or rr.id==rect.id:
                    #     rd.type = 'door'
            else:
                rect.opening = [o1,'left']
                self.list_door.append(rect)
                # for rd in l1:
                #     # for rr in self.list_rect:
                #     #     if rr.id==rd.id or rr.id==rect.id:
                #     rd.type = 'door'

    def find_right_arc_door(self,dic_rect):

        for rect in dic_rect['right']:
            thickness_door = rect.get_min_len()+1
            if thickness_door<0:
                thickness_door=np.abs(thickness_door)
            elif thickness_door==0:
                thickness_door=1
            #aggiungo lo spessore del muro alla porta

            c1,o1,l1 = self.check_door_right( rect.x_min, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_right_rect, 'right')
            if(not(c1)):
                c2,o2,l2 = self.check_door_right( rect.x_min, rect.y_min, rect.x_max, rect.y_max+self.max_tink_wall, thickness_door, rect.list_right_rect, 'right')
                if(not(c2)):
                    c3,o3,l3 = self.check_door_right( rect.x_min, rect.y_min - self.max_tink_wall, rect.x_max, rect.y_max, thickness_door, rect.list_right_rect, 'right')
                    if(not(c3)):
                        c4,o4,l4 = self.check_door_right( rect.x_min, rect.y_min, rect.x_max, rect.y_max + int(self.max_tink_wall/2), thickness_door, rect.list_right_rect, 'right')
                        if(not(c4)):
                            c5,o5,l5 = self.check_door_right( rect.x_min, rect.y_min - int(self.max_tink_wall/2), rect.x_max, rect.y_max, thickness_door, rect.list_right_rect, 'right')
            #tolgo lo spessore del muro alla porta
                            if(not(c5)):
                                c6,o6,l6 = self.check_door_right( rect.x_min, rect.y_min, rect.x_max, rect.y_max - self.max_tink_wall, thickness_door, rect.list_right_rect, 'right')
                                if(not(c6)):
                                    c7,o7,l7 = self.check_door_right( rect.x_min, rect.y_min + self.max_tink_wall, rect.x_max, rect.y_max, thickness_door, rect.list_right_rect, 'right')
                                    if(c7):
                                        rect.opening = [o7,'right']
                                        self.list_door.append(rect)

                                        # for rd in l7:
                                        #     # for rr in self.list_rect:
                                        #     #     if rr.id==rd.id or rr.id==rect.id:
                                        #     rd.type = 'door'
                                else:
                                    rect.opening = [o6,'right']
                                    self.list_door.append(rect)
                                    # for rd in l6:
                                    #     # for rr in self.list_rect:
                                    #     #     if rr.id==rd.id or rr.id==rect.id:
                                    #     rd.type = 'door'
                            else:
                                rect.opening = [o5,'right']
                                self.list_door.append(rect)
                                # for rd in l5:
                                #     # for rr in self.list_rect:
                                #     #     if rr.id==rd.id or rr.id==rect.id:
                                #     rd.type = 'door'

                        else:
                            rect.opening = [o4,'right']
                            self.list_door.append(rect)
                            # for rd in l4:
                            #     # for rr in self.list_rect:
                            #     #     if rr.id==rd.id or rr.id==rect.id:
                            #     rd.type = 'door'
                    else:
                        rect.opening = [o3,'right']
                        self.list_door.append(rect)
                        # for rd in l3:
                        #     # for rr in self.list_rect:
                        #     #     if rr.id==rd.id or rr.id==rect.id:
                        #     rd.type = 'door'
                else:
                    rect.opening = [o2,'right']
                    self.list_door.append(rect)
                    # for rd in l2:
                    #     # for rr in self.list_rect:
                    #     #     if rr.id==rd.id or rr.id==rect.id:
                    #     rd.type = 'door'
            else:
                rect.opening = [o1,'right']
                self.list_door.append(rect)
                # for rd in l1:
                #     # for rr in self.list_rect:
                #     #     if rr.id==rd.id or rr.id==rect.id:
                #     rd.type = 'door'

    def find_down_arc_door(self,dic_rect):

        for rect in dic_rect['down']:
            thickness_door = rect.get_min_len()+1
            if thickness_door<0:
                thickness_door=np.abs(thickness_door)
            elif thickness_door==0:
                thickness_door=1
            #aggiungo lo spessore del muro alla porta

            c1,o1,l1 = self.check_door_down( rect.x_min, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_down_rect, 'down')
            if(not(c1)):
                c2,o2,l2 = self.check_door_down( rect.x_min, rect.y_min, rect.x_max + self.max_tink_wall, rect.y_max, thickness_door, rect.list_down_rect, 'down')
                if(not(c2)):
                    c3,o3,l3 = self.check_door_down( rect.x_min  - self.max_tink_wall, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_down_rect, 'down')
                    if(not(c3)):
                        c4,o4,l4 = self.check_door_down( rect.x_min, rect.y_min, rect.x_max + int(self.max_tink_wall/2) , rect.y_max , thickness_door, rect.list_down_rect, 'down')
                        if(not(c4)):
                            c5,o5,l5 = self.check_door_down( rect.x_min - int(self.max_tink_wall/2), rect.y_min , rect.x_max, rect.y_max, thickness_door, rect.list_down_rect, 'down')
            #tolgo lo spessore del muro alla porta
                            if(not(c5)):
                                c6,o6,l6 = self.check_door_down( rect.x_min, rect.y_min, rect.x_max - self.max_tink_wall, rect.y_max, thickness_door, rect.list_down_rect, 'down')
                                if(not(c6)):
                                    c7,o7,l7 = self.check_door_down( rect.x_min  + self.max_tink_wall, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_down_rect, 'down')
                                    if(c7):
                                        rect.opening = [o7,'down']
                                        self.list_door.append(rect)
                                        # for rd in l7:
                                        #     # for rr in self.list_rect:
                                        #     #     if rr.id==rd.id or rr.id==rect.id:
                                        #     rd.type = 'door'
                                else:
                                    rect.opening = [o6,'down']
                                    self.list_door.append(rect)
                                    # for rd in l6:
                                    #     # for rr in self.list_rect:
                                    #     #     if rr.id==rd.id or rr.id==rect.id:
                                    #     rd.type = 'door'
                            else:
                                rect.opening = [o5,'down']
                                self.list_door.append(rect)
                                # for rd in l5:
                                #     # for rr in self.list_rect:
                                #     #     if rr.id==rd.id or rr.id==rect.id:
                                #     rd.type = 'down'

                        else:
                            rect.opening = [o4,'down']
                            self.list_door.append(rect)
                            # for rd in l4:
                            #     # for rr in self.list_rect:
                            #     #     if rr.id==rd.id or rr.id==rect.id:
                            #     rd.type = 'door'
                    else:
                        rect.opening = [o3,'down']
                        self.list_door.append(rect)
                        # for rd in l3:
                        #     # for rr in self.list_rect:
                        #     #     if rr.id==rd.id or rr.id==rect.id:
                        #     rd.type = 'door'
                else:
                    rect.opening = [o2,'down']
                    self.list_door.append(rect)
                    # for rd in l2:
                    #     # for rr in self.list_rect:
                    #     #     if rr.id==rd.id or rr.id==rect.id:
                    #     rd.type = 'door'
            else:
                rect.opening = [o1,'down']
                self.list_door.append(rect)
                # for rd in l1:
                #     # for rr in self.list_rect:
                #     #     if rr.id==rd.id or rr.id==rect.id:
                #     rd.type = 'door'

    def find_up_arc_door(self,dic_rect):

        for rect in dic_rect['up']:
            thickness_door = rect.get_min_len()+1
            if thickness_door<0:
                thickness_door=np.abs(thickness_door)
            elif thickness_door==0:
                thickness_door=1
            #aggiungo lo spessore del muro alla porta
            c1,o1,l1 = self.check_door_up( rect.x_min, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_up_rect, 'up',1)

            if(not(c1)):
                c2,o2,l2 = self.check_door_up( rect.x_min, rect.y_min, rect.x_max + self.max_tink_wall, rect.y_max, thickness_door, rect.list_up_rect, 'up',2)
                if(not(c2)):
                    c3,o3,l3 = self.check_door_up( rect.x_min  - self.max_tink_wall, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_up_rect, 'up',3)
                    if(not(c3)):
                        c4,o4,l4 = self.check_door_up( rect.x_min, rect.y_min, rect.x_max + int(self.max_tink_wall/2) , rect.y_max , thickness_door, rect.list_up_rect, 'up',4)
                        if(not(c4)):
                            c5,o5,l5 = self.check_door_up( rect.x_min - int(self.max_tink_wall/2), rect.y_min , rect.x_max, rect.y_max, thickness_door, rect.list_up_rect, 'up',5)
            #tolgo lo spessore del muro alla porta
                            if(not(c5)):
                                c6,o6,l6 = self.check_door_up( rect.x_min, rect.y_min, rect.x_max - self.max_tink_wall, rect.y_max, thickness_door, rect.list_up_rect, 'up',6)
                                if(not(c6)):
                                    c7,o7,l7 = self.check_door_up( rect.x_min  + self.max_tink_wall, rect.y_min, rect.x_max, rect.y_max, thickness_door, rect.list_up_rect, 'up',7)
                                    if(c7):
                                        rect.opening = [o7,'up']
                                        self.list_door.append(rect)
                                        # for rd in l7:
                                        #     # for rr in self.list_rect:
                                        #     #     if rr.id==rd.id or rr.id==rect.id:
                                        #     rd.type = 'door'
                                else:
                                    rect.opening = [o6,'up']
                                    self.list_door.append(rect)
                                    # for rd in l6:
                                    #     # for rr in self.list_rect:
                                    #     #     if rr.id==rd.id or rr.id==rect.id:
                                    #     rd.type = 'door'
                            else:
                                rect.opening = [o5,'up']
                                self.list_door.append(rect)
                                # for rd in l5:
                                #     # for rr in self.list_rect:
                                #     #     if rr.id==rd.id or rr.id==rect.id:
                                #     rd.type = 'door'

                        else:
                            rect.opening = [o4,'up']
                            self.list_door.append(rect)
                            # for rd in l4:
                            #     # for rr in self.list_rect:
                            #     #     if rr.id==rd.id or rr.id==rect.id:
                            #     rd.type = 'door'
                    else:
                        rect.opening = [o3,'up']
                        self.list_door.append(rect)
                        # for rd in l3:
                        #     # for rr in self.list_rect:
                        #     #     if rr.id==rd.id or rr.id==rect.id:
                        #     rd.type = 'door'
                else:
                    rect.opening = [o2,'up']
                    self.list_door.append(rect)
                    # for rd in l2:
                    #     # for rr in self.list_rect:
                    #     #     if rr.id==rd.id or rr.id==rect.id:
                    #     rd.type = 'door'
            else:
                rect.opening = [o1,'up']
                self.list_door.append(rect)
                # for rd in l1:
                #     # for rr in self.list_rect:
                #     #     if rr.id==rd.id or rr.id==rect.id:
                #     rd.type = 'door'

    def check_door_left(self,x_min, y_min, x_max, y_max, thickness_door, list_rectangle, side):

        if self.max_tink_wall > 20:
            t1=3
            t2=3
            perc=0.5
        else:
            t1=1
            t2=1
            perc=0.6
        # calcolo il baricentro di ogni rettangolo e guardo se la distanza dal centro el cerchio e' minore di r1  e maggiore di r2
        for rectangle in list_rectangle:
            h = y_max-y_min
            img =  np.array(self.image_rect)
            r1 = int(h*0.8)
            r2 = int((float(h)*1.2))
            check = False
            list_rect_door_1 = []
            list_rect_door_2 = []
            if self.max_tink_wall > 20:
                z=(int(h/thickness_door))*0.25
            else:
                z=(int(h/thickness_door))*0.5
            # conto i numeri di rettangoli che hanno il baricentro a una distanza compresa tra r1 e r2
            for rec in rectangle:
                dist_1 = rec.get_distance_to_centroid(x_max,y_max)
                dist_2 = rec.get_distance_to_centroid(x_max,y_min)
                if dist_1 < r2 and dist_1 > r1:
                    list_rect_door_1.append(rec)
                if dist_2 < r2 and dist_2 > r1:
                    list_rect_door_2.append(rec)

            # guardo apertura della porta e dal basso verso alto o dal alto verso il basso
            # e controllo se i rettangoli sono disposti in modo omogeneo
            if len(list_rect_door_1) > math.ceil(len(rectangle)*perc):
                arc = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                arc2 = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                list_rip_11 = []
                list_rip_12 = []
                l11 = list_rect_door_1[:]
                l12 = list_rect_door_1[:]
                for i in range(0,(int((y_max-y_min)/thickness_door))+1):
                    p1 = y_min + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l11:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                           # arc[i] = 1
                            if (list_rip_11.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l11:
                            #         l11.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_11.append(r.id)
                    p3 = (x_min-h) + (i*thickness_door)
                    p4 = p3+thickness_door
                    for r in l12:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                            # arc2[i] = 1
                            if (list_rip_12.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l12:
                            #         l12.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_12.append(r.id)
                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0

                #controllo se ci sono dei "buchi" nel presunto arco
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0

                if count_zeros > max_zeros:
                    max_zeros = count_zeros

                if count2_zeros > max2_zeros:
                    max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                  #   print '####################'
                  #   print 'left 1 si'
                  # # print self.max_tink_wall
                  #   print math.ceil(len(rectangle)*perc)
                  #   print len(list_rect_door_1)
                  # #  print len(list_rect_door_2)
                  #   print arc
                  #   print arc2
                  #   cv2.circle(img,(x_max,y_max),r1,(255,255,255),1)
                  #   cv2.circle(img,(x_max,y_max),r2,(255,255,255),1)
                  #   # cv2.circle(img,(x_max,y_min),r1,(255,255,0),1)
                  #   # cv2.circle(img,(x_max,y_min),r2,(255,255,0),1)
                  #   cv2.rectangle(img,(x_min,y_min),(x_max,y_max),(255,255,0),1)
                  #   img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                  #   plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min-r2:x_max+1], cmap = 'gray', interpolation = 'bicubic')
                  #   # plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
                  #   plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                  #   plt.title('left-prima')
                  #   plt.show()

                    check = True
                    opening = 'up-down'
                   # print len(list_rect_door_1)
                    return check,opening,list_rect_door_1
                #else:
                    # print '####################'
                    # print 'left 1 no'
                    # #print self.max_tink_wall
                    # print math.ceil(len(rectangle)*perc)
                    # print len(list_rect_door_1)
                    # #print len(list_rect_door_2)
                    # print arc
                    # print arc2
                    # cv2.circle(img,(x_max,y_max),r1,(255,255,255),1)
                    # cv2.circle(img,(x_max,y_max),r2,(255,255,255),1)
                    # # cv2.circle(img,(x_max,y_min),r1,(255,255,0),1)
                    # # cv2.circle(img,(x_max,y_min),r2,(255,255,0),1)
                    # cv2.rectangle(img,(x_min,y_min),(x_max,y_max),(255,255,0),1)
                    # img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                    # plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min-r2:x_max+1], cmap = 'gray', interpolation = 'bicubic')
                    # # plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
                    # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                    # plt.title('left-prima')
                    # plt.show()


            if len(list_rect_door_2)> math.ceil(len(rectangle)*perc):

                arc2 = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                arc = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                list_rip_21 = []
                list_rip_22 = []
                l21 = list_rect_door_2[:]
                l22 = list_rect_door_2[:]
                for i in range(0,(int((y_max-y_min)/thickness_door))+1):
                    p1 = y_min + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l21:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                            #arc[i] = 1
                            if (list_rip_21.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l21:
                            #         l21.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_21.append(r.id)
                    p3 = (x_min-h)+ (i*thickness_door)
                    p4 = p3+thickness_door
                    for r in l22:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                            # arc2[i] = 1
                            if (list_rip_22.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l22:
                            #         l22.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_22.append(r.id)

                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0
                if count_zeros > max_zeros:
                        max_zeros = count_zeros
                if count2_zeros > max2_zeros:
                        max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                    # print '####################'
                    # print 'left 2 si'
                    # # print self.max_tink_wall
                    # print math.ceil(len(rectangle)*perc)
                    # # print len(list_rect_door_1)
                    # print len(list_rect_door_2)
                    # print arc
                    # print arc2
                    # # cv2.circle(img,(x_max,y_max),r1,(255,255,255),1)
                    # # cv2.circle(img,(x_max,y_max),r2,(255,255,255),1)
                    # cv2.circle(img,(x_max,y_min),r1,(255,255,0),1)
                    # cv2.circle(img,(x_max,y_min),r2,(255,255,0),1)
                    # cv2.rectangle(img,(x_min,y_min),(x_max,y_max),(255,255,0),1)
                    # img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                    # plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min-r2:x_max+1], cmap = 'gray', interpolation = 'bicubic')
                    # # plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
                    # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                    # plt.title('left-prima')
                    # plt.show()
                    check = True
                    opening = 'down-up'
                   # print len(list_rect_door_2)
                    return check,opening,list_rect_door_2
                #else:
                    # print '####################'
                    # print 'left 2 no'
                    # # print self.max_tink_wall
                    # print math.ceil(len(rectangle)*perc)
                    # #print len(list_rect_door_1)
                    # print len(list_rect_door_2)
                    # print arc
                    # print arc2
                    # # cv2.circle(img,(x_max,y_max),r1,(255,255,255),1)
                    # # cv2.circle(img,(x_max,y_max),r2,(255,255,255),1)
                    # cv2.circle(img,(x_max,y_min),r1,(255,255,0),1)
                    # cv2.circle(img,(x_max,y_min),r2,(255,255,0),1)
                    # cv2.rectangle(img,(x_min,y_min),(x_max,y_max),(255,255,0),1)
                    # img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                    # plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min-r2:x_max+1], cmap = 'gray', interpolation = 'bicubic')
                    # # plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
                    # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                    # plt.title('left-prima')
                    # plt.show()


        list_none = []

        return False,'None', list_none

    def check_door_right(self,x_min, y_min, x_max, y_max, thickness_door, list_rectangle, side):

        if self.max_tink_wall > 20:
            t1=3
            t2=3
            perc=0.5
        else:
            t1=1
            t2=1
            perc=0.6

        for rectangle in list_rectangle:
            h = y_max-y_min
            img =  np.array(self.image_rect)
            r1 = int(h*0.8)
            r2 = int((float(h)*1.2))

            check = False
            list_rect_door_1 = []
            list_rect_door_2 = []
            if self.max_tink_wall > 20:
                z=(int(h/thickness_door))*0.25
            else:
                z=(int(h/thickness_door))*0.5
            for rec in rectangle:
                dist_1 = rec.get_distance_to_centroid(x_min,y_max)
                dist_2 = rec.get_distance_to_centroid(x_min,y_min)
                if dist_1 < r2 and dist_1 > r1:
                    list_rect_door_1.append(rec)
                if dist_2 < r2 and dist_2 > r1:
                    list_rect_door_2.append(rec)


            if len(list_rect_door_1) > math.ceil(len(rectangle)*perc):


                arc = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                arc2 = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                list_rip_11 = []
                list_rip_12 = []
                l11 = list_rect_door_1[:]
                l12 = list_rect_door_1[:]
                for i in range(0,(int((y_max-y_min)/thickness_door))+1):
                    p1 = y_min + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l11:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                            # arc[i] = 1
                            if (list_rip_11.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l11:
                            #         l11.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_11.append(r.id)
                    p3 = x_max + (i*thickness_door)
                    p4 = p3+thickness_door

                    for r in l12:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                           # arc2[i] = 1
                            if (list_rip_12.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l12:
                            #         l12.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_12.append(r.id)
                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0
                if count_zeros > max_zeros:
                    max_zeros = count_zeros

                if count2_zeros > max2_zeros:
                    max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                   # print '####################'
                   # print 'right  1 si'
                   # # print self.max_tink_wall
                   # print math.ceil(len(rectangle)*perc)
                   # print len(list_rect_door_1)
                   # #  print len(list_rect_door_2)
                   # print arc
                   # print arc2
                   # # cv2.circle(img,(x_min,y_min),r1,(255,255,255),1)
                   # # cv2.circle(img,(x_min,y_min),r2,(255,255,255),1)
                   # cv2.circle(img,(x_min,y_max),r1,(255,255,0),1)
                   # cv2.circle(img,(x_min,y_max),r2,(255,255,0),1)
                   # img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                   # plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min:x_max+r2], cmap = 'gray', interpolation = 'bicubic')
                   # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                   # plt.title('left-prima')
                   # plt.show()
                   check = True
                   opening = 'up-down'
                   return check,opening,list_rect_door_1
                # else:
                #     print '####################'
                #     print 'right  1 no'
                #     # print self.max_tink_wall
                #     print math.ceil(len(rectangle)*perc)
                #     print len(list_rect_door_1)
                #     # print len(list_rect_door_2)
                #     print arc
                #     print arc2
                #     # cv2.circle(img,(x_min,y_min),r1,(255,255,255),1)
                #     # cv2.circle(img,(x_min,y_min),r2,(255,255,255),1)
                #     cv2.circle(img,(x_min,y_max),r1,(255,255,0),1)
                #     cv2.circle(img,(x_min,y_max),r2,(255,255,0),1)
                #     img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                #     plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min:x_max+r2], cmap = 'gray', interpolation = 'bicubic')
                #     plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                #     plt.title('left-prima')
                #     plt.show()
            # else:
            #     print '####################'
            #     print 'right  1 no'
            #     # print self.max_tink_wall
            #     print math.ceil(len(rectangle)*perc)
            #     print len(list_rect_door_1)
            #     #  print len(list_rect_door_2)
            #     # cv2.circle(img,(x_min,y_min),r1,(255,255,255),1)
            #     # cv2.circle(img,(x_min,y_min),r2,(255,255,255),1)
            #     cv2.circle(img,(x_min,y_max),r1,(255,255,0),1)
            #     cv2.circle(img,(x_min,y_max),r2,(255,255,0),1)
            #     img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
            #     plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min:x_max+r2], cmap = 'gray', interpolation = 'bicubic')
            #     plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
            #     plt.title('left-prima')
            #     plt.show()



            if len(list_rect_door_2) > math.ceil(len(rectangle)*perc):

                arc2 = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                arc = np.zeros((int((y_max-y_min)/thickness_door))+1,dtype=np.uint8)
                list_rip_21 = []
                list_rip_22 = []
                l21 = list_rect_door_2[:]
                l22 = list_rect_door_2[:]
                for i in range(0,(int((y_max-y_min)/thickness_door))+1):
                    p1 = y_min + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l21:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                            #arc[i] = 1
                            if (list_rip_21.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l21:
                            #         l21.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_21.append(r.id)
                    p3 = x_max + (i*thickness_door)
                    p4 = p3+thickness_door
                    for r in l22:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                            #arc2[i] = 1
                            if (list_rip_22.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l22:
                            #         l22.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_22.append(r.id)

                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0
                if count_zeros > max_zeros:
                    max_zeros = count_zeros
                if count2_zeros > max2_zeros:
                    max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                    # print '####################'
                    # print 'right list 2 si'
                    # # print self.max_tink_wall
                    # print math.ceil(len(rectangle)*perc)
                    # # print len(list_rect_door_1)
                    # print len(list_rect_door_2)
                    # print arc
                    # print arc2
                    # cv2.circle(img,(x_min,y_min),r1,(255,255,255),1)
                    # cv2.circle(img,(x_min,y_min),r2,(255,255,255),1)
                    # # cv2.circle(img,(x_min,y_max),r1,(255,255,0),1)
                    # # cv2.circle(img,(x_min,y_max),r2,(255,255,0),1)
                    # img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                    # plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min:x_max+r2], cmap = 'gray', interpolation = 'bicubic')
                    # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                    # plt.title('left-prima')
                    # plt.show()
                    check = True
                    opening = 'down-up'
                    return check,opening,list_rect_door_2
                # else:
                #     print '####################'
                #     print 'right list 2 no'
                #     # print self.max_tink_wall
                #     print math.ceil(len(rectangle)*perc)
                #     # print len(list_rect_door_1)
                #     print len(list_rect_door_2)
                #     print arc
                #     print arc2
                #     cv2.circle(img,(x_min,y_min),r1,(255,255,255),1)
                #     cv2.circle(img,(x_min,y_min),r2,(255,255,255),1)
                #     # cv2.circle(img,(x_min,y_max),r1,(255,255,0),1)
                #     # cv2.circle(img,(x_min,y_max),r2,(255,255,0),1)
                #     img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
                #     plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min:x_max+r2], cmap = 'gray', interpolation = 'bicubic')
                #     plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                #     plt.title('left-prima')
                #     plt.show()
            # else:
            #     print '####################'
            #     print 'right list 2 no'
            #     # print self.max_tink_wall
            #     print math.ceil(len(rectangle)*perc)
            #     # print len(list_rect_door_1)
            #     print len(list_rect_door_2)
            #
            #     cv2.circle(img,(x_min,y_min),r1,(255,255,255),1)
            #     cv2.circle(img,(x_min,y_min),r2,(255,255,255),1)
            #     # cv2.circle(img,(x_min,y_max),r1,(255,255,0),1)
            #     # cv2.circle(img,(x_min,y_max),r2,(255,255,0),1)
            #     img[y_min-1:y_max,x_min-1:x_max]=(255,0,0)
            #     plt.imshow(img[y_min-(h*0.2):y_max+(h*0.2),x_min:x_max+r2], cmap = 'gray', interpolation = 'bicubic')
            #     plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
            #     plt.title('left-prima')
            #     plt.show()

        list_none = []

        return False,'None', list_none

    def check_door_down(self,x_min, y_min, x_max, y_max, thickness_door, list_rectangle, side):

        if self.max_tink_wall > 20:
            t1=3
            t2=3
            perc=0.5
        else:
            t1=1
            t2=1
            perc=0.6

        for rectangle in list_rectangle:
            h = x_max-x_min
            img =  np.array(self.image_rect)
            r1 = int(h*0.8)
            r2 = int((float(h)*1.2))

            check = False
            list_rect_door_1 = []
            list_rect_door_2 = []

            if self.max_tink_wall > 20:
                z=(int(h/thickness_door))*0.25
            else:
                z=(int(h/thickness_door))*0.5

            # conto i numeri di rettangoli che hanno il baricentro a una distanza compresa tra r1 e r2
            for rec in rectangle:
                dist_1 = rec.get_distance_to_centroid(x_min,y_min)
                dist_2 = rec.get_distance_to_centroid(x_max,y_min)
                if dist_1 < r2 and dist_1 > r1:
                    list_rect_door_1.append(rec)
                if dist_2 < r2 and dist_2 > r1:
                    list_rect_door_2.append(rec)

            # guardo apertura della porta e dal basso verso alto o dal alto verso il basso
            # e controllo se i rettangoli sono disposti in modo omogeneo sia sull asse x che sull assey
            if len(list_rect_door_1) > math.ceil(len(rectangle)*perc):
                list_rip_11 = []
                list_rip_12 = []
                l11 = list_rect_door_1[:]
                l12 = list_rect_door_1[:]
                arc = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)
                arc2 = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)
                for i in range(0,(int(h/thickness_door))+1):
                    p1 = y_min + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l11:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                            # arc[i] = 1
                            if (list_rip_11.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l11:
                            #         l11.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_11.append(r.id)
                    p3 = x_min + (i*thickness_door)
                    p4 = p3+thickness_door
                    for r in l12:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                            # arc2[i] = 1
                            if (list_rip_12.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l12:
                            #         l12.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_12.append(r.id)
                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0

                #controllo se ci sono dei "buchi" nel presunto arco
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0

                if count_zeros > max_zeros:
                    max_zeros = count_zeros

                if count2_zeros > max2_zeros:
                    max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                    check = True
                    opening = 'right-left'
                    return check,opening,list_rect_door_1

            if len(list_rect_door_2)> math.ceil(len(rectangle)*perc):

                arc2 = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)
                arc = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)
                list_rip_21 = []
                list_rip_22 = []
                l21 = list_rect_door_2[:]
                l22 = list_rect_door_2[:]
                for i in range(0,(int(h/thickness_door))+1):
                    p1 = y_min + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l21:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                            # arc[i] = 1
                            if (list_rip_21.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l21:
                            #         l21.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_21.append(r.id)
                    p3 = x_min+ (i*thickness_door)
                    p4 = p3+thickness_door
                    for r in l22:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                            #arc2[i] = 1
                            if (list_rip_22.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l22:
                            #         l22.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_22.append(r.id)

                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0
                if count_zeros > max_zeros:
                    max_zeros = count_zeros
                if count2_zeros > max2_zeros:
                    max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                    check = True
                    opening = 'left-right'
                 #   print len(list_rect_door_1)
                    return check,opening,list_rect_door_2



        list_none = []

        return False,'None', list_none

    def check_door_up(self,x_min, y_min, x_max, y_max, thickness_door, list_rectangle, side,nn):

        if self.max_tink_wall > 20:
            t1=4
            t2=4
            perc=0.5
        else:
            t1=1
            t2=1
            perc=0.6

        for rectangle in list_rectangle:
            h = x_max-x_min
            img =  np.array(self.image_rect)
            r1 = int(h*0.8)
            r2 = int((float(h)*1.2))

            check = False
            list_rect_door_1 = []
            list_rect_door_2 = []
            if self.max_tink_wall > 20:
                z=(int(h/thickness_door))*0.25
            else:
                z=(int(h/thickness_door))*0.5
            # conto i numeri di rettangoli che hanno il baricentro a una distanza compresa tra r1 e r2
            for rec in rectangle:
                dist_1 = rec.get_distance_to_centroid(x_min,y_max)
                dist_2 = rec.get_distance_to_centroid(x_max,y_max)
                if dist_1 < r2 and dist_1 > r1:
                    list_rect_door_1.append(rec)
                if dist_2 < r2 and dist_2 > r1:
                    list_rect_door_2.append(rec)

            # guardo apertura della porta e dal basso verso alto o dal alto verso il basso
            # e controllo se i rettangoli sono disposti in modo omogeneo sia sull asse x che sull assey
            # print '#####################'

            if len(list_rect_door_1) > math.ceil(len(rectangle)*perc):
                arc = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)
                arc2 = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)
                list_rip_11 = []
                list_rip_12 = []
                l11 = list_rect_door_1[:]
                l12 = list_rect_door_1[:]
                for i in range(0,int(h/thickness_door)+1):
                    p1 = (y_min-h) + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l11:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                            # arc[i] = 1
                            if (list_rip_11.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l11:
                            #         l11.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_11.append(r.id)
                    p3 = x_min + (i*thickness_door)
                    p4 = p3+thickness_door
                    for r in l12:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                            # arc2[i] = 1
                            if (list_rip_12.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l12:
                            #         l12.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_12.append(r.id)

                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0

                #controllo se ci sono dei "buchi" nel presunto arco
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0

                if count_zeros > max_zeros:
                    max_zeros = count_zeros

                if count2_zeros > max2_zeros:
                    max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                   check = True
                   opening = 'right-left'
                   #   print len(list_rect_door_1)
                   return check,opening,list_rect_door_1



            if len(list_rect_door_2)> math.ceil(len(rectangle)*perc):

                arc2 = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)
                arc = np.zeros((int(h/thickness_door))+1,dtype=np.uint8)

                list_rip_21 = []
                list_rip_22 = []
                l21 = list_rect_door_2[:]
                l22 = list_rect_door_2[:]

                for i in range(0,(int(h/thickness_door))+1):
                    p1 = (y_min-h) + (i*thickness_door)
                    p2 = p1+thickness_door
                    for r in l21:
                        y1 = r.y1
                        y2 = r.y3
                        if y1 <= p2 and y2 >= p1:
                            # arc[i] = 1
                            if (list_rip_21.count(r.id)<z):
                            #     arc[i] = 1
                            #     if r in l21:
                            #         l21.remove(r)
                            # else:
                                arc[i] = 1
                                list_rip_21.append(r.id)

                    p3 = x_min+ (i*thickness_door)
                    p4 = p3+thickness_door
                    for r in l22:
                        x1 = r.x1
                        x2 = r.x3
                        if x1 <= p4 and x2 >= p3:
                            #arc2[i] = 1
                            if (list_rip_22.count(r.id)<z):
                            #     arc2[i] = 1
                            #     if r in l22:
                            #         l22.remove(r)
                            # else:
                                arc2[i] = 1
                                list_rip_22.append(r.id)

                count_zeros = 0
                max_zeros = 0
                count2_zeros = 0
                max2_zeros = 0
                for j in range(0,len(arc)):
                    if arc[j] == 0:
                        count_zeros = count_zeros + 1
                    else:
                        if count_zeros > max_zeros:
                            max_zeros = count_zeros
                            count_zeros = 0
                    if arc2[j] == 0:
                        count2_zeros = count2_zeros + 1
                    else:
                        if count2_zeros > max2_zeros:
                            max2_zeros = count2_zeros
                            count2_zeros = 0
                if count_zeros > max_zeros:
                    max_zeros = count_zeros
                if count2_zeros > max2_zeros:
                    max2_zeros = count2_zeros

                if max_zeros <= t1 and max2_zeros <= t2:
                    check = True
                    opening = 'left-right'
                    return check,opening,list_rect_door_2


        list_none = []

        return False,'None', list_none


    def align_door_to_wall(self,debug_mode):
        door_list = []
        image = np.zeros((self.height,self.width,3),dtype=np.uint8)
        image2 = np.zeros((self.height,self.width,3),dtype=np.uint8)
        list_wall = self.graph_wall.nodes()

       # print 'numero di porte trovate: '+str(len(self.list_door))
        if self.max_tink_wall>20:
            lim_max = self.max_tink_wall
        else:
            lim_max = self.max_tink_wall*0.3

        for d in self.list_door:
            opening = d.opening[0]
            direction = d.opening[1]
            length = d.get_max_len()
            min_dist1 = max(self.height,self.width)
            min_dist2 = max(self.height,self.width)
            wall_1 = None
            wall_2 = None
            if (opening=='up-down' and direction=='left'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                        dist1 = w1.get_minimum_distance_from_the_point(d.x3, d.y3)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x1-length, d.y1+length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1
                # This is because there may be no walls found
                if min_dist1 > length*0.3 or min_dist2 > length*0.3:
                    for rect in self.list_rect:
                        if rect.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x3, d.y3)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x1-length, d.y1+length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x3, d.y3)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x1-length, d.y1+length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect
                if wall_1 and wall_2:
                    if not(wall_1.y_max > wall_2.y_min and wall_2.y_max>wall_1.y_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)

            elif (opening=='down-up' and direction=='left'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                        dist1 = w1.get_minimum_distance_from_the_point(d.x2, d.y2)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x3-length, d.y3-length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1

                if min_dist1 > length*0.3 or min_dist2 > length*0.3:
                    for rect in self.list_rect:
                        if rect.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x2, d.y2)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x3-length, d.y3-length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x2, d.y2)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x3-length, d.y3-length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect

                if wall_1 and wall_2:
                    if not(wall_1.y_max > wall_2.y_min and wall_2.y_max>wall_1.y_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)

            elif (opening=='up-down' and direction=='right'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                        dist1 = w1.get_minimum_distance_from_the_point(d.x4, d.y4)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x1+length, d.y1+length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1

                if min_dist1 > length*0.3 or min_dist2 > length*0.3:
                    for rect in self.list_rect:
                        if rect.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x4, d.y4)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x1+length, d.y1+length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x4, d.y4)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x1+length, d.y1+length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect
                if wall_1 and wall_2:
                    if not(wall_1.y_max > wall_2.y_min and wall_2.y_max>wall_1.y_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)

            elif (opening=='down-up' and direction=='right'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                        dist1 = w1.get_minimum_distance_from_the_point(d.x1, d.y1)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x4+length, d.y4-length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1

                if min_dist1 > length*0.3 or min_dist2 > length*0.3:
                    for rect in self.list_rect:
                        if rect.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x1, d.y1)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x4+length, d.y4-length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x1, d.y1)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x4+length, d.y4-length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect
                if wall_1 and wall_2:
                    if not(wall_1.y_max > wall_2.y_min and wall_2.y_max>wall_1.y_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)

            elif (opening=='left-right' and direction=='down'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                    # dist1 = w1.get_minimum_distance_from_the_point(d.x3, d.y3)
                        dist1 = w1.get_minimum_distance_from_the_point(d.x2, d.y2)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x1+length, d.y1+length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1

                if min_dist1 > length*0.3 or min_dist2 > length*0.3:
                    for rect in self.list_rect:
                        if w1.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x2, d.y2)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x1+length, d.y1+length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x2, d.y2)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x1+length, d.y1+length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect
                if wall_1 and wall_2:
                    if not(wall_1.x_max > wall_2.x_min and wall_2.x_max>wall_1.x_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)

            elif (opening=='right-left' and direction=='down'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                        dist1 = w1.get_minimum_distance_from_the_point(d.x1, d.y1)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x2-length, d.y2+length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1
                if min_dist1 > length*0.3 or min_dist2 > length*0.3:
                    for rect in self.list_rect:
                        if rect.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x1, d.y1)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x2-length, d.y2+length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x1, d.y1)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x2-length, d.y2+length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect
                if wall_1 and wall_2:
                    if not(wall_1.x_max > wall_2.x_min and wall_2.x_max>wall_1.x_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)

            elif (opening=='left-right' and direction=='up'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                    # dist1 = w1.get_minimum_distance_from_the_point(d.x3, d.y3)
                        dist1 = w1.get_minimum_distance_from_the_point(d.x3, d.y3)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x4+length, d.y4-length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1

                if min_dist1 > length*0.3 or min_dist2 > length*0.3:
                    for rect in self.list_rect:
                        if rect.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x3, d.y3)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x4+length, d.y4-length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x3, d.y3)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x4+length, d.y4-length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect
                if wall_1 and wall_2:
                    if not(wall_1.x_max > wall_2.x_min and wall_2.x_max>wall_1.x_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)

            elif (opening=='right-left' and direction=='up'):
                for w1 in list_wall:
                    if w1.id!= d.id:
                        dist1 = w1.get_minimum_distance_from_the_point(d.x4, d.y4)
                        if (dist1 < min_dist1):
                            min_dist1 = dist1
                            wall_1 = w1
                        dist2 = w1.get_minimum_distance_from_the_point(d.x3-length, d.y3-length)
                        if (dist2 < min_dist2):
                            min_dist2 = dist2
                            wall_2 = w1
                if min_dist1 > length*0.3 or min_dist2 > length*0.3:

                    for rect in self.list_rect:
                        if rect.id!= d.id:
                            if self.max_tink_wall>20:
                                stop=True
                                while(stop):
                                    lim_max = lim_max*0.4
                                    if lim_max<=3 and lim_max<10:
                                        stop = False
                                        lim_max=4
                                    if rect.id != d.id and rect.get_min_len()>lim_max:
                                        dist1 = rect.get_minimum_distance_from_the_point(d.x4, d.y4)
                                        if (dist1 < min_dist1):
                                            min_dist1 = dist1
                                            wall_1 = rect
                                        dist2 = rect.get_minimum_distance_from_the_point(d.x3-length, d.y3-length)
                                        if (dist2 < min_dist2):
                                            min_dist2 = dist2
                                            wall_2 = rect
                            else:
                                if rect.id != d.id and rect.get_min_len()>lim_max:
                                    dist1 = rect.get_minimum_distance_from_the_point(d.x4, d.y4)
                                    if (dist1 < min_dist1):
                                        min_dist1 = dist1
                                        wall_1 = rect
                                    dist2 = rect.get_minimum_distance_from_the_point(d.x3-length, d.y3-length)
                                    if (dist2 < min_dist2):
                                        min_dist2 = dist2
                                        wall_2 = rect
                if wall_1 and wall_2:
                    if not(wall_1.x_max > wall_2.x_min and wall_2.x_max>wall_1.x_min):
                        min_dist1 =  max(self.height,self.width)
                        min_dist2 =  max(self.height,self.width)
                else:
                    min_dist1 =  max(self.height,self.width)
                    min_dist2 =  max(self.height,self.width)
            r=random.randint(0, 255)
            g=random.randint(0, 255)
            b=random.randint(0, 255)

            dist_bw_walls = self.minimum_distance_between_two_rectangles(wall_1,wall_2);
            if min_dist1 < length*0.3 and min_dist2 < length*0.3 and wall_1.id != wall_2.id and dist_bw_walls> 0.2*length:

                self.list_door_2.append(d)
                # If the rectangle of the door is already present as a wall I remove it
                if (self.graph_wall.isIn(d)):
                    self.graph_wall.remove_node(d)

                image[d.y_min-1:d.y_max-1,d.x_min-1:d.x_max-1]=(r,g,b)
                image[wall_1.y_min-1:wall_1.y_max-1,wall_1.x_min-1:wall_1.x_max-1]=(r,g,b)
                image[wall_2.y_min-1:wall_2.y_max-1,wall_2.x_min-1:wall_2.x_max-1]=(r,g,b)
                thinckness_1 = wall_1.get_min_len()
                thinckness_2 = wall_2.get_min_len()
                thinckness = max(thinckness_1,thinckness_2)
                orientation_door = d.get_orientation()
                if orientation_door == 'vertical':
                    #because the it rotates 90 degrees
                    orientation_door == 'horizontal'
                    d_y_min = max(wall_1.y_min,wall_2.y_min)
                    d_y_max = min(wall_1.y_max,wall_2.y_max)
                    d_x_max = max(wall_1.x_min,wall_2.x_min)
                    d_x_min = min(wall_1.x_max,wall_2.x_max)
                    if d_y_max-d_y_min>thinckness*1.2:
                        if np.abs(d_y_max-d.y1) > np.abs(d_y_min-d.y1):
                            d_y_max = thinckness + d_y_min
                        else:
                            d_y_min = d_y_max - thinckness
                    # print 'x_min: '+str( d_x_min)+'   y_min: '+str(d_y_min)+'   x_max: '+str(d_x_max)+'   y_max: '+str(d_y_max)
                    image2[d_y_min-1:d_y_max-1,d_x_min-1:d_x_max-1]=(r,g,b)
                    image2[wall_1.y_min-1:wall_1.y_max-1,wall_1.x_min-1:wall_1.x_max-1]=(r,g,b)
                    image2[wall_2.y_min-1:wall_2.y_max-1,wall_2.x_min-1:wall_2.x_max-1]=(r,g,b)

                    wall_1.type = 'wall'
                    wall_2.type = 'wall'
                    self.numberDoors+=1
                    new_rect_door = Rectangle(len(self.list_rect)+self.numberDoors+self.numberWindows+1)
                    # self.list_rect.append(new_rect_door)

                    # if d_x_min>d_x_max:
                    #     print 'primo  x_min: '+str( d_x_min)+'   y_min: '+str(d_y_min)+'   x_max: '+str(d_x_max)+'   y_max: '+str(d_y_max)
                    # if d_y_min>d_y_max:
                    #     print 'primo x_min: '+str( d_x_min)+'   y_min: '+str(d_y_min)+'   x_max: '+str(d_x_max)+'   y_max: '+str(d_y_max)
                    new_rect_door.x_max = d_x_max
                    new_rect_door.y_max = d_y_max
                    new_rect_door.x_min = d_x_min
                    new_rect_door.y_min = d_y_min
                    new_rect_door.type = 'door'
                    new_rect_door.opening = d.opening

                    self.graph_image_output.add_node(new_rect_door)
                    self.graph_image_output.add_node(wall_1)
                    self.graph_image_output.add_node(wall_2)
                    id_edge = str(max(wall_1.id,new_rect_door.id)) + str(min(wall_1.id,new_rect_door.id)) + str(min(wall_1.id,new_rect_door.id)) + str(max(wall_1.id,new_rect_door.id))
                    a1 = Edge(id_edge,new_rect_door,'ADIACENT')
                    a2 = Edge(id_edge,wall_1,'ADIACENT')
                    wall_1.add_edge(a1)
                    new_rect_door.add_edge(a2)

                    id_edge = str(max(wall_2.id,new_rect_door.id)) + str(min(wall_2.id,new_rect_door.id)) + str(min(wall_2.id,new_rect_door.id)) + str(max(wall_2.id,new_rect_door.id))
                    a3 = Edge(id_edge,new_rect_door,'ADIACENT')
                    a4 = Edge(id_edge,wall_2,'ADIACENT')
                    wall_2.add_edge(a3)
                    new_rect_door.add_edge(a4)



                else:
                    orientation_door == 'vertical'
                    d_y_max = max(wall_1.y_min,wall_2.y_min)
                    d_y_min = min(wall_1.y_max,wall_2.y_max)
                    d_x_min = max(wall_1.x_min,wall_2.x_min)
                    d_x_max = min(wall_1.x_max,wall_2.x_max)
                    if d_x_max-d_x_min>thinckness*1.2:
                        if np.abs(d_x_max-d.x1) > np.abs(d_x_min-d.x1):
                            d_x_max = thinckness + d_x_min
                        else:
                            d_x_min = d_x_max - thinckness
                    #print 'x_min: '+str( d_x_min)+'   y_min: '+str(d_y_min)+'   x_max: '+str(d_x_max)+'   y_max: '+str(d_y_max)
                    image2[d_y_min-1:d_y_max-1,d_x_min-1:d_x_max-1]=(r,g,b)
                    image2[wall_1.y_min-1:wall_1.y_max,wall_1.x_min-1:wall_1.x_max]=(r,g,b)
                    image2[wall_2.y_min-1:wall_2.y_max,wall_2.x_min-1:wall_2.x_max]=(r,g,b)
                    wall_1.type = 'wall'
                    wall_2.type = 'wall'
                    self.numberDoors+=1
                    new_rect_door = Rectangle(len(self.list_rect)+self.numberDoors+self.numberWindows+1)
                    # self.list_rect.append(new_rect_door)
                    # if d_x_min>d_x_max:
                    #     print 'secondo x_min: '+str( d_x_min)+'   y_min: '+str(d_y_min)+'   x_max: '+str(d_x_max)+'   y_max: '+str(d_y_max)+" "
                    #     print d.opening
                    # if d_y_min>d_y_max:
                    #     print 'secondo x_min: '+str( d_x_min)+'   y_min: '+str(d_y_min)+'   x_max: '+str(d_x_max)+'   y_max: '+str(d_y_max)+" " + d.opening
                    #     print d.opening
                    new_rect_door.x_max = d_x_max
                    new_rect_door.y_max = d_y_max
                    new_rect_door.x_min = d_x_min
                    new_rect_door.y_min = d_y_min
                    new_rect_door.type = 'door'
                    new_rect_door.opening = d.opening

                    self.graph_image_output.add_node(new_rect_door)
                    self.graph_image_output.add_node(wall_1)
                    self.graph_image_output.add_node(wall_2)
                    id_edge = str(max(wall_1.id,new_rect_door.id)) + str(min(wall_1.id,new_rect_door.id)) + str(min(wall_1.id,new_rect_door.id)) + str(max(wall_1.id,new_rect_door.id))
                    a1 = Edge(id_edge,new_rect_door,'ADIACENT')
                    a2 = Edge(id_edge,wall_1,'ADIACENT')
                    wall_1.add_edge(a1)
                    new_rect_door.add_edge(a2)

                    id_edge = str(max(wall_2.id,new_rect_door.id)) + str(min(wall_2.id,new_rect_door.id)) + str(min(wall_2.id,new_rect_door.id)) + str(max(wall_2.id,new_rect_door.id))
                    a3 = Edge(id_edge,new_rect_door,'ADIACENT')
                    a4 = Edge(id_edge,wall_2,'ADIACENT')
                    wall_2.add_edge(a3)
                    new_rect_door.add_edge(a4)


                # image[d.y_min-1:d.y_max-1,d.x_min-1:d.x_max-1]=(r,g,b)
                # image[wall_1.y_min-1:wall_1.y_max-1,wall_1.x_min-1:wall_1.x_max-1]=(r,g,b)
                # image[wall_2.y_min-1:wall_2.y_max-1,wall_2.x_min-1:wall_2.x_max-1]=(r,g,b)
                # c=c+1
        s = self.file_name[0:-4]
        # cv2.imwrite(self.path_debug+'merge_walls_doors_windows/'+s+'_1.png', image)
        # cv2.imwrite(self.path_debug+'merge_walls_doors_windows/'+s+'_2.png', image2)


        for w in self.graph_wall.nodes():
            self.graph_image_output.add_node(w)

        if debug_mode:
            self.print_graph_image(self.graph_image_output.nodes(),debug_mode)

        for rec in self.graph_image_output.nodes():
            if rec.type == 'door':
                image[rec.y_min - 1:rec.y_max, rec.x_min - 1:rec.x_max] = (0, 255, 0)
                p1, p2, p3, p4 = self.iterative_obj.find_points_rectangle(int(rec.x_min-1),
                                                         int(rec.y_min -1),
                                                         int(rec.x_max),
                                                         int(rec.y_max))
                door_list.append([p1, p2, p3, p4])

        return door_list

    def print_graph_image(self, list_rect_graph, debug_mode):
        image = np.zeros((self.height, self.width, 3), dtype=np.uint8)
        for rec in list_rect_graph:
            if rec.type == 'door':
                image[rec.y_min - 1:rec.y_max, rec.x_min - 1:rec.x_max] = (0, 255, 0)
            if rec.type == 'wall':
                p = np.array([[[rec.x1, rec.y1], [rec.x2, rec.y2], [rec.x3, rec.y3], [rec.x4, rec.y4]]],
                             dtype=np.int32)
                cv2.drawContours(image, [p], -1, (0, 0, 255), -1)

            # if rec.type == 'window':
            #     image[rec.y_min - 1:rec.y_max, rec.x_min - 1:rec.x_max] = (0, 255, 255)
        if debug_mode:
            cv2.imwrite(
                self.path_debug + 'wall/' + str(self.file_name[0:len(self.file_name) - 4]) + '_WallAndDoor.png',
                image)
            cv2.imwrite(self.path_debug + 'merge_all/' + str(
                self.file_name[0:len(self.file_name) - 4]) + '_WallAndDoor.png', image)

    def minimum_distance_between_two_rectangles(self,w1,w2):
        #calculated minumun distance between two rectangle
        if w1.x_max >= w2.x_min and w1.x_min <= w2.x_max:
            p1 = max(w1.y_min,w2.y_min)
            p2 = min(w1.y_max,w2.y_max)
            mind_dist = p1 - p2
        elif w1.y_max >= w2.y_min and w1.y_min <= w2.y_max:
            p1 = max(w1.x_min,w2.x_min)
            p2 = min(w1.x_max,w2.x_max)
            mind_dist = p1 - p2
        else:
            mind_dist=max(self.height,self.width)
            points_1 = [[w1.x1,w1.y1],[w1.x2,w1.y2],[w1.x3,w1.y3],[w1.x4,w1.y4]]
            points_2 = [[w2.x1,w2.y1],[w2.x2,w2.y2],[w2.x3,w1.y3],[w2.x4,w2.y4]]

            for x1,y1 in points_1:
                for x2,y2 in points_2:
                    d = math.sqrt(float(x1 - x2)**2 + float(y1 - y2)**2)
                    if d < mind_dist:
                        mind_dist = d

        return mind_dist


    # ------method 07: find windows
    def classification_windows(self,debug_mode):
        # self.max_tink_wall = 43
        # with open(self.current_directory + '/Test_object_store/for_windows/rect.pkl', 'rb') as input:
        #     self.list_rect = pickle.load(input)
        # with open(self.current_directory + '/Test_object_store/for_windows/wall.pkl', 'rb') as input:
        #     for wall in pickle.load(input):
        #         self.graph_wall.add_node(wall)
        # with open(self.current_directory + '/Test_object_store/for_windows/output.pkl', 'rb') as input:
        #     for output in pickle.load(input):
        #         self.graph_image_output.add_node(output)





        window_list = []
        self.walls_window=[]
        list_return = []
        for w1 in self.graph_wall.nodes():
            adjacent = w1.edges()

            for a in adjacent:
                list_new = []
                n = a.get_node()
                #if a.get_type() == 'TANGENT'and n.type!='wall':
                if n.type!='wall'and  n.type!="door":
                    list_new.extend(self.find_adjacent_loop2(n,[w1]))
                    if len( list_new)>0:
                        for e2 in list_new:
                            e2.append(n)
                            e2.append(w1)
                    for l in list_new:
                        list_return.append(l)

        dic_list = {}

        while len(list_return)>0:
            l = list_return.pop()
            w = []
            for e in l:
                if e.type=="wall":
                    w.append(e)
            if len(w)==2:
                l_key = dic_list.keys()
                w1 = w[0]
                w2 = w[1]
                m1 = max(w1.id,w2.id)
                m2 = min(w1.id,w2.id)
                key = str(m1)+'-'+str(m2)
                if key in l_key:
                    dic_list[key].append(l)
                else:
                    dic_list[key] = [l]

        dic_final = {}
        for k in dic_list.keys():
            min_leng = max(self.height,self.height)
            min_leng2 = max(self.height,self.height)
            check_app= False
            for l in dic_list[k]:

                leng = 0
                for e in l:
                    pp = e.get_max_len()
                    leng = leng + e.get_max_len()
                leng2 = len(l)

                if leng/min_leng<=2:
                    if leng2/min_leng2<=2:
                        min_leng2 = leng2
                        min_leng = leng
                        check_app=True
                        app = l
                # if leng2 < min_leng2:
                #     min_leng2 = leng2
                #     check_app=True
                #     app = l

            if(check_app):
                dic_final[k] = app

        for k in dic_final.keys():
            l_w = []
            w_x_min = self.width
            w_x_max = 0
            w_y_min = self.height
            w_y_max = 0
            check_window = 0
            for p in dic_final[k]:
                if p.type != 'wall':
                    if w_x_max < p.x_max:
                        w_x_max = p.x_max
                    if w_x_min > p.x_min:
                        w_x_min = p.x_min
                    if w_y_max < p.y_max:
                        w_y_max = p.y_max
                    if w_y_min > p.y_min:
                        w_y_min = p.y_min
                    p.type = 'window'
                else:
                    check_window = True
                    l_w.append(p)
            #
            # if check_window:
            #     m = min((w_x_max-w_x_min),(w_y_max-w_y_min))
            #     if m<(self.max_tink_wall*4):
            #         for p in dic_final[k]:
            #             if p.type != 'wall':
            #                 p.type = 'window'
            #     else:
            #         check_window = False
            #         for p in dic_final[k]:
            #             if p.type != 'wall':
            #                 if p.type !='window':
            #                     p.type = 'line-structure'

            if check_window:
                self.numberWindows+=1
                r = Rectangle(len(self.list_rect)+self.numberDoors+self.numberWindows+1)
                #self.list_rect.append(r)
                w = l_w[0]
                n = l_w[1]
                if (w_x_max - w_x_min) < (w_y_max - w_y_min):
                    r.y_max = max(w.y_min,n.y_min)
                    r.y_min = min(w.y_max,n.y_max)
                    r.x_min = max(w.x_min,n.x_min)
                    r.x_max = min(w.x_max,n.x_max)

                    if r.x_min < r.x_max and r.y_min<r.y_max:
                        # print  "primo "+ str(r.x_min)+"  " +str(r.y_min)+"  " +str(r.x_max)+"  " +str(r.y_max)
                        r.type = 'window'
                        r.side_touch_wall=['l1','l3']
                        #print 'verticale    x_min: '+str( r.x_min)+'   y_min: '+str(r.y_min)+'   x_max: '+str(r.x_max)+'   y_max: '+str(r.y_max)
                        self.list_windows.append(r)
                        # self.walls_window.append([n.id,w.id])
                        # self.walls_window.append([w.id,n.id])

                else:
                    r.y_min = max(w.y_min,n.y_min)
                    r.y_max = min(w.y_max,n.y_max)
                    r.x_max = max(w.x_min,n.x_min)
                    r.x_min = min(w.x_max,n.x_max)
                    if r.x_min < r.x_max and r.y_min<r.y_max:
                        # print  "secondo "+ str(r.x_min)+"  " +str(r.y_min)+"  " +str(r.x_max)+"  " +str(r.y_max)
                        r.type = 'window'
                        r.side_touch_wall=['l2','l4']
                        self.list_windows.append(r)
                        # print 'orizzontale    x_min: '+str( r.x_min)+'   y_min: '+str(r.y_min)+'   x_max: '+str(r.x_max)+'   y_max: '+str(r.y_max)
                        # self.walls_window.append([n.id,w.id])
                        # self.walls_window.append([w.id,n.id])

        #---Image mold with rectangles
        image_rect=np.zeros((self.height,self.width,3),dtype=np.uint8)
        for rec in self.list_rect:
            r=random.randint(0, 255)
            g=random.randint(0, 255)
            b=random.randint(0, 255)
            p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
            cv2.drawContours(image_rect, [p], -1, (r,g,b), -1)
            #image_rect[rec.y_min-1:rec.y4,rec.x1-1:rec.x2]=(r,g,b)
        if debug_mode:
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_1.png', image_rect)

        #---First step, black and white image print with rectangles of yellow window windows
        image2=np.zeros((self.height,self.width,3),dtype=np.uint8)
        for rec in self.list_rect:
            if rec.type == 'window':
                image2[rec.y_min-1:rec.y4,rec.x1-1:rec.x2]=(0,255,255)
            elif rec.type =='line-structure':
                image2[rec.y_min-1:rec.y4,rec.x1-1:rec.x2]=(255,0,255)
            else:
                image2[rec.y1-1:rec.y4,rec.x1-1:rec.x2]=(255,255,255)
        if debug_mode:
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_2.png', image2)

        #-- secondo step, It compresses the image of the windows (presumed) found between two walls, with doors and walls

        image3=np.zeros((self.height,self.width,3),dtype=np.uint8)
        for rec in self.graph_image_output.nodes():
            if rec.type == 'door':
                image3[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,0)
            elif rec.type == 'wall':
                p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
                cv2.drawContours(image3, [p], -1, (0,0,255), -1)
                #image3[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
        for rec in self.list_windows:
            if rec.type == 'window':
                #image3[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,255)
                p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
                cv2.drawContours(image3, [p], -1, (0,255,255), -1)
        if debug_mode:
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_3.png', image3)

        # I remove windows that overlap the walls
        list_windows_remove=[]
        i = 0
        for rec in self.list_windows:
            check_window=False
            i1 = np.zeros((self.height,self.width),dtype=np.uint8)
            i1[rec.y_min:rec.y_max+1,rec.x_min:rec.x_max+1]=1

            for rec2 in self.graph_image_output.nodes():
                if rec2.type=='wall' or rec2.type=='door':
                    i2 = np.zeros((self.height,self.width),dtype=np.uint8)
                    i2[rec2.y_min:rec2.y_max+1,rec2.x_min:rec2.x_max+1]=1
                    s = np.sum(i2)
                    # print str(rec2.y_min)+" "+str(rec2.y_max)+" "+str(rec2.x_min)+" "+str(rec2.x_max)
                    logic_and = i1*i2
                    ss = np.sum(logic_and)
                    i+=1
                    # print 'ciclo '+str(i)
                    if float(s/2) < ss:
                        list_windows_remove.append(rec)
                        break
        # print str(len(list_windows_remove))+' finestre da rimuovere'
        k = len(list_windows_remove)
        i = 0
        while len(list_windows_remove)>0 and i<k+1:
            rem = list_windows_remove.pop()
            self.list_windows.remove(rem)
            i+=0

        image6 = np.zeros((self.height,self.width,3),dtype=np.uint8)
        for rec in self.graph_image_output.nodes():
                if rec.type == 'door':
                    image6[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,0)
                elif rec.type == 'wall':
                    p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
                    cv2.drawContours(image6, [p], -1, (0,0,255), -1)
                    #image6[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
        for rec in self.list_windows:
            image6[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,255)
        if debug_mode:
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_7.png', image6)




        # Third step, I'm going to find the related background components to determine if and a window
        imageBackground=np.zeros((self.height,self.width),dtype=np.uint8)
        list_app = self.list_windows[:]
        list_app.extend(self.graph_image_output.nodes())
        for rec in list_app:
            if rec.type=='wall':
                p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
                cv2.drawContours(imageBackground, [p], -1, 255, -1)
            else:
                imageBackground[rec.y_min-1:rec.y_max,rec.x_min-1:rec.x_max]=255

        s1 = [[0, 1, 0], [1,1,1], [0,1,0]] # elemento strutturato
        s2 = [[1, 1, 1], [1,1,1], [1,1,1]]
        labels,numlabels =ndimage.label(imageBackground<127,s2)
        labels2 = (np.where(labels != 1, 0, labels)*255)

        if debug_mode:
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_4.png', labels*10)
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_5.png', labels2)



        labels2 = np.abs(labels2 - (np.ones((self.height,self.width))*255))
        list_windows_remove=[]
        for win in self.list_windows:
            check_l1 = 0
            check_l2 = 0
            check_l3 = 0
            check_l4 = 0
            if not('l1' in win.side_touch_wall[0]):

                s = np.sum(labels2[win.y_min-2:win.y_min-1,win.x_min+3:win.x_max-3])
                if s!=0:
                   check_l1 = 1

            if not('l2' in win.side_touch_wall[0]):

                s = np.sum(labels2[win.y_min+3:win.y_max-3,win.x_max+1:win.x_max+2 ])
                if s!=0:
                   check_l2 = 1

            if not('l3' in win.side_touch_wall[0]):

                s = np.sum(labels2[win.y_max+1:win.y_max+2,win.x_min+3:win.x_max-3 ])
                if s!=0:
                    check_l3 = 1

            if not('l4' in win.side_touch_wall[0]):

                s = np.sum(labels2[win.y_min+3:win.y_max-3,win.x_min-2:win.x_min-1 ])
                if s!=0:
                    check_l4 = 1

            if check_l1 and check_l3:
                list_windows_remove.append(win)
            if check_l2 and check_l4:
                list_windows_remove.append(win)

        # list_dww = []
        # for rec in list_app():
        #     if not(rec in list_windows_remove):
        #         list_dww.append(rec)
        while len(list_windows_remove)>0:
            rem = list_windows_remove.pop()
            self.list_windows.remove(rem)


        # for rec in self.list_windows:
        #     if (rec in list_windows_remove):
        #         self.list_windows.remove(rec)

        image4 = np.zeros((self.height,self.width,3),dtype=np.uint8)
        for rec in self.list_windows:
            image4[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,255)

        for rec in self.graph_image_output.nodes():
            if rec.type == 'door':
                image4[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,0)
            elif rec.type == 'wall':
                p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
                cv2.drawContours(image4, [p], -1, (0,0,255), -1)
                #image4[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
        if debug_mode:
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_6.png', image4)





        # Remove windows that have a thickness greater than the thickness of the wall
        l_remove=[]
        for rec in self.list_windows:
            l_win =  min(np.abs(rec.x_max-rec.x_min),np.abs(rec.y_max-rec.y_min))
            if l_win>(self.max_tink_wall*1.5) or l_win < (self.max_tink_wall*0.2):

                l_remove.append(rec)
            else:

                self.graph_image_output.add_node(rec)
        while len(l_remove)>0:
            rem = l_remove.pop()
            self.list_windows.remove(rem)
        # list_dww.extend(self.list_windows)
        # list_dww.extend(self.graph_image_output.nodes())

        for rec in self.graph_image_output.nodes():
            if rec.type == 'window':
                # window_list.append([[int(rec.x1),int(rec.y1)],[int(rec.x2),int(rec.y2)],
                #                     [int(rec.x3), int(rec.y3)],[int(rec.x4),int(rec.y4)]])
                p1,p2,p3,p4 = self.iterative_obj.find_points_rectangle(int(rec.x_min - 1),
                                                         int(rec.y_min - 1),
                                                         int(rec.x_max),
                                                         int(rec.y_max))
                window_list.append([p1,p2,p3,p4])

        # self.save_object(self.graph_wall.nodes(), self.path_debug + 'windows/wall.pkl')
        # self.save_object(self.list_rect, self.path_debug + 'windows/rect.pkl')
        # self.save_object(self.graph_image_output.nodes(), self.path_debug + 'windows/output.pkl')

        return window_list

    def save_wall_door_window_results(self,debug_mode):
        # name = str(self.file_name[0:len(self.file_name) - 4])
        # path = str(self.path_output + name + '/')
        # if not (os.path.exists(path)):
        #     os.mkdir(path)

        self.height, self.width = self.original_image.shape


        image7 = np.zeros((self.height,self.width,3),dtype=np.uint8)
        image8 = np.zeros((self.height,self.width,3),dtype=np.uint8)
        image9 = np.array(self.color_image)
        for rec in self.graph_image_output.nodes():
            if rec.type == 'window':
                image7[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,255)
                image8[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
                image9[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
            elif rec.type == 'door':
                image7[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,255,0)
                image8[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
                image9[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
            if rec.type == 'wall':
                p = np.array([[[int(rec.x1), int(rec.y1)],[int(rec.x2), int(rec.y2)],
                               [int(rec.x3), int(rec.y3)],[int(rec.x4), int(rec.y4)]]],
                             dtype=np.int32)
                cv2.drawContours(image7, [p], -1, (0,0,255), -1)
                cv2.drawContours(image8, [p], -1, (0,0,255), -1)
                cv2.drawContours(image9, [p], -1, (0,0,255), -1)
            # else:
            #     image7[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(255,255,0)
            #     image8[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
            #     image9[rec.y_min -1:rec.y_max,rec.x_min-1:rec.x_max]=(0,0,255)
        if debug_mode:
            cv2.imwrite(self.path_debug+'windows/'+str(self.file_name[0:len(self.file_name)-4])+'_8.png', image7)
        if len(self.list_scale)>0:
            for s in self.list_scale:
                cv2.drawContours(image7, [s], -1, (255,0,255), -1)
                cv2.drawContours(image8, [s], -1, (0,0,255), -1)
                cv2.drawContours(image9, [s], -1, (0,0,255), -1)


        name = str(self.file_name[0:-4])
        path = str(self.path_output+name+'/')
        #--change later
        # path = str(self.path_output+name+'_')
        cv2.imwrite(path+'Stefano_output.png', image7)
        cv2.imwrite(path+'walls_and_doors.png', image8)
        cv2.imwrite(path+'red_wall.png', image9)
        self.img_red_wall = image9
        self.img_walls_and_doors = image8
        self.list_wwd.extend(self.graph_image_output.nodes())


    def find_adjacent_loop(self,rect,pre_rect):
        pre_rect.append(rect)
        adjacent = rect.edges()
        list_return = []
        for a in adjacent:
            list_new = []
            n = a.get_node()
            if n.type != 'wall':
                if not( n in pre_rect ):
                    list_new.extend(self.find_adjacent_loop2(n,pre_rect))
                    if len( list_new)>0:
                        for e2 in list_new:
                            e2.append(n)
            else:
                if not( n in pre_rect ):
                    list_new.append([n])
            for l in list_new:
                list_return.append(l)

        return list_return

    # def find_adjacent_loop2(self,rect,pre_rect):
    #     pre_rect.append(rect)
    #     adjacent = rect.edges()
    #     for a in adjacent:
    #         check_window = 0
    #         n = a.get_node()
    #         if n.type != 'wall' and n.type != 'door':
    #             if not( n in pre_rect ):
    #                self.find_adjacent_loop(n,pre_rect)
    #         elif n.type == 'wall':
    #             if not( n in pre_rect ):
    #                 w_x_min = self.width
    #                 w_x_max = 0
    #                 w_y_min = self.height
    #                 w_y_max = 0
    #                 check_window = False
    #                 for p in pre_rect:
    #                     if p.type != 'wall':
    #                         if w_x_max < p.x_max:
    #                             w_x_max = p.x_max
    #                         if w_x_min > p.x_min:
    #                             w_x_min = p.x_min
    #                         if w_y_max < p.y_max:
    #                             w_y_max = p.y_max
    #                         if w_y_min > p.y_min:
    #                             w_y_min = p.y_min
    #                     else:
    #                         check_window = True
    #                         w = p
    #
    #                 if check_window:
    #                     m = min((w_x_max-w_x_min),(w_y_max-w_y_min))
    #                     if m<(self.max_tink_wall*4):
    #                         for p in pre_rect:
    #                             if p.type != 'wall':
    #                                 p.type = 'window'
    #                     else:
    #                         check_window = False
    #                         for p in pre_rect:
    #                             if p.type != 'wall':
    #                                 if p.type !='window':
    #                                     p.type = 'line-structure'
    #
    #                 if check_window and not([w.id,n.id] in self.walls_window):
    #                     self.numberWindows+=1
    #                     r = Rectangle(len(self.list_rect)+self.numberDoors+self.numberWindows+1)
    #                     #self.list_rect.append(r)
    #                     if (w_x_max - w_x_min) < (w_y_max - w_y_min):
    #                         r.y_max = max(w.y_min,n.y_min)
    #                         r.y_min = min(w.y_max,n.y_max)
    #                         r.x_min = max(w.x_min,n.x_min)
    #                         r.x_max = min(w.x_max,n.x_max)
    #
    #                         if r.x_min < r.x_max and r.y_min<r.y_max:
    #                             # print  "primo "+ str(r.x_min)+"  " +str(r.y_min)+"  " +str(r.x_max)+"  " +str(r.y_max)
    #                             r.type = 'window'
    #                             r.side_touch_wall=['l1','l3']
    #                             #print 'verticale    x_min: '+str( r.x_min)+'   y_min: '+str(r.y_min)+'   x_max: '+str(r.x_max)+'   y_max: '+str(r.y_max)
    #                             self.list_windows.append(r)
    #                             self.walls_window.append([n.id,w.id])
    #                             self.walls_window.append([w.id,n.id])
    #
    #                     else:
    #                         r.y_min = max(w.y_min,n.y_min)
    #                         r.y_max = min(w.y_max,n.y_max)
    #                         r.x_max = max(w.x_min,n.x_min)
    #                         r.x_min = min(w.x_max,n.x_max)
    #                         if r.x_min < r.x_max and r.y_min<r.y_max:
    #                             # print  "secondo "+ str(r.x_min)+"  " +str(r.y_min)+"  " +str(r.x_max)+"  " +str(r.y_max)
    #                             r.type = 'window'
    #                             r.side_touch_wall=['l2','l4']
    #                             self.list_windows.append(r)
    #                            # print 'orizzontale    x_min: '+str( r.x_min)+'   y_min: '+str(r.y_min)+'   x_max: '+str(r.x_max)+'   y_max: '+str(r.y_max)
    #                             self.walls_window.append([n.id,w.id])
    #                             self.walls_window.append([w.id,n.id])








    #Pierpaolo's
    # def find_connected_componet_no_wall(self,input_image,return_value):
    def find_connected_componet_no_wall(self,path_gravvitas,current_directory,only_image_name,debug_mode):
        name = str(self.file_name[0:-4])
        #--non-testing
        input_path = str(self.path_output+name+'/')
        output_path = str(self.path_output+name+'/')
        #-- end non-testing

        # #--testing paths
        # input_path = str(self.current_directory+'/input_fps/test_svg/object/')
        # output_path = str(self.current_directory+'/input_fps/test_svg/object/')
        # #--end testing



        #---check if CC (Conected Component) is a
        #---window
        window = GraphCC()
        #---maybe a window
        may_win = GraphCC()
        #---external CC
        external = GraphCC()
        #---maybe a external or maybe internal CC - If CC is surrounded by walls its internal
        #--but when it has walls only on 2 sides maybe internal, maybe external
        may_be = GraphCC()



        #i add it because i have to reset the lists once extracted text
        del self.list_cc[:]


        #--testing
        # path = self.current_directory+'/input_fps/test_svg/object/'
        #--non-testing
        path = str(self.path_output+name+'/')
        ## correct_path = path

        #---get text from text_cordinates.txt -its a file written using Methods_Text_Recognition_Based_Analysis_cc identification
        text_data = self.load_text_coordinates(input_path)
        del self.text_coordinates[:]
        self.text_coordinates = text_data
        # print self.text_coordinates
        # load stefanos output in grascale and color


        #--normal run
        self.img_walls_and_doors = cv2.imread(path+'Stefano_output.png',cv2.IMREAD_GRAYSCALE)
        use_height, use_width = self.img_walls_and_doors.shape
        self.img_walls_and_doors_color = cv2.imread(path+'Stefano_output.png',cv2.IMREAD_COLOR)
        #--end normal run

        #-- find average door width. If dw<40 fix to 40, otherwise get new value
        avg_doors = imop.avg_doors_width(self.img_walls_and_doors_color)


        #--discarded CC is where all the CC too small to be anything is stored
        if not(os.path.exists(output_path)):
            os.mkdir(output_path)
        path_discarded = output_path+'discarded_cc/'
        if not(os.path.exists(path_discarded)):
            os.mkdir(path_discarded)
        #-------------------
        # self.remove_text = cv2.imread(test_path+'no_text.png', 0)
        self.remove_text = cv2.imread(input_path+'no_text.png', 0)
        #--thresholding graysale image. Meaning take pixels between 1-255 color values. So dont take black and draw in White
        #---inRange returns binary mask: white pixels (255) are pixels within upper and lower limit range and black pixels (0) are ones that do not
        img_tmp = cv2.inRange(self.img_walls_and_doors,1,255)
        if debug_mode:
            cv2.imwrite(output_path+'img_tmp.png', img_tmp)
        #--- remove all stefano identification(walls+doors+windows) from no text image (this image is original image with text erased)
        #---intention being final image to have objects+doors, windows not identified by stefano
        self.remove_text = self.remove_text-img_tmp
        img = self.remove_text
        if debug_mode:
            cv2.imwrite(output_path+'image_input.png', img)

        #---initial operation to find rooms
        rooms_cc = imop.find_connected_componet_rooms(self.img_walls_and_doors, img, output_path)
        self.img_no_wall = self.no_walls()
        self.avg_width = imop.calculate_avg_width(self.img_no_wall)
        #-------------------
        self.img_red_wall = cv2.imread(input_path+'red_wall.png', cv2.IMREAD_COLOR)
        cv2.imwrite(output_path+'no_wall.png', self.img_no_wall)
        #### print 'len(rooms_cc)',len(rooms_cc)
        for i in range(0,len(rooms_cc)):
            room = Room(i, rooms_cc[i], output_path)
            room.set_images(self.img_red_wall,  self.img_no_wall, img)
            room.width=self.avg_width
            self.rooms.append(room)
            self.room_index[room] = len(self.rooms)-1
        self.height,self.width=img.shape
        self.labels, self.numlabels =ndimage.label(img>127)
        self.graph_cc.image=self.labels
        if debug_mode:
            s=self.file_name[0:-4]
        for i in range(1,self.numlabels+1):
            cc=CC(i)
            point= np.argwhere(self.labels == i)
            # find x_max, x_min, y_max, y_min
            cc.y_min=point[:,0].min()
            cc.x_min=point[:,1].min()
            cc.y_max=point[:,0].max()
            cc.x_max=point[:,1].max()
            #I create image of the connected compis an open image with black background and white lines, i want as output an image with white background and black linesonent
            cc.image=np.where(self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, 255)#self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
            cc.numPixel=(cc.image.sum()/255)
            cc.image = imop.add_pad_single_pixel(cc.image)
            cc.set_coordinates()
            cc.image_dist_tran=ndimage.morphology.distance_transform_cdt(cc.image)
            cc.find_contour()
            if i == 1: #external cc
                external.add_node(cc)
            elif cc.area>=20:
                cc.check_shape()
                # I add the connected component to the list
                self.list_cc.append(cc)
                isin = False
                for room in self.room_index:
                    if isin == False:
                        isin = imop.is_inside(cc, room.contour_cc)
                        if isin == True and len(room.label) == 0:
                            # print 'add node '+str(cc.id)+' to the room '+str(room.id)
                            cc.create_histogram(bins1,bins2)
                            room.add_node(cc)
                            self.num_cc_inside+=1
                        elif isin == True and len(room.label) != 0:
                            check = self.check_extern(cc)
                            if check:
                                cc.create_histogram(bins1,bins2)
                                may_be.add_node(cc)

                            else:
                                checkWin = self.check_windows(cc)
                                if checkWin:
                                    cc.create_histogram(bins1,bins2)
                                    may_win.add_node(cc)
                                external.add_node(cc)
                                # print 'connected component '+str(cc.id)+' added to external ambient'
                                # cv2.imwrite(path_external+str(cc.id)+'.png', cc.image)
                if isin == False:
                    # print 'discarded connected component '+str(cc.id)
                    cv2.imwrite(path_discarded+str(cc.id)+'.png', cc.image)


        ymax, mxax = self.img_walls_and_doors.shape
        #----update room identification
        new_contours, self.rooms[0].contour_cc = imop.update_room(self.rooms[0].contour_cc, img, external, ymax, mxax, self.avg_width)
        if len(new_contours) > 0:
            id = len(self.rooms)
            for i in range(0, len(new_contours)):
                room = Room(i+id, new_contours[i], output_path)
                room.set_images(self.img_red_wall,  self.img_no_wall, img)
                room.width = self.avg_width
                self.rooms.append(room)
                self.room_index[room] = len(self.rooms)-1
            for cc in may_be.get_indices():
                isin = False
                for room in self.room_index:
                    if room.id >= id and isin == False:
                        isin = imop.is_inside(cc, room.contour_cc)
                        if isin == True:
                            check = self.check_windows(cc)
                            touching, dst = imop.touching_dilation(cc, self.rooms[0].contour_cc, self.avg_width)
                            if check and touching == True:
                                # print 'add new window cc: '+str(cc.id)+' to the room '+str(room.id)
                                room.add_window(cc)
                                window.add_node(cc)
                            else:
                                # print 'add node '+str(cc.id)+' to the room '+str(room.id)
                                room.add_node(cc)
                                self.num_cc_inside+=1
            for cc in may_win.get_indices():
                touching = False
                for room in self.room_index:
                    if room.id >= id and touching == False:
                        touching, dst = imop.touching_dilation(cc, room.contour_cc, self.avg_width)
        else:
            id = 0


        self.save_windows_2(id, output_path,self.img_walls_and_doors_color,avg_doors)

        self.set_room_labels(output_path)
        self.color_rooms(output_path,debug_mode)
        for room in self.room_index:
            if room.id == 0:
                room.graph = external
                room.set_images(self.img_red_wall,  self.img_no_wall, img)
            room.calculate_graph(avg_doors)
            room.save_images()
            room.save_windows(debug_mode)
        self.save_windows(window, output_path,debug_mode)
        img_edges = cv2.cvtColor(self.remove_text, cv2.COLOR_GRAY2RGB)
        for room in self.room_index:
            if room.id != 0:
                img_edges = room.draw_graph(img_edges)

        cv2.imwrite(output_path+'new_walls_windows_door-test.png', self.img_walls_and_doors_color)
        cv2.imwrite(output_path+'new_red_wall-test.png', self.img_red_wall)

        # print 'Stefano details',use_width,use_height
        iteration_obj = iterative_functions_class()
        cropped_image_image_new_walls = iteration_obj.crop_image(output_path+'new_walls_windows_door-test.png',use_height,use_width)
        cropped_image_image_new_walls.save(output_path+'new_walls_windows_door.png')
        cropped_image_new_red_wall = iteration_obj.crop_image(output_path+'new_red_wall-test.png',use_height,use_width)
        cropped_image_new_red_wall.save(output_path+'new_red_wall.png')
        cropped_image_red_wall = iteration_obj.crop_image(input_path+'red_wall.png',use_height,use_width)
        cropped_image_red_wall.save(output_path+'red_wall.png')
        cropped_image_no_text = iteration_obj.crop_image(input_path+'no_text.png',use_height,use_width)
        cropped_image_no_text.save(output_path+'no_text.png')
        cropped_image_stefano_output = iteration_obj.crop_image(input_path+'Stefano_output.png',use_height,use_width)
        cropped_image_stefano_output.save(output_path+'Stefano_output.png')
        cropped_image_walls_and_doors = iteration_obj.crop_image(input_path+'walls_and_doors.png',use_height,use_width)
        cropped_image_walls_and_doors.save(output_path+'walls_and_doors.png')

        self.find_window_cordinates(output_path,path_gravvitas,only_image_name)

    def find_window_cordinates(self,path,path_gravvitas,only_image_name):
        pil_image = Image.open(path+'new_walls_windows_door.png')
        pixels_2 = pil_image.load()
        empty_new_image = ~(np.zeros((self.height,self.width,3), np.uint8))
        cv2.imwrite(path+'_INTERMEDIATE.png', empty_new_image)
        image_3 = Image.open(path+'_INTERMEDIATE.png')
        pixels_3 = image_3.load()
        for img_row in range(self.height-1):
            for img_column in range(self.width-1):
                color = pixels_2[img_column,img_row]
                #--find yellow color (windows)
                if color[0]==255 and color[1]==255 and color[2]==0:
                    pixels_3[img_column,img_row]= (0,0,0)
        image_3.save(path+'only_windows.png')
        os.remove(path+'_INTERMEDIATE.png')

        gray_room = cv2.imread(path+'only_windows.png',cv2.IMREAD_GRAYSCALE)
        ret,thresh = cv2.threshold(gray_room,0,255,1)
        contours,hierachy = cv2.findContours(thresh,1,2)
        image_contour = []
        if len(contours) > 0:
            for cnt in contours:
                temp_image_contour = []
                for l, level1 in enumerate(cnt):
                    for level2 in level1:
                        x,y = level2
                        temp_image_contour.append([int(x),int(y)])
                if len(temp_image_contour)>0:
                    image_contour.append(temp_image_contour)

        window_details_file = open(path_gravvitas+only_image_name+'_4_window_details.txt', 'w')
        for r,row in enumerate(image_contour):
            window =''
            for c,cord in enumerate(row):
                if c==0:
                    window  = window+'['+str(cord[0])+','+str(cord[1])+']'
                else:
                    window  = window+',['+str(cord[0])+','+str(cord[1])+']'
            window_details_file.write('Window : '+window+'\n')
        window_details_file.close()

    def set_room_labels(self, path):
        for text in self.text_coordinates:
            cx, cy = text[1]
            isin = False
            for room in self.room_index:
                if room.id != 0 and isin == False:
                    isin = room.contour_cc.check_point_inside(cx, cy)
                    if isin:
                        # print 'add label '+text[0]+' to the room '+str(room.id)
                        room.add_label(text)
            # if isin == False:
            #     print 'add label '+text[0]+' to the room external'
            #     self.rooms[0].add_label(text)
        for room in self.room_index:
            if len(room.label) == 0:
                room.add_none()

    def color_rooms(self,path,debug_mode):
        ccWhole = CC(0)
        ccWhole.image = np.array(self.img_walls_and_doors_color, dtype=np.uint8)
        # ccWhole.image = imop.add_pad_single_pixel(ccWhole.image)
        # ccWhole.image = imop.add_pad_single_pixel(ccWhole.image)
        ccWhole.x_min = 1
        ccWhole.y_min = 1
        ccWhole.set_coordinates()
        # image = deepcopy(self.img_walls_and_doors_color)
        font = cv2.FONT_HERSHEY_COMPLEX
        j = 0
        for room in self.room_index:
            if room.label[0].name!='external':
                cc= room.contour_cc
                cc.image = np.where(room.contour_cc.image<127, 0, 255)
                cc.y_min = cc.y_min+1
                cc.x_min = cc.x_min+1
                cc.set_coordinates()
                img, ccWhole.image = imop.same_size(room.contour_cc, ccWhole)
                img = np.array(img, dtype=np.uint8)
                img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
                color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

                ccWhole.image = np.where(img!=(0,0,0), color, ccWhole.image)
                # cv2.imwrite(path+str(j)+'rooms.png', ccWhole.image)
                j+=1
                # image[cc.y_min-1:cc.y_max, cc.x_min-1:cc.y_max]=np.where(img!=(0,0,0),color,image[cc.y_min-1:cc.y_max, cc.x_min-1:cc.y_max])
                # h1,w1,c1 = img.shape
                # i = 0
                # go = True
                # while go and i < 20:
                #     h2,w2,c2 = image.shape
                #     if h1 == h2 and w1 == w2:
                #         go = False
                #     else:
                #         image = imop.add_pad_single_pixel(image)
                #         i+=1
                #         print 'ciclo '+str(i)
                # color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
                # ccWhole.image = np.where(img!=(0,0,0), color, ccWhole.image)


                # cv2.imwrite(path+str(j)+'more_text_rooms.png', ccWhole.image)
                # j+=1
                # print 'inserito il testo '+room.label[0].name+' alla altezza ('+str(room.label[0].x_min)+', '+str(room.label[0].y_min)+')'
        for text in self.text_coordinates:
            x1, y1, x2, y2 = text[2]
            # print 'added text '+text[0]+' at the (x,y) = '+str(text[2])
            cv2.putText(ccWhole.image,text[0],(x1, y1), font, 2,(0,0,0),2)
        if debug_mode:
            cv2.imwrite(path+'rooms.png', ccWhole.image)

    def save_windows_2(self, id, path,img_walls_and_doors_color,avg_doors):
        # print 'saving the floorplan with new windows!'
        # isbreak1 = False
        if id != 0:
        
            ccWhole = CC(0)
            img = np.array(self.img_walls_and_doors_color, dtype=np.uint8)
            ccWhole.image=cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            ccWhole.x_min = 1
            ccWhole.y_min = 1
            ccWhole.set_coordinates()
            self.rooms[0].contour_cc.image, ccWhole.image = imop.same_size(self.rooms[0].contour_cc, ccWhole)
            ccWhole.image = imop.add_pad_single_pixel(ccWhole.image)

            for r,room in enumerate(self.room_index):
                if room.id >= id:
                    labels, numlabels =ndimage.label(self.img_walls_and_doors<10)
                    num = numlabels
                    # final_dst = np.zeros(ccWhole.image.shape, dtype=np.uint8)
                    j = 0
                    # cv2.imwrite(path+'room_'+str(room.id)+str(r)+'.png', room.contour_cc.image)
                    # isbreak= False
                    while num <= numlabels and j < 50*self.avg_width:
                        # print 'in loop - ',str(room.id)+str(r)+str(j)
                        touching, dst = imop.touching_dilation(room.contour_cc, self.rooms[0].contour_cc, self.avg_width+j)
                        # cv2.imwrite(path+'room_'+str(room.id)+str(r)+'_and_external_'+str(j)+'.png', dst)
                        #--anuradhas addition
                        # preprocess_object = pre_process_class()
                        # contour_to_delete = False
                        # contour_to_delete,input_image = preprocess_object.edit_image(dst,img_walls_and_doors_color,input_image,path,str(room.id)+str(r)+str(j),avg_doors,contour_to_delete)
                        # if contour_to_delete:
                        #     j=10*self.avg_width
                        #     isbreak=True
                        #     break
                        # else:
                        if touching == True:
                            dst = np.array(dst, dtype=np.uint8)
                            dst = np.where(dst<127, 0, 255)
                            dst = np.array(dst, dtype=np.uint8)
                            dst2 = deepcopy(dst)
                            # final_dst+=dst

                            dst = cv2.cvtColor(dst, cv2.COLOR_GRAY2RGB)
                            # cv2.imwrite(path+'new_walls_windows_door-0'+str(r)+str(j)+'.png', dst)

                            go = True
                            h1,w1,c1 = dst.shape
                            i = 0
                            while go and i < 20:
                                h2,w2,c2 = self.img_walls_and_doors_color.shape
                                if h1 == h2 and w1 == w2:
                                    go = False
                                else:
                                    self.img_walls_and_doors_color = imop.add_pad_single_pixel(self.img_walls_and_doors_color)
                                    i+=1
                            go = True
                            h1,w1,c1 = dst.shape

                            i = 0
                            while go and i < 20:
                                h2,w2,c2 = self.img_red_wall.shape
                                if h1 == h2 and w1 == w2:
                                    go = False
                                else:
                                    self.img_red_wall = imop.add_pad_single_pixel(self.img_red_wall)
                                    i+=1
                            self.img_walls_and_doors_color = np.where(dst!=(0,0,0), (0,255,255), self.img_walls_and_doors_color)
                            self.img_red_wall = np.where(dst!=(0,0,0), (0,0,255), self.img_red_wall)
                            self.img_walls_and_doors = np.array(self.img_walls_and_doors_color, dtype=np.uint8)
                            self.img_walls_and_doors = cv2.cvtColor(self.img_walls_and_doors, cv2.COLOR_RGB2GRAY)
                            labels, num =ndimage.label(self.img_walls_and_doors<50)
                        j+=1

    def save_windows(self, window, path,debug_mode):
        if window.size()>0:
            path = path+'windows/'
            if not(os.path.exists(path)):
                os.mkdir(path)
            window.save_graph(path,'graph.txt',dictionary=False)
            window.save_images(path)
            ccWhole = CC(0)
            img = np.array(self.img_red_wall, dtype=np.uint8)
            ccWhole.image=cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            ccWhole.x_min = 1
            ccWhole.y_min = 1
            ccWhole.set_coordinates()
            img = self.img_red_wall
            for cc in window.get_indices():
                imgWin, image = imop.same_size(cc,ccWhole)
                imgWin = np.array(imgWin,dtype=np.uint8)
                imgWin = cv2.cvtColor(imgWin, cv2.COLOR_GRAY2BGR)
                img = np.where(imgWin!=(0,0,0), (0,255,0), img )
            if debug_mode:
                cv2.imwrite(path+'room_with_new_windows.png', img)

    def check_extern(self, cc):
        walls = 0
        xmin, xmax, ymin, ymax = cc.get_extremes_from_center()
        # print 'estremi della cc '+str(cc.id)+': '+str(cc.get_extremes())
        cx = cc.cx[0]+cc.x_min
        cy = cc.cy[0]+cc.y_min
        # print 'cui centroide: ('+str(cx)+', '+str(cy)+')'
        find = imop.count_red(self.img_red_wall, 1, ymin, cx, cx+1)
        if find:
            walls+=1
        find = imop.count_red(self.img_red_wall, cy, cy+1, 1, xmin)
        if find:
            walls+=1
        find = imop.count_red(self.img_red_wall, ymax, self.height-1, cx, cx+1)
        if find:
            walls+=1
        find = imop.count_red(self.img_red_wall, cy, cy+1, xmax, self.width-1)
        if find:
            walls+=1
        if walls == 4:
            #cc surrounded by 4 walls!
            return True
        elif walls == 3:
            #suspected: maybe a door or window didn't found
            #self.check_again(): --- to be implemented
            return True
        else:
            return False

    #check if a cc is a window
    def check_windows(self, cc):

        walls = 0
        xmin, xmax, ymin, ymax = cc.get_extremes_from_center()
        # print 'estremi della cc '+str(cc.id)+': '+str(cc.get_extremes())
        cx = cc.center_x
        cy = cc.center_y
        # print 'cui centroide: ('+str(cx)+', '+str(cy)+')'
        find = imop.count_red(self.img_red_wall, 0, ymin, cx, cx+1)
        if find:
            walls+=1
        find = imop.count_red(self.img_red_wall, cy, cy+1, 0, xmin)
        if find:
            walls+=1
        find = imop.count_red(self.img_red_wall, ymax, self.height-1, cx, cx+1)
        if find:
            walls+=1
        find = imop.count_red(self.img_red_wall, cy, cy+1, xmax, self.width-1)
        if find:
            walls+=1
        if walls == 4:
            #cc surrounded by 4 walls!
            return False
        else:
            # print 'forse una finestra: cc '+str(cc.id)
            ccWhole = CC(0)
            ccWhole.image=self.image_remove_text
            ccWhole.x_min = 0
            ccWhole.y_min = 0
            ccWhole.set_coordinates()
            imgRed, img = imop.same_size(cc,ccWhole)
            i = 2
            numlabels = 0
            go = True
            kernel = np.ones((8,8),np.uint8)
            imgRed2 = cv2.dilate(imgRed,kernel,iterations = 1)
            imgRed2 = np.array(imgRed2,dtype=np.uint8)
            imgRed2 = cv2.cvtColor(imgRed2, cv2.COLOR_GRAY2BGR)
            image = np.where(imgRed2!=(0,0,0), self.img_red_wall, (0,0,0))
            image = cv2.inRange(image, (0,0,255), (0,0,255))
            # kernel = np.ones((1,1),np.uint8)
            # image = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
            s2 = [[1, 1, 1], [1,1,1], [1,1,1]]
            labels, numlabels = ndimage.label(image!=0, s2)


            if numlabels == 2:
                corners = [0,0]
                area = []
                #calcualte, for each red cc, the max angle between the centroid and walls
                for i in range(1,3):
                    ccRed = CC(1)
                    point= np.argwhere(labels == i)

                    ccRed.y_min=point[:,0].min()
                    ccRed.x_min=point[:,1].min()
                    ccRed.y_max=point[:,0].max()
                    ccRed.x_max=point[:,1].max()
                    #I create image of the connected compis an open image with black background and white lines, i want as output an image with white background and black linesonent
                    ccRed.image=np.where(labels[ccRed.y_min:ccRed.y_max+1,ccRed.x_min:ccRed.x_max+1] == i, 255, labels[ccRed.y_min:ccRed.y_max+1,ccRed.x_min:ccRed.x_max+1])#self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)

                    ccRed.image = imop.add_pad_single_pixel(ccRed.image)
                    ccRed.set_coordinates()

                    ccRed.find_contour()
                    over = imop.check_over_90(ccRed, cc.center_x, cc.center_y)

                    if over == False:
                        area.append(ccRed.area)

                        for xA in ccRed.contourSimple[0]:
                            for xB in ccRed.contourSimple[0]:
                                corner = mm.calculate_angle(xA[0][0], xA[0][1], xB[0][0],xB[0][1], cc.center_x, cc.center_y)
                                # print 'corner = '+str(corner)
                                if corner > corners[i-1]:
                                    corners[i-1] = corner
                    else:
                        return False

                diff = abs(corners[0]-corners[1])
                if min(area[0], area[1])!=0:
                    areaRatio = max(area[0], area[1])/min(area[0], area[1])
                else:
                    areaRatio = 9999
                # print 'diff = '+str(diff)
                # print 'corners = '+str(corners)
                if diff < 20 and corners[0] < 60 and corners[1] < 60 and areaRatio < 2:

                    return True
                else:
                    return False
        return False

    def save_text(self,path):
        #---write 1.text, 2.centroid of text, 3.topleft and bottomright of text
        file = open(path+'recognized_text.txt', 'w')
        file.write(str(len(self.text_coordinates))+'\n')
        for text in self.text_coordinates:
            for i in range(0, len(text)):
                if i == 0:
                    file.write(str(text[i])+'\n')
                elif i == 1:
                    for j in range(0, 2):
                        file.write(str(text[i][j])+'\n')
                elif i ==2:
                    for j in range(0, 4):
                        file.write(str(text[i][j])+'\n')

        file.close()

    def load_text_coordinates(self, path):
        # print path+'/recognized_text.txt'
        file = open(path+'/recognized_text.txt', 'r')
        num = int(file.readline())
        text_data = []
        for k in range(0, num):
            text = []
            name = str(file.readline())
            while name[-1] == '\n':
                name = name[0:-1]
            text.append(name)
            cx = int(file.readline())
            cy = int(file.readline())
            text.append([cx,cy])
            x1 = int(file.readline())
            y1 = int(file.readline())
            x2 = int(file.readline())
            y2 = int(file.readline())
            text.append([x1,y1,x2,y2])
            text_data.append(text)
        file.close()
        return text_data

    def find_connected_componet_dictionary(self):
        del self.list_cc[:]
        img=self.image_remove_text
        self.height,self.width=img.shape
        self.labels, self.numlabels =ndimage.label(img>127)
        cv2.imwrite(self.path_debug+self.file_name, img)
        # print 'number of connected components: '+str(self.numlabels)
        s=self.file_name[0:-4]
        if self.numlabels == 1:
            b = 1
        else:
            b = 2
        for i in range(b,self.numlabels+1):

                cc=CC(i)
                point= np.argwhere(self.labels == i)
                # find x_max, x_min, y_max, y_min
                cc.y_min=point[:,0].min()
                cc.x_min=point[:,1].min()
                cc.y_max=point[:,0].max()
                cc.x_max=point[:,1].max()
                # print 'the component ' +str(i) +' has max coordinates [(' +str(cc.x_min) +', ' +str(cc.x_max) +'), (' +str(cc.y_min) +', '+str(cc.y_max)+')]'
                cc.height= cc.y_max-cc.y_min+1
                cc.width= cc.x_max-cc.x_min+1
                #the center of the cc
                cc.center_x = cc.y_min+(cc.height/2)
                cc.center_y = cc.x_min+(cc.width/2)
                #I create image of the connected comps an open image with black background and white lines, I want as output an image with white background and black lines onent
                cc.image=(np.where(self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, self.labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
                #print self.path_output+'white_'+str(cc.id)+'.png'
                #cc2.imwrite(self.path_output+'white_'+str(cc.id)+'.png', cc.image_dist_tran)

                cc.numPixel=(cc.image.sum()/255)
                #add padding to cc.image
                cc.image = imop.add_pad_single_pixel(cc.image)
                cc.set_coordinates()
                cv2.imwrite(self.path_output+'white_'+str(cc.id)+'.png', cc.image)

                cc.image_dist_tran=ndimage.morphology.distance_transform_cdt(cc.image)
                #print self.path_output+'dst_'+str(cc.id)+'.png'
                #cv2.imwrite(self.path_output+'dst_'+str(cc.id)+'.png', cc.image_dist_tran)
                # I add the connected component to the list
                cc.find_contour()
                cc.check_shape()
                #print cc.area
                if cc.area>20:
                    cc.create_histogram(bins1, bins2)
                    self.list_cc.append(cc)
                    self.graph_cc.add_node(cc)
        self.avg_width=imop.calculate_avg_width(img)
        self.graph_cc.img_no_text=img
        self.graph_cc.create_edges(self.avg_width, dictionary = True)

        if len(self.list_cc) > 1:
            # print len(self.list_cc)
            floor = self.graph_cc.getMaxNode()
            self.graph_cc.floor_adjacence(floor, dictionary=True)
            #----removes nodes (reason can be: that it has bad features)
            #self.graph_cc.remove_node_and_edge(floor)
            img, img2 = self.graph_cc.save_edges(floor)
            cv2.imwrite(self.path_output+'edges.png', img)
            cv2.imwrite(self.path_output+'edges_graph.png', img2)
        self.graph_cc.save_graph(self.path_output, self.file_name, dictionary=True)
        self.graph_cc.save_images(self.path_output)

        self.graph_cc.save_graph(self.path_output, self.file_name, dictionary=True)
        self.graph_cc.save_images(self.path_output)

    def no_walls(self):
        img_bw = np.array(self.image_remove_text)
        # print img_bw.shape
        # print self.img_walls_and_doors.shape
        img_bw = np.where(self.img_walls_and_doors>10, 255, img_bw)
        return img_bw



    def draw_all_open_plan_lines(self,output_path,all_open_plan_lines,debug_mode):
        # #--testing
        # output_path = '/home/ub/Documents/floorplan_recognition/tesimagistrale/input_fps/object_test/output_test/fp_1/'
        # #--end testing
        pierpaolo_image = cv2.imread(output_path+'new_walls_windows_door.png',cv2.IMREAD_COLOR)
        walls_and_doors_image = cv2.imread(output_path+'walls_and_doors.png',cv2.IMREAD_COLOR)
        red_wall_image = cv2.imread(output_path+'red_wall.png',cv2.IMREAD_COLOR)

        # print output_path+'new_walls_windows_door.png'
        #--fill in missing line segments
        bougs_path1,bogus_path_2 ,name = '','',''
        line_opt = line_optimization_class(bougs_path1,bogus_path_2,name)
        new_max_lines = line_opt.find_closest_contour_point_region_grow(output_path,'new_walls_windows_door.png',all_open_plan_lines)


        output_path_list = os.listdir(output_path)
        output_path_list.sort()

        for each_item in output_path_list:
            content_name = str(each_item)
            if content_name != 'no_text.png' and content_name != 'recognized_text.txt':
                if os.path.isdir(output_path+content_name):
                    shutil.rmtree(output_path+content_name)
                else:
                    os.remove(output_path+content_name)

        for each_line in new_max_lines:
            # print 'each_line',each_line
            cv2.line(pierpaolo_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,255),5,cv2.cv.CV_AA)
            cv2.line(walls_and_doors_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,255),5,cv2.cv.CV_AA)
            cv2.line(red_wall_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,255),5,cv2.cv.CV_AA)
        cv2.imwrite(output_path+'Stefano_output.png', pierpaolo_image)
        cv2.imwrite(output_path+'walls_and_doors.png', walls_and_doors_image)
        cv2.imwrite(output_path+'red_wall.png', red_wall_image)


    def find_adjacent_loop2(self,rect,pre_rect):
        pre_rect.append(rect)
        adjacent = rect.edges()
        list_return = []
        for a in adjacent:
            list_new = []
            n = a.get_node()
            if n.type != 'wall':
                if not( n in pre_rect ):
                    #if (a.get_type()=='ADIACENT' or a.get_type()=='ALIGNED')                   :
                    list_new.extend(self.find_adjacent_loop2(n,pre_rect))
                    if len( list_new)>0:
                        for e2 in list_new:
                            e2.append(n)
            else:
                if not( n in pre_rect ):
                    list_new.append([n])
            for l in list_new:
                list_return.append(l)

        return list_return

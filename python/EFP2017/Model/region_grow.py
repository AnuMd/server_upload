__author__ = 'anu'

import cv2,time
import numpy as np
import os

from PIL import Image
import PIL.ImageOps
from color_check import color_check_class
from iterative_functions import iterative_functions_class
from open_plan_line import line



class region_grow_class:
    def __init__(self, output_directory,image,image_name):
        self.image = image
        # self.image_name=image_complete_name[0:-4]
        # image_extension = image_complete_name[-4:]
        self.image_name = image_name
        self.height,self.width = image.shape
        self.outer_directory = output_directory
        # self.directory = output_directory
        # self.output_directory = output_directory+'Rooms/'+self.image_name+'/Voronoi_Lines/'
        self.output_directory = output_directory+'Voronoi_Lines/'
        self.iterative_obj = iterative_functions_class()


    def grow_regions(self,current_open_plan_contour_data,open_plan_text_cordinate,name,
                     image_name,image_path,debug_mode,text_words,text_bounding_boxes,
                     path_final_lines,only_image_name):
        # contour_image = ~(np.zeros((self.height,self.width,3), np.uint8))
        # for l,each_line in enumerate(self.all_contour_lines):
        #     cv2.line(contour_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,0),1,cv2.cv.CV_AA)
        # cv2.imwrite(self.output_directory+name+'_STEP_01.png',contour_image)

        contour_check = current_open_plan_contour_data[2]
        # gray_room = cv2.imread(self.output_directory+name+'_STEP_01.png',cv2.IMREAD_COLOR)
        # ret,thresh = cv2.threshold(gray_room,0,255,1)
        # contours,hierachy = cv2.findContours(thresh,1,2)
        # for count, cnt in enumerate(contours):
        #     if count ==1:
        #         contour_check = cnt
        #         break

        outer_cont = ~(np.zeros((self.height,self.width,3), np.uint8))
        cv2.drawContours(outer_cont,[contour_check],0,(0,0,0),2)
        # cv2.imwrite(self.output_directory+name+'_STEP_01.png',outer_cont)

        image = Image.fromarray(outer_cont)

        # image = Image.open(self.output_directory+name+'_STEP_01.png')
        width, height = image.size
        pixels = image.load()


        #---- recheck if can be combined
        threshold_array_row_length = len(open_plan_text_cordinate)
        top_threshold_pixel_array = [[0 for b in range(threshold_array_row_length)] for c in range(width)]
        bottom_threshold_pixel_array = [[height for b in range(threshold_array_row_length)] for c in range(width)]
        left_threshold_pixel_array = [[0 for b in range(threshold_array_row_length)] for c in range(height)]
        right_threshold_pixel_array = [[width for b in range(threshold_array_row_length)] for c in range(height)]

        #-----create array to store all direction and pixel data and initialize it
        pixel_data = [[-1 for column in range(width)] for row in range(height)]
        color_val_obj = color_check_class(pixels, top_threshold_pixel_array,bottom_threshold_pixel_array,left_threshold_pixel_array, right_threshold_pixel_array)
        temp_array_copy = []
        temp_ring_array = []
        flag_list = [0 for x in range(len(open_plan_text_cordinate))]
        for ring_count in range(2000):
            for current_txt_label in range(len(open_plan_text_cordinate)):
                if flag_list[current_txt_label]==0:
                    each_cordinate = open_plan_text_cordinate[current_txt_label]
                    if ring_count == 0:
                        #---- update pixel_data array with txt_lable cordinate
                        column,row = each_cordinate
                        pixel_data[row][column]= current_txt_label
                        #-----add txt_label cordinate to temp_array
                        ta_length = len(temp_array_copy)
                        temp_array_copy.append([])
                        temp_array_copy[ta_length].append(each_cordinate)
                        temp_array_copy[ta_length].append(current_txt_label)
                    flag_count = 0
                    text_exists_flag = -1
                    for row in temp_array_copy:
                        if row[1]==current_txt_label:
                            text_exists_flag=1
                            col_coord, row_coord = row[0]
                            top_pixel = col_coord, row_coord-1
                            left_pixel = col_coord-1, row_coord
                            bottom_pixel = col_coord, row_coord+1
                            right_pixel = col_coord+1, row_coord

                            top_col,top_row = top_pixel
                            if cv2.pointPolygonTest(contour_check,(top_col,top_row),False)==1:
                                if pixel_data[top_row][top_col] ==-1:
                                    temp_ring_array, pixel_data = color_val_obj.color_match(top_pixel,pixel_data,temp_ring_array,'T',current_txt_label)
                                    flag_count= flag_count+1


                            left_col,left_row = left_pixel
                            if cv2.pointPolygonTest(contour_check,(left_col,left_row),False)==1:
                                if pixel_data[left_row][left_col] ==-1:
                                    temp_ring_array, pixel_data = color_val_obj.color_match(left_pixel,pixel_data,temp_ring_array,'L',current_txt_label)
                                    flag_count= flag_count+1

                            bottom_col,bottom_row = bottom_pixel
                            if cv2.pointPolygonTest(contour_check,(bottom_col,bottom_row),False)==1:
                                if pixel_data[bottom_row][bottom_col] ==-1:
                                    temp_ring_array, pixel_data = color_val_obj.color_match(bottom_pixel,pixel_data,temp_ring_array,'B',current_txt_label)
                                    flag_count= flag_count+1

                            right_col,right_row = right_pixel
                            if cv2.pointPolygonTest(contour_check,(right_col,right_row),False)==1:
                                if pixel_data[right_row][right_col] ==-1:
                                    # print right_col,right_row
                                    temp_ring_array, pixel_data = color_val_obj.color_match(right_pixel,pixel_data,temp_ring_array,'R',current_txt_label)
                                    flag_count= flag_count+1
                        else:
                            text_exists_flag=0

                    if flag_count==0 and text_exists_flag==1:
                        flag_list[current_txt_label]= 1
                        break
            temp_array_copy=temp_ring_array
            temp_ring_array = []
            if sum(flag_list)==len(open_plan_text_cordinate):
                # if debug_mode:
                #     print ring_count
                break

            # #--intermediate rings
            # image_2 = Image.open(self.output_directory + name + '_STEP_01.png')
            # pixels_2 = image_2.load()
            # for img_row in range(height - 1):
            #     for img_column in range(width - 1):
            #         # colors go as R,G,B
            #         if pixel_data[img_row][img_column] == 0:
            #             pixels_2[img_column, img_row] = (255, 0, 0)
            #         if pixel_data[img_row][img_column] == 1:
            #             pixels_2[img_column, img_row] = (0, 255, 0)
            #         if pixel_data[img_row][img_column] == 2:
            #             pixels_2[img_column, img_row] = (0, 0, 255)
            #         if pixel_data[img_row][img_column] == 3:
            #             pixels_2[img_column, img_row] = (255, 255, 0)
            #         if pixel_data[img_row][img_column] == 4:
            #             pixels_2[img_column, img_row] = (102, 51, 0)
            #         if pixel_data[img_row][img_column] == 5:
            #             pixels_2[img_column, img_row] = (255, 0, 255)
            #         if pixel_data[img_row][img_column] == 6:
            #             pixels_2[img_column, img_row] = (0, 255, 255)
            #         if pixel_data[img_row][img_column] == 7:
            #             pixels_2[img_column, img_row] = (255, 0, 125)
            #         if pixel_data[img_row][img_column] == 8:
            #             pixels_2[img_column, img_row] = (255, 128, 0)
            #         if pixel_data[img_row][img_column] == 9:
            #             pixels_2[img_column, img_row] = (125, 0, 255)
            #         if pixel_data[img_row][img_column] == 10:
            #             pixels_2[img_column, img_row] = (102, 0, 0)
            #         if pixel_data[img_row][img_column] == 11:
            #             pixels_2[img_column, img_row] = (0, 150, 75)
            #         if pixel_data[img_row][img_column] == 12:
            #             pixels_2[img_column, img_row] = (102, 0, 102)
            #         if pixel_data[img_row][img_column] == 13:
            #             pixels_2[img_column, img_row] = (150, 150, 0)
            #         if pixel_data[img_row][img_column] == 14:
            #             pixels_2[img_column, img_row] = (200, 255, 200)
            #
            # image_2.save(self.output_directory + name + '_'+str(ring_count)+'.png')
            # test_image = cv2.imread(self.output_directory + name + '_'+str(ring_count)+'.png', cv2.IMREAD_COLOR)
            # for row in text_bounding_boxes:
            #     x1,y1,x2,y2 = row
            #     cv2.rectangle(test_image, (x1, y1), (x2, y2), (255, 255, 255), -1)
            #
            # for r, row in enumerate(text_words):
            #     cord1 = row[1][0]
            #     cord2 = row[1][1]
            #     x_position = (len(row[0]) / 2) * 25
            #     text1 = str(row[0]).upper()
            #     if text1=='DINING':
            #         text = 'SEJOUR'
            #     elif text1 =='LIVING':
            #         text = 'REPAS'
            #     elif text1 =='KITCHEN':
            #         text = 'CUISINE'
            #     elif text1 =='ENTRY':
            #         text = 'HALL'
            #     cv2.putText(test_image, text, (cord1 - x_position, cord2 + 12), cv2.FONT_HERSHEY_COMPLEX, 1,
            #                 (0, 0, 0), 2, cv2.cv.CV_AA)
            # cv2.imwrite(self.output_directory + name + '_'+str(ring_count)+'.png', test_image)


                # empty_new_image2 = ~(np.zeros((self.height,self.width,3), np.uint8))
        # cv2.imwrite(self.output_directory+name+'_INTERMEDIATE.png', empty_new_image2)
        # image_2 = Image.open(self.output_directory+name+'_INTERMEDIATE.png')
        # pixels_2 = image_2.load()
        for img_row in range(height-1):
            for img_column in range(width-1):
                # colors go as R,G,B
                if pixel_data[img_row][img_column]==0:
                    pixels[img_column,img_row] = (255, 0, 0)
                elif pixel_data[img_row][img_column]==1:
                    pixels[img_column,img_row] = (0, 255, 0)
                elif pixel_data[img_row][img_column]==2:
                    pixels[img_column,img_row] = (0, 0, 255)
                elif pixel_data[img_row][img_column]==3:
                    pixels[img_column,img_row] = (255, 255, 0)
                elif pixel_data[img_row][img_column]==4:
                    pixels[img_column,img_row] = (102, 51, 0)
                elif pixel_data[img_row][img_column]==5:
                    pixels[img_column,img_row] = (255, 0, 255)
                elif pixel_data[img_row][img_column]==6:
                    pixels[img_column,img_row] = (0, 255, 255)
                elif pixel_data[img_row][img_column]==7:
                    pixels[img_column,img_row] = (255, 0, 125)
                elif pixel_data[img_row][img_column]==8:
                    pixels[img_column,img_row] = (255, 128, 0)
                elif pixel_data[img_row][img_column]==9:
                    pixels[img_column,img_row] = (125, 0, 255)
                elif pixel_data[img_row][img_column]==10:
                    pixels[img_column,img_row] = (102, 0, 0)
                elif pixel_data[img_row][img_column]==11:
                    pixels[img_column,img_row] = (0, 150, 75)
                elif pixel_data[img_row][img_column]==12:
                    pixels[img_column,img_row] = (102, 0, 102)
                elif pixel_data[img_row][img_column]==13:
                    pixels[img_column,img_row] = (150, 150, 0)
                elif pixel_data[img_row][img_column]==14:
                    pixels[img_column,img_row] = (200, 255, 200)


        # output_image.save(self.output_directory+name+'_STEP_02-1.png')
        # print self.output_directory+name+'_STEP_02.png'
        # image.save(self.output_directory+name+'_STEP_02.png')

        # #--thesis test
        # thesis_image = cv2.imread(self.output_directory+name+'_STEP_02.png', cv2.IMREAD_COLOR)
        # for row in text_words:
        #     cord1 = row[1][0]
        #     cord2 = row[1][1]
        #     x_position = (len(row[0]) / 2) * 25
        #     text = str(row[0]).upper()
        #     cv2.putText(thesis_image, text, (cord1 - x_position, cord2 + 12), cv2.FONT_HERSHEY_COMPLEX,
        #                 1, (0, 0, 0), 2, cv2.cv.CV_AA)
        # cv2.imwrite(path_final_lines + 'B_' +only_image_name+'_region_grow_'+image_name, thesis_image)
        # #--end thesis test
        return image

    def get_new_lines(self,open_plan_text_cordinate,name,image_name,debug_mode,path_final_lines,text_words,region_growed_image):
        if debug_mode:
            os.mkdir(self.output_directory+name+'/')
            # open_plan_path = self.output_directory+name+'/'

        clrs = region_growed_image.getcolors()
        existing_colors =[]
        for row in clrs:
            color = row[1]
            exists = False
            if (color[0]==0 and color[1]==0 and color[2]==0) or (color[0]==255 and color[1]==255 and color[2]==255):
                exists=True
            if row[0]>4 and exists==False:
                existing_colors.append(row[1])


        all_contours=[]
        all_contour_lines=[]
        count = 0
        if debug_mode:
            redraw_contours = []
        # width, height = self.width,self.height
        # pixels_2 = region_growed_image.load()

        np_image = np.array(region_growed_image)

        for r,g,b in existing_colors:

            indices = np.where(np.all(np_image == (r, g, b), axis=-1))
            coordinates = zip(indices[0], indices[1])

            image_3 = Image.new('RGB', (self.width, self.height), color="white")
            pixels_3 = image_3.load()

            for row in coordinates:
                x, y = row
                pixels_3[y, x] = (0, 0, 0)


            gray_room = cv2.cvtColor(np.array(image_3),cv2.COLOR_RGB2GRAY)
            ret,thresh = cv2.threshold(gray_room,0,255,1)
            contours,hierachy = cv2.findContours(thresh,1,2)
            if debug_mode:
                redraw_contours.append(contours)

            if len(contours) > 0:
                current_cont = contours[-1]
                image_contour = self.iterative_obj.get_points_from_contour(current_cont)
                image_contour_lines = self.iterative_obj.convert_points_to_lines(image_contour)

                #--find text inside current cont
                text_cordinate = 0
                for text_row, text_cord in enumerate(open_plan_text_cordinate):
                    if cv2.pointPolygonTest(current_cont,(tuple(text_cord)),False)==1:
                        text_cordinate = text_row
                        break

                # ------ add all room contours to one array
                all_contours.append(image_contour)
                if len(image_contour_lines) > 0:
                    all_contour_lines.append([])
                    all_contour_lines[count].append(image_contour_lines)
                    all_contour_lines[count].append(text_cordinate)

            count = count+1


        #-------write all room contours to a new image
        if debug_mode:
            empty_new_image2 = ~(np.zeros((self.height,self.width,3), np.uint8))
            for each in redraw_contours:
                cv2.drawContours(empty_new_image2, each, -1, (0,0,0), 1,cv2.cv.CV_AA)
            cv2.imwrite(self.output_directory+name+'_STEP_03.png', empty_new_image2)

        # print ('------all_contour_lines-------')
        # for row in all_contour_lines:
        #     print ' text is ', row[1]
        #     for line in row[0]:
        #         print line
        #     print '------------------------'
        # for each in all_contour_lines:
        #     empty_new_image_test = ~(np.zeros((self.height, self.width, 3), np.uint8))
        #     for line in each[0]:
        #         cv2.line(empty_new_image_test, (tuple(line[0])), (tuple(line[1])), (0, 0, 0), 1, cv2.cv.CV_AA)
        #     cv2.imwrite(self.output_directory + name + '_'+str(each[1])+'_STEP_Test_C.png', empty_new_image_test)

        #--------cant get common lines by checking if lines in 2 contours are the same
        #--------so using check if line1 & line2 are parellel and is very close to each other
        #--------06/03/2017


        voronoi_data = []
        common_contour_line = []
        count = 0
        # outer_contour=[]
        common_outer_contour_line = []
        outer_count = 0
        for cont, contour in enumerate(all_contour_lines):
            for line1 in contour[0]:
                x1, y1 = line1[0]
                x2, y2 = line1[1]
                #--get gradeint and centre point of line1
                m1, c1 = self.iterative_obj.find_mc_of_line(x1, y1, x2, y2)
                # center_x, center_y = iterative_obj.find_centre_of_line(line1)
                for cont2, contour2 in enumerate(all_contour_lines):
                    if cont2 != cont:
                        for line2 in contour2[0]:
                            x3, y3 = line2[0]
                            x4, y4 = line2[1]
                            #--measure distance from centre_point in line to line2
                            # distance_from_point, intersect_x, intersect_y = iterative_obj.calculate_distance_from_point_to_line([center_x, center_y],line2)
                            #---get gradient of line2
                            m2, c2 = self.iterative_obj.find_mc_of_line(x3, y3, x4, y4)
                            voronoi_line = False
                            if (m1=='a' and m2=='a') and (abs(x1-x3)<=2):
                                voronoi_line = self.iterative_obj.find_line_subsets(line1,line2)
                            elif (m1!='a' and m2!='a') and abs(m1-m2)<0.02 and (abs(c1-c2)<=2):
                                voronoi_line = self.iterative_obj.find_line_subsets(line1, line2)

                            #----in line & line2 if gradient is appprox similar, close to each other: add as voronoi
                            if voronoi_line:
                                common_contour_line.append([])
                                common_contour_line[count].append(contour[1])
                                common_contour_line[count].append(contour2[1])
                                common_contour_line[count].append(line1)
                                count = count + 1
                            else:
                                common_outer_contour_line.append([])
                                common_outer_contour_line[outer_count].append(contour[1])
                                common_outer_contour_line[outer_count].append(contour2[1])
                                common_outer_contour_line[outer_count].append(line1)
                                outer_count = outer_count + 1




        temp_voronoi_data = []
        for r,row in enumerate(common_contour_line):
            if r==0:
                temp_voronoi_data.append(row)
            else:
                round1_cord1 = row[0]
                round1_cord2 = row[1]
                exists = False
                for v, vor_row in enumerate(temp_voronoi_data):
                    round2_cord1 = vor_row[0]
                    round2_cord2 = vor_row[1]
                    if round1_cord1==round2_cord1 and round1_cord2==round2_cord2:
                        exists = True
                        count = v
                        break
                if exists is True:
                    temp_voronoi_data[count].append(row[2])
                else:
                    temp_voronoi_data.append(row)

        # --check if [0,1 [line...]] and [1,0[line2.....] exists
        for r1, row1 in enumerate(temp_voronoi_data):
            if r1 == 0:
                voronoi_data.append(row1)
            else:
                exists = False
                for r2, row2 in enumerate(voronoi_data):
                    if row2[0] == row1[1] and row2[1] == row1[0]:
                        exists = True
                        break
                if exists == False:
                    voronoi_data.append(row1)


        if debug_mode:
            contour_image = cv2.imread(self.output_directory+name+'_STEP_01.png',cv2.IMREAD_COLOR)
            for r, row in enumerate(voronoi_data):
                contour_image_copy = contour_image.copy()
                for line in row[2:]:
                    cv2.line(contour_image_copy,(tuple(line[0])),(tuple(line[1])),(0,0,255),2,cv2.cv.CV_AA)
                cv2.imwrite(self.output_directory+name+' : '+str(r)+'_STEP_04.png', contour_image_copy)

            cv2.imwrite(self.output_directory+name+'_STEP_04.png', contour_image)

            outer_cont_image = ~(np.zeros((self.height,self.width,3), np.uint8))
            for row in common_outer_contour_line:
                for line_c in row[2:]:
                    cv2.line(outer_cont_image,(tuple(line_c[0])),(tuple(line_c[1])),(0,0,0),1,cv2.cv.CV_AA)
            cv2.imwrite(self.output_directory+name+'_STEP_05.png', outer_cont_image)

            cv2.imwrite(self.outer_directory+'Rooms/'+self.image_name+'/OpenPlans/'+image_name, contour_image)

#**********************************************************added
            #-------write all room contours to a new image
            # voronoi_image = ~(np.zeros((self.height,self.width,3), np.uint8))
            voronoi_image = cv2.imread(self.output_directory+name+'_STEP_01.png',cv2.IMREAD_COLOR)

            # for each in redraw_contours:
            #     cv2.drawContours(voronoi_image, each, -1, (0,0,0), 1,cv2.cv.CV_AA)
            for r,row in enumerate(voronoi_data):
                for v_line in row[2:]:
                    cv2.line(voronoi_image,(tuple(v_line[0])),(tuple(v_line[1])),(0,0,255),2,cv2.cv.CV_AA)
            for cordinate in open_plan_text_cordinate:
                cv2.circle(voronoi_image, (tuple(cordinate)), 5, (0, 0, 255), -1)
            cv2.imwrite(self.output_directory+name+'_STEP_06.png', voronoi_image)


        # # timing test
        # voronoi_image = ~(np.zeros((self.height, self.width, 3), np.uint8))
        # for r, row in enumerate(voronoi_data):
        #     for v_line in row[2:]:
        #         cv2.line(voronoi_image, (tuple(v_line[0])), (tuple(v_line[1])), (0, 0, 255), 2, cv2.cv.CV_AA)
        # for cordinate in open_plan_text_cordinate:
        #     cv2.circle(voronoi_image, (tuple(cordinate)), 5, (0, 0, 255), -1)
        # cv2.imwrite(path_final_lines + '_Thesis_Step_03_' + image_name, voronoi_image)

        return voronoi_data, all_contour_lines


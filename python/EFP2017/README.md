# README #

### install opencv 2.4.9

	Follow this guide:
	http://www.samontab.com/web/2014/06/installing-opencv-2-4-9-in-ubuntu-14-04-lts/
	Sometimes "sudo gedit /etc/ld.so.conf.d/opencv.conf" will not work becuase there is no file called "sudo gedit /etc/ld.so.conf.d/opencv.conf". So have to "sudo vi /etc/ld.so.conf.d/opencv.conf" add the "/usr/local/lib" to the newly created file and save it.



### install tesseract 
	
	Follow this guide:
	http://delimitry.blogspot.com.au/2014/10/installing-tesseract-for-python-on.html
	Attention: it's suggested to use the 659 revistion of python-tesseract!
	** but if it cannot be found 
		1. go to this link "https://code.google.com/archive/p/python-tesseract/source/default/source"
		2. Download latest(or anyting that can be found from "download" link
		3. build it
			cd python-tesseract
			python setup.py clean
			python setup.py build
			python setup.py install
	
	**When you get 2nd error "undefined symbol: cvSetData"
		1. go to "/home/ubuntu/EFP_Data/leptonica-1.71/tesseract-3.03/python-tesseract/src/build/lib.linux-x86_64-2.7" 
		2. Check that opencv library is linked in the _tesseract.so, because cvSetData is opencv's function.
		ldd _tesseract.so | grep libopencv 
		3. If the output is empty: get back out to "/home/ubuntu/EFP_Data/leptonica-1.71/tesseract-3.03/python-tesseract/"
		4. Type "python setup.py build"
		5. Go to "/home/ubuntu/EFP_Data/leptonica-1.71/tesseract-3.03/python-tesseract/src/"
		6. Try to build _tesseract.so using this command:

		sudo c++ -pthread -shared -Wl,-O1 -Wl,-Bsymbolic-functions -Wl,-Bsymbolic-functions -Wl,-z,relro -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -Wstrict-prototypes -D_FORTIFY_SOURCE=2 -g -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security build/temp.linux-x86_64-2.7/tesseract_wrap.o build/temp.linux-x86_64-2.7/main.o -lstdc++ -ltesseract -llept -lopencv_superres -lopencv_video -lopencv_videostab -lopencv_ml -lopencv_ocl -lopencv_contrib -lopencv_flann -lopencv_calib3d -lopencv_imgproc -lopencv_core -lopencv_legacy -lopencv_stitching -lopencv_features2d -lopencv_photo -lopencv_ts -lopencv_objdetect -lopencv_highgui -lopencv_gpu -o build/lib.linux-x86_64-2.7/_tesseract.so

		7.And check again that opencv library is linked in the _tesseract.so.
		ldd _tesseract.so | grep libopencv
		Should be something like:
		libopencv_core.so.2.4 => /usr/local/lib/libopencv_core.so.2.4 (0x000070d310313370)

		8.Now install python-tesseract:
		python setup.py install

		9. After that the problem "undefined symbol: cvSetData" will be solved


### Package Dependencies
### install pip
   
	Download “get-pip.py” file from https://pip.pypa.io/en/latest/installing.html
   
	Run that file by typing "sudo python get-pip.py"

	(from shell:)

   	sudo pip install beautifulsoup4

   	sudo pip install python-Levenshtein 


### install eart mover's distance for arrays ###

	git clone https://github.com/garydoranjr/pyemd
	
	cd pyemd
	
	python setup.py install


### install pyhull

	sudo pip install pyhull


### install scikit-image
	sudo apt-get install python-matplotlib python-numpy python-pil python-scipy
	sudo apt-get install build-essential cython
	sudo apt-get install python-skimage

### install franges 
        to iterate over decimal points in the 'for loops'
	sudo pip install franges

### install imagemagick
	sudo apt-get install imagemagick

### instal naturalsort
	sudo pip install natsort

### instal cairo for svg
        sudo pip install cairosvg

### install image resize
	sudo pip install python-resize-image
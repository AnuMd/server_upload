// var app = require('http').createServer(handler)
//   , io = require('socket.io').listen(app)
const fs_extra = require('fs-extra')
var express = require('express')
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var path = require('path')

var fs = require('fs')
  , exec = require('child_process').exec
  , util = require('util')
  , Files = {};
var PythonShell = require('python-shell');
// var pyshell = new PythonShell('python/test.py');
var pyshell = new PythonShell('python/EFP2017/Model/MainLogic.py');


server.listen(8080);

app.use( express.static( "output" ) );
//path to store image uploaded
InputImagePath = __dirname+'/uploads/'
//output image name
// OutputImageName = 'final_floorplan.svg'
//path to store the SVG created by python program
// OutputImagePath = __dirname+'/output/'+OutputImageName
OutputPath = __dirname+'/output/'
//path to current directory passed over to python progam
CurrentDirectoryPath = String(__dirname)+'/python/EFP2017'
//test_directory
// CurrentDirectoryPath = String(__dirname)+'/python'



//when user types only port number: 
app.get('/', function (req, res) {
  //redirected through this to 'index.html'
  res.sendFile(path.join(__dirname+'/index.html'));
});



io.on('connection', function (socket) {
  	socket.on('Start', function (data) { //data contains the variables that we passed through in the html file
		//empty the 'uploads/' folder before inserting the new upload image
		//Ensures that a directory is empty. Deletes directory contents if the directory is not empty. If the directory does not exist, it is created. The directory itself is not deleted.
		fs_extra.emptyDir(InputImagePath)
		.then(() => {
		  console.log('success!')
		})
		.catch(err => {
		  console.error(err)
		})



		console.log('Start Executed')
		var Name = data['Name'];
		Files[Name] = {  //Create a new Entry in The Files Variable
			FileSize : data['Size'],
			Data	 : "",
			Downloaded : 0
		}
		var Place = 0;
		try{
			var Stat = fs.statSync('uploads/' +  Name);
			if(Stat.isFile())
			{
				Files[Name]['Downloaded'] = Stat.size;
				Place = Stat.size / 524288;
			}
		}
  		catch(er){} //It's a New File

		fs.open("uploads/" + Name, 'a', 0755, function(err, fd){
			if(err)
			{
				console.log(err);
			}
			else
			{
				Files[Name]['Handler'] = fd; //We store the file handler so we can write to it later
				socket.emit('MoreData', { 'Place' : Place, Percent : 0 });
				
			}
		});
	});
	
	socket.on('Upload', function (data){
		var Name = data['Name'];
		Files[Name]['Data'] += data['Data'];
		console.log('Upload Executed')
		fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
			console.log('started executing python');
			pyshell.send(CurrentDirectoryPath);
			pyshell.send(InputImagePath);
			// pyshell.send(OutputImagePath);
			pyshell.send(OutputPath);

			pyshell.on('message', function (message) {
			  // received a message sent from the Python script (a simple "print" statement)
			  console.log(message);
			});

			// end the input stream and allow the process to exit
			pyshell.end(function (err) {
			  if (err) {
					throw err;
					socket.emit('Progress','Image not transcribed');
				}
				else{
					console.log('finished');
					//show output when program has finished running
					// socket.emit('Done',OutputImageName);
					socket.emit('Progress','Image Conversion Completed');
				}
			});
			//show still loading message while program is running
			// socket.emit('Progress','loading.gif');
			socket.emit('Progress','Converting.....');
		});
	});
});